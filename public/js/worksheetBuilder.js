	function updateFeetNet(){
		$.each( $('.other-select-variables'), function(k,v){
			var total = $(v).find('input[name="feet"]').val() - $(v).find('input[name="inc"]').val();
			$(v).find('input[name="net"]').val(total);
		});
	}
	function showJobUnits($paren, choice_key){
		var $select_units = $paren.find('.choice-job-unit'),
			$chosen_units = $paren.find('.choice-job-unit[data-choice-key="' + choice_key + '"]');
		$select_units.hide().removeClass('active');
		$chosen_units.show().addClass('active');
	}
	function updateLineTotal(){
		$prices = {};
		$prices.all = 0;
		$('.repeater-group').not('.template .repeater-group').each(function(k,v){
			var $group = $(v),
				$all = $group.find('.choice-price.active, .blank-price').first().find('input[data-price-name="all"]');
				$platinum = $group.find('.choice-price.active, .blank-price').find('input[data-price-name="platinum"]'),
				$gold = $group.find('.choice-price.active, .blank-price').find('input[data-price-name="gold"]'),
				$silver = $group.find('.choice-price.active, .blank-price').find('input[data-price-name="silver"]'),
				$bronze = $group.find('.choice-price.active, .blank-price').find('input[data-price-name="bronze"]'),
				$all_total = $group.find('.total-all'),
				$platinum_total = $group.find('.total-platinum'),
				$gold_total = $group.find('.total-gold'),
				$silver_total = $group.find('.total-silver'),
				$bronze_total = $group.find('.total-bronze'),
				$net = $group.find('input[name="net"]'),
				$price_cat = $group.closest('[data-price-category]').data('price-category'),
				$markup = window.presentation_cats[$price_cat].markup; // defined in worksheet.blade.php
			if( $all.length ){
				var $all_val = $all.val();
				// check if has Net or just flat rate
				if($net.length ){
					$net_val = $net.val();
					$all_total.val($net_val * $all_val);
				}
				else{
					//alert(1);

					$all_total.val($all_val);
				}
			}
			else{
				$net_val = $(v).find('input[name="total_quantity"]').val();
			}
			if( $platinum.length ){
				//alert('updateLineTotal - get markup value!');
				if($net_val ){
					//$net_val = $net.val();
					$platinum_total.val( $net_val * $platinum.val() * $markup);
					$gold_total.val($net_val * $gold.val() * $markup);
					$silver_total.val($net_val * $silver.val() * $markup);
					$bronze_total.val($net_val * $bronze.val() * $markup);

				}
				else{
					$platinum_total.val( $platinum.val() * $markup);
					$gold_total.val($gold.val() * $markup);
					$silver_total.val( $silver.val() * $markup);
					$bronze_total.val( $bronze.val() * $markup);
				}				
			}
		})
		$('.total-all').each(function(k,v){
			if( !isNaN( parseInt( $(v).val() ) ) ){
				$prices.all += parseInt($(v).val());
			}
		})
		$prices.platinum = $prices.all;
		$('.total-platinum').each(function(k,v){
			if( !isNaN( parseInt( $(v).val() ) ) ){
				$prices.platinum += parseInt($(v).val());
			}
		})
		$prices.silver = $prices.all;
		$('.total-silver').each(function(k,v){
			if( !isNaN( parseInt( $(v).val() ) ) ){
				$prices.silver += parseInt($(v).val());
			}
		})
		$prices.bronze = $prices.all;
		$('.total-bronze').each(function(k,v){
			if( !isNaN( parseInt( $(v).val() ) ) ){
				$prices.bronze += parseInt($(v).val());
			}
		})
		$prices.gold = $prices.all;
		$('.total-gold').each(function(k,v){
			if( !isNaN( parseInt( $(v).val() ) ) ){
				$prices.gold += parseInt($(v).val());
			}
		})
		
		$('.ws-total-platinum').val( $prices.platinum );
		$('.ws-total-gold').val( $prices.gold );
		$('.ws-total-silver').val( $prices.silver );
		$('.ws-total-bronze').val( $prices.bronze );


	}

var isPageLoaded = false;

$(window).load(function(){
    isPageLoaded = true;
});

$( document ).ready(function() {

	updateFeetNet();
	$('.repeater-group select').each(function(k,v){
		var choice_key = $(v).find('option:selected').val();
			$paren = $(v).parent();
		showPrices($paren, choice_key);
	});
	updateLineTotal();
	function saveEstimate(){
		var $estimate = {},
			estimate_id = $('#estimate_id').val(),
			worksheet_id = $('#worksheet_id').val(),
			version = $('#version').val(),
			$inputs = $('.worksheet-option').not('.template .worksheet-option') ,
			$levels = ['platinum','gold','silver','bronze'],
			presentation = {},
			job_sheet = [];
			$estimate.sections = [];
		$.each( presentation_cats, function(k,v){
			presentation[k] = [];
		})
		if( 'undefined' == typeof presentation){
			//presentation = [];
		}
		/// iterate over all the selects and build out the option chosen and the prices set (prices could have been override by estimator)
		$job_labor_total = 0;
		$.each($inputs, function(k,v){
//console.log(prices);

			var $input = getInputParts( v );

			if( 'undefined' ==  typeof $estimate.sections[$input.section] ){
				$estimate.sections[$input.section] = new Object();
			}
			else{
			}
			if( 'undefined' ==  typeof $estimate.sections[$input.section].inputs ){
				$estimate.sections[$input.section].inputs = [];
			}
// if repeater build repeater array
			if( 'undefined' == typeof $estimate.sections[$input.section].inputs[$input.input] ){
				$estimate.sections[$input.section].inputs[$input.input] = new Object();
				$estimate.sections[$input.section].inputs[$input.input].repeater = [];
				$estimate.sections[$input.section].inputs[$input.input].blank = [];
			}
			var option = $(v).find(":selected").val(),
				selected_val = $(v).find(":selected").html();
			if( $(v).hasClass('blank') ){
				$label = $(v).closest('.inputs-wrapper').find('.template-wrapper label').html();
				$input.price = $(v).closest('.repeater-group').find('.choice-price input').val();
				$input.value = $(v).closest('.repeater-group').find('.worksheet-option.blank').val();

				var $form_group = $(v).closest('.form-group')[0];
				$input.price_category = $($form_group).data('price-category');
				var cat_array = {};
				cat_array.label = $label;
				cat_array.value = $input.value;
				if( typeof cat_array.label != 'undefined'){
					presentation[$input.price_category].push( cat_array );
				}
				$estimate.sections[$input.section].inputs[$input.input].blank.push($input);
			}
			else{
				$label = $(v).closest('.inputs-wrapper').find('.template-wrapper label').html();
				var inputPrices = $(v).closest('.repeater-group').find('.choice-price.active input');
				$input.feet = $(v).siblings('.other-select-variables').find('input[name="feet"]').val();
				$input.inc = $(v).siblings('.other-select-variables').find('input[name="inc"]').val();
				$input.selected = option;
				$input.prices = {};
				$.each(inputPrices,function(ka,va){
					var price_name = $(va).data('price-name'),
						price = $(va).val();
					$input.prices[price_name] = price;
				});
				var $form_group = $(v).closest('.form-group')[0];
				$input.price_category = $($form_group).data('price-category');
				var cat_array = {};
				cat_array.label = $label;
				if( $input.feet > 0){
					cat_array.feet = $input.feet;
				}
				cat_array.value = selected_val;
				if( option != 'none' && typeof selected_val != 'undefined' && typeof cat_array.label != 'undefined'){
					
					presentation[$input.price_category].push( cat_array );
					$job_labor_input = $job_labor = $(v).closest('.inputs-wrapper').find('.choice-job-unit.active input').val();

					if( 'undefined' != $job_labor && $job_labor ){
						if( $input.feet > 0 ){
							var $net = $input.feet - $input.inc;
							if( $net > 0 ){
								console.log('init val for net:' + $net);
								$job_labor_input = $job_labor * $net;
							}
							else{
								$job_labor_input = 0;								
							}
						}
						if($input.total_quantity){
							$job_labor_input = parseFloat($job_labor) * parseFloat($input.total_quantity);
						}
						$job_labor_total = parseFloat($job_labor_total) + parseFloat($job_labor_input);
						console.log('JLI:' + $job_labor_input + '   - ' + $job_labor_total);
						$input.job_labor = $job_labor;
					}
				}
				$estimate.sections[$input.section].inputs[$input.input].repeater.push($input);
			}
			if( $(v).hasClass('job-sheet')){
				job_sheet.push(cat_array);
			}

//			console.log('vvvvvvvvvvvvvvvvvvvvv');
//			console.log(v);
		});
		$job_labors = $('.choice-job-unit.active');
		console.log('JOOBLLAAAAAAAAAAAAABORS');
		console.log($job_labors);
		/*
		$.each($job_labors, function(k,v){
			var val = $(v).find('input').val();
			console.log('v' + val);
			if( 'undefined' != val && val ){
				$job_labor_total += parseFloat($(v).find('input').val());
			}
		})
		*/
		$estimate.job_labor_total = $job_labor_total;

		includes = {};
		includes.trane = 0;
		if( $('input[name="incl_trane"]').is(':checked')){
			includes.trane = 1;
		}
		includes.lennox = 0;
		if( $('input[name="incl_lennox"]').is(':checked')){
			includes.lennox = 1;
		}
		includes.tep = 0;
		if( $('input[name="incl_tep"]').is(':checked')){
			includes.tep = 1;
		}
		includes.citi = 0;
		if( $('input[name="incl_citi"]').is(':checked')){
			includes.citi = 1;
		}
		includes.costco = 0;
		if( $('input[name="incl_costco"]').is(':checked')){
			includes.costco = 1;
		}
		includes.exec = 0;
		if( $('input[name="incl_exec"]').is(':checked')){
			includes.exec = 1;
		}

		$estimate.prices = {};
		$estimate.prices.platinum = $('.ws-total-platinum').val();
		$estimate.prices.platinum_box = $('[name="platinum_boxprice"]').val();
		$estimate.prices.gold = $('.ws-total-gold').val();
		$estimate.prices.gold_box = $('[name="gold_boxprice"]').val();
		$estimate.prices.silver = $('.ws-total-silver').val();
		$estimate.prices.silver_box = $('[name="silver_boxprice"]').val();
		$estimate.prices.bronze = $('.ws-total-bronze').val();
		$estimate.prices.bronze_box = $('[name="bronze_boxprice"]').val();


		$estimate.systems = {};
		$estimate.systems.platinum = $('#platinum_system').val();
		$estimate.systems.gold = $('#gold_system').val();
		$estimate.systems.silver = $('#silver_system').val();
		$estimate.systems.bronze = $('#bronze_system').val();
		$estimate.thermostats = {};
		$estimate.thermostats.platinum = $('#platinum_thermostat').val();
		$estimate.thermostats.gold = $('#gold_thermostat').val();
		$estimate.thermostats.silver = $('#silver_thermostat').val();
		$estimate.thermostats.bronze = $('#bronze_thermostat').val();
		//$estimate.presentation = presentation;

		var $unit_available_width = $('input[name="unit_available_width"]').val();
		if( !$unit_available_width){
			$unit_available_width = "0";
		}
		var $coil_available_width = $('#coil_available_width').val();
		if( !$coil_available_width){
			$coil_available_width = "0";
		}
		var $available_height = $('#available_height').val();
		if( !$available_height){
			$available_height = "0";
		}
		var $ac_hp = $('select[name="ac_hp"]').val();
		if( !$ac_hp){
			$ac_hp = "0";
		}
		var $capacity = $('select[name="capacity"]').val();
		if( !$capacity){
			$capacity = "0";
		}
		var $new_replace = $('select[name="new_replace"]').val();
		if( !$new_replace){
			$new_replace = "0";
		}
	//	alert(2);
		console.log('job_sheet');
		console.log(job_sheet);
		$.ajax({
			url: '/api/estimate',
			data:     {
				estimate : $estimate,
				estimate_id : estimate_id,
				worksheet_id : worksheet_id,
				version : version,
				presentation : presentation,
				job_sheet : job_sheet,
				includes : includes,
				unit_available_width: $unit_available_width,
				coil_available_width: $coil_available_width,
				available_height: $available_height,
				ac_hp: $ac_hp,
				capacity : $capacity,
				new_replace: $new_replace
			},
			error: function() {
				console.log('error');
			},
			success: function(data) {
				//console.log(data);
			},
			type: 'POST'
		});
		


	}
	
	setInterval ( function(){
		saveEstimate();
	}, 60000 );
	
	$('#save_estimate').click(function(e){
		e.preventDefault();
	    if (isPageLoaded){
			saveEstimate();
		}
		else{
			alert('The Page has not loaded yet.');
		}
	});
	$('#present_estimate').click(function(e){
		e.preventDefault();
		saveEstimate();
		window.location = $('#present_estimate').data('href'); 
	});
});
$('.financing_select').on('change', function(e){
	e.preventDefault();
	updateFinancing(e);
})

function getInputParts( input ){
//console.log(input);
	var inputObject = {};
	inputObject.section = "";
	inputObject.input = "";
	inputObject.choice = "";
	inputObject.repeater = "";
	//inputObject.prices = ['one'];
	var name = $(input).attr('name'),
		$repeater_group = $(input).closest('.repeater-group');
//console.log(name);
	if( $repeater_group.length > 0){
		inputObject.repeater = $repeater_group.data('repeater-number');
	}
	if( 'undefined' != typeof name ){
		var reg = /s:\s*(.*?)\s*_/g;
		var section = String(name.match( reg ));
		if( null != section ){	
			section = section.replace(/s:/g,"");
			section = section.replace(/_/g,"");
			inputObject.section = section;
		}

		reg = /c:\s*(.*?)\s*_/g;
		var choice = String(name.match( reg ));
		if( null != choice ){	
			choice = choice.replace(/c:/g,"");
			choice = choice.replace(/_/g,"");
			inputObject.choice = choice;
		}

		reg = /i:\s*(.*?)\s*_/g;
		var input = String(name.match( reg ));
		if( null != input ){	
			input = input.replace(/i:/g,"");
			input = input.replace(/_/g,"");
			inputObject.input = input;
		}
	}
	return inputObject;	
}
function updateDimensionCheck( cat ){
	var $id = $("#" + cat + "_system").val();
	$unit_available_width = $("#unit_available_width").val();
	$coil_available_width = $("#coil_available_width").val();
	$total_available_height = $("#available_height").val();
	var $message = "";
	if( 'undefined' != typeof dimensions && dimensions[$id] ){
		var coil_width = dimensions[$id]['coil']['width'];
		var coil_height = dimensions[$id]['coil']['height'];
		var furnace_width = dimensions[$id]['furnace']['width'];
		var furnace_height = dimensions[$id]['furnace']['height'];
		var $alert = 0;
		if( 'split' == window.estimate_type ){
			if( coil_width > $coil_available_width ){
				$message += " The coil width exceeds available width.";
				$alert = 1;
			}
		}
		/*
		console.log('coil_height');
		console.log(coil_height);
		console.log('furnace_height');
		console.log(furnace_height);
		console.log('totalavailable_height');
		console.log($total_available_height);
		console.log('furnace_width');
		console.log(furnace_width);
		console.log('coil_available_width');
		console.log($coil_available_width);
		console.log('coil_width');
		console.log(coil_width);
		*/
		if( (parseInt(coil_height) + parseInt(furnace_height)) > $total_available_height ){
			$message += " The combined coil & furnace height exceeds available height.";
			$alert = 1;
		}
		if( parseInt(furnace_width) > $unit_available_width ){
			$message += " The furnace width exceeds available width.";
			$alert = 1;
		}
		if( $alert ){
			$('.' + cat + '_size_message').html( $message ).show();
		}
		else{
			$('.' + cat + '_size_message').hide();
		}
	}

}
$('.tr-click td').click(function(e){
	var $this = $(e.target).closest('.tr-click');
	var $id = $this.data('id');
	$('#delete-form').show();

	if( $this.hasClass('tr-coil') ){
		$form = $('#add-new-coil');
		$form.modal('show');
		$form.find('#id').val($id);
		$('.coil-delete input[name="id"]').val($id);

		$form.find('#brand').val(coils[$id].brand);
		$form.find('#model_number').val(coils[$id].model_number);
		$form.find('#width').val(coils[$id].width);
		$form.find('#height').val(coils[$id].height);
		$form.find('#price').val(coils[$id].price);
		$form.find('#depth').val(coils[$id].depth);
		$form.find('#parts_warranty').val(coils[$id].parts_warranty);
		$form.find('#labor_warranty').val(coils[$id].labor_warranty);
		$form.find('#orientation').val(coils[$id].orientation);
		$form.find('#markup').val(coils[$id].markup);
		$form.find('#order_number').val(coils[$id].order_number);

		var checkimg = coils[$id].image;
		checkimg = checkimg.split("images");
		if( coils[$id].divide){
			$form.find('#divide').prop('checked', true);
		}

		if(checkimg[1] == "")
		{
        $form.find('#image').hide();
		}
		else
		{
		$form.find('#image').attr('src', coils[$id].image).show();
	    }

	}
	if( $this.hasClass('tr-condenser') ){
		$form = $('#add-new-condenser');
		$form.modal('show');
		$form.find('#id').val($id);
		$('.condenser-delete input[name="id"]').val($id);

		$form.find('#brand').val(condensers[$id].brand);
		$form.find('#model_number').val(condensers[$id].model_number);
		$form.find('#width').val(condensers[$id].width);
		$form.find('#height').val(condensers[$id].height);
		$form.find('#price').val(condensers[$id].price);
		$form.find('#depth').val(condensers[$id].depth);
		$form.find('#parts_warranty').val(condensers[$id].parts_warranty);
		$form.find('#labor_warranty').val(condensers[$id].labor_warranty);
		$form.find('#orientation').val(condensers[$id].orientation);
		$form.find('#compressor_warranty').val(condensers[$id].compressor_warranty);
		$form.find('#capacity').val(condensers[$id].capacity);
		$form.find('#markup').val(condensers[$id].markup);
		$form.find('#stages').val(condensers[$id].stages);
		$form.find('#hp_ac').val(condensers[$id].hp_ac);
		$form.find('#amps').val(condensers[$id].amps);
		$form.find('#order_number').val(condensers[$id].order_number);
		var checkimg = condensers[$id].image;
		checkimg = checkimg.split("images");
		if( condensers[$id].divide){
			$form.find('#divide').prop('checked', true);
		}
		if(checkimg[1] == "")
		{
        $form.find('#image').hide();
		}
		else
		{
		$form.find('#image').attr('src', condensers[$id].image).show();
	    }
	}
	if( $this.hasClass('tr-thermostat') ){
		$form = $('#add-new-thermostat');
		$form.modal('show');
				$form.find('#id').val($id);
		$('.thermostat-delete input[name="id"]').val($id);

		$form.find('#brand').val(thermostats[$id].brand);
		$form.find('#model_number').val(thermostats[$id].model_number);
		$form.find('#width').val(thermostats[$id].width);
		$form.find('#height').val(thermostats[$id].height);
		$form.find('#price').val(thermostats[$id].price);
		$form.find('#depth').val(thermostats[$id].depth);
		$form.find('#parts_warranty').val(thermostats[$id].parts_warranty);
		$form.find('#labor_warranty').val(thermostats[$id].labor_warranty);
		$form.find('#order_number').val(thermostats[$id].order_number);
		if( thermostats[$id].wifi){
			$form.find('#wifi[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#wifi[value="0"]').prop('checked', true);			
		}
		if( thermostats[$id].touchscreen == 1){
			$form.find('#touchscreen[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#touchscreen[value="0"]').prop('checked', true);			
		}
		if( thermostats[$id].programmable){
			$form.find('#programmable[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#programmable[value="0"]').prop('checked', true);			
		}
		$form.find('#markup').val(thermostats[$id].markup);
		var checkimg = thermostats[$id].image;
		checkimg = checkimg.split("images");
		if( thermostats[$id].divide){
			$form.find('#divide').prop('checked', true);
		}

		if(checkimg[1] == ""){
	        $form.find('#image').hide();
		}
		else{
			$form.find('#image').attr('src', thermostats[$id].image).show();
	    }
	}
	if( $this.hasClass('tr-furnace') ){
		$form = $('#add-new-furnace');
		$form.modal('show');
				$form.find('#id').val($id);
		$('.furnace-delete input[name="id"]').val($id);

		$form.find('#brand').val(furnaces[$id].brand);
		$form.find('#model_number').val(furnaces[$id].model_number);
		$form.find('#width').val(furnaces[$id].width);
		$form.find('#height').val(furnaces[$id].height);
		$form.find('#price').val(furnaces[$id].price);
		$form.find('#depth').val(furnaces[$id].depth);
		$form.find('#parts_warranty').val(furnaces[$id].parts_warranty);
		$form.find('#labor_warranty').val(furnaces[$id].labor_warranty);
		$form.find('#heat_exchanger_warranty').val(furnaces[$id].heat_exchanger_warranty);
		$form.find('#afue').val(furnaces[$id].afue);
		$form.find('#blower_type').val(furnaces[$id].blower_type);
		$form.find('#blower_size').val(furnaces[$id].blower_size);
		$form.find('#stages').val(furnaces[$id].stages);
		$form.find('#capacity').val(furnaces[$id].capacity);
		$form.find('#furnace_air_handler').val(furnaces[$id].furnace_air_handler);
		$form.find('#markup').val(furnaces[$id].markup);
		$form.find('#location').val(furnaces[$id].location);
		$form.find('#order_number').val(furnaces[$id].order_number);
		var checkimg = furnaces[$id].image;
		checkimg = checkimg.split("images");
		if( furnaces[$id].filter_base){
			$form.find('#filter_base[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#filter_base[value="0"]').prop('checked', true);			
		}
		if( furnaces[$id].divide){
			$form.find('#divide').prop('checked', true);
		}

		if(checkimg[1] == "")
		{
        $form.find('#image').hide();
		}
		else
		{
		$form.find('#image').attr('src', furnaces[$id].image).show();
	    }

	}
	if( $this.hasClass('tr-package') ){
		$form = $('#add-new-packages');
		$form.modal('show');
				$form.find('#id').val($id);
		$('.package-delete input[name="id"]').val($id);

		$form.find('#brand').val(packages[$id].brand);
		$form.find('#model_number').val(packages[$id].model_number);
		$form.find('#width').val(packages[$id].width);
		$form.find('#height').val(packages[$id].height);
		$form.find('#price').val(packages[$id].price);
		$form.find('#depth').val(packages[$id].depth);
		$form.find('#parts_warranty').val(packages[$id].parts_warranty);
		$form.find('#labor_warranty').val(packages[$id].labor_warranty);
		$form.find('#heat_exchanger_warranty').val(packages[$id].heat_exchanger_warranty);
		$form.find('#filter_base').val(packages[$id].filter_base);
		$form.find('#afue').val(packages[$id].afue);
		$form.find('#blower_type').val(packages[$id].blower_type);
		$form.find('#blower_size').val(packages[$id].blower_size);
		$form.find('#stages').val(packages[$id].stages);
		$form.find('#capacity').val(packages[$id].capacity);
		$form.find('#amps').val(packages[$id].amps);
		$form.find('#package_air_handler').val(packages[$id].package_air_handler);
		$form.find('#location').val(packages[$id].location);
		$form.find('#price').val(packages[$id].price);
		$form.find('#seer').val(packages[$id].seer);
		$form.find('#hspf').val(packages[$id].hspf);
		$form.find('#weight').val(packages[$id].weight);
		$form.find('#stages_of_heating').val(packages[$id].stages_of_heating);
		$form.find('#stages_of_cooling').val(packages[$id].stages_of_cooling);
		$form.find('#capacity_for_cooling').val(packages[$id].capacity_for_cooling);
		$form.find('#capacity_for_heating').val(packages[$id].capacity_for_heating);
		$form.find('#sensible_cooling').val(packages[$id].sensible_cooling);
		$form.find('#order_number').val(packages[$id].order_number);
		if( packages[$id].tax_credit){
			$form.find('#tax_credit[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#tax_credit[value="0"]').prop('checked', true);			
		}
		if( packages[$id].energy_star){
			$form.find('#energy_star[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#energy_star[value="0"]').prop('checked', true);			
		}
		if( packages[$id].utility_rebate){
			$form.find('#utility_rebate[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#utility_rebate[value="0"]').prop('checked', true);			
		}

		$form.find('#energy_star').val(packages[$id].energy_star);
		$form.find('#tax_credit').val(packages[$id].tax_credit);
		$form.find('#utility_rebate').val(packages[$id].utility_rebate);
		$form.find('#ahri').val(packages[$id].ahri);
		$form.find('#hp_ac').val(packages[$id].hp_ac);
		$form.find('#markup').val(packages[$id].markup);
		$form.find('#compressor_warranty').val(packages[$id].compressor_warranty);
		$form.find('#eer').val(packages[$id].eer);
		var checkimg = packages[$id].image;
		checkimg = checkimg.split("images");
		if( packages[$id].divide){
			$form.find('#divide').prop('checked', true);
		}

		if(checkimg[1] == "")
		{
        $form.find('#image').hide();
		}
		else
		{
		$form.find('#image').attr('src', packages[$id].image).show();
	    }
		
		
	}
	if( $this.hasClass('tr-system') ){
		$form = $('#add-new-systems');
		$form.modal('show');
		$form.find('#id').val($id);
		$('.system-delete input[name="id"]').val($id);
		$form.find('#parts_warranty').val(systems[$id].parts_warranty);
		$form.find('#labor_warranty').val(systems[$id].labor_warranty);
		$form.find('#heat_exchanger_warranty').val(systems[$id].heat_exchanger_warranty);
		$form.find('#compressor_warranty').val(systems[$id].compressor_warranty);
		$form.find('#seer').val(systems[$id].seer);
		$form.find('#eer').val(systems[$id].eer);
		$form.find('#hspf').val(systems[$id].hspf);
		$form.find('#afue').val(systems[$id].afue);
		$form.find('#sensible_cooling').val(systems[$id].sensible_cooling);
		$form.find('#order_number').val(systems[$id].order_number);
		if( systems[$id].tax_credit){
			$form.find('#tax_credit[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#tax_credit[value="0"]').prop('checked', true);			
		}
		if( systems[$id].energy_star){
			$form.find('#energy_star[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#energy_star[value="0"]').prop('checked', true);			
		}
		if( systems[$id].utility_rebate){
			$form.find('#utility_rebate[value="1"]').prop('checked', true);
		}
		else{
			$form.find('#utility_rebate[value="0"]').prop('checked', true);			
		}
		$form.find('#ahri').val(systems[$id].ahri);
		$form.find('#rw_sysetm_id').val(systems[$id].rw_sysetm_id);
		$form.find('#thermostat_id').val(systems[$id].thermostat_id);
		$form.find('#coil_id').val(systems[$id].coil_id);
		$form.find('#furnace_id').val(systems[$id].furnace_id);
		$form.find('#condenser_id').val(systems[$id].condenser_id);
		$form.find('#name').val(systems[$id].name);
		$form.find('#location').val(systems[$id].location);
		$form.find('#stages_of_heating').val(systems[$id].stages_of_heating);
		$form.find('#stages_of_cooling').val(systems[$id].stages_of_cooling);
	
		$form.find('#compressor_warranty').val(systems[$id].compressor_warranty);
		$form.find('#rw_system_id').val(systems[$id].rw_system_id);
		var checkimg = systems[$id].image;
		checkimg = checkimg.split("images");
		if( systems[$id].divide){
			$form.find('#divide').prop('checked', true);
		}

		if(checkimg[1] == "")
		{
        $form.find('#image').hide();
		}
		else
		{
		$form.find('#image').attr('src', systems[$id].image).show();
	    }
	}
});
$("#platinum_system").on('change',function(){
	updateDimensionCheck( 'platinum');
})
$("#gold_system").on('change',function(){
	updateDimensionCheck( 'gold');
})
$("#silver_system").on('change',function(){
	updateDimensionCheck( 'silver');
})
$("#bronze_system").on('change',function(){
	updateDimensionCheck( 'bronze');
})
$('.worksheet input, .worksheet select').on('change', function(){
	updateAllDimensions();	
});
function updateAllDimensions(){
	updateDimensionCheck( 'platinum');
	updateDimensionCheck( 'gold');
	updateDimensionCheck( 'silver');
	updateDimensionCheck( 'bronze');
}
updateAllDimensions();


// Equalize column heights

$('#carousel-proposal-details').bind('slide.bs.carousel', function() {
	setBulletHeight();
});
	function filter_systems(){
		var ac_hp = $('select[name="ac_hp"]').val();
		var capacity = $('select[name="capacity"]').val();
		var system_selects = $('#silver_system, #platinum_system, #gold_system, #bronze_system');
		$.each(system_selects, function(k,v){
			$.each( $(v).children('option'), function(a,b){
				var $system_id = $(b).val(); 
				var $system = system_filters[$system_id];
				if( typeof ac_hp == 'undefined'){
					if( $system.capacity != capacity || $system.hp_ac != ac_hp){
						$(b).hide();
					}
					else{
						$(b).show();
					}
				}
				else{
					if( $system.capacity != capacity ){
						$(b).hide();
					}
					else{
						$(b).show();
					}

				}
			});
		})
	}
filter_systems();
	$('select[name="ac_hp"]').on('change', function(){
		filter_systems();
	})
	$('select[name="capacity"]').on('change', function(){
		filter_systems();
	})

//=========================================disable input filds if user role !=admin & proposal status = sol

$(document).ready(function(){
/*	var role = $("#hide_input_fields").val();//hidden field in worksheet.blade.php
	var estimate_info = jQuery.parseJSON(role);
	if(estimate_info.estimate_status=='sold' && estimate_info.user_role != 'admin')
	{
		$(":input").attr("disabled","disabled");	
		
	}else{}

	if(estimate_info.user_role != 'admin')
	{
		$(".choice-job-unit.active").children("input[type='text']").attr("disabled","disabled");
		$(".choice-price.active").children("input[type='text']").attr("disabled","disabled");
	}
*/
});
//=========================================