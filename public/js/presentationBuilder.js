	$('input').on('change',function(){
		estimate_id = window.current_estimate.id;
		updateEstimatePrices(estimate_id);
		var estimate = window._estimates[estimate_id];
		console.log('NEEEEEEEEEEEW ESTIMATE:');
		console.log(estimate);
		updatePricingInfo(estimate);	
		updatePresentationPrice(estimate);	
		updatePrintView(estimate);	
	});
	$('#proposalDetailAltModal').on('hidden.bs.modal', function () {
    // do something…
    	$("#proposalDetailModal").modal('hide');
	
	})

	function presentEstimate(id){
		$("#proposalDetailAltModal").modal();
		getEstimate(id, function(){
			var estimate = window._estimates[id],
				newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?proposal=' + estimate.proposal.id + '&estimate=' + id;
			buildEstimateModal( estimate );
			window.history.pushState({path:newurl},'',newurl);
		});
	}
	function buildProposalModal($proposal, $id){
			$("#modal-address").html( $proposal.address );
				$("#job-address").html( $proposal.address );
				$("#job-zip").html( $proposal.location.zipcode );
			$("#modal-rep").html( $proposal.rep.first_name + " " + $proposal.rep.last_name );
				$("#job-estimator").html( $proposal.rep.first_name + " " + $proposal.rep.last_name );
			$("#modal-customer").html( $proposal.customer.first_name + " " + $proposal.customer.last_name );
				$("#job-customer").html( $proposal.customer.first_name + " " + $proposal.customer.last_name );
				$("#job-phone").html( $proposal.customer.phone );
				$("#job-email").html( $proposal.customer.email );
			$("#modal-expiration").html( $proposal.expiration );
			$("#modal-display_id").html( "Opportunity "+$proposal.id );

			$(".proposal-id").val( $id ).data('id',$id).html('#' + $id);

			var v_number = 1;
			$('#estimate').find('option').remove();
			$('#estimateTable').find('tbody tr').remove();
			$('#chosen_estimate option').remove();

			var $included_estimates = JSON.parse($proposal.included_estimates);
			var $can_sell_proposal = 0;
			if( $included_estimates && $included_estimates.length > 0){
				$can_sell_proposal = 1;
			}
			$.each( $proposal.estimates, function(k,v){
				var _type = v.type,
					checked = "",
					chosen_plan = "";
				if( v.chosen_plan  ){
					var chosen_plan = v.chosen_plan,
						curr_estimate = v,
						chosen_system_id = curr_estimate[chosen_plan + '_system_id'];
					$('.plan-chosen').show();
				}

				if($proposal.included_estimates ){
					$.each(JSON.parse($proposal.included_estimates), function(m,n){
						if( v.id == n){
							checked = " checked";
							// check if has payment method
							if(v.presentation_prices){
								if( v.presentation_prices.payment_method == 7){
									$can_sell_proposal = 0;
								}
							}
							else{
								$can_sell_proposal = 0;
							}
							if( !v.chosen_plan){
								$can_sell_proposal = 0;
							}
						}
					})
				}
			    $('#estimate').append('<option value="' + v.id + '" data-type="' + v.type + '">Estimate #' + v_number + ' | ' + _type.charAt(0).toUpperCase() + _type.slice(1) + '</option>');
			    $('#chosen_estimate').append('<option value="' + v.id + '" data-type="' + v.type + '">Estimate #' + v_number + ' | ' + _type.charAt(0).toUpperCase() + _type.slice(1) + '</option>');
			    $('#estimateTable').append('<tr><td>' + v_number + '</td><td><input type="checkbox" data-estimate-id="' + v.id + '" ' + checked + '></td><td>' + chosen_plan +'</td><td>4</td><td><a class="link" href="/' + v.type + '/?estimate_id=' + v.id + '"><span class="icon edit"></span>Edit</a></td><td><a style="margin-left:0;"  href="javascript:void(0)" onclick="presentEstimate(' + v.id + ')" class="btn-submit btn btn-primary btn-block" data-estimate-id="' + v.id + '">Present</a></td>');
			    v_number++;
			});
			setEstimateHref();
			$("#prop_leadgen").val( $proposal.leadgen_id );
			$("#prop_source").val( $proposal.source_id );

			$("#prop_status").val( $proposal.status ).removeClass().addClass('status').addClass('form-control');
			$('#prop_status').change(function(){
				if( $('#prop_status').hasClass('cant-sell') && 'sold' == $('#prop_status').val() ){
						$("#prop_status option[value=pending]").attr('selected','selected');
						alert('You must have at least one estimate included in the proposal and all included estimates must have a chosen Payment Method and a chosen Plan.');
		// cdoes not have an included estimate or has no payment method
				}
				else{
					$('#proposal_status').submit();
				}
			})

			if( !$can_sell_proposal ){
				$("#prop_status").addClass('cant-sell');
			}
			$('#new_estimate').change(function(){
				var $option = $('#new_estimate').find(":selected"),
					type = $option.val(),
					newEstimateHref = '/' + type + '/?proposal_id=' + $id + '&new_estimate=true';
				window.location = newEstimateHref;
			})
	}
	function updatePrintView(estimate){
		$('.preso-notes').hide();
		var price_obj = JSON.parse(estimate.presentation_prices);
		console.log(price_obj);
		console.log('price_obj');
		$('#print_payment_method').html( $('select[name="payment_method"] option:selected').text());
		$('#print_down_payment').html( $('select[name="down_payment"] option:selected').text());
		$('#print_downpayment_amount').html( "$" +  $('[name="downpayment_amount"]').val());
		$('#print_balance_by').html( $('select[name="balance_by"] option:selected').text());

		if( 'undefined' != typeof estimate.notes ){

			var notes = JSON.parse(estimate.notes);
			var all_text = "";
			$.each([1,2,3,4,5], function(k,v){
				if( 'undefined' != typeof notes && notes && 'undefined' != typeof notes[v] ){
						
					$text = notes[v].text;
					$('#note'+v+'_text').val($text);
					$user = notes[v].user;
					$('input[name=note'+v+'_user]').val($user);
					if ( notes[v].view == 1 ){
						$('input[name=note'+v+'_customer_view]').prop('checked',true);
						if( 'undefined' != typeof $text && 'undefined' != $text){
							all_text += " * " + $text;
						}
					}
					else{
						$('input[name=note'+v+'_customer_view]').prop('checked',false);
					}
				}
			})
			$('.print-notes').html(all_text);
		}		
	}
	function updatePricingInfo(estimate){
		var price_obj = JSON.parse(estimate.presentation_prices);
		if( price_obj ){
			var systems = window['_' + estimate.type + 's'];
			getPlanTiers(1, function(){
				var types = window._planTiers;
				$.each(types,function(k,v){
					var system_id = estimate[v + '_system_id'],
						ucType = estimate.type.charAt(0).toUpperCase() + estimate.type.slice(1);
					window['get' + ucType ](system_id, function(){
						var system = systems[system_id],
							prices = price_obj[v],
							price = parseInt(system[v + '_price']),
							htmlPrice = '';
		console.log('	updateoringinifo PRICE_OBJ');
console.log(system);
console.log(price);
						if (price) {
							htmlPrice = formatPrice(price.toFixed(2));
						}

						//$('.' + v + '-system .system-price').html( htmlPrice );
						if( 'null' != prices.plan && prices.plan){
							$('[name="' + v + '_financing_plan"]').val(prices.plan);
						}
						$('[name="' + v + '_trane"]').val(prices.trane_rebate);
						$('[name="' + v + '_tep"]').val(prices.tep_rebate);
						$('[name="' + v + '_misc"]').val(prices.misc);
						$('[name="' + v + '_lennox"]').val(prices.lennox_rebate);
						$('[name="' + v + '_costco"]').val(prices.costco);
						$('[name="' + v + '_exec"]').val(prices.exec);
						$('[name="' + v + '_citi"]').val(prices.citi);

						if( 'undefined' != typeof prices.trane_rebate && prices.trane_rebate){
							$('.' + v +'-trane-print').html("$" + prices.trane_rebate);
						}
						else{
							$('.' + v +'-trane-print').html();							
						}
						if( 'undefined' != typeof prices.tep_rebate && prices.tep_rebate){
							$('.' + v +'-tep-print').html("$" + prices.tep_rebate);
						}
						else{
							$('.' + v +'-tep-print').html();							
						}

						if( 'undefined' != typeof prices.lennox_rebate && prices.lennox_rebate){
							$('.' + v +'-lennox-print').html("$" + prices.lennox_rebate);
						}
						else{
							$('.' + v +'-lennox-print').html();							
						}

						if( 'undefined' != typeof prices.misc && prices.misc){
							$('.' + v +'-misc-print').html("$" + prices.misc);
						}
						else{
							$('.' + v +'-misc-print').html();							
						}

						if( 'undefined' != typeof prices.costco && prices.costco){
							$('.' + v +'-costco-print').html("$" + prices.costco);
						}
						else{
							$('.' + v +'-costco-print').html();							
						}

						if( 'undefined' != typeof prices.exec && prices.exec){
							$('.' + v +'-exec-print').html("$" + prices.exec);
						}
						else{
							$('.' + v +'-exec-print').html('');							
						}

						if( 'undefined' != typeof prices.citi && prices.citi){
							$('.' + v +'-citi-print').html("$" + prices.citi);
						}
						else{
							$('.' + v +'-citi-print').html();							
						}

/*
						if( 'undefined' != typeof system.coil_obj && system.coil_obj && system.coil_obj.price ){
							$('.' + v + '-system li.coil').html( 'Coil:' + system.coil_obj.brand + ' ... $' + formatPrice(parseInt(system.coil_obj.price).toFixed(2)));
						}
						if( 'undefined' != typeof system.thermostat_obj && system.thermostat_obj ){
							$('.' + v + '-system li.thermostat').html( 'Thermostat:' + system.thermostat_obj.brand + ' ... $' + formatPrice(system.thermostat_obj.price.toFixed(2)));
						}
						if( 'undefined' != typeof system.condenser_obj && system.condenser_obj ){
							$('.' + v + '-system li.condenser').html( 'Condenser:' + system.condenser_obj.brand + ' ... $' + formatPrice(system.condenser_obj.price.toFixed(2)));
						}
						if( 'undefined' != typeof system.furnace_obj && system.furnace_obj){
							$('.' + v + '-system li.furnace').html( 'Furnace:' + system.furnace_obj.brand + ' ... $' + formatPrice(system.furnace_obj.price.toFixed(2)));
						}
						*/
					}) // end getSystem
				}) // end each of plan $types
			}) // end getPlanTiers
		}
					$('input').on('change',function(){
				estimate_id = window.current_estimate.id;
				updateEstimatePrices(estimate_id);
				updatePresentationPrice(estimate);
			});

	}
	function buildEstimateModal( estimate ){
		if( estimate ){
			var proposal = estimate.proposal,
				price_obj = JSON.parse(estimate.presentation_prices),
				plan = "",
				chosen_plan = estimate.chosen_plan;

			window.current_estimate = estimate;
			window.current_proposal = proposal;
			$('.bullets li').not('#clone').remove();
			// MODAL HEADER
			$('.proposal-id').val( proposal.id );
			$('.estimate-id').val( estimate.id );
			$("#edit_href").attr('href','/' + estimate.type + '?estimate_id=' + estimate.id);
			$("#proposal_href").attr('href','/opportunities/?proposal=' + proposal.id );
			var type = estimate.type.charAt(0).toUpperCase() + estimate.type.slice(1);
			$("#alt-display-estimate-name").html( '#' + estimate.id + ' | ' + type );

			d = new Date(estimate.updated_at.date );
			exp = new Date( proposal.expiration );
			var exp_month = exp.getMonth() + 1; // date indexes jan as 0
			if( exp.getMonth() == 12){
				exp_month = 1;
			}
			$(".alt-expiration").html( exp_month + "/" + exp.getDate()    + "/" + exp.getFullYear()  );
			$(".alt-rep-start").html( (d.getMonth() + 1)  + "/" + d.getDate()    + "/" + d.getFullYear()  );

			// .details / sumamry (first carousel slide)
			$("#alt-address").html( proposal.address );
			$("#alt-rep").html( proposal.rep.first_name + " " + proposal.rep.last_name );
			$("#alt-customer").html( proposal.customer.name );
			$("#alt-customer-phone").html( proposal.customer.phone );
			$("#alt-customer-email").html( proposal.customer.email );
			$("#alt-rep-phone").html( proposal.rep.phone );
			$("#alt-rep-email").html( proposal.rep.email );
			$("#alt-start").html( proposal.start );

var sections = JSON.parse(estimate.worksheet_data),
					pricing = JSON.parse(estimate.presentation_prices),
					$title = "";
				if( estimate.new_replace == 'new'){
					$title += "New ";
				}
				if( estimate.new_replace == 'replace'){
					$title += "Replacement ";
				}
				if( estimate.ac_hp == 'ac'){
					$title += "Air Conditioner ";
				}
				if( estimate.ac_hp == 'hp'){
					$title += "Heat Pump ";
				}
				if( estimate.type == 'split'){
					$title += "Split ";
				}
				if( estimate.type == 'furnace'){
					$title += "Furnace ";
				}
				if( estimate.type == 'package'){
					$title += "Pack ";
				}
				$('#replace_title').html($title);
				$('#job-type').html($title);

				if( estimate.type == 'furnace'){
					var replace_text = $('#furnace_only').html();			
				}
				else{
					var replace_text = $('#' + estimate.new_replace + "_" + estimate.ac_hp + "_" + estimate.type).html();			
				}
				$('#replace_text').html(replace_text);

			// get appropriate type of systems 
			var systems = window['_' + estimate.type + 's'];
			
			//  "tiers" are plan levels - like gold, silver etc
			getPlanTiers(1, function(){
				var tiers = window._planTiers;
				$.each(tiers,function(k,v){
					// get system as :
					//getSystem, getFurnace, getPackage
					var system_id = estimate[v + '_system_id'],
						ucType = estimate.type.charAt(0).toUpperCase() + estimate.type.slice(1);
					if(system_id){
						window['get' + ucType ](system_id, function(){
							var system = systems[system_id],
								thermostat_id = estimate[v + '_thermostat_id'];
							getThermostat(thermostat_id, function(){
								var thermostat = window._thermostats[thermostat_id],
								thermostat_html = "n/a";
								if( 'null' != thermostat && thermostat){
									var model = "";
									if('undefined' != typeof thermostat.model){
										model = thermostat.model;
									}
									var model_number = "";
									if('undefined' != typeof thermostat.model_number){
										model_number = thermostat.model_number;
									}
									var thermostat_html =  model + ' ' + thermostat.model_number;
									$('.' + v + '-system .thermostat-title').html( thermostat_html);
									if('split' == estimate.type || 'package' == estimate.type || 'furnace' == estimate.type){
										bulletClone(v).html('Thermostat: ' + thermostat_html);
									}
								}
								if('undefined' != typeof system){
									if( system.brand && system.model_number){	
										$('.' + v + '-system .title').html( system.model_number);
									}
									else{
										if( system.name ){	
											$('.' + v + '-system .title').html( system.name );
										}						
									}

									if( 'null' != system.image && system.image){
										$('.' + v + '-system .unit-image').attr( 'src', '/images/' + system.image);
									}		
									var brand = "",
										system_brand = "";
									if( "split" == estimate.type && 'undefined' != typeof system.condenser_obj && system.condenser_obj){
										system_brand = system.condenser_obj.brand;
									}
									else{
										system_brand = system.brand;
									}
									if( system_brand == 'Lennox' ){
										brand = "lennox"
									}
									if( system_brand == 'Trane' ){
										brand = "trane"
									}
									if( brand && 'undefined' != brand){
										$('.' + v + '-system .logo img').attr( 'src', '/img/logo-' + brand + '.png').attr('alt', system.Brand);
									}

									// build bullet points
									if('split' == estimate.type || 'package' == estimate.type){
										bulletClone(v).html('SEER: ' + system.seer);
										bulletClone(v).html('EER: ' + system.eer);
										bulletClone(v).html('HSPF: ' + system.hspf);
									}
									if('split' == estimate.type || 'package' == estimate.type || 'furnace' == estimate.type){
										bulletClone(v).html('AFUE: ' + system.afue);
									}
									if('split' == estimate.type || 'package' == estimate.type){
										bulletClone(v).html('Stages of Cooling: ' + system.stages_of_cooling);
									}
									if('split' == estimate.type || 'package' == estimate.type || 'furnace' == estimate.type){
										console.log('PPPPPPPPPPPPPPPPPPPPPPP');
										console.log(system);
										bulletClone(v).html('Stages of Heating: ' + system.stages_of_heating);
									}
									if('package' == estimate.type){
										var blower = "N/A";
										if(1 == system.blower_type){
											blower = "Enhanced";
										}
										if(2 == system.blower_type){
											blower = "Limited";
										}
										if(3 == system.blower_type){
											blower = "Standard";
										}
										bulletClone(v).html('Blower Type: ' + blower);
									}
									if('platinum' == v || 'gold' == v){
										bulletClone(v).html('Smart Dehumidification');
									}
									if('split' == estimate.type || 'package' == estimate.type){
										bulletClone(v).html('Compressor Warranty: ' + system.compressor_warranty);
									}
									if('split' == estimate.type || 'package' == estimate.type || 'furnace' == estimate.type){
										bulletClone(v).html('Parts Warranty: ' + system.parts_warranty);
										bulletClone(v).html('Labor Warranty: ' + system.labor_warranty);
										bulletClone(v).html('Heat Exchanger Warranty: ' + system.heat_exchanger_warranty);
									}
									// hardcoded tier options
									if('platinum' == v || 'gold' == v){
										bulletClone(v).html('Lifetime New Ductwork Warranty');
									}
								}

							});
						}); // end get Specific instance of system
					}
					else{
						$('.' + v + '-system').addClass('empty-system');
					}
					$("#proposalDetailAltModal").modal();
					var $paren = $('.'+v+'-system'),
						box_price = estimate[v + '_box'];
					$paren.find('.box-price').html('$' + box_price).data('val',box_price);
				finalCleanUp();

				}); // end loop of plantypes

			}); // end get planTypes
		}
		$('.' + chosen_plan + '-save').html( 'Selected' ).addClass('btn-primary').val('Selected'); 

		// PAYMENT SECTION
		var price_obj = JSON.parse(estimate.presentation_prices);
		if( price_obj ){
			if( 'null' != price_obj.payment_method && price_obj.payment_method){
				$('[name="payment_method"]').val(price_obj.payment_method);
			}
			if( 'null' != price_obj.down_payment && price_obj.down_payment){
				$('[name="down_payment"]').val(price_obj.down_payment);
			}
			if( 'null' != price_obj.balance_by && price_obj.balance_by){
				$('[name="balance_by"]').val(price_obj.balance_by);
			}
			if( 'null' != price_obj.balance_due && price_obj.balance_due){
				$('[name="balance_due"]').val(price_obj.balance_due);
			}
			if( 'null' != price_obj.downpayment_amount && price_obj.downpayment_amount){
				$('[name="downpayment_amount"]').val(price_obj.downpayment_amount);
			}
			if( 'null' != estimate.chosen_plan && estimate.chosen_plan){
				plan = estimate.chosen_plan.charAt(0).toUpperCase() + estimate.chosen_plan.slice(1);
				$('#chosen_plan').html("Chosen Plan: " + plan);
			}
		}

		// NOTES SECTION
		if( 'undefined' != typeof estimate.notes ){
			var notes = JSON.parse(estimate.notes);
			$.each([1,2,3,4,5], function(k,v){
				if( 'undefined' != typeof notes && notes && 'undefined' != typeof notes[v] ){
					$text = notes[v].text;
					$('#note'+v+'_text').val($text);
					$user = notes[v].user;
					$('input[name=note'+v+'_user]').val($user);
					if ( notes[v].view == 1 ){
						$('input[name=note'+v+'_customer_view]').prop('checked',true);
					}
					else{
						$('input[name=note'+v+'_customer_view]').prop('checked',false);
					}
				}
			})
		}

		var incl =  JSON.parse(estimate.includes),
			rebates = ['citi','tep','costco','lennox','trane','exec'];
		$.each(rebates, function(k,v){
			if(incl && 'undefined' != typeof incl[v]){
				if( incl[v] == 1){
					$('.' + v).show();
				}
				else{
					$('.' + v).hide();
				}
			}
		});
		$('#job-labor-units').html( parseFloat(estimate.job_labor_total).toFixed(2));
		
		var chosen_system_id = estimate[chosen_plan + '_system_id'],
			type = estimate['type'];

		getSystem(type, chosen_system_id, function(){
			if('package' == type){
				var chosen_system = window._packages[chosen_system_id];
			}
			if('split' == type){
				var chosen_system = window._splits[chosen_system_id];
			}
			if('furnace' == type){
				var chosen_system = window._furnaces[chosen_system_id];
			}
			var chosen_thermostat_id = estimate[chosen_plan + '_thermostat_id'],
				preso = JSON.parse(estimate.presentation);
			getThermostat( chosen_thermostat_id, function(){

				// DETAILS LIST
				var chosen_thermostat = window._thermostats[chosen_thermostat_id];
				if( 'object' == typeof preso && preso){
					$.each(preso, function(k,v){
						var $side = 1;
						$.each(v, function(n,o){
							var $paren = $('#' + k + '_wrap_' + $side );
							if( 'undefined' != typeof o.feet && o.feet > 0 ){
								$paren.append( "<dt>" + o.label + "</dt><dd style='margin-bottom:0px;'>" + o.value + "<span style='margin-left:10px; font-weight:600; font-style:italic; '>-   Run up to " + o.feet + " feet.</span></dd>" );
							}
							else{
								$paren.append( "<dt>" + o.label + "</dt><dd>" + o.value + "</dd>" );							
							}
							if( $side == 1 ){
								$side = 2;
							}
							else{
								$side = 1;
							}
						})
					})
				}

				/// JOB SHEET
				if( 'undefined' != typeof chosen_system ){
					var $amps = "",
						$system_price = 0,
						$condenser_price = 0;
					if( 'undefined' != typeof chosen_system.price ){
						$system_price = parseFloat(chosen_system.price);
					}
					if( 'undefined' != typeof chosen_system.condenser && chosen_system.condenser ){
						if( 'undefined' != typeof chosen_system.condenser.brand ){
							$('#job-condenser').html(chosen_system.condenser.brand + " - " + chosen_system.condenser.model_number);
						}
						if( 'undefined' != typeof chosen_system.condenser.amps ){
							$amps = chosen_system.condenser.amps;
						}
						$condenser_price = parseFloat(chosen_system.condenser.price);
					}
					if( 'undefined' != typeof chosen_system.amps ){
						$amps = chosen_system.amps;
					}
					$('#job-condenser-amps').html($amps);
					$('#job-ari').html(chosen_system.ahri);
					$('#job-thermostat').html(chosen_thermostat.brand + " " + chosen_thermostat.model_number);

					var $coil_price = 0,
						coil_obj_depth = 0,
						coil_obj_height = 0;
					if( 'undefined' != typeof chosen_system.coil && chosen_system.coil ){
						coil_obj_depth =  chosen_system.coil.depth;
						coil_obj_height =  chosen_system.coil.height;
						if( 'undefined' != typeof chosen_system.coil.brand && 'undefined' != typeof chosen_system.coil.model_number ){
							$('#job-coil-model').html(chosen_system.coil.brand + ' - ' + chosen_system.coil.model_number);
						}
						if( 'undefined' != typeof chosen_system.coil.height && 'undefined' != typeof chosen_system.coil.depth ){
							$('#job-coil-dimensions').html('Height: ' + chosen_system.coil.height + ' Width: ' + chosen_system.coil.width);
						}
						$coil_price = parseFloat(chosen_system.coil.price);
					}

					var $furnace_price = 0,
						furnace_obj_brand = "",
						furnace_obj_model = "",
						furnace_obj_height = "",
						furnace_obj_width = "";
					if( 'undefined' != typeof chosen_system.furnace && chosen_system.furnace){
						$furnace_price = parseFloat(chosen_system.furnace.price);
						furnace_obj_brand = chosen_system.furnace.brand;
						furnace_obj_model = chosen_system.furnace.model_number;
						furnace_obj_height = parseFloat(chosen_system.furnace.height);
						furnace_obj_width = parseFloat(chosen_system.furnace.width);
					}
					$('#job-furnace').html(furnace_obj_brand + ' - ' + furnace_obj_model);
					$('#job-furnace-dimensions').html('Height: ' + furnace_obj_height + ' Width: ' + furnace_obj_width);
					var total_width = 0,
						total_height = 0;

					// check if packge:)
					if('package' != estimate.type){
						if(coil_obj_depth > furnace_obj_width){
							total_width = coil_obj_depth; 
						}else{
							total_width = furnace_obj_width; 				
						}
						total_height = coil_obj_height + furnace_obj_height;
					}
					else{
						total_width = chosen_system.width;	
						total_height = chosen_system.height;
						$('#job-package-brand').html( chosen_system.brand);
						$('#job-package-model').html( chosen_system.model_number);
					}
					$('#job-total-width').html( total_width);
					$('#job-total-height').html( total_height );

					var $thermostat_price = 0;
					if( 'undefined' != typeof chosen_thermostat && chosen_thermostat ){
						$thermostat_price = parseFloat(chosen_thermostat.price);
					}
					var $control_price = parseFloat($thermostat_price) + parseFloat($furnace_price) + parseFloat($coil_price) + parseFloat($condenser_price) + parseFloat($system_price);
					$('#job-control-number').html($control_price);
				}
				var job_sheet_items = JSON.parse(estimate.job_sheet);
				if( $.isArray(job_sheet_items) ){
					$.each(job_sheet_items, function(k,v){
						if(v.value != 'Select an Option'){
							$('table.job-sheet').append( "<tr><td class='js-label'>" + v.label +  "</td><td>" + v.value + "</td></tr>" );
						}
					});
				}

				

				updatePricingInfo(estimate);
				updatePresentationPrice(estimate);
			})
		}); // end getSystem
	}
	function updateFinancing(e){
		var estimate = window.current_estimate;
		updatePresentationPrice(estimate);
		estimate_id = window.current_estimate.id;
		updateEstimatePrices(estimate_id);
	}
	function updateEstimatePrices($estimate_id){
		var $prices = {};
		var tiers = ['platinum', 'gold', 'silver', 'bronze'];
		$.each(tiers, function(k,v){
			$prices[v] = {};
			//			$prices.platinum = {};
			$prices[v].plan = $('[name="' + v + '_financing_plan"]').val();
			$prices[v].system_price = $('.' + v + '-system .system-price').data('val');
			$prices[v].trane_rebate = $('[name="' + v + '_trane"]').val();
			$prices[v].tep_rebate = $('[name="' + v + '_tep"]').val();
			$prices[v].box_price = $('.' + v + '-system .box-price').data('val');
			$prices[v].sub_total = $('.' + v + '-system .sub-total').data('val');
			$prices[v].misc = $('[name="' + v + '_misc"]').val();
			$prices[v].total_due = $('.' + v + '-system .total-due').data('val');
			$prices[v].lennox_rebate = $('[name="' + v + '_lennox"]').val();
			$prices[v].costco = $('[name="' + v + '_costco"]').val();
			$prices[v].exec = $('[name="' + v + '_exec"]').val();
			$prices[v].citi = $('[name="' + v + '_citi"]').val();
			$prices[v].total_investment = $('.' + v + '-system .total-investment').data('val');
			$prices[v].finance_rate = $('.' + v + '-system .finance-rate').data('val');
			$prices[v].finance_fee = $('.' + v + '-system .finance-fee').data('val');
			$prices[v].boxstore_fee = $('.' + v + '-system .box-store-fee').data('val');
			$prices[v].qualified_amount = $('.' + v + '-system .qualified-amount').data('val');
		});
		$prices.payment_method = $('[name="payment_method"]').val();
		$prices.down_payment = $('[name="down_payment"]').val();
		$prices.balance_by = $('[name="balance_by"]').val();
		$prices.balance_due = $('[name="balance_due"]').val();
		$prices.downpayment_amount = $('[name="downpayment_amount"]').val();

		// get notes
		var $notes= {};
		$.each([1,2,3,4,5], function(k,v){
			$notes[v] = {};
			$notes[v].text = $('#note'+v+'_text').val();
			$notes[v].user = $('input[name=note'+v+'_user]').val();
			if ($('input[name=note'+v+'_customer_view]').is(':checked') ){
				$notes[v].view = 1;
			}
			else{
				$notes[v].view = 0;				
			}
		})
		window._estimates[$estimate_id].presentation_prices = JSON.stringify($prices);
		$.ajax({
			url: '/api/estimate-prices',
			data:     {
				estimate_id : $estimate_id,
				presentation_prices : $prices,
				notes : $notes
			},
			error: function() {
				console.log('error');
			},
			success: function(data) {
				console.log(data);
			},
			type: 'POST'
		});

	}

	function updatePresentationPrice(estimate){
		console.log('estimate');
		console.log(estimate);
		var estimate_id = getQueryVariable('present'),
			proposal_id = estimate.proposal_id,
			proposal = estimate.proposal,
			source_id = proposal.source_id;
			store = proposal.store,
			boxstore_rate = 0,
			cushion = 0,
			comm_box_price = 0,
			comm_misc = 0,
			comm_finance_fee = 0,
			comm_total_due = 0,
			comm_sub_total = 0,
			blended_financing_rate = 0.08,
			comm_atLarge = 0,
			comm_b2 = 0;
		if( store == 'lowes'){ 
			boxstore_rate = .13;
		}
		if( store == 'costco'){
			boxstore_rate = .13;
		}
		if( !store ){
			cushion = 0.1;
		}
		getPlanTiers(1, function(){
			var tiers = window._planTiers;
			$.each(tiers,function(k,v){			
				var system_id = estimate[v+'_system_id'],
					system = estimate[v + '_system'],
					system_price = 0;
				if(system && system.price && system.markup ){
					if( system.divide ){
						var system_price = parseInt(system.price) / system.markup;
					}
					else{
						var system_price = parseInt(system.price) * system.markup;
					}
				}

				var thermostat_price = 0;
				if( estimate[v+'_thermostat'] ){
					var thermostat = estimate[v+'_thermostat'];
					if(thermostat && thermostat.price ){
						var thermostat_price = parseInt(thermostat.price);
						if( thermostat.markup ){
							if( thermostat.divide ){
								var thermostat_price = parseInt(thermostat.price) / thermostat.markup;				
							}
							else{
								var thermostat_price = parseInt(thermostat.price) * thermostat.markup;				
							}
						}
					}
				}
				var coil_price = 0;
				if( system &&  'undefined' != typeof system.coil && system.coil ){
					var coil = system.coil;
					if(coil && coil.price ){
						var coil_price = parseInt(coil.price);				
						if( coil.markup ){
							if( coil.divide ){
								var coil_price = parseInt(coil.price) / coil.markup;				
							}
							else{
								var coil_price = parseInt(coil.price) * coil.markup;				
							}
						}
					}
				}
				var condenser_price = 0;
				if( system &&  'undefined' != typeof system.condenser && system.condenser ){
					var condenser = system.condenser;
					if(condenser && condenser.price ){
						var condenser_price = parseInt(condenser.price);				
						if( condenser.markup ){
							if( condenser.divide ){
								var condenser_price = parseInt(condenser.price) / condenser.markup;				
							}
							else{
								var condenser_price = parseInt(condenser.price) * condenser.markup;				
							}
						}
					}
				}
				var furnace_price = 0;
				if( system &&  'undefined' != typeof system.furnace_air_handler ){
					var furnace = system;
					if(furnace && furnace.price ){
						var furnace_price = parseInt(furnace.price);				
						if( furnace.markup ){
							if( furnace.divide ){
								var furnace_price = parseInt(furnace.price) / furnace.markup;				
							}
							else{
								var furnace_price = parseInt(furnace.price) * furnace.markup;				
							}
						}
					}
				}
				if( system &&  'undefined' != typeof system.furnace && system.furnace ){
					var furnace = system.furnace;
					if(furnace && furnace.price ){
						var furnace_price = parseInt(furnace.price);				
						if( furnace.markup ){
							if( furnace.divide ){
								var furnace_price = parseInt(furnace.price) / furnace.markup;				
							}
							else{
								var furnace_price = parseInt(furnace.price) * furnace.markup;				
							}
						}
					}
				}
				// get box price for tier from EDIT page save:
				edit_box_price = 0;
				if( estimate[v+'_box'] ){
					edit_box_price = estimate[v+'_box'];
				}

//console.log(v + '========systemprice' +  parseInt(system_price) + 'box price:' + parseInt(edit_box_price) + 'condesner price:' + parseInt( condenser_price) + 'furnace price:' + parseInt(furnace_price) + ' coilprice: ' + parseInt(coil_price) + ' estimatev:' + estimate[v] + ' thermostat price: ' + parseInt(thermostat_price));

				var package_price = parseInt(system_price) + parseInt(edit_box_price) + parseInt( condenser_price) + parseInt(furnace_price) + parseInt(coil_price) + estimate[v] + parseInt(thermostat_price);
				$('.'+v+'-system .system-price').html('$'+ formatPrice(package_price.toFixed(2))).data('val', package_price);

				//$('.'+v+'-system .system-price').data('val');
				var $paren = $('.'+v+'-system');

				var trane_rebate = parseInt($paren.find('[name="' +  v + '_trane"]').val());
				if( isNaN( trane_rebate)) {
					trane_rebate = 0;
				} 
				var tep_rebate = parseInt($paren.find('[name="' +  v + '_tep"]').val());
				if( isNaN( tep_rebate)){
					tep_rebate = 0;
				}
				box_price = parseInt($paren.find('.box-price').data('val'));
				if( isNaN (box_price)){
					box_price = 0;
				}

				var sub_total = parseInt(package_price) - parseInt(trane_rebate) - parseInt(tep_rebate);
				$paren.find('.sub-total').html('$' + formatPrice(sub_total.toFixed(2))).data('val',sub_total);

				var misc = parseInt($paren.find('[name="'+ v + '_misc"]').val());
				if( isNaN (misc)){
					misc = 0;
				}
				var total_due = parseInt(sub_total) - parseInt(misc);
				$paren.find('.total-due').html('$' + formatPrice(total_due.toFixed(2))).data('val',total_due);

				var lennox_rebate = parseInt($paren.find('[name="' +  v + '_lennox"]').val());
				if( isNaN( lennox_rebate)) {
					lennox_rebate = 0;
				} 
				var costco_rebate = parseInt($paren.find('[name="' +  v + '_costco"]').val());
				if( isNaN( costco_rebate)) {
					costco_rebate = 0;
				} 
				var exec_rebate = parseInt($paren.find('[name="' +  v + '_exec"]').val());
				if( isNaN( exec_rebate)) {
					exec_rebate = 0;
				}
				var citi_rebate = parseInt($paren.find('[name="' +  v + '_citi"]').val());
				if( isNaN( citi_rebate)) {
					citi_rebate = 0;
				}
				var total_investment = total_due - parseInt(lennox_rebate) - parseInt(costco_rebate) - parseInt(exec_rebate) - parseInt(citi_rebate);
				$paren.find('.total-investment').html('$' + formatPrice(total_investment.toFixed(2))).data('val',total_investment);

				var plan_id = $('[data-price-type="'+ v + '"]').val(),
					plan = window.finance_plans[plan_id],
					apr = 0,
					dealer_fee = 0;
				if( typeof plan != 'undefined'){

					apr = plan.APR.replace(/%/g,"");
					dealer_fee = plan.Dealer_Fee.replace(/%/g,"");
					payment_factor = plan.Payment_Factor.replace(/%/g,"");
					if( 0 >= payment_factor ){
						var per_month = total_due / parseInt(plan.Duration);
					}
					else{
						var per_month = total_due * parseFloat(payment_factor)/100 ;
					}
					if( !isNaN(per_month)){
						$paren.find('.h3-title').html('$' + formatPrice(per_month.toFixed(2)) + '/mo');
					}
					else{
						$paren.find('.h3-title').html('$0.00');
					}
				}
				$paren.find('.finance-rate').html(apr + '%').data('val',apr);

				var finance_fee = dealer_fee/100 * total_due;
				$paren.find('.finance-fee').html('$' + formatPrice(finance_fee.toFixed(2))).data('val', formatPrice(finance_fee.toFixed(2)));

				var boxstore_fee = total_due * boxstore_rate; 
				$paren.find('.box-store-fee').html('$' + formatPrice(boxstore_fee.toFixed(2))).data('val', formatPrice(boxstore_fee.toFixed(2)));

				var qualified_amount = total_due - parseInt(boxstore_fee) - parseInt(finance_fee); 
				$paren.find('.qualified-amount').html('$' + formatPrice(qualified_amount.toFixed(2))).data('val', formatPrice(qualified_amount.toFixed(2)));

				if( v == estimate.chosen_plan){
					var comm_package_price = package_price;
					var trane_rebate_credit = trane_rebate * -0.5;
					if( isNaN( trane_rebate_credit)) {
						trane_rebate_credit = 0;
					}
					$('.comm-b3').html('$'+ trane_rebate_credit).data('val', trane_rebate_credit);
					comm_b2 = comm_package_price.toFixed(2) - box_price.toFixed(2);
					comm_atLarge = cushion * comm_b2;
					$('.comm-b2').html('$'+ comm_b2).data('val', comm_b2);
					$('.comm-sub-minus-box').html("$" + comm_b2);

					var true_miscellaneous = misc - box_price;
					$('.comm-b6').html('$'+ true_miscellaneous).data('val', true_miscellaneous);

					var lennox_rebate_fee = lennox_rebate * -0.3;
					if( isNaN( lennox_rebate_fee)) {
						lennox_rebate_fee = 0;
					}
					$('.comm-b8').html('$'+ lennox_rebate_fee).data('val', lennox_rebate_fee);


					var comm_total_due = comm_b2 - true_miscellaneous - tep_rebate - trane_rebate; // b2 - b6 - b16 - b14
					$('.comm-b10').html('$'+ comm_total_due).data('val', comm_total_due);

					$('.comm-b14').html('$'+ trane_rebate);
					$('.comm-b16').html('$'+ tep_rebate);
					$('.comm-b18').html('$'+ lennox_rebate);
					$('.comm-b20').html('$'+ comm_package_price);
					$('.comm-b22').html('$'+ misc);
					$('.comm-b25').html('$'+ box_price);

					$('.comm-e14').html(plan.title);
					$('.comm-e15').html(dealer_fee);

					$('.comm-e18').html(store);
					$('.comm-e19').html(boxstore_rate);

					$('.comm-e23').html(blended_financing_rate);

					$('.comm-g2').html('$'+ comm_total_due).data('val', comm_total_due);
					$('.comm-g3').html(finance_fee.toFixed(2));
					$('.comm-g4').html(boxstore_fee.toFixed(2));
					

					comm_box_price = box_price;
					comm_misc = misc;
					comm_finance_fee = finance_fee;
					comm_sub_total = sub_total;
					comm_qualified_amount = qualified_amount - trane_rebate_credit + lennox_rebate_fee + tep_rebate ;
					$('.comm-g5').html('$' +comm_qualified_amount);
					$paren.find('.at-large').html('$' + formatPrice(comm_atLarge.toFixed(2))).data('val',comm_atLarge.toFixed(2));
				}

				$('.comm-box').html("$" + box_price);
				$('.comm-misc').html("$" + comm_misc);
				var rate_reducer = 0.0025,
					starting_commission_rate = .10,
					fees_over_eight = 0;
				if( comm_finance_fee > blended_financing_rate * comm_total_due  ){
					fees_over_eight = comm_finance_fee - blended_financing_rate * comm_total_due;
				}
				var true_discount =  comm_misc - comm_box_price  + fees_over_eight - comm_atLarge;
				$('.comm-discount').html("$" + true_discount.toFixed(2));
				var discount_percent = parseInt(true_discount) / parseInt(comm_b2);

				if( !isNaN(discount_percent)){
					var dp = Math.ceil(discount_percent * 100 );
					$('.comm-discount-percent').html( dp /100);
				}

				$('.comm-rate-reducer').html(rate_reducer * 100);
				var commission_discount_percent = Math.ceil(rate_reducer * dp * 100);
				$('.comm-commission-discount-percent').html(commission_discount_percent.toFixed() + "%"  );
				$('.comm-starting-commission-rate').html( starting_commission_rate * 100 + "%");
				var commission_rate_i = starting_commission_rate - commission_discount_percent/100,
					comm_rate = 0;
				if( !isNaN(commission_rate_i)){
					if(commission_rate_i < 0.1 ){
						comm_rate_ii = commission_rate_i;
					}
					if(commission_rate_i > 0.1 ){
						comm_rate_ii = 0.1;
					}
					if(commission_rate_i == starting_commission_rate ){
						comm_rate_ii = 0.1;
					}			

					$('.comm-commission-rate-i').html( commission_rate_i.toFixed(3) * 100 + "%");
					$('.comm-rate-ii').html( comm_rate_ii.toFixed(3) * 100 + "%");
					$('.comm-qual-amount').html('$' + formatPrice(comm_qualified_amount.toFixed(2))).data('val', formatPrice(comm_qualified_amount.toFixed(2)));
					var comm_final = (comm_rate_ii * comm_qualified_amount);
					$('.comm-amount').html('$' + formatPrice(comm_final.toFixed(2))).data('val', formatPrice(comm_final.toFixed(2)));
				}
				// update balance due
				var downpayment_amount = parseInt($('input[name=downpayment_amount]').val());
				var comm_total_due = $('.comm-b10').data('val');
				if('undefined' != typeof comm_total_due){
					var balance_due = comm_total_due - downpayment_amount;
					if( !isNaN( balance_due)){
						$('input[name=balance_due]').val(balance_due);
					}
				}
			})
			finalCleanUp();
		})
	}
	function setBulletHeight(){
		var tallestUL = 0;
		var bulletColumns = $('#proposalDetailAltModal .caption .bullets');
		bulletColumns.height('500px');
		setTimeout( function () {
			bulletColumns.each(function() {
				var height = $(this).find('li').length * 31.6;

				if( height > tallestUL ) {
		      tallestUL = height; 
		    }
			});

			bulletColumns.height(tallestUL);
			$('.controls-wrapper').show();
		}, 100);

	}
	$('#proposalDetailAltModal').on('hidden.bs.modal', function () {
		$('#proposalDetailAltModal .item.active').removeClass('active');
		var items = $('#proposalDetailAltModal .item');
		$(items[0]).addClass('active');	

	})
	function finalCleanUp(){
			$('.plans input, .plans select, .rebate-table input').on('change',function(){
				estimate_id = window.current_estimate.id;
				updateEstimatePrices(estimate_id);
				getEstimate(estimate_id, function(){
					var estimate = window._estimates[id];
					updatePresentationPrice(estimate);
				});

			})

		setBulletHeight();
	}
	$( document ).ready(function() {
 		if( getParameterByName('estimate') ){
			$("#proposalDetailAltModal").modal();
			var id = getQueryVariable('estimate');
			getEstimate(id, function(){
				var estimate = window._estimates[id];
				var proposal_id = estimate.proposal_id;
				getProposal(proposal_id, function(){
					buildEstimateModal(estimate);
				});
			});
		}
	});