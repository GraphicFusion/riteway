	// New Ajax GET functions can be added here by 
	// 1. Copy and paste the getProposal() function below and change the two instances of 'proposal' to the new model name. 
	// 2. Add the model name to the window cache - Window Object Item List - in the form of leading underscore and plural model name : 	window._models = {};
	// 3. Define the buildAjaxModels() function (like buildAjaxProposals) in helpers.php. 
	// 4. call the GET with getModel();
	// Routing for these simple ajax GET calls are handled automatically in routes/api.php if there is nothing unique about the query using the url of form: '/GET/{group_id}/{type}/{uri_id}'
	// If the query requires more params - build a unique route handler (like for proposals)
	
	/**** WINDOW OBJECT ITEM LIST - CACHE ****/
	window._estimates = {};
	window._proposals = {};
	window._customers = {};
	window._furnaces = {};
	window._packages = {};
	window._planTiers = {};
	window._systems = {}; // split
	window._splits = window._systems;
	window._thermostats = {};
	/**** END WINDOW OBJECT ITEM LIST - CACHE ****/


	/****AJAX GET FUNCTIONS****/ 

	// getProposal will "cache" any previous calls in window._proposals and will check automatically
	function getProposal(id, _callback){
		getAjaxItem('proposal',id, _callback); 
	}
	function getEstimate(id, _callback){
		getAjaxItem('estimate',id, _callback);
	}
	function getCustomer(id, _callback){
		getAjaxItem('customer',id, _callback);
	}
	function getSystem(type, id, _callback){
		if('furnace' == type){
			getAjaxItem('furnace',id, _callback); 
		}
		if('split' == type){
			getAjaxItem('system',id, _callback); 
		}
		if('package' == type){
			console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPKKKKKLLLLLLLLLPLLLLLLLLLLLLLLLLLLP');
			getAjaxItem('package',id, _callback); 
		}
	}
	function getFurnace(id, _callback){
		getAjaxItem('furnace',id, _callback); 
	}
	function getThermostat(id, _callback){
		getAjaxItem('thermostat',id, _callback); 
	}
	function getPackage(id, _callback){
		getAjaxItem('package',id, _callback); 
	}
	function getSplit(id, _callback){
		getAjaxItem('system',id, _callback); 
	}
	function getPlanTiers(id, _callback){
		getAjaxItem('planTier', id, _callback); 
	}
	/****END AJAX GET FUNCTIONS****/ 

	// getAjaxItem is a generic solution for all ajax requests - will "cache" any previous calls in window._{item} and will check automatically
	function getAjaxItem(itemType,id, _callback){
		// UNCLEAR HOW TO HANDLE GROUP ID YET - TO DO
		var group_id = 3,
			returnItems = {};

		if( id && id in window['_' + itemType + 's'] ){
			_callback(); // runs callback function from original GET call
		}
		else{
			// item does not exist in window; call appropriate ajax function, and when ajax is complete, add returned item to the window list
			$.when((getItem(itemType,group_id,id,'','GET'))).done(function(ajaxModels){
				_ajaxModels = JSON.parse(ajaxModels);
				$.each(_ajaxModels,function(k,v){
					window['_' + itemType + 's'][k] = v;
					returnItems[k] = v;
				})
				_callback(); // runs callback function from original GET call
			});
		}
	}

	function getItem( type, group_id, id, data, method){
		var uriId = encodeURIComponent(JSON.stringify(id)),
			ajaxModels = {};
		return $.ajax({
			url: '/api/' + method + '/' + group_id + '/' + type + '/' + uriId,
			data: data,
			error: function() {
				console.log('error');
			},
			success: function(data) {
				console.log('success');
				console.log(data);
			},
			type: method
		});
	}
$( document ).ready(function() {


	/*** TEST ------ comment out  ****/
	var $id = [3,2];
	var $id = 3;
	//var $id = "";
	//var $est_id = [13,14,15];
		var $est_id = 13;
	//	var $est_id = "";
	//getProposal($id);
	var $cust_id = 8;
//	var $cust_id = [7,8];
/*
getProposal($id, function(){
	console.log('reqProposals');
	$.each(window._proposals, function(k,v){
		console.log(k);
		console.log(v);

	})
});
getCustomer($cust_id, function(){
	console.log('reqcustomers');
	$.each(window._customers, function(k,v){
		console.log(k);
		console.log(v);

	})
});
getEstimate($est_id, function(){
	console.log('reqestimates');
	$.each(window._estimates, function(k,v){
		console.log(k);
		console.log(v);

	})
});
*/
	//getEstimate($est_id);

});