/**** GENERIC FUNCTIONS ****/	
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function setEstimateHref(){
	var $option = $('#estimate').find(":selected"),
		type = $option.data('type'),
		id = $('#estimate').val(),
		editHref = '/' + type + '/?estimate_id=' + id,
		proposal_id = $("#modal-proposal-id").val();
	$("#alt-edit").attr('href', editHref );		
}

/**** END GENERIC FUNCTIONS ****/	