$( document ).ready(function() {
	bindEvents();
	$( "#sections" ).sortable({
		stop: function(){
			numberSections()
		}	
	});
	function showMessage( mssg ){
		$('.fixed-message').html( mssg ).show();
		setTimeout(function(){
			$('.fixed-message').addClass('fade-message');
		}, 500);
	}
    $( "#sections" ).disableSelection();

	$('#add_section').click(function(){
		buildSection();
	});

	// $loaded_worksheet is output on edit-worksheet.blade
	if( 'undefined' != typeof $loaded_worksheet ){
//console.log($loaded_worksheet);
		if( $loaded_worksheet.sections ){
			$.each( $loaded_worksheet.sections, function(k,v){
				buildSection( v );
				
			});
			buildNav();
		}
	}
	else{
		buildSection();
		buildNav();
		unlockInputs();
	}
	
	$('#formBuilderSubmit, #save_worksheet').click(function(e){
		e.preventDefault();
		var $worksheet = {},
			$sections = $('.section'),
			$levels = ['platinum','gold','silver','bronze'];
		$worksheet.sections = [];
		$.each($sections, function(k,v){
			var $section = new Object();
			$section.title = $(v).find('input[name="section_title"]').val();
			var $inputs = $(v).find('.inputs > li');
			$section.inputs = [];
			$.each($inputs, function(ik,iv){
				var $input = new Object();
				if( $(iv).hasClass('dropdown-wrapper') ){
					$input.type = 'dropdown';
					$input.title = $(iv).find('input[name="dropdown_title"]').val();
					$input.allow_null = 0;
					if( $(iv).find('input[name="allow_null"]').is(':checked') ){
						$input.allow_null = 1;
					}
					$input.cost_included = 0;
					if( $(iv).find('input[name="cost_included"]').is(':checked') ){
						$input.cost_included = 1;
					}
					$input.quantity = 0;
					if( $(iv).find('input[name="quantity"]').is(':checked') ){
						$input.quantity = 1;
					}
					$input.note = 0;
					if( $(iv).find('input[name="note"]').is(':checked') ){
						$input.note = 1;
					}
					$input.total_feet = 0;
					if( $(iv).find('input[name="total_feet"]').is(':checked') ){
						$input.total_feet = 1;
					}
					$input.job_sheet = 0;
					if( $(iv).find('input[name="job_sheet"]').is(':checked') ){
						$input.job_sheet = 1;
					}
					$input.included_amount_check = 0;
					if( $(iv).find('input[name="included_amount_check"]').is(':checked') ){
						$input.included_amount_check = 1;
					}
					$input.make_repeater = 0;
					if( $(iv).find('input[name="make_repeater"]').is(':checked') ){
						$input.make_repeater = 1;
					}
					$input.price_category = $(iv).find('select[name="price_category"]').val();
					var $options = new Object();
					$input_options = $(iv).find('.options input');
					$.each($input_options, function(iok,iov){
						if( $(iov).is(':checked') ){
						}
					});

					var $choices = [];
					$input_choices = $(iv).find('.choice');
					$.each($input_choices, function(ick,icv){
						var $choice = new Object();
						$choice.title = $(icv).find('input[name="choice_title"]').val();

						$choice.variables = new Object();
						$choice.variables.included_amount = $(icv).find('input[name="included_amount"]').val();
						var $prices = new Object();
						if( $(icv).find('[name="tiered_pricing"]').is(':checked') ){
							$choice.tiered_pricing = 1;
							$.each($levels,function(lk,level){
								$prices[level] = new Object();

								var $selector_base = 'input[name="' + level + '_base_price"]';
								$prices[level].base = $(icv).find($selector_base).val();
								var $selector_unit = 'input[name="' + level + '_unit_price"]';
								$prices[level].unit = $(icv).find($selector_unit).val();
							});
						}
						else{
							$choice.tiered_pricing = 0;
							$prices.all = new Object();
							var $selector_base = 'input[name="base_price"]';
							$prices.all.base = $(icv).find($selector_base).val();
							var $selector_unit = 'input[name="unit_price"]';
							$prices.all.unit = $(icv).find($selector_unit).val();
						}
						$choice.job_unit = $(icv).find('input[name="job_unit"]').val();
						$choice.prices = $prices;
						$choices.push($choice);
					});
					$input.choices = $choices;
				}
				else if( $(iv).hasClass('custom-wrapper') ){
					$input.type = 'custom';
						$choice.job_unit = $(icv).find('input[name="job_unit"]').val();
				}
				else if( $(iv).hasClass('blank-wrapper') ){
					$input.type = 'blank';
					$input.title = $(iv).find('input[name="blank_title"]').val();
				
					$input.quantity = 0;
					if( $(iv).find('input[name="quantity"]').is(':checked') ){
						$input.quantity = 1;
					}
					$input.note = 0;
					if( $(iv).find('input[name="note"]').is(':checked') ){
						$input.note = 1;
					}
					$input.make_repeater = 0;
					if( $(iv).find('input[name="make_repeater"]').is(':checked') ){
						$input.make_repeater = 1;
					}
					$input.price_category = $(iv).find('select[name="price_category"]').val();
				
				}
				$section.inputs.push( $input );
			});
			$worksheet.sections.push( $section );
		});
		var $group_id = $('input[name="group_id"]').val(),
			$type = $('input[name="type"]').val();
		$.ajax({
			url: '/api/worksheet',
			data:     {
				worksheet : $worksheet,
				type : $type,
				group_id : $group_id
			},
			error: function() {
				console.log('error');
			},
			success: function(data) {
				console.log(data);
				$('.form-changes').html('');
				setTimeout(function(){
					window.showMessage('This worksheet has been saved.');
				}, 500);
				$('#formBuilderSubmit').removeClass('form-changed .main').html('Save');
				window.last_change = timeNow();
			},
			type: 'POST'
		});
	});
});
function delayUpdate(){
	setTimeout(function(){
		updateLineTotal();
	}, 300);
}
function lockPrices(){
	$(".choice-price input").attr("disabled", true);
	$(".admin-unlock .choice-price input").attr("disabled", false);
}
	function unlockInputs(){
		$('.form-group .worksheet-option.locked').not('.template .worksheet-option.locked').first().removeClass('locked').prop("disabled", false);		
	} 
		function showPrices($paren, choice_key){
		var $select_prices = $paren.find('.choice-price'),
			$chosen_prices = $paren.find('.choice-price[data-choice-key="' + choice_key + '"]');
		$select_prices.hide().removeClass('active');
		$chosen_prices.show().addClass('active');
	}

function bindEvents(){
	lockPrices();
	// LOCKING INPUTS
	$('.worksheet-option').unbind('change');
	$('.worksheet-option').change(function(){
		unlockInputs();
	})

	// REMOVE SECTION
	$('.remove_section').unbind('click'); 		// cleanup previous binds
	$('.remove_section').click(function(e){
		var $this = $(e.target);
		$this.closest('.ui-state-default').remove();
		delayUpdate();
		formChanged();
	});

	// REMOVE DROPDOWN
	$('.remove_dropdown').unbind('click'); 		// cleanup previous binds
	$('.remove_dropdown').click(function(e){
		var $this = $(e.target);
		$this.closest('.dropdown-wrapper').remove();
		delayUpdate();
		formChanged();
	});

	// REMOVE BLANK
	$('.remove_blank').unbind('click'); 		// cleanup previous binds
	$('.remove_blank').click(function(e){
		var $this = $(e.target);
		$this.closest('.blank-wrapper').remove();
		delayUpdate();
		formChanged();
	});

	
	// COLLAPSE DROPDOWN
	$('.collapse_dropdown .ico').unbind('click'); 		// cleanup previous binds
	$('.collapse_dropdown .ico').click(function(e){
		var $this = $(e.target);
		if( $this.hasClass('collapsed') ){
			$this.removeClass('plus collapsed').addClass('minus').html('&minus;');
			$this.closest('.dropdown-wrapper').find('.dropdown-content').show();
			$this.closest('.dropdown-wrapper').find('.input-hidden-title').hide();
		}
		else{
			$this.addClass('plus collapsed').removeClass('minus').html('&plus;');
			$this.closest('.dropdown-wrapper').find('.dropdown-content').hide();
			var $dropdown_title = $this.closest('.dropdown-wrapper').find('input[name="dropdown_title"]').val();
			$this.closest('.dropdown-wrapper').find('.input-hidden-title').html($dropdown_title).show();
		}
	});

	// COLLAPSE BLANK
	$('.collapse_blank .ico').unbind('click'); 		// cleanup previous binds
	$('.collapse_blank .ico').click(function(e){
		var $this = $(e.target);
		if( $this.hasClass('collapsed') ){
			$this.removeClass('plus collapsed').addClass('minus').html('&minus;');
			$this.closest('.blank-wrapper').find('.blank-content').show();
			$this.closest('.blank-wrapper').find('.input-hidden-title').hide();
		}
		else{
			$this.addClass('plus collapsed').removeClass('minus').html('&plus;');
			$this.closest('.blank-wrapper').find('.blank-content').hide();
			var $blank_title = $this.closest('.blank-wrapper').find('input[name="blank_title"]').val();
			$this.closest('.blank-wrapper').find('.input-hidden-title').html($blank_title).show();
		}
	});

	// COLLAPSE SECTION
	$('.collapse_section .ico').unbind('click'); 		// cleanup previous binds
	$('.collapse_section .ico').click(function(e){
		var $this = $(e.target);
		if( $this.hasClass('collapsed') ){
			$this.removeClass('plus collapsed').addClass('minus').html('&minus;');
			$this.closest('.section').find('.inputs-wrapper').show();
		}
		else{
			$this.addClass('plus collapsed').removeClass('minus').html('&plus;');
			$this.closest('.section').find('.inputs-wrapper').hide();
		}
	});

	// REMOVE DROPDOWN CHOICE
	$('.remove_choice').unbind('click'); 		// cleanup previous binds
	$('.remove_choice').click(function(e){
		var $this = $(e.target);
		$this.closest('.choice').remove();
		formChanged();
	});

	// REMOVE CUSTOM
	$('.remove_custom').unbind('click'); 		// cleanup previous binds
	$('.remove_custom').click(function(e){
		var $this = $(e.target);
		$this.closest('.custom-wrapper').remove();
		formChanged();
	});

	// DROPDOWN OPTIONS
	$('input[name="included_amount_check"]').unbind('change'); 		// cleanup previous binds
	$('input[name="included_amount_check"]').change( function(e){
		var $this = $(e.target),
			$paren = $($this).closest('.options');
		if ($this.is(':checked')){
			$paren.find('input[name="total_feet"]').prop('checked', true);
			$($this).closest('.dropdown-wrapper').find('.included_amount_input_wrapper').removeClass('inactive-input');
		}
		else{
			$($this).closest('.dropdown-wrapper').find('.included_amount_input_wrapper').addClass('inactive-input');
			$($this).closest('.dropdown-wrapper').find('.included_amount_input_wrapper input').val('');
		}		
		delayUpdate();
		formChanged();
	});
	$('input[name="total_quantity"]').unbind('change'); 		// cleanup previous binds
	$('input[name="total_quantity"]').change( function(e){
		delayUpdate();
		formChanged();
	});
	$('input[name="total_feet"]').unbind('change'); 		// cleanup previous binds
	$('input[name="total_feet"]').change( function(e){
		var $this = $(e.target),
			$paren = $($this).closest('.options');
		if ($this.is(':checked')){
			$paren.find(".included_amount_wrapper").css('display','inline-block');
			$($this).closest('.dropdown-wrapper').find('.included_amount_input_wrapper').removeClass('inactive-input');
		}
		else{
			var $incl = $paren.find(".included_amount_wrapper");
			$incl.css('display','none');
			$incl.find('input').attr('checked',false);

			// clear value on included_amount (only included if has total feet)
			$($this).closest('.dropdown-wrapper').find('.included_amount_input_wrapper').addClass('inactive-input');
			$($this).closest('.dropdown-wrapper').find('.included_amount_wrapper input').val('');
		}
		delayUpdate();
		formChanged();
	});
	$('input[name="tiered_pricing"]').unbind('change'); 		// cleanup previous binds
	$('input[name="tiered_pricing"]').change( function(e){
		var $this = $(e.target),
			$paren = $($this).closest('.choice');
		toggleTiered( $this, $paren );
		delayUpdate();
		formChanged();
	});

	$('input[data-price-name]').unbind('change'); 		// cleanup previous binds
	$('input[data-price-name]').change( function(e){
		delayUpdate();
		formChanged();
	});
	
	// CUSTOM BUILD
	$('.add_custom').unbind('click'); 		// cleanup previous binds
	$('.add_custom').click(function(e){
		var $this = $(e.target),
			$paren = $($this).closest('.ui-state-default');
		if( $this.hasClass('add_custom')){
			var data = {
			    dropdown_title: "Custom"
			}
			var template = $("#input_custom_template").html();
			var html = rw_render(template, data);
			$paren.find(".inputs").append(html);
		}
		$paren.find( ".inputs" ).sortable();
		$paren.find( ".inputs" ).disableSelection();
		bindEvents();
		formChanged();
		scrollToNewElement(html);
	});

	// DROPDOWN BUILD
	$('.add_dropdown').unbind('click'); 		// cleanup previous binds
	$('.add_dropdown').click(function(e){
		var $this = $(e.target),
		$paren = $($this).closest('.ui-state-default');
		var html = addDropdown($paren);	
	 	$paren.find('.inputs-wrapper').show();		
		$paren.find('.collapse_section .ico').removeClass('plus collapsed').addClass('minus').html('&minus;');		
		setTimeout(function(){
			window.showMessage('A new dropdown field has been added.');
		}, 500);
		formChanged();
		scrollToNewElement(html);
	});
	
	// BLANK BUILD
	$('.add_blank').unbind('click'); 		// cleanup previous binds
	$('.add_blank').click(function(e){
		var $this = $(e.target),
		$paren = $($this).closest('.ui-state-default');
		var $newBlank = addBlank($paren);
	 	$paren.find('.inputs-wrapper').show();		
		$paren.find('.collapse_section .ico').removeClass('plus collapsed').addClass('minus').html('&minus;');	
		setTimeout(function(){
			window.showMessage('A new blank field has been added.');
		}, 500);
		formChanged();
		scrollToNewElement($newBlank);
	});

	// DROPDOWN CHOICES BUILD
	$('.add_dropdown_choice').unbind('click'); 		// cleanup previous binds
	$('.add_dropdown_choice').click(function(e){
		var $this = $(e.target);
			var html = $($("#dropdown_choices_template").html()),
				$paren = $this.closest('.dropdown-wrapper');
			$paren.find(".choices").append(html);
		if ( $paren.find('input[name="included_amount_check"]').is(':checked')){
			$paren.find('.included_amount_input_wrapper').removeClass('inactive-input');
		}
		$( "#inputs" ).sortable();
		$( "#inputs" ).disableSelection();
		bindEvents();
		formChanged();
	});
	$('.delete-repeater').unbind('click'); 		// cleanup previous binds
	$('.delete-repeater').click(function(e){
		var $target = $(e.target);
		if( $('.worksheet').hasClass('new-estimate') ){
			if( ($target.closest('.inputs-wrapper').children('.form-group').length + $target.closest('.inputs-wrapper').find('.field-wrapper > .form-group:not(.template)').length) > 1){
				$target.closest('.form-group').remove();
			}

		}
		else{
			if( $target.closest('.inputs-wrapper').children('.form-group').length > 1){
				$target.closest('.form-group').remove();
			}

		}
		delayUpdate();
		formChanged();
	})

	$('input:not(#save_estimate)').unbind('click'); 		// cleanup previous binds
	$('input:not(#save_estimate)').change(function(e){
		formChanged();
	})


	// REPEATER OPTION (ADD INPUT)
	$('.add-link').unbind('click'); 		// cleanup previous binds
	$('.add-link').click(function(e){
		e.preventDefault();
		var $this = $(e.target),
			repeater_count = parseInt($this.data('repeater-count')),
			new_count = repeater_count + 1,
			paren = $this.closest('.form-group '),
			grand_paren = $this.closest('.inputs-wrapper'),
			repeater = $(paren).find('.repeater-group').first(),
			price_cat = $(repeater).closest('.template').data('price-category'),
			clone = $(repeater).clone();
		$(clone).appendTo( $(grand_paren));
		$(clone).find('.worksheet-option.locked').removeClass('locked').prop("disabled", false);	
		$this.data('repeater-count', new_count );
		$(clone).find('.choice-price').hide();
		$(clone).find('option:selected').prop("selected", false);
		showPrices($(clone) );
		$(clone).find('input[name="feet"], input[name="inc"], input[name="net"]').val("");
		$(clone).data('repeater-number',new_count);
		$(clone).wrap( "<div class='form-group  input-group-mix' data-price-category='" + price_cat + "'></div>" );
		bindEvents();
		formChanged();
	})

	$('.edit_title, .save_title').unbind('click'); 		// cleanup previous binds
	$('.edit_title, .save_title').click(function(e){
		var $this = $(e.target),
			$paren = $this.closest( '.ui-state-default' ),
			$input = $paren.find('input[name="section_title"]'),
			$label = $paren.find('label[for="section_title"]'),
			header = $paren.find('.section_title_head'),
			editTitle = $paren.find('.edit_title'),
			saveTitle = $paren.find('.save_title');
		if( $this.attr('class') == 'edit_title'){
			$($input).show();			
			$($label).show();			
			$(header).hide();
			editTitle.hide();			
			saveTitle.show();			
		}
		else{
			$($input).hide();
			$($label).hide();
			$(header).show();
			saveTitle.hide();			
			editTitle.show();			
		}
	});

	// update section h3 when input is changed
	$('.section_title_input').unbind('click'); 		// cleanup previous binds
	$('.section_title_input').change(function(e){
		var $this = $(e.target);
		$this.closest( '.ui-state-default' ).find('.section_title_head').html( $this.val());
		formChanged();
	});

	$('input[name="feet"], input[name="inc"]').unbind('click'); 		// cleanup previous binds
	$('input[name="feet"], input[name="inc"]').change(function(e){
		updateFeetNet();
		delayUpdate();
		formChanged();
	});


	// UPDATE PRICES AND VARIABLES FOR WORKSHEET SELECTION
	$('.worksheet-option:not(.blank)').unbind('click'); 		// cleanup previous binds
	$('.worksheet-option:not(.blank)').change(function(e){
// THIS IS FIRING TWICE!!!!!!!!!!!!!!!! REVIEW AND FIX
		e.preventDefault();
		var $this = $(e.target),
			choice_key = $this.val(),
			$paren = $this.closest('.repeater-group'),
			included_amount = $paren.find('.select-variables').data('included-amount-' + choice_key);
			$paren.find('input[name="inc"]').val(included_amount);
		showPrices($paren, choice_key);
		showJobUnits($paren, choice_key);
		delayUpdate();
		formChanged();
	});

}
function toggleTiered( $this, $paren ){
	if ($this.is(':checked')){
		$paren.find(".tiered").show();
		$paren.find(".all-tiers-prices").hide();
		$paren.find('.all-tiers-prices input').val('');
	}
	else{
		$paren.find(".tiered").hide();
		$paren.find('.tiered input').val('');
		$paren.find(".all-tiers-prices").show();
		// clear value 
	}
}
function numberSections(){
	$i = 1;
	$.each( $('#sections > li.ui-state-default'), function(k,v){
		$(v).data('order', $i );
		$(v).find('.section_number').html('Section ' + $i );
		$i++;
	});
}
function addChoice( $parent, $choice ){

	var html = $($("#dropdown_choices_template").html());
	html.find("input[name='choice_title']").val( $choice.title );
	html.find("input[name='included_amount']").val( $choice.variables.included_amount );
	if( parseInt($choice.tiered_pricing ) ){
		var $tieredToggle = html.find("input[name='tiered_pricing']");
		$tieredToggle.prop('checked', true);
		toggleTiered( $tieredToggle, html );
	}
	if( 'object' == typeof $choice.prices ){
		$.each( $choice.prices, function(k,v){
			var $prefix = "";
			if( 'all' != k ){
				$prefix = k + "_";
			}
			$(html).find('input[name="' + $prefix + 'base_price"]').val(v.base); 
			$(html).find('input[name="' + $prefix + 'unit_price"]').val(v.unit); 
		})
		$(html).find('input[name="job_unit"]').val($choice.job_unit); 
	}	
	if( $parent.find("input[name='included_amount_check']").is(':checked') ){
//console.log( 'input');
//console.log(html[0]);
		html.find(".included_amount_input_wrapper").removeClass('inactive-input' );
	}
		
	$parent.find(".choices").append(html);
	$( "#inputs" ).sortable();
	$( "#inputs" ).disableSelection();
	bindEvents();
}
function addDropdown( $parent, $dropdown ){
	var html = $($("#input_dropdown_template").html());
	if( 'object' == typeof $dropdown ){
		if( 0 != $dropdown.title ){
			html.find("input[name='dropdown_title']").val( $dropdown.title );
		}
		if( 0 != $dropdown.quantity ){
			html.find("input[name='quantity']").prop('checked', true);
		}
		if( 0 != $dropdown.cost_included ){
			html.find("input[name='cost_included']").prop('checked', true);
		}
		if( 0 != $dropdown.note ){
			html.find("input[name='note']").prop('checked', true);
		}
		if(0 !=  $dropdown.allow_null ){
			html.find("input[name='allow_null']").prop('checked', true);
		}
		if(0 !=  $dropdown.total_feet ){
			html.find("input[name='total_feet']").prop('checked', true);
		}
		if(0 !=  $dropdown.job_sheet ){
			html.find("input[name='job_sheet']").prop('checked', true);
		}
		if( 0 != $dropdown.included_amount_check ){
			html.find("input[name='included_amount_check']").prop('checked', true);
		}
		if( 0 != $dropdown.make_repeater ){
			html.find("input[name='make_repeater']").prop('checked', true);
		}

		html.find("select[name='price_category']").val($dropdown.price_category);

		if( 'object' == typeof $dropdown.choices ){
			$.each( $dropdown.choices, function(k,v){
				//console.log('v');
				addChoice( $(html) , v );
			})
		}
	}

	$parent.find(".inputs").append(html);
	$parent.find( ".inputs" ).sortable();
	$parent.find( ".inputs" ).disableSelection();
	bindEvents();
	return html;
}
function addBlank( $parent, $blank ){

	var html = $($("#input_blank_template").html());
	if( 'object' == typeof $blank ){
		if( $blank.title ){
			html.find("input[name='blank_title']").val( $blank.title );
		}
		if( $blank.quantity ){
			html.find("input[name='quantity']").prop('checked', true);
		}
		if( $blank.cost_included ){
			html.find("input[name='cost_included']").prop('checked', true);
		}
		if( $blank.note ){
			html.find("input[name='note']").prop('checked', true);
		}
		if( $blank.allow_null ){
			html.find("input[name='allow_null']").prop('checked', true);
		}
		if( $blank.total_feet ){
			html.find("input[name='total_feet']").prop('checked', true);
		}
		if( $blank.job_sheet ){
			html.find("input[name='job_sheet']").prop('checked', true);
		}
		if( $blank.included_amount_check ){
			html.find("input[name='included_amount_check']").prop('checked', true);
		}
		if( $blank.make_repeater ){
			html.find("input[name='make_repeater']").prop('checked', true);
		}

	}

	$parent.find(".inputs").append(html);
	$parent.find( ".inputs" ).sortable();
	$parent.find( ".inputs" ).disableSelection();
	bindEvents();
	return html;
}

function buildSection( $section ){
//console.log('BUILDSECTION:');
//console.log($section);
	var $i = $('#sections > li.ui-state-default').length + 1;	
	var data = {
		section_title : $section
	}
	var html = $($("#section_li_template").html());
	if( 'object' == typeof $section ){
		$(html).find('input#section_title').val( $section.title );
		if( 'object' == typeof $section.inputs ){
			$.each( $section.inputs, function(k,v){
				if( 'dropdown' == v.type ){
					addDropdown( $(html) , v );
				}
				if( 'blank' == v.type ){
					addBlank( $(html) , v );
				}
			})
		}		
	}
	$("#sections").append(html);
	bindEvents();
}
	var menu_offset = $('.worksheet-nav').offset();
	var menu_width = $('.worksheet-nav').width();
	var builder_offset = $('#worksheet-builder').offset();
//console.log('menu_offset');
//console.log(menu_offset);
//alert(1);
	if( builder_offset ){
		$(window).scroll(function () {
		    if ($(window).scrollTop() > builder_offset.top) {
		        $('.worksheet-nav').css('top', 0).css('left',(menu_offset.left - 8)).css('position','fixed').css('padding-left','0');
		        $('#worksheet-builder').addClass('col-md-offset-3');
		    }
		    else{
		        $('.worksheet-nav').css('top', 0).css('left',0).css('position','relative').css('padding-left',0)	;   	
		        $('#worksheet-builder').removeClass('col-md-offset-3');
		    }
		});
	}

window.last_change = timeNow();
function formChanged(){
	$('#formBuilderSubmit').addClass('form-changed');
	$('.form-changes').html(' (Not saved since ' + window.last_change + ')');
}
function timeNow() {
  var d = new Date(),
      h = (d.getHours()<10?'0':'') + d.getHours(),
      m = (d.getMinutes()<10?'0':'') + d.getMinutes();
  return h + ':' + m;
}
function scrollToNewElement(target){
  $('html, body').animate({
        scrollTop: ($(target).offset().top - 20 )
    }, 2000);
}

function buildNav(){
	$('.section_link').unbind('click'); 		// cleanup previous binds
	var $ul = $("<ul>");
	var $home_li = $("<li>");
	var $home_a = $("<a>", {"class": "home_link"}).html('To Top');
	$home_li.append($home_a);
	$ul.append($home_li);
	$.each( $('.section_title_input[name="section_title"]'), function(k,v){	
		var $li = $("<li>");
		var $a = $("<a>", {"class": "section_link"});
		var $sub_ul = $("<ul>");
		var sub_inputs = $(v).closest('.section').find('.inputs li input#dropdown_title');
		$.each(sub_inputs,function(o,p){
			var $sub_li = $("<li>",);
			var $sub_a = $("<a>", {"class": "input_link"});
			if( $(p).val() ){
				var $link = $(p).val();
			}
			else{
				var $link = "No Title";
			}
			$sub_li.append($sub_a.html($link));
			$sub_ul.append($sub_li);
		})
		$li.append($a.html($(v).val()));
		$li.append($sub_ul);
		$ul.append($li);
	//		$(v).val();
	})
	$ul.append($home_li.clone());
	$('.worksheet-nav .nav-wrapper').append($ul);
	bindLinks();
}
function bindLinks(){
	$('.section_link').click(function(e){
		var $this = $(e.target);
		$.each( $('.section_title_input'), function(k,v){
			if( $(v).val() == $this.html()){
				 $('html, body').animate({
			        scrollTop: ($(v).offset().top - 75 )
			    }, 2000);
			}
		} )
	})
	$('.input_link').click(function(e){
		var $this = $(e.target);
		$.each( $('.inputs input'), function(k,v){
			if( $(v).val() == $this.html()){
				 $('html, body').animate({
			        scrollTop: ($(v).offset().top - 5 )
			    }, 2000);
			}
		} )
	})
	$('.home_link').click(function(e){
		 $('html, body').animate({
	        scrollTop: ($('html').offset().top)
	    }, 2000);
	})

}