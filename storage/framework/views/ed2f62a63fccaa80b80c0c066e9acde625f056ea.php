
<style>
    .gen-modal .table > tbody > tr > td {
        font-size: 14px;
        padding: 20px 25px;    
        text-transform: uppercase;
        color: #000;
        font-weight: bold;
    }
    .gen-modal .table > tbody > tr:nth-child(even) > td {
        color: #000 !important;
    }
    .gen-modal .table > tbody > tr > th {
        font-size: 12px;
        font-weight: bold;
        color: #9E9E9E;
        text-transform: uppercase;
        padding: 24px 28px;
    }
    .gen-modal span.icon
    {
        background-position-x: -30px !important;
    }
    .xtra-padding a
    {
        color:#2196F3 !important;
        font-weight: bold;
    }
    .estimate h3
    {
        font-weight: bold;
        color:#000;
    }
    #estimateTable > thead > tr > th {
        border-bottom-width: 1px !important;
        font-size: 13px;
        font-weight: 700;
        color: #9E9E9E;
        text-transform: uppercase;
        font-weight: normal;
    }
    .table-striped > tbody > tr:nth-of-type(odd) {
        background-color: #eee;
    }
    .xtra-padding .close
    {
        color:#2196F3 !important;
    }
    .xtra-padding .close:hover
    {
        color:#2196F3 !important;
    }
    .xtra-padding button.close {
        padding: 0;
        cursor: pointer;
        background: transparent;
        border: 0;
        -webkit-appearance: none;
        opacity: 5 !important;
    }
    .close
    {
        color:#333 !important;
    }
    button.close {
        padding: 0;
        cursor: pointer;
        background: transparent;
        border: 0;
        -webkit-appearance: none;
        opacity: 5 !important;
    }
    .Opt2 .table > tbody > tr > td {
        font-size: 14px;
        padding: 20px 100px;
        text-transform: uppercase;
        color: #000;
        font-weight: bold;
    }
    .Opt1
    {
        box-shadow: 2px 0px 0px #ddd;
    }
    .Opt2 .btn-primary {
        color: #fff;
        background-color: #2196F3;
        border-color: transparent;
        font-weight: 700;
         padding: 11px 7px; 
    }
</style>
<div class="modal fade gen-modal" id="proposalDetailModal">
    <div class="modal-dialog modal-lg" role="document" style="width:1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
<!--                    <svg version="1.1" id="file-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 20" style="enable-background:new 0 0 16 20;" xml:space="preserve">
                        <path d="M10,0H2C0.9,0,0,0.9,0,2l0,16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V6L10,0z M12,16H4v-2h8V16z M12,12H4v-2h8V12z M9,7V1.5L14.5,7H9z"/>
                    </svg>-->
                    <span id="modal-display_id">Opportunity #321815</span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php echo e(Form::open(	array('class'=>'form','id'=>'proposal_status', 'url'=>'/proposal-save', 'method' => 'post'))); ?>

                    <div class="col-lg-6 Opt1">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Customer</th>
                                    <td id="modal-customer">Ronny Bartkowiak</td>
                                </tr>
                                <tr>
                                    <th>Team</th>
                                    <td id="modal-rep">
                                        Rene Neal
                                    </td>
                                </tr>
                                <tr>
                                    <th>Expiration</th>
                                    <td id="modal-expiration">01/01/2018</td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td id="modal-address">0000 S. Address Name Ave. Tucson, AZ 85701</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6 Opt2">
                        <table class="table">
                            <tbody>

                                <tr>
                                   
                                    <td id="modal-leadgen" colspan="2">
                                        Lead Generator
                                        <?php
                                        $leadgens = App\User::where('leadgen', '=', 1)->get();
                                        $leadgens_select = [];

                                        foreach ($leadgens as $lead) {
                                            $leadgens_select[$lead->id] = $lead->first_name . " " . $lead->last_name;
                                        }
                                        ?>
                                        <?php echo e(Form::select('leadgen_id',$leadgens_select, '', ['id'=>'prop_leadgen', 'class'=>'form-control','placeholder'=>'Choose a Generator'])); ?>

                                    </td>
                                </tr>
                                <tr>
                                    <div class="rm-group <?php if($errors->has('status')): ?> has-error <?php endif; ?>">                                      
                                            <td colspan="2">Status	<?php echo e(Form::hidden('included_estimates','', ['id'=>'included_estimates'])); ?>


                                                <?php echo e(Form::hidden('proposal_id','', ['class'=>'form-control proposal-id'])); ?>

                                                <?php echo e(Form::select('status',array('pending'=>'Pending','draft'=>'Draft','sold'=>'Sold','lost'=>'Lost'), '', ['id'=>'prop_status', 'class'=>'form-control'])); ?>

                                            </td>
                                            <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

                                            <?php if($errors->has('status')): ?>
                                            <span class="status error">
                                                <?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
                                            </span>
                                            <?php else: ?>
                                            <span class="status success">
                                                <?php echo embed_svg('icon', 'img/check-icon.svg', '20px', '20px'); ?>
                                            </span>
                                            <?php endif; ?>
                                    </div>

                                    <tr>
                                        <tr>  
                                            <td id="modal-lead-source" colspan="2">
                                                Lead Source
                                                <?php
                                                $sources = App\Source::get();
                                                $source_select = [];

                                                foreach ($sources as $source) {
                                                    $source_select[$source->id] = $source->name;
                                                }
                                                ?>
                                                <?php echo e(Form::select('source_id',$source_select, '', ['id'=>'prop_source', 'class'=>'form-control','placeholder'=>'Lead Source'])); ?>

                                            </td>
                                        </tr>    
                                        <tr>
                                            <td></td>
                                            <td><a class="btn-submit btn btn-primary btn-block" id="save_proposal_info" onclick="saveProposalInfo(event);">Save Proposal Info</a></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        <script type="text/javascript">
                                            function saveProposalInfo(e) {
                                                e.preventDefault();
                                                var chosen_estimates = [];
                                                $('[data-estimate-id]').each(function (k, v) {
                                                    if ($(v).is(':checked')) {
                                                        chosen_estimates.push($(v).data('estimate-id'));
                                                    }
                                                });
                                                console.log('this');
                                                console.log(JSON.stringify(chosen_estimates));
                                                $('#included_estimates').val(JSON.stringify(chosen_estimates));
                                                $('#proposal_status').submit();
                                            }
                                        </script>
                                        </div>
                                        <?php echo e(Form::close()); ?>

                                        <div class="col-lg-12 xtra-padding">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <a class="link" href="javascript:window.print();"><span class="icon print"></span>Print</a>
                                                </div>	
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <a class="link" href="#x"><span class="icon clone"></span>Clone</a>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <a class="link" href="#x"><span class="icon invoice"></span>Invoice</a>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <?php echo e(Form::open(array('class'=>'form','url'=>'/opportunity-delete', 'method' => 'post', 'files'=>true))); ?>

                                                    <?php echo e(Form::hidden('proposal_id','', ['class'=>'form-control', 'id'=>'modal-proposal-id'])); ?>

                                                    <button type="submit" class="close link"  aria-label="Close" style="float:left;"><span aria-hidden="true">&times;</span><span style="text-align:top; margin-left:7px; font-family: 'Noto Sans', sans-serif; font-size: 14px; line-height: 1.6;"> Delete</span></button>
                                                    <?php echo e(Form::close()); ?>

                                                </div>
                                            </div>
                                            <hr style="margin-bottom:15px;">
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 estimate">
                                                        <h3>Estimates</h3>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
<?php
$forms = array('Split' => 'split', 'Package' => 'package', 'Furnace' => 'furnace');
$ws_select_options = [];
foreach ($forms as $type_name => $form_type) {
    if (isset($group)) {
        $form_worksheet_model = App\Worksheet::firstOrNew(array('group_id' => $group->id, 'type' => $form_type));
        $form_worksheet_object = json_decode($form_worksheet_model->worksheet_object);
        if (is_object($form_worksheet_object)) {
            $ws_select_options[$form_type] = $type_name;
        }
    }
}
?>
                                                        <?php echo e(Form::select('new_estimate', $ws_select_options,'', ['id'=>'new_estimate','placeholder'=>'Start a New Estimate','class'=>'form-control'])); ?>

                                                    </div>
                                                </div>
                                                <table id="estimateTable" class="table table-striped not-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Est #</th>
                                                            <th>Included</th>
                                                            <th>Plan</th>
                                                            <th>Amount</th>
                                                            <th>Edit</th>
                                                            <th>Present</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
