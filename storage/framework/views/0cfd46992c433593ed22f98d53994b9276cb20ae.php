
<?php $group = App\Group::find( $user->current_group_id ); ?>
<style>
   #optionModal .modal-body .thumbnail .img-wrap {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    display: block;
    margin: 0 auto 21px;
    line-height: 88px;
    text-align: center;
    padding-top: 3px !important;
}
#optionModal .modal-dialog {
    margin-top: 10% !important;
    background-color: #fff;
    padding: 30px;
    border: 1px solid #ddd;
    width: 91%;
}
</style>
<div class="modal fade" id="optionModal">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
	  		<div class="modal-body">
		  		<div class="row">
		  			<div class="col-sm-4 col-xs-12">
		  				<?php 
							$worksheet_model = App\Worksheet::firstOrNew( array('group_id' => $group->id, 'type' => 'package')); 
							$worksheet_object = json_decode($worksheet_model->worksheet_object);
						?>
						<?php if(is_object($worksheet_object)) : ?>
						<a href="<?php echo e(url('/package?step=customer')); ?>">
						<?php endif; ?>
						<div class="thumbnail blue">
		  					<div class="img-wrap">
								 <img src="<?php echo e(URL::to('img/home-icon-1.png')); ?>" style="width:56px;height: 56px;"> 
		  					</div>
							<div class="caption">
								<h3>Package Unit</h3>
								
								<?php if(is_object($worksheet_object)) : ?>
									
										<?php echo embed_svg('icon', 'img/arrow-right.svg', '16px', '16px'); ?>
									
								<?php else : ?>
									<p>You haven't built a Package Worksheet yet.</p>
									<a href="<?php echo e(url('/edit-package')); ?>">
										Build one now
									</a>
								<?php endif; ?>
							</div>
		  				</div>
						<?php if(is_object($worksheet_object)) : ?>
						</a>
						<?php endif; ?>
		  			</div>
		  			<div class="col-sm-4 col-xs-12">
						<?php 
							$worksheet_model = App\Worksheet::firstOrNew( array('group_id' => $group->id, 'type' => 'split')); 
							$worksheet_object = json_decode($worksheet_model->worksheet_object);
						?>
						<?php if(is_object($worksheet_object)) : ?>
						<a href="<?php echo e(url('/split?step=customer')); ?>">
						<?php endif; ?>
		  				<div class="thumbnail purple">
		  					<div class="img-wrap">
								 <img src="<?php echo e(URL::to('img/home-icon-2.png')); ?>" style="width:56px;height: 56px;"> 
							</div>
							<div class="caption">
								<h3>Split System</h3>
								
								<?php if(is_object($worksheet_object)) : ?>
									
										<?php echo embed_svg('icon', 'img/arrow-right.svg', '16px', '16px'); ?>
									
								<?php else : ?>
									<p>You haven't built a Split System Worksheet yet.</p>
									<a href="<?php echo e(url('/edit-split')); ?>">
										Build one now
									</a>
								<?php endif; ?>
							</div>
		  				</div>
						<?php if(is_object($worksheet_object)) : ?>
						</a>
						<?php endif; ?>
		  			</div>
		  			<div class="col-sm-4 col-xs-12">
		  				<?php 
							$worksheet_model = App\Worksheet::firstOrNew( array('group_id' => $group->id, 'type' => 'furnace')); 
							$worksheet_object = json_decode($worksheet_model->worksheet_object);
						?>
						<?php if(is_object($worksheet_object)) : ?>
						<a href="<?php echo e(url('/furnace?step=customer')); ?>">
						<?php endif; ?>
						<div class="thumbnail orange">
		  					<div class="img-wrap">
								 <img src="<?php echo e(URL::to('img/home-icon-3.png')); ?>" style="width:56px;height: 56px;"> 
							</div>
							<div class="caption">
								<h3>Furnace</h3>
								
								<?php if(is_object($worksheet_object)) : ?>
									
										<?php echo embed_svg('icon', 'img/arrow-right.svg', '16px', '16px'); ?>
									
								<?php else : ?>
									<p>You haven't built a Furnace Worksheet yet.</p>
									<a href="<?php echo e(url('/edit-furnace')); ?>">
										Build one now
									</a>
								<?php endif; ?>
							</div>
		  				</div>
						<?php if(is_object($worksheet_object)) : ?>
						</a>
						<?php endif; ?>
		  			</div>
		  		</div>
      		</div>
		</div>
  	</div>
</div>