var elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
	mix.styles([
		'datatables/jquery.dataTables.min.css',
		'datatables/dataTables.bootstrap.min.css',
		'datatables/responsive.bootstrap.min.css',
		//'jsgrid.min.css',  //added for incomplete note repeater feature on estimate view
		//'jsgrid-theme.min.css'
	], 'public/css/plugins.css')
	.scripts([
		'datatables/jquery.dataTables.min.js',
		'datatables/dataTables.bootstrap.min.js',
		'datatables/dataTables.responsive.min.js',
		'datatables/responsive.bootstrap.min.js',
		//'jsgrid.min.js'
	], 'public/js/plugins.js')
    .sass('app.scss')
    .webpack('app.js');
});
