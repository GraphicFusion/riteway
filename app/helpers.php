<?php
use App\Coil;
use App\Condenser;
use App\Source;
use App\Furnace;
use App\System;
use App\Package;
use App\Thermostat;
use App\Proposal;
/**
 *	print_r
 */
function pr($x) {
	echo "<pre>";
	print_r($x);
	echo "</pre>";
}
function getAdminEmails(){
	return [
		'wjgrass@gmail.com',
		'mauman@ritewayac.com',
		'csundin@ritewayac.com',
		'marketing@ritewayac.com',
		'rwalter@ritewayac.com',
		'rdorman@ritewayac.com'
	];
}
// could build on this in th future to provide different options for customers
function getPlanTiers(){
	return [
		'platinum',
		'gold',
		'silver',
		'bronze'
	];
}
function getCoreEmails(){
	return [
		'vwalter@ritewayac.com',
		'agarcia@ritewayac.com',
		'dwyatt@ritewayac.com',
		'cwalter@ritewayac.com'
	];
}
function getWarehouseEmails(){
	return [
		'jsundin@ritewayac.com',
		'smainville@ritewayac.com',
		'rgunn@ritewayac.com',
		'lklasen@ritewayac.com',
		'lrogan@ritewayac.com'
	];

}
function getUser( $id = null ){
	if( !$id ){
		$user = Auth::user();
	}
	else{
		$user = App\User::find($id);
	}
	return $user;
}
//echo getAll('coils');
function getAll( $model, $ids = null ){
	return call_user_func('get' . ucfirst($model), $ids );
}
function getCoils($ids){
//	$coils = App\Group::all();
	//find( $user->current_group_id );
}
function buildAjaxProposal( $model ){
	// SHOW IN DOCUMENTATIONS AVAILABLE PROPERTIES
	$obj = new stdClass();
	$obj->id = $model->id;
	$obj->status = $model->status;
	$obj->active = $model->active;
	$obj->sold_date = $model->sold_date;
	$obj->leadgen_id = $model->leadgen_id;
	$obj->status = $model->status;
	$obj->start = date('m/d/Y',strtotime($model->created_at ));
	$obj->expiration =  date('m/d/Y',strtotime($model->created_at ) + 30*60*60*24);
	$obj->included_estimates = $model->included_estimates;
	if( !is_array($obj->included_estimates)){
		$obj->included_estimates = json_encode( [] );
	}
	$obj->estimates = [];
	foreach( $model->estimates as $est ){
		$estimateArr = [];
		$estimateArr['id'] = $est->id;
		$estimateArr['type'] = $est->type;
		$estimateArr['chosen_plan'] = $est->chosen_plan;
		$estimateArr['presentation_prices'] = json_decode($est->presentation_prices);
		$obj->estimates[] = $estimateArr;
	}
	//get customer
	$obj->customer = new stdClass();
		$obj->customer->name = $model->customer->first_name . " " . $model->customer->last_name;
		$obj->customer->first_name = $model->customer->first_name;
		$obj->customer->last_name = $model->customer->last_name;
		$obj->customer->email = $model->customer->email;
		$obj->customer->phone = $model->customer->phone;

	//get lead gen
	$obj->rep = new stdClass();
		$obj->rep->first_name = $model->rep->first_name;
		$obj->rep->last_name = $model->rep->last_name;
		$obj->rep->email = $model->rep->email;
		$obj->rep->phone = $model->rep->phone;
			
	// get lead source
	$obj->source = "";
	if( $model->source_id ){
		$source = App\Source::find( $model->source_id );
		$obj->source_id = $model->source_id;
		$obj->store = $source->store;
	}

	$location = $model->location;
		$obj->location = new stdClass();
		$obj->location->address = $location->address;
		$obj->location->unit = $location->unit;
		$obj->location->city = $location->city;
		$obj->location->state = $location->state;
		$obj->location->zipcode = $location->zipcode;
		$obj->address = $location->address. " " . $location->city . "," . $location->state . " " . $location->zip; 
	return $obj;
}
function buildAjaxCustomer( $model ){
	$obj = new stdClass();
	$obj->id = $model->id;
	$attrs = $model->toArray();
	foreach($attrs as $attr => $val){
		$obj->$attr = $val;
	}
	return $obj;
}
function buildAjaxCoil( $model ){
	$obj = new stdClass();
	$obj->id = $model->id;
	$attrs = $model->toArray();
	foreach($attrs as $attr => $val){
		$obj->$attr = $val;
	}

	return $obj;
}
function buildAjaxCondenser( $model ){
	$obj = new stdClass();
	$obj->id = $model->id;
	$attrs = $model->toArray();
	foreach($attrs as $attr => $val){
		$obj->$attr = $val;
	}

	return $obj;
}
function buildAjaxFurnace( $model ){
	$obj = new stdClass();
	$obj->id = $model->id;
	$attrs = $model->toArray();
	foreach($attrs as $attr => $val){
		$obj->$attr = $val;
	}	
	return $obj;
}		
function buildAjaxSystem( $model ){
	if($model->id == 14){
	//	print_r($model);
	}
	
	$obj = new stdClass();
	$obj->id = $model->id;
	$obj->brand = $model->brand;
	$obj->model_number = $model->model_number;

	$obj->parts_warranty = $model->parts_warranty;
	$obj->labor_warranty = $model->labor_warranty;
	$obj->heat_exchanger_warranty = $model->heat_exchanger_warranty;
	$obj->compressor_warranty = $model->compressor_warranty;
	$obj->seer = $model->seer;
	$obj->eer = $model->eer;
	$obj->hspf = $model->hspf;
	$obj->image = $model->image;
	$obj->afue = $model->afue;
	$obj->price = $model->price;
//W	echo "STAGES: ".$model->stages;
	if($model->stages){
//		echo "exists";
			$val = $model->stages;
			if(1 == $val ){
				$val = "Enhanced";				
			}
			if(2 == $val ){
				$val = "Limited";				
			}
			if(3 == $val ){
				$val = "Standard";				
			}

		$obj->stages_of_heating = $val;
	}
	else{
//		echo "nooooooooooooope:";
		$obj->stages_of_heating = $model->stages_of_heating;
	}
//		print_r($model);
	$obj->stages_of_cooling = $model->stages_of_cooling;
	$obj->sensible_cooling = $model->sensible_cooling;
	$obj->tax_credit = $model->tax_credit;
	$obj->energy_star = $model->energy_star;
	$obj->utility_rebate = $model->utility_rebate;
	$obj->ahri = $model->ahri;
	$obj->rw_system_id = $model->rw_system_id;
	$obj->name = $model->name;
	$obj->function = "buildAjaxSystem";
//shift();
	//get thermostat
	if( $model->thermostat_id ){
		$thermostat_obj = Thermostat::find($model->thermostat_id);
		$obj->thermostat = buildAjaxThermostat($thermostat_obj);
	}
	if( $model->coil_id ){
		$coil_obj = Coil::find($model->coil_id);
		$obj->coil = buildAjaxCoil($coil_obj);
	}
	if( $model->condenser_id ){
		$condenser_obj = Condenser::find($model->condenser_id);
		$obj->condenser = buildAjaxCondenser($condenser_obj);
		$obj->stages_of_cooling = $condenser_obj->stages;
	}	
	if( $model->furnace_id ){
		$furnace_obj = Furnace::find($model->furnace_id);
		$obj->furnace = buildAjaxFurnace($furnace_obj);
		$obj->brand = $furnace_obj->brand;
	}

	return $obj;
}
function buildAjaxThermostat( $model ){
	$obj = new stdClass();
	//print_r($model);
	$obj->id = $model->id;
	$attrs = $model->toArray();
	foreach($attrs as $attr => $val){
		$obj->$attr = $val;
	}
	return $obj;
}
function buildAjaxPackage( $model ){
	$obj = new stdClass();
	$obj->id = $model->id;
	$attrs = $model->toArray();
	foreach($attrs as $attr => $val){
		$obj->$attr = $val;
	}
	return $obj;
}
function buildAjaxEstimate( $model ){

		$obj = new stdClass();
		$obj->id = $model->id;
		$obj->type = $model->type;
		$obj->proposal_id = $model->proposal_id;
			$proposal = Proposal::find($model->proposal_id);
		$obj->proposal = buildAjaxProposal($proposal);

		$obj->updated_at = $model->updated_at;
		$obj->worksheet_data = $model->worksheet_data;
		$obj->worksheet_id = $model->worksheet_id;
		$obj->worksheet_version = $model->worksheet_version;
		$obj->platinum = $model->platinum;
		$obj->gold = $model->gold;
		$obj->silver = $model->silver;
		$obj->bronze = $model->bronze;
		$obj->platinum_system_id = $model->platinum_system_id;
		$obj->gold_system_id = $model->gold_system_id;
		$obj->silver_system_id = $model->silver_system_id;
		$obj->bronze_system_id = $model->bronze_system_id;
		$obj->chosen = $model->chosen;
		$obj->chosen_plan = $model->chosen_plan;
		// get type
		if( 'split' == $model->type ){
			if($model->platinum_system_id ){
				$platinum_system = System::find($model->platinum_system_id);
				$obj->platinum_system = buildAjaxSystem($platinum_system);
			}
			if($model->gold_system_id ){
				$gold_system = System::find($model->gold_system_id);
				$obj->gold_system = buildAjaxSystem($gold_system);
			}
			if($model->silver_system_id ){
				$silver_system = System::find($model->silver_system_id);
				$obj->silver_system = buildAjaxSystem($silver_system);
			}
			if($model->bronze_system_id ){
				$bronze_system = System::find($model->bronze_system_id);
				$obj->bronze_system = buildAjaxSystem($bronze_system);
			}
		}
		if( 'package' == $model->type ){
			if($model->platinum_system_id ){
				$platinum_system = Package::find($model->platinum_system_id);
				$obj->platinum_system = buildAjaxPackage($platinum_system);
			}

			if($model->gold_system_id ){
				$gold_system = Package::find($model->gold_system_id);
				$obj->gold_system = buildAjaxPackage($gold_system);
			}
			if($model->silver_system_id ){
				$silver_system = Package::find($model->silver_system_id);
				$obj->silver_system = buildAjaxPackage($silver_system);
			}
			if($model->bronze_system_id ){
				$bronze_system = Package::find($model->bronze_system_id);
				$obj->bronze_system = buildAjaxPackage($bronze_system);
			}
		}
		if( 'furnace' == $model->type ){
			if($model->platinum_system_id ){
				$platinum_system = Furnace::find($model->platinum_system_id);
				$obj->platinum_system = buildAjaxFurnace($platinum_system);
			}
			if($model->gold_system_id ){
				$gold_system = Furnace::find($model->gold_system_id);
				$obj->gold_system = buildAjaxFurnace($gold_system);
			}
			if($model->silver_system_id ){
				$silver_system = Furnace::find($model->silver_system_id);
				$obj->silver_system = buildAjaxFurnace($silver_system);
			}
			if($model->bronze_system_id ){
				$bronze_system = Furnace::find($model->bronze_system_id);
				$obj->bronze_system = buildAjaxFurnace($bronze_system);
			}
		}
		$obj->platinum_thermostat_id = $model->platinum_thermostat_id;
			$platinum_thermostat = Thermostat::find($model->platinum_thermostat_id);
			$obj->platinum_thermostat = buildAjaxThermostat($platinum_thermostat);

		$obj->gold_thermostat_id = $model->gold_thermostat_id;
			$gold_thermostat = Thermostat::find($model->gold_thermostat_id);
			$obj->gold_thermostat = buildAjaxThermostat($gold_thermostat);

		$obj->silver_thermostat_id = $model->silver_thermostat_id;
			$silver_thermostat = Thermostat::find($model->silver_thermostat_id);
			$obj->silver_thermostat = buildAjaxThermostat($silver_thermostat);

		$obj->bronze_thermostat_id = $model->bronze_thermostat_id;
			$bronze_thermostat = Thermostat::find($model->bronze_thermostat_id);
			$obj->bronze_thermostat = buildAjaxThermostat($bronze_thermostat);

		$obj->presentation = $model->presentation;
		$obj->unit_available_width = $model->unit_available_width;
		$obj->coil_available_width = $model->coil_available_width;
		$obj->available_height = $model->available_height;
		$obj->ac_hp = $model->ac_hp;
		$obj->new_replace = $model->new_replace;
		$obj->presentation_prices = $model->presentation_prices;
		$obj->platinum_box = $model->platinum_box;
		$obj->gold_box = $model->gold_box;
		$obj->silver_box = $model->silver_box;
		$obj->bronze_box = $model->bronze_box;
		$obj->job_labor_total = $model->job_labor_total;
		$obj->notes = $model->notes;
		$obj->job_sheet = $model->job_sheet;
		$obj->capacity = $model->capacity;
		$obj->includes = $model->includes;
	return $obj;
}
function getPayments(){
	return array(
		"3" => "None",
		"0" => "Cash/Check",
		"1" => "Credit Card",
		"2" => "Financing (OAC)"
	);
}
function getPaymentMethods(){
	//maintain existing keys!!!!!!!!!
	return array(
		"7" => "None",
		"0" => "50% Down, Balance Due On Equipment Startup",
		'1' => "Full Balance On Equipment Startup (Manager Approval Required To Be Valid)",
		'2' => "In-house Financing (Manager Approval Required To Be Valid)",
		'3' => 'Downpayment With Financing OAC',
		'4' => 'Full Balance By Financing OAC',
		'5' => 'Paid in Full',
		'6' => 'Costco Member Credit'		
	);
}
function getFinancePlans(){
	return array(
		"None" =>
			array(
				"ID" => "None",
				"title" => "None", 
				"Description" => "None", 
				"Duration" => "", 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => ""), 
		"Costco 48 Month" =>
			array(
				"ID" => "Costco 48 Month",
				"title" => "Costco 48M 2.99%", 
				"Description" => "Costco 48 Month 2.99%", 
				"Duration" => 48, 
				"APR" => "2.99%", 
				"Payment_Factor" => "2.213%", 
				"Dealer_Fee" => "0%"),
		"Costco 18 Month" =>
			array(
				"ID" => "Costco 18 Month",
				"title" => "Costco 18M 0%", 
				"Description" => "Costco 18 Month 0%", 
				"Duration" => 18, 
				"APR" => "0%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "0%"), 
		"Promo3072" =>
			array(
				"ID" => "Promo3072",
				"title" => "SF Promo3072 0% 72M",
				"Description" => "Promo 72M",
				"Duration" => 72,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "10.0%"),
		"Promo3060" =>
			array(
				"ID" => "Promo3060",
				"title" => "SF Promo3060 0% 60M",
				"Description" => "Promo 60M",
				"Duration" => 60,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "8%"),
		"Promo3036" =>
			array(
				"ID" => "Promo3036",
				"title" => "SF Promo3036 0% 36M",
				"Description" => "Promo 36M",
				"Duration" => 36,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "6%"),
		"Promo1018" =>
			array(
				"ID" => "Promo1018",
				"title" => "SF Promo1018 0% 18M",
				"Description" => "Promo 18M",
				"Duration" => 18,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "2%"),
		"Promo2012" =>
			array(
				"ID" => "Promo2012",
				"title" => "SF Promo2012 0% 12M",
				"Description" => "Promo 12M",
				"Duration" => 12,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "2.00%"),
		
		"Promo3315" =>
			array(
				"ID" => "Promo3315",
				"title" => "WF Promo3315 0% 72M",
				"Description" => "Promo 72M",
				"Duration" => 72,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "10%"),
		"Promo3314" =>
			array(
				"ID" => "Promo3314",
				"title" => "WF Promo3314 0% 60M",
				"Description" => "Promo 60M",
				"Duration" => 60,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "8%"),
		"Promo3313" =>
			array(
				"ID" => "Promo3313",
				"title" => "WF Promo3313 0% 24M",
				"Description" => "Promo 24M",
				"Duration" => 24,
				"APR" => "",
				"Payment_Factor" => "",
				"Dealer_Fee" => "5%"),
		"SF1006" =>
			array(
				"ID" => "SF1006", 
				"title" => "SF 6M DI", 
				"Description" => "6 Months Deferred Interest with Minimum Monthly Payments", 
				"Duration" => 6, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "4%"), 
		"SF1012" =>
			array(
				"ID" => "SF1012",
 				"title" => "SF 12M DI",					
				"Description" => "12 Months Deferred Interest with Minimum Monthly Payments", 
				"Duration" => 12, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "6%"), 
		"SF11018" =>
			array(
				"ID" => "SF1018",
				"title" => "SF 18M DI", 
				"Description" => "18 Months Deferred Interest with Minimum Monthly Payments", 
				"Duration" => 18, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "7%"), 
		"SF1024" =>
			array(
				"ID" => "SF1024",
				"title" => "SF 24M DI", 
				"Description" => "24 Months Deferred Interest with Minimum Monthly Payments", 
				"Duration" => 24, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "9%"), 
		"SF2003" =>
			array(
				"ID" => "SF2003",
				"title" => "SF 0% 3M SAC", 
				"Description" => "3 Months Zero Interest/No Monthly Payments - Same As Cash", 
				"Duration" => 3, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "3%"), 
		"SF2006" =>
			array(
				"ID" => "SF2006", 
				"title" => "SF 0% 6M SAC",
				"Description" => "6 Months Zero Interest/No Monthly Payments - Same As Cash", 
				"Duration" => 6, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "5%"), 
		"SF2012" =>
			array(
				"ID" => "SF2012",
				"title" => "SF 0% 12M SAC", 
				"Description" => "12 Months Zero Interest/No Monthly Payments - Same As Cash", 
				"Duration" => 12, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "8%"), 
		"SF2018" => 
			array(
				"ID" => "SF2018",
				"title" => "SF 0% 18M SAC", 
				"Description" => "18 Months Zero Interest/No Monthly Payments - Same As Cash", 
				"Duration" => 18, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "11%"), 
		"SF3025" => 
			array(
				"ID" => "SF3025", 
				"title" => "SF 0% 25M EP",
				"Description" => "25 Months Zero Interest With Equal Monthly Payments", 
				"Duration" => 25, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "9%"), 
		"SF3036" => 
			array(
				"ID" => "SF3036", 
				"Description" => "36 Months Zero Interest With Equal Monthly Payments", 
				"title" => "SF 0% 36M EP", 
				"Duration" => 36, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "14%"), 
		"SF3048" => 
			array(
				"ID" => "SF3048", 
				"Description" => "48 Months Zero Interest With Equal Monthly Payments",
				"title" => "SF 0% 48M EP", 
				"Duration" => 48, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "15%"), 
		"SF3060" => 
			array(
				"ID" => "SF3060", 
				"Description" => "60 Months Zero Interest With Equal Monthly Payments", 
				"title" => "SF 0% 60M EP",
				"Duration" => 60, 
				"APR" => "", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "15%"), 
		"SF4198" => 
			array(
				"ID" => "SF4198", 
				"Description" => "60 Months Reduced Interest Loans - Short Term", 
				"title" => "SF 6.99% 60M",
				"Duration" => 60, 
				"APR" => "6.99%", 
				"Payment_Factor" => "1.98%", 
				"Dealer_Fee" => "7%"), 
		"SF4202" => 
			array(
				"ID" => "SF4202", 
				"Description" => "60 Months Reduced Interest Loans - Short Term",
				"title" => "SF 7.99% 60M", 
				"Duration" => 60, 
				"APR" => "7.99%", 
				"Payment_Factor" => "2.02%", 
				"Dealer_Fee" => "6%"), 
		"SF4165" => 
			array(
				"ID" => "SF4165",
				"title" => "SF 9.99% 85M", 
				"Description" => "85 Months Reduced Interest Loans - Short Term", 
				"Duration" => 85, 
				"APR" => "9.99%", 
				"Payment_Factor" => "1.65%", 
				"Dealer_Fee" => "5%"), 
		"SF4150" => 
			array(
				"ID" => "SF4150",
				"title" => "SF 9.99% 97M", 
				"Description" => "97 Months Reduced Interest Loans - Short Term", 
				"Duration" => 97, 
				"APR" => "9.99%", 
				"Payment_Factor" => "1.50%", 
				"Dealer_Fee" => "6%"), 
		"SF4011" => 
			array(
				"ID" => "SF4011", 
				"title" => "SF 11.99% 84M",
				"Description" => "84 Months Reduced Interest Loans - Short Term", 
				"Duration" => 84, 
				"APR" => "11.99%", 
				"Payment_Factor" => "1.76%", 
				"Dealer_Fee" => "2.00%"), 
		"SF4012" => 
			array(
				"ID" => "SF4012",
				"title" => "SF 12.99% 84M", 
				"Description" => "84 Months Reduced Interest Loans - Short Term", 
				"Duration" => 84, 
				"APR" => "12.99%", 
				"Payment_Factor" => "1.81%", 
				"Dealer_Fee" => "0.00%"), 
		"SF4082" => 
			array(
				"ID" => "SF4082", 
				"title" => "SF 2.99% 144M",
				"Description" => "144 Months Reduced Interest Loans - Long Term", 
				"Duration" => 144, 
				"APR" => "2.99%", 
				"Payment_Factor" => "0.82%", 
				"Dealer_Fee" => "16.00%"), 
		"SF4092" => 
			array(
				"ID" => "SF4092",
				"title" => "SF 4.99% 144M", 
				"Description" => "144 Months Reduced Interest Loans - Long Term", 
				"Duration" => 144, 
				"APR" => "4.99%", 
				"Payment_Factor" => "0.92%", 
				"Dealer_Fee" => "11%"), 
		"SF4071" => 
			array(
				"ID" => "SF4071",
				"title" => "SF 5.99% 240M", 
				"Description" => "240 Months Reduced Interest Loans - Long Term", 
				"Duration" => 240, 
				"APR" => "5.99%", 
				"Payment_Factor" => "0.71%", 
				"Dealer_Fee" => "14.00%"), 
		"SF4116" => 
			array(
				"ID" => "SF4116",
				"title" => "SF 6.99% 120M", 
				"Description" => "120 Months Reduced Interest Loans - Long Term", 
				"Duration" => 120, 
				"APR" => "6.99%", 
				"Payment_Factor" => "1.16%", 
				"Dealer_Fee" => "8%"), 
		"SF4102" => 
			array(
				"ID" => "SF4102",
				"title" => "SF 6.99% 144M", 
				"Description" => "144 Months Reduced Interest Loans - Long Term", 
				"Duration" => 144, 
				"APR" => "6.99%", 
				"Payment_Factor" => "1.02%", 
				"Dealer_Fee" => "9%"), 
		"SF4089" => 
			array(
				"ID" => "SF4089",
				"title" => "SF 6.99% 180M", 
				"Description" => "180 Months Reduced Interest Loans - Long Term", 
				"Duration" => 180, 
				"APR" => "6.99%", 
				"Payment_Factor" => "0.89%", 
				"Dealer_Fee" => "10%"), 
		"SF4121" => 
			array(
				"ID" => "SF4121",
				"title" => "SF 7.99% 120M", 
				"Description" => "120 Months Reduced Interest Loans - Long Term", 
				"Duration" => 120, 
				"APR" => "7.99%", 
				"Payment_Factor" => "1.21%", 
				"Dealer_Fee" => "7%"), 
		"SF4096" => 
			array(
				"ID" => "SF4096",
				"title" => "SF 7.99% 180M", 
				"Description" => "180 Months Reduced Interest Loans - Long Term", 
				"Duration" => 180, 
				"APR" => "7.99%", 
				"Payment_Factor" => "0.96%", 
				"Dealer_Fee" => "9%"), 
		"SF4132" => 
			array(
				"ID" => "SF4132", 
				"Description" => "120 Months Reduced Interest Loans - Long Term",
				"title" => "SF 9.99% 120M", 
				"Duration" => 120, 
				"APR" => "9.99%", 
				"Payment_Factor" => "1.32%", 
				"Dealer_Fee" => "3%"), 
		"WF2228" => 
			array(
				"ID" => "WF2228",
				"title" => "WF 9.99% 1.9%PF", 
				"Description" => "Special Rate With Custom Monthly Payments - 9.99%", 
				"Duration" => "", 
				"APR" => "9.99%", 
				"Payment_Factor" => "1.90%", 
				"Dealer_Fee" => "5%"), 
		"WF1275" => 
			array(
				"ID" => "WF1275",
				"title" => "WF 7.9% 1.75%PF", 
				"Description" => "Special Rate With Custom Monthly Payments - 7.90%", 
				"Duration" => "", 
				"APR" => "7.90%", 
				"Payment_Factor" => "1.75%", 
				"Dealer_Fee" => "7%"), 
		"WF1276" => 
			array(
				"ID" => "WF1276",
				"title" => "WF 5.9% 1.75% PF", 
				"Description" => "Special Rate With Custom Monthly Payments - 5.90%", 
				"Duration" => "", 
				"APR" => "5.90%", 
				"Payment_Factor" => "1.75%", 
				"Dealer_Fee" => "8%"), 
		"WF1890" => 
			array(
				"ID" => "WF1890",
				"title" => "WF 3.9% 1.75%PF", 
				"Description" => "Special Rate With Custom Monthly Payments - 3.90%", 
				"Duration" => "", 
				"APR" => "3.90%", 
				"Payment_Factor" => "1.75%", 
				"Dealer_Fee" => "12%"), 
		"WF4079" => 
			array(
				"ID" => "WF4079",
				"title" => "WF 0% 24M EP", 
				"Description" => "24 Months Special Rate With Equal Monthly Payments", 
				"Duration" => 24, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "10%"), 
		"WF4091" => 
			array(
				"ID" => "WF4091",
				"title" => "WF 0% 36M EP",
				"Description" => "36 Months Special Rate With Equal Monthly Payments", 
				"Duration" => 36, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "12%"), 
		"WF4103" => 
			array(
				"ID" => "WF4103", 
				"title" => "WF 0% 48M EP",
				"Description" => "48 Months Special Rate With Equal Monthly Payments", 
				"Duration" => 48, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "14%"), 
		"WF4115" => 
			array(
				"ID" => "WF4115",
				"title" => "WF 0% 60M EP", 
				"Description" => "60 Months Special Rate With Equal Monthly Payments", 
				"Duration" => 60, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "16%"), 
		"WF1148" => 
			array(
				"ID" => "WF1148",
				"title" => "WF 0% 6M WP", 
				"Description" => "6 Months No Interest If Paid In Full With Regular Monthly Payments", 
				"Duration" => 6, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "3%"), 
		"WF1019" => 
			array(
				"ID" => "WF1019",
				"title" => "WF 0% 12M WP", 
				"Description" => "12 Months No Interest If Paid In Full With Regular Monthly Payments", 
				"Duration" => 12, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "4%"), 
		"WF1066" => 
			array(
				"ID" => "WF1066",
				"title" => "WF 0% 18M WP", 
				"Description" => "18 Months No Interest If Paid In Full With Regular Monthly Payments", 
				"Duration" => 18, 
				"APR" => "0.00%", 
				"Payment_Factor" => "", 
				"Dealer_Fee" => "7%"),
		"Promo3408" =>
			array(
				"ID" => "Promo3408",
				"title" => "WF Promo3408 0% 24M",
				"Description" => "Promo 24M",
				"Duration" => 24,
				"APR" => "",
				"Payment Factor" => "",
				"Dealer Fee" => "5%"),
		"Promo3410" =>
			array(
				"ID" => "Promo3410",
				"title" => "WF Promo3410 0% 48M",
				"Description" => "Promo 48M",
				"Duration" => 48,
				"APR" => "",
				"Payment Factor" => "",
				"Dealer Fee" => "7%"),
		"Promo3425" =>
			array(
				"ID" => "Promo3425",
				"title" => "WF Promo3425 0% 60M",
				"Description" => "Promo 60M",
				"Duration" => 60,
				"APR" => "",
				"Payment Factor" => "",
				"Dealer Fee" => "9%"),
		"Promo3431" =>
			array(
				"ID" => "Promo3431",
				"title" => "WF Promo3431 0% 72M",
				"Description" => "Promo 72M",
				"Duration" => 72,
				"APR" => "",
				"Payment Factor" => "",
				"Dealer Fee" => "11%")
		);
}
function getPresentationCategories($by_id = null){
	// markup as 1.1 for 10%
	$cats = [
		[
			"id" => 'code_compliance',
			"name" => 'Code Compliance',
			"markup" => '1'
		],
		[
			"id" => 'options_accessories',
			"name" => 'Options Accessories',
			"markup" => '1'
		],
		[
			"id" => 'system_installation',
			"name" => 'System Installation',
			"markup" => '1'
		],
		[
			"id" => 'prices',
			"name" => 'Additional Items',
			"markup" => '1'
		],
		[
			"id" => 'none',
			"name" => 'None',
			"markup" => ''
		]
	];
	if( $by_id ){
		$id_cats = [];
		foreach($cats as $cat){
			$id = $cat['id'];
			$id_cats[$id] = [];
		}
		return $id_cats;
	}
	else{
		return $cats;
	}

	return $cats;
}
function getPricingCategories(){
	return [
		[
			"id" => 'platinum',
			"name" => 'Platinum'
		],
		[
			"id" => 'gold',
			"name" => 'Gold'
		],
		[
			"id" => 'silver',
			"name" => 'Silver'
		],
		[
			"id" => 'bronze',
			"name" => 'Bronze'
		]
	];
}

function getBrands(){
	// maintain keys!!!!
	return [
		0 => [
			"id" => "trane",
			"name" => "Trane"
		],
		1 => [
			"id" => "lennox",
			"name" => "Lennox"
		],
		2 => [
			"id" => "ruud",
			"name" => "Ruud"
		],
		3 => [
			"id" => "carrier",
			"name" => "Carrier"
		],


	];
}

/*
 * embed_svg helper function is used to embed svg in frontend
 * because <img /> tag only works with the mozila and chrome browser, safari dosen't support this
 * <object></opject> tag works in almost major browsers like mozila, chrome and safari
 */
function embed_svg($class, $url, $height = '20px', $width = '20px') {
	$path = App::make('url')->to($url);
	$obj = '<object class="'.$class.'" type="image/svg+xml" height="'.$height.'" width="'.$width.'" data="'.$path.'">Your browser doesnot support SVG</object>';
	return $obj;
}
?>