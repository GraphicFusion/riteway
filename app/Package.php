<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'brand',
        'model_number',
        'platinum_price',
        'gold_price',
        'silver_price',
        'bronze_price',
        'width',
        'height',
        'depth',
        'parts_warranty',
        'labor_warranty',
        'heat_exchanger_warranty',
        'compressor_warranty',
        'seer',
        'eer',
        'hspf',
        'afue',
        'weight',
        'stages_of_cooling',
        'stages_of_heating',
        'blower_type',
        'capacity_for_cooling',
        'capacity_for_heating',
        'sensible_cooling',
        'tax_credit',
        'energy_star',
        'utility_rebate',
        'ahri',
        'hp_ac',
        'amps',
        'markup',
        'divide',
        'location',
        'price',
        'image',
        'order_number'
    ];

	public function getAllAttributes()
	{
	    $columns = $this->getFillable();
	    // Another option is to get all columns for the table like so:
	    // $columns = \Schema::getColumnListing($this->table);
	    // but it's safer to just get the fillable fields
	
	    $attributes = $this->getAttributes();
	
	    foreach ($columns as $column)
	    {
	        if (!array_key_exists($column, $attributes))
	        {
	            $attributes[$column] = null;
	        }
	    }
	    return $attributes;
	}

}
