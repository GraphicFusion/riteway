<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Split extends Model
{
    protected $table = 'splits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'model',
		'brand',
		'pics',
		'condenser',
		'filter_base',
		'furnace',
		'coil',
		'cu',
		'furn_ah',
		'price',
		'size',
		'seer',
		'eer',
		'hspf',
		'afue',
		'vs',
		'fw',
		'fh',
		'cw',
		'ch',
		'tot_w',
		'tot_h',
		'cu_w',
		'cu_d',
		'cu_h',
		'c_stage',
		'ht_stage',
		'cu_amp',
		'f_ah_amp',
		'compressor',
		'ht_exch',
		'parts',
		'labor',
		'stat',
		'capacity',
		'gaurds',
		'ari',
		'high',
		'low',
        'markup',
        'divide'
    ];

	public function getAllAttributes()
	{
	    $columns = $this->getFillable();
	    // Another option is to get all columns for the table like so:
	    // $columns = \Schema::getColumnListing($this->table);
	    // but it's safer to just get the fillable fields
	
	    $attributes = $this->getAttributes();
	
	    foreach ($columns as $column)
	    {
	        if (!array_key_exists($column, $attributes))
	        {
	            $attributes[$column] = null;
	        }
	    }
	    return $attributes;
	}

}
