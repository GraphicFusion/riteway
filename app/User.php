<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, Billable;
    protected $appends = array('roles','title','location_role','proposal_role');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'current_group_id'
    ];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	* Get the groups for the user.
	*/
	public function groups()
	{
		return $this->belongsToMany('App\Group')->withPivot('role','title');
	}

	/**
	* Get the locations for the user.
	*/
	public function locations()
	{
		return $this->belongsToMany('App\Location')->withPivot('location_role');
	}

	/**
	* Get the proposals for the user.
	*/
	public function proposals()
	{
		return $this->belongsToMany('App\Proposal')->withPivot('proposal_role');
	}

	public function getInitials(){
		if( $this->first_name || $this->last_name ){
			return substr( $this->first_name, 0, 1 ) . substr( $this->last_name, 0, 1 );
		}
		else{
			return substr( $this->name, 0, 1 );
		}
	}

	/**
	* Build User roles.
	*/
	 public function getrolesAttribute()
	{
		$groups = $this->groups;
		$roles = [];
		foreach( $groups as $group ){
			if( $group->id == $this->current_group_id ){
				$roles[] = $group->pivot->role;
			}
		}
		if( !empty( $roles ) ){
			return $roles;
		}
	}

	/**
	* Get Current Title.
	*/
 	public function getTitleAttribute()
	{
		$groups = $this->groups;
		$roles = [];
		foreach( $groups as $group ){
			if( $group->id == $this->current_group_id ){
				return $group->pivot->title;
				exit;
			}
		}
	}


	/**
	* Check if User has role.
	*/
	public function hasRole($role)
	{
		if( is_array( $this->roles ) ){
			if( in_array( $role, $this->roles) ){
				return true;
			}
			return false;
		}
	}

}
