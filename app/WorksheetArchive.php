<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorksheetArchive extends Model
{
	protected $fillable = ['group_id', 'type', 'worksheet_object', 'version', 'worksheet_id'];

	/**
	* Get the group for the WorksheetArchive.
	*/
	public function group()
	{
		return $this->belongsTo('App\Group');
	}
}
