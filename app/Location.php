<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use Notifiable;
    protected $appends = array('location_role');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address','unit','city', 'state', 'zipcode', 'country'
    ];


	/**
	* Get the customers for the location.
	*/
	public function users()
	{
		return $this->belongsToMany('App\User')->withPivot('location_role');
	}

}
