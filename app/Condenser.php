<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condenser extends Model
{
    protected $table = 'condensers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
    	'brand',
        'model_number',
        'price',
        'width',
        'height',
        'depth',
        'parts_warranty',
        'labor_warranty',
        'compressor_warranty',
        'hp_ac',
        'stages',
        'capacity',
        'amps',
        'markup',
        'divide',
        'image',
        'order_number'
    ];
}
