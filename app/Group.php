<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

	/**
	* Get the users for the group.
	*/
	public function users()
	{
		return $this->belongsToMany('App\User')->withPivot('role');
	}

	/**
	* Get the worksheets for the group.
	*/
	public function worksheets()
	{
		return $this->belongsToMany('App\Worksheet');
	}

	/**
	* Get the proposals for the group.
	*/
	public function proposals()
	{
		return $this->hasMany('App\Proposal');
	}


	/**
	* Get the users for the group consolidated by user.
	*/
	public function getTeam( $type = null )
	{
		$members = [];
		if( 'leadgen' == $type ){
			$users = $this->users->where('leadgen','=',1);
		}
		else{
			$users = $this->users;
		}
//print_r($this->users->where('leadgen','=',1));
		if( is_object( $users ) ){
			foreach( $users as $tmember ){
				if( 'customer' != $tmember->pivot->role ){
					$member = User::find($tmember->id);
					if( empty($members[$tmember->id]) ){
						$members[$tmember->id] = $member;
					}
				}
			}
			if( !empty($members ) ){
				foreach( $members as $member ){
					if( is_array( $member->roles ) ){
						$member->roles = implode(', ',$member->roles);
						$members[$member->id] = $member;
					}
				}
			}
		}
		return $members;
	}
	public function getMonthSold(){
		echo count($this->proposals->where('status','=','sold'));
	}
	public function getAnnualSold(){
		echo count($this->proposals->where('status','=','sold'));		
	}

	/**
	* Get the customers for the group.
	*/
	public function getCustomers()
	{
		$customers = [];
		if( is_object( $this->users ) ){
			foreach( $this->users as $tmember ){
				if( 'customer' == $tmember->pivot->role ){
					$member = User::find($tmember->id);
					if( empty($customers[$tmember->id]) ){
						foreach( $member->proposals as $proposal){
							$location = Location::find($proposal->location_id);
							if( is_object( $location ) ){
								$address = $location->address;
							}
							else{
								$address = "";
							}
							$customer = [
								'id' => $tmember->id,
								'address' => $address,
								'name' => $member->first_name . " " .$member->last_name,
								'status' => $proposal->status,
								'team' => 'WG'
							];
							$customers[] = $customer;
						}
					}
				}
			}
		}
//print_r($members);
		return $customers;
	}

}
