<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worksheet extends Model
{
	protected $fillable = ['group_id', 'type', 'worksheet_object', 'version'];

	/**
	* Get the group for the Worksheet.
	*/
	public function group()
	{
		return $this->belongsTo('App\Group');
	}
}
