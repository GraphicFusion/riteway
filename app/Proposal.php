<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\Group;
use App\Estimator;

class Proposal extends Model
{
    use Notifiable;
    protected $appends = array('roles','title','location_role','proposal_role','customer','rep','sold_date');

	public function __construct(array $attributes = array())
	{
	    parent::__construct($attributes);
	}

	/* Get the estimates for the proposal.
     */
    public function estimates()
    {
        $estimates = $this->hasMany('App\Estimate');
		return $estimates;
    }

	/* Get the group for the proposal.
     */
    public function group()
    {
        return $this->belongsTo('App\Group');
    }


	/* Get the location for the proposal.
     */
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

	/* Get the users for the proposal.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('proposal_role');
    }

	/**
	* Get Customer.
	*/
	 public function getCustomerAttribute()
	{
		$users = $this->users;
		foreach( $users as $user ){
			if( 'customer' == $user->pivot->proposal_role ){
				return $user;
				exit;
			}
		}
	}

	public function getPrice(){
		//price is sum of all estimates chosen
		/*$est_id = $this->primary_estimate_id;
		if( $est_id ){
			$estimate = Estimate::find( $est_id );
			if( is_object($estimate) ){
				$plan = $this->chosen_plan;			
				return $estimate->$plan;
			}
		}
		*/
	}

	/**
	* Get Rep.
	*/
	 public function getRepAttribute()
	{
		$users = $this->users;
		foreach( $users as $user ){
			if( 'rep' == $user->pivot->proposal_role ){
				return $user;
				exit;
			}
		}
	}

	public function getEstimateNumbers(){
		$i = 1;
		foreach( $this->estimates as $key => $estimate ){
			$estimate->number = $i;
			$i++;
		}
	}
}
