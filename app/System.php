<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// Split System
class System extends Model
{
    protected $table = 'systems';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'parts_warranty',
        'labor_warranty',
        'heat_exchanger_warranty',
        'compressor_warranty',
        'seer',
        'eer',
        'hspf',
        'afue',
        'sensible_cooling',
        'tax_credit',
        'energy_star',
        'utility_rebate',
        'ahri',
        'rw_system_id',
        'thermostat_id',
        'coil_id',
        'furnace_id',
        'condenser_id',
        'stages_of_cooling',
        'stages_of_heating',
		'name',
        'location',
        'compressor_warranty',
        'image',
        'order_number'
    ];

        public function getAllAttributes()
    {
        $columns = $this->getFillable();
        // Another option is to get all columns for the table like so:
        // $columns = \Schema::getColumnListing($this->table);
        // but it's safer to just get the fillable fields
    
        $attributes = $this->getAttributes();
    
        foreach ($columns as $column)
        {
            if (!array_key_exists($column, $attributes))
            {
                $attributes[$column] = null;
            }
        }
        return $attributes;
    }

}
