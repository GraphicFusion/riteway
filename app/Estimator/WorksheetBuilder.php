<?php
	namespace App\Estimator;
	use Form;
	use Html;
	class WorksheetBuilder {
		function __construct(){
			$this->worksheets = new \stdClass();
		}
		private function buildText( $field , $data){
			$text = Form::text( $field['name'], '', ['class'=>'form-control']);
			return $text;
		}		
		private function buildBlank( $field , $class = null, $result = null, $data = null, $template = false){
			$cats = getPricingCategories();
			$price_inputs = "";
			$title = "";
			if( $field->title ){
				$title = $field->title;
			}
			$clear = "<div style='clear:both'></div>";
			$input = "";
			$datas = "";
			$price = "";

			if( property_exists( $data, 'price' ) ){
				$price = $data->price;
			}
			$value = "";
			if( property_exists( $data, 'value')){
				$value = $data->value;
			}
			$price_inputs .= "<div class='form-group col-xs-2 choice-price active' style='display:block;' ><label>Unit Price</label>" . Form::text("_all_unit", $price,  ['class'=>'form-control', 'data-price-name'=>'all']) . "</div>";
			$input .= "<div class='select-variables' ".$datas."></div>";
			$input .= Form::text( $field->name, $value, ['class'=>'form-control worksheet-option locked blank','disabled'=>'true']);

//Form::select($field->name, $field_choices, $selected, ['class'=>'form-control worksheet-option']);
			$add = "";
			//if( $field->make_repeater ){
				$add = "<a class='add-link' data-repeater-count='1' href='#x'>+ Add</a>".$clear;
			//}
	
			$output = "<div class='form-group ".$class."'  data-price-category='".$field->price_category."'>";
			$label = "";
			if( $title && @$template){
				$label = Form::label( $field->name, $title, array('class'=>'repeater-label'));
			}		
			$price_inputs .= "<div class='price-holder'></div>";
			$output .= $label;
			if( $template ){
				$output .= $add;
			}
			$total = Form::text('total-all','', ['class'=>'form-control total-all']);
			$output .= "<div class='repeater-group' data-repeater-number='1'><a class='close delete-repeater'>x</a>" . $input . $price_inputs . $total. $clear . "</div><!--/repeater-group--></div>";
			return $output;
		}		
		public function checkFieldChoices($field_choices){
			if( count($field_choices)<2){
				$field_choices[''] = 'Unlock Next Input';
			}
			return $field_choices;
		}
		private function buildSelect( $field , $class = null, $result = null, $data = null, $template = false){
			$cats = getPricingCategories();
			$field_choices = [];
			$price_inputs = "";
			$job_inputs = "";
			$title = "";
			if( $field->title ){
				$title = $field->title;
			}
			$clear = "<div style='clear:both'></div>";
			$field_choices['none'] = 'Select an Option';
			if( property_exists($field, 'allow_null' ) && $field->allow_null ){
//				$field_choices['none'] = 'Select an Option';
			}
			$input = "";
			$datas = "";

			if( property_exists($field, 'choices' ) && count ($field->choices ) ){
				//print_r($field->choices);
				//print_r($data);
				foreach( $field->choices as $choice_key => $choice ){
					$choice_name = $field->name . "c:" .$choice_key."_";
					$field_choices[$choice_key] = $choice->title;
					if($included_amount = $choice->variables->included_amount){
						$datas .= " data-included-amount-".$choice_key."='".$included_amount."' ";
					}
					$job_unit_arr = [];
					$job_class = "";
	 				$job_style = "display:none;";
					if( property_exists( $data, 'selected' ) && $choice_key == $data->selected){
		 				$job_unit_arr['class'] = 'form-control active';
						$job_style = "";
						$job_class = "active";
		 			}
					if( property_exists( $data, 'job_labor' ) && $choice_key == $data->selected ){
		 				$job_unit_arr['class'] = 'form-control';
						$job_unit = $data->job_labor;
					}
					else{
						$job_unit = 0;
						if(property_exists($choice, 'job_unit')){
							$job_unit = $choice->job_unit;
						}
						$job_unit_arr['class'] = 'form-control';
					}
					$job_inputs .= "<div class='form-group col-xs-2 choice-job-unit ".$job_class."' data-choice-key='".$choice_key."' style='".$job_style."'><label>Job Unit</label>" . Form::text($choice_name."_job_unit", $job_unit,  $job_unit_arr) . "</div>";

					if( $choice->tiered_pricing ){

						foreach( $cats as $cat ){
							$price_cat = $cat['id'];
							$price = $choice->prices->$price_cat->unit;
							if( is_object($data) ){
								if( property_exists( $data, 'prices' ) && property_exists( $data->prices, $price_cat ) ){
									$price = ($data->prices->$price_cat ? $data->prices->$price_cat :$choice->prices->$price_cat->unit);
								}
								else{
									$price = $choice->prices->$price_cat->unit;
								}
							}
							$price_inputs .= "<div class='form-group col-xs-2 choice-price'  data-choice-key='".$choice_key."'><label>".$cat['name']."</label>" . Form::text($choice_name."_".$price_cat."_unit", $price,  ['class'=>'form-control','data-price-name'=>$price_cat]) . "</div>";
						}
					}
					else{
						$price = "";				
						if( is_object($data) ){
							if( property_exists( $data, 'prices' ) && property_exists( $data->prices, 'all')  && $choice_key == $data->selected ){
								$price = $data->prices->all;
							}
							else{
								$price = $choice->prices->all->unit;
							}
						}
						else{
							$price = $choice->prices->all->unit;
						}
						$price_inputs .= "<div class='form-group col-xs-2 choice-price' data-choice-key='".$choice_key."'><label>Unit Price</label>" . Form::text($choice_name."_all_unit", $price,  ['class'=>'form-control', 'data-price-name'=>'all']) . "</div>";
					}
				}
				$input .= "<div class='select-variables' ".$datas."></div>";
			}
			$selected = 'none';
			$feet = 0;
			$inc = 0;
			if(is_object($data) && property_exists($data,'selected')){
				if( 'none' != $data->selected ){
					$selected = $data->selected;
				}
				$feet = ( property_exists( $data, 'feet') ? $data->feet : 0);
				$inc = ( property_exists( $data, 'inc') ? $data->inc : 0);
			}
			$field_choices = $this->checkFieldChoices($field_choices);
			$dis_class = "form-control worksheet-option";
			$arr = [];
			if('none' != $selected  ){
				$dis_class .= " unlocked";
			}
			else{
				$arr['disabled'] = true;
				$dis_class .= " locked";
			}
			if( property_exists( $field, 'job_sheet') && $field->job_sheet ){
				$dis_class .= " job-sheet";
			}
			$arr['class'] = $dis_class;
			$input .= Form::select($field->name, $field_choices, $selected, $arr);
			$add = "";
			if( $field->total_feet ){
				$class .= " input-group-mix";
				$input .= "<div class='row other-select-variables'>
							<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-pd-right'>
								<div class='input-group first'>
									<div class='input-group-addon'>Feet</div>
									".Form::text('feet', $feet, ['class'=>'form-control']) ."
								</div>
							</div>
							<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-pd'>
								<div class='input-group second'>
									<div class='input-group-addon'>Inc</div>
									". Form::text('inc', $inc, ['class'=>'form-control']) ."
								</div>
							</div>
							<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-pd-left'>
								<div class='input-group last'>
									<div class='input-group-addon'>Net</div>
									". Form::text('net', '0', ['class'=>'form-control']) ."
								</div>
							</div>
							<div class='ttip' data-toggle='tooltip' data-placement='top' title='Help Top'>". embed_svg('icon', 'img/help-icon.svg', '20px', '20px') ."</div>".$result."
						</div>";
				if( $field->make_repeater ){
					$add = "<a class='add-link' data-add='input-group-mix' data-repeater-count='1' href='#x'>+ Add</a>".$clear;
				}
				$result = "";
			}
			else{
				if( $field->quantity ){
					$class .= " input-group-mix";
					$input .= "<div class='row other-select-variables'>
								<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-pd-right'>
									<div class='input-group first'>
										<div class='input-group-addon' style='padding-right:20px'>Total Quantity</div>
										".Form::text('total_quantity', @$quantity, ['class'=>'form-control', 'style'=>'padding:0 20px 0 0; margin-left:-15px; text-align:right;']) ."
									</div>
								</div>
								<div class='ttip' data-toggle='tooltip' data-placement='top' title='Help Top'>". embed_svg('icon', 'img/help-icon.svg', '20px', '20px') ."</div>".$result."
							</div>";
					if( $field->make_repeater ){
						$add = "<a class='add-link' data-add='input-group-mix' data-repeater-count='1' href='#x'>+ Add</a>".$clear;
					}
					$result = "";
				}				
				if( $field->make_repeater ){
					$add = "<a class='add-link' data-repeater-count='1' href='#x'>+ Add</a>".$clear;
				}
			}
	
			$output = "<div class='form-group ".$class."' data-price-category='".$field->price_category."'>";
			$label = "";
			if( $title && $template){
				$label = Form::label( $field->name, $title, array('class'=>'repeater-label'));
			}		
			$price_inputs .= "<div class='price-holder'></div>";
			$output .= $label;
			if( $template ){
				$output .= $add;
			}
			$total = Form::text('total-all','', ['class'=>'form-control total-all']).Form::text('total-platinum','', ['class'=>'form-control total-platinum']).Form::text('total-gold','', ['class'=>'form-control total-gold']).Form::text('total-silver','', ['class'=>'form-control total-silver']).Form::text('total-bronze','', ['class'=>'form-control total-bronze']);
			$output .= "<div class='repeater-group' data-repeater-number='1'><a class='close delete-repeater'>x</a>" . $input . "<div class='row'>". $job_inputs . "</div>" . $price_inputs .$total. $clear . $result . "</div><!--/repeater-group--></div>";
			return $output;
		}		
		public function buildField( $field , $errors, $data, $template = false){
			$class = "";
			$result = "";
			if( $template){
				$class .= " template";
			}
			if( !empty( $_POST ) ){
				$result = "<span class='status success'>".Html::image('img/check-icon.svg','Success',array('width'=>'20px'))."</span>";
			}
			if( $errors->has($field->name) ){
				$class = " has-error";
				$result = "<span class='status error'>".Html::image('img/close-icon.svg','Error',array('width'=>'20px'))."</span>";
			}
			$output = "";
			if( 'text' == $field->type ){
				$output .= $this->buildText( $field , $class, $result , $data, $template );
			}
			if( 'blank' == $field->type ){
				$output .= $this->buildBlank( $field , $class, $result , $data, $template );
			}
			if( 'select' == $field->type ){
				$output .= $this->buildSelect( $field , $class, $result, $data, $template);
			}
			if( 'dropdown' == $field->type ){
				$output .= $this->buildSelect( $field, $class, $result , $data, $template);
			}
			return $output;
		}
	}