<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Furnace extends Model
{
    
    protected $table = 'furnaces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'brand',
        'model_number',
        'price',
        'width',
        'height',
        'depth',
        'parts_warranty',
        'labor_warranty',
        'heat_exchanger_warranty',
        'filter_base',
        'afue',
        'blower_type',
        'blower_size',
        'stages',
        'capacity',
        'furnace_air_handler',
        'markup',
        'divide',
        'location',
        'image',
        'order_number'
    ];

	public function getAllAttributes()
	{
	    $columns = $this->getFillable();
	    // Another option is to get all columns for the table like so:
	    // $columns = \Schema::getColumnListing($this->table);
	    // but it's safer to just get the fillable fields
	
	    $attributes = $this->getAttributes();
	
	    foreach ($columns as $column)
	    {
	        if (!array_key_exists($column, $attributes))
	        {
	            $attributes[$column] = null;
	        }
	    }
	    return $attributes;
	}


}