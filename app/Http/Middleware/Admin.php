<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if ( Auth::check() && Auth::user()->hasRole('admin') )
		{
			return $next($request);
		}
        if (!Auth::check()) {
//dd(8);
            return redirect('/login');
        }

///dd(9);
        return redirect('home');
    }
}
