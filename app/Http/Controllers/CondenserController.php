<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Condenser;
use Session;
use Redirect; 
use DB;

class CondenserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $imageName = "";    
        if ($_FILES['image']['size']>0) {
             $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
        $input = $request->all();
        $input['image'] = $imageName;
        $Condensers = Condenser::create($input);
        Session::flash('success', 'New Condensers added successfully.');
        return redirect('/unit-list?unit_type=condenser');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $Condensers = Condenser::find($_POST['uid']);
        return response()->json(['Condensers' => $Condensers], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
    {
        $obj = Condenser::find($request->id);
        $obj->forceDelete();
        return Redirect::to( '/unit-list?unit_type=condenser' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $imageName = "";
        if ($_FILES['image']['size']>0) {
             $this->validate($request, [
                'brand' => 'required',
                'model_number' => 'required',
                'price' => 'required|numeric',
                'width' => 'required|numeric',
                'height' => 'required|numeric',
                'depth' => 'required|numeric',
                'parts_warranty' => 'required|numeric',
                'labor_warranty' => 'required|numeric',
                'compressor_warranty' => 'required',
                'hp_ac' => 'required',
                'stages' => 'required',
                'capacity' => 'required',
                'markup' => 'required',
             //   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:250|dimensions:max_width=250,max_height=250',
            ]);
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
        // $Condensers = Condensers::create($request->all());
        DB::table('condensers')
            ->where('id', $request->updatecondenserid)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'hp_ac' => $request->hp_ac,
                        'amps' => $request->amps,
                       'stages' => $request->stages,
                       'capacity' => $request->capacity,
                       'compressor_warranty' => $request->compressor_warranty,
                       'markup' => $request->markup,
                       'divide' => $request->divide,
                       'image' => $imageName,
                       'order_number' => $request->order_number

                     ]);
            Session::flash('success', 'Condensers updated successfully.');
            
            return redirect('/unit-list?unit_type=condenser');
        
    }
}