<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proposal;
use App\Location;
use App\Estimate;
use App\User;
use App\Worksheet;
use Validator;
use DB;
use Hash;
use Auth;
use App\Group;
use Redirect; 
class WorksheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//echo "ppppppp";
//print_r($_POST);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCustomer(Request $request)
    {
//print_r($request->all());
		$email = $request->email;
		$data = [];
		$rules = array(
            'email' => 'email|max:255|unique:users',
        );
		$prop = "";
		$data['request']['user_registration']['email'] = $email;
        $validator = Validator::make( array('email'=>$email), $rules);
		$data['success'] = false;
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			$loggedInUser = getUser();
			$group = Group::find( $loggedInUser->current_group_id );

			$prop = new Proposal;
// need to save the worksheet version # with the estimate
// save with group id
			$prop->group_id = $group->id;
			$prop->save();

			$estimate = new Estimate;
			$estimate->type = $request->worksheet_type;

			$worksheet_model = Worksheet::firstOrNew( array('group_id' => $group->id, 'type' => $request->worksheet_type)); 
			$estimate->worksheet_version = $worksheet_model->version;
			$estimate->worksheet_id = $worksheet_model->id;
			$estimate->proposal_id = $prop->id; 
			$estimate->save();
			$prop->estimates()->save($estimate);


			// store
			$user = new User;
			$user->name = $email;
			$user->first_name = $request->first_name;
			$user->last_name = $request->last_name;
			$user->email = $email;
			$user->phone = $request->phone;
			$user->password = Hash::make(str_random(8));

			$user->save();

			// location 
			$loc = new Location;
			$loc->address = $request->address;
			$loc->unit = $request->unit;
			$loc->city = $request->city;
			$loc->state = $request->state;
			$loc->zipcode = $request->zipcode;
			$loc->save();
			if( is_object( $user ) && is_object($loc ) ){
				$user->locations()->save($loc, ['location_role' => $request->role ]);

				// save user to current group as customer
				$auth_user = Auth::user();
				$group_id = $auth_user->current_group_id;
				if( $request->group_id ){
					$group_id = $request->group_id;
				}
				$group = Group::find( $group_id );		
				$user->current_group_id = $group->id;
				$user->groups()->save($group, ['role' => 'customer' ]);

				$user->proposals()->save($prop, ['proposal_role' => 'customer' ]);
				$auth_user->proposals()->save($prop, ['proposal_role' => 'rep' ]);

				$user->save();

				$prop->location()->associate($loc);
				$prop->save();
			}
        }
		if( @$estimate ){
			return Redirect::to( '/' . $request->worksheet_type . '?estimate_id=' . $estimate->id );
		}
        else{
            return Redirect::to( '/' .  $request->worksheet_type . '?step=customer&message=email' );
        }
    }

    /**
     * Create a new opportunity from an existing customer.
     *
     * @param  int  $customer_id
     * @return \Illuminate\Http\Response
     */
    public function newProposal($type, $id)
    {
		
//echo "NewProposal".$type.$id;
			$loggedInUser = getUser();
			$group = Group::find( $loggedInUser->current_group_id );

			$prop = new Proposal;
// need to save the worksheet version # with the estimate
// save with group id
			$prop->group_id = $group->id;
			$prop->save();

			$estimate = new Estimate;
			$estimate->type = $type;

			$worksheet_model = Worksheet::firstOrNew( array('group_id' => $group->id, 'type' => $type)); 
			$estimate->worksheet_version = $worksheet_model->version;
			$estimate->worksheet_id = $worksheet_model->id;
			$estimate->proposal_id = $prop->id; 
			$estimate->save();
			$prop->estimates()->save($estimate);


			// store
			$user = User::find($id);
			$loc = $user->locations->first();
			$pivot = DB::table('location_user')->where('location_id', $loc->id)->first();
			if( is_object( $user ) ) {
				$auth_user = Auth::user();
				$user->proposals()->save($prop, ['proposal_role' => 'customer' ]);
				$auth_user->proposals()->save($prop, ['proposal_role' => 'rep' ]);
				$user->locations()->save($loc, ['location_role' => $pivot->location_role ]);


				$user->save();
				$prop->location()->associate($loc);
				$prop->save();
			}
		if( @$estimate ){
			return Redirect::to( '/' . $type . '?estimate_id=' . $estimate->id );
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}