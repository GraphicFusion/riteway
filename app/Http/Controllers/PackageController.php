<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use Session;
use Redirect; 
use DB;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if( !$request->tax_credit ){
            $request->tax_credit = (int) 0;
        }
        if( !$request->energy_star ){
            $request->energy_star = (int) 0;
        }
        if( !$request->utility_rebate ){
            $request->utility_rebate = (int) 0;
        }

        
        $this->validate($request, [
            'platinum_price' => 'numeric',
            'gold_price' => 'numeric',
            'silver_price' => 'numeric',
            'bronze_price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            'heat_exchanger_warranty' => 'numeric',
            'compressor_warranty' => 'numeric',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $imageName = "";
        if ($_FILES['image']['size']>0) {
           $imageName = time().'.'.$request->image->getClientOriginalExtension();
          $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
         $input = $request->all();
        $input['image'] = $imageName;
        $coil = Package::create($input);
        Session::flash('success', 'New packages added successfully.');
        return redirect('/unit-list?unit_type=package');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if( !$request->tax_credit ){
            $request->tax_credit = (int) 0;
        }
        if( !$request->energy_star ){
            $request->energy_star = (int) 0;
        }
        if( !$request->utility_rebate ){
            $request->utility_rebate = (int) 0;
        }
        if ($_FILES['image']['size']>0) {
  $this->validate($request, [
            'platinum_price' => 'numeric',
            'gold_price' => 'numeric',
            'silver_price' => 'numeric',
            'bronze_price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            'heat_exchanger_warranty' => 'numeric',
            'compressor_warranty' => 'numeric',
            'seer' => 'numeric',
            'eer' => 'numeric',
            'weight' => 'numeric',
            'capacity_for_cooling' => 'numeric',
            'capacity_for_heating' => 'numeric',
            'ahri' => 'numeric',
         'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $imageNamepath = $request->image->move(public_path('images'), $imageName);
        DB::table('packages')
            ->where('id', $request->Packageid)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'heat_exchanger_warranty' => $request->heat_exchanger_warranty,
                       'compressor_warranty' => $request->compressor_warranty,
                       'seer' => $request->seer,
                       'eer' => $request->eer,
                       'hspf' => $request->hspf,
                       'afue' => $request->afue,
                       'weight' => $request->weight,
                       'stages_of_cooling' => $request->stages_of_cooling,
                       'stages_of_heating' => $request->stages_of_heating,
                       'blower_type' => $request->blower_type,
                       'capacity_for_cooling' => $request->capacity_for_cooling,
                       'capacity_for_heating' => $request->capacity_for_heating,
                       'sensible_cooling' => $request->sensible_cooling,
                       'tax_credit' => $request->tax_credit,
                       'energy_star' => $request->energy_star,
                       'utility_rebate' => $request->utility_rebate,
                       'ahri' => $request->ahri,
                       'hp_ac' => $request->hp_ac,
                       'amps' => $request->amps,
                       'markup' => $request->markup,
                       'divide' => $request->divide,
                       'image' => $imageName,
                       'order_number' => $request->order_number
                       


                     ]);
        
        Session::flash('success', 'Package Updated successfully.');
        return redirect('/unit-list?unit_type=package');
    }
    else
    {
      $this->validate($request, [
            'platinum_price' => 'numeric',
            'gold_price' => 'numeric',
            'silver_price' => 'numeric',
            'bronze_price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            'heat_exchanger_warranty' => 'numeric',
            'compressor_warranty' => 'numeric',
            'seer' => 'numeric',
            'eer' => 'numeric',
            'weight' => 'numeric',
            'capacity_for_cooling' => 'numeric',
            'capacity_for_heating' => 'numeric',
            'ahri' => 'numeric',
        
        ]);
         DB::table('packages')
            ->where('id', $request->Packageid)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'heat_exchanger_warranty' => $request->heat_exchanger_warranty,
                       'compressor_warranty' => $request->compressor_warranty,
                       'seer' => $request->seer,
                       'eer' => $request->eer,
                       'hspf' => $request->hspf,
                       'afue' => $request->afue,
                       'weight' => $request->weight,
                       'stages_of_cooling' => $request->stages_of_cooling,
                       'stages_of_heating' => $request->stages_of_heating,
                       'blower_type' => $request->blower_type,
                       'capacity_for_cooling' => $request->capacity_for_cooling,
                       'capacity_for_heating' => $request->capacity_for_heating,
                       'sensible_cooling' => $request->sensible_cooling,
                       'tax_credit' => $request->tax_credit,
                       'energy_star' => $request->energy_star,
                       'utility_rebate' => $request->utility_rebate,
                       'ahri' => $request->ahri,
                       'hp_ac' => $request->hp_ac,
                       'amps' => $request->amps,
                       'markup' => $request->markup,
                       'divide' => $request->divide,
                       'order_number' => $request->order_number
                     ]);
        
        Session::flash('success', 'Package Updated successfully.');
        return redirect('/unit-list?unit_type=package');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
    {
        $obj = Package::find($request->id);
        $obj->forceDelete();
        return Redirect::to( '/unit-list?unit_type=package' );
    }}
