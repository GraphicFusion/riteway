<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Source;

use Auth;
use Hash;
use Redirect;
use Validator;

class SourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$name = $request->source_name;
		$id = $request->source_id;
		$data = [];
		$rules = array(
            'name' => 'required|unique:sources',
        );
        $validator = Validator::make( array('name'=>$name), $rules);
        $data['success'] = false;
        if( $id ){
            $source = Source::find($id);
            $source->name = $name;
            $source->store = $request->source_store;
            $source->update();
        }
        else{
            if ($validator->fails()) {
    			$messages = $validator->messages();
    			foreach ($messages->all() as $message)
    			{
    				$data['request']['validation_messages'][] = $message;
    			}
            }
    		else {
   				$source = new Source;
    			$source->name = $name;
                $source->store = $request->source_store;
    			$source->save();
    		}
        }
		return Redirect::to( '/settings/lead-sources' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
