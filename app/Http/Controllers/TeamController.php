<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\User;
use Auth;
use Hash;

class TeamController extends Controller
{
    public function show($id)
    {
    	$user = User::find($id);
		return view('modals.edit-team', compact('user'));
    }

    public function update(Request $request)
    {
    	$team = User::find($request->id);
		$team->name = $request->member_name;
		$team->first_name = $request->first_name;
		$team->last_name = $request->last_name;
		$team->phone = $request->phone;
		$team->email = $request->member_email;
		if($request->password ){
			$team->password = Hash::make($request->password);
		}
		$team->save();
		$team->groups()->updateExistingPivot($team->current_group_id, ['title' => $request->member_title]);

		return redirect('team');
    }

    public function showDel($id)
    {
    	$user = User::find($id);
		return view('modals.delete-team-member', compact('user'));
    }

    public function deleteMember(Request $request)
    {
		$user = User::find($request->uid);
		$groups = $user->groups()->get();
		foreach ($groups as $key => $group) {
			$user->groups()->detach($group->id);
		}
		$user->delete();
		return redirect('team')->with(['msg'=>'Team member account successfully deleted.']);
    }
}
