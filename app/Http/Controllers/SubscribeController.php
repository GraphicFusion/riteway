<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SubscribeController extends Controller
{
	public function __construct()
	{
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here: https://dashboard.stripe.com/account/apikeys
		\Stripe\Stripe::setApiKey(env('STRIPE_KEY'));
	}

	/*
	 * Redirect to subscription form
	 */
	public function index()
	{
		return view('subscribe');
	}

	/*
	 * Get all plans fron stripe
	 */
	public function getAllPlans()
	{
		return \Stripe\Plan::all(array("limit" => -1));
	}

	/*
	 * Check plan is available or not
	 */
	public function checkPlan($planID)
	{
		$status = 0;
		$error = '';
		try {
			$plan = \Stripe\Plan::retrieve($planID);
			$status = 1;
		} catch (\Stripe\Error\InvalidRequest $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\Authentication $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\ApiConnection $e) {
			$error = $e->getMessage();
		} catch (Exception $e) {
			$error = $e->getMessage();
		}

		if( $status == 1 ) {
			return $plan;
		}/* else {
			return $error;
		}*/
	}

	/*
	 * Create new plan
	 */
	public function createPlan($planArray)
	{
		if ( empty($this->checkPlan($planArray['id'])) ) {
			return \Stripe\Plan::create($planArray);
		} else {
			return $this->checkPlan($planArray['id']);
		}
	}

	/*
	 * List All Customer
	 */
	public function getAllCustomer()
	{
		return \Stripe\Customer::all(array("limit" => -1));
	}

	/*
	 * get customer by id
	 */
	public function getCustomer($id)
	{
		$status = 0;
		$error = '';
		try {
			$customer = \Stripe\Customer::retrieve($id);
			$status = 1;
		} catch (\Stripe\Error\InvalidRequest $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\Authentication $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\ApiConnection $e) {
			$error = $e->getMessage();
		} catch (Exception $e) {
			$error = $e->getMessage();
		}

		if( $status == 1 ) {
			return $customer;
		}/* else {
			return $error;
		}*/
	}

	/*
	 * create customer to stripe and store data to db
	 */
	public function createCustomer($email, $metaArray, $token) 
	{
		$customer = \Stripe\Customer::create(array(
			"email" => $email,
			"source" => $token,
			"metadata"=>$metaArray
		));

		// DB::table('users')
	 //            ->where('id', $user->id)
	 //            ->update([
	 //            	'strip_id' => $customer->id,
	 //            	'card_brand' => '',
	 //            	'card_last_four' => '',
	 //            	'trial_ends_at' => ''
	 //        	]);     	
		return $customer;
	}

	/*
	 * Make subscription
	 */
	public function create(Request $request)
    {
    	$user = Auth::user();

    	$planArray = array(
    		"name" => "Basic Plan",
			"id" => "basic-monthly",
			"interval" => "month",
			"currency" => "usd",
			"amount" => 200,
		);
		$plan = $this->createPlan($planArray);

		$customerMeta = array(
			'name' => $user->name
		);
		$customer = $this->createCustomer($user->email, $customerMeta, $request->stripeToken);


		\Stripe\Subscription::create(array(
			"customer" => $customer->id,
			"plan" => $plan->id,
		));
    }
}
