<?php

namespace App\Http\Controllers;
use App\Proposal;
use App\Estimate;
use Redirect; 
use Mail;
use Illuminate\Http\Request;

class ProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Update the proposal status.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveInfo(Request $request)
    {

        $proposal = Proposal::find($request->proposal_id);
		$proposal->status = $request->status;
		$proposal->source_id = $request->source_id;
		$proposal->leadgen_id = $request->leadgen_id;
        $proposal->included_estimates = $request->included_estimates;
        //print_r($proposal);
        //
         
		if('sold' == $request->status ){
			$proposal->sold_date = date("Y-m-d H:i:s");
            $data = [
                'text' => "
            <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                <html xmlns='http://www.w3.org/1999/xhtml'>
                    <p>Great news team - </p>
                    <p>".$proposal->rep->first_name." ".$proposal->rep->last_name." sold another new job.</p>
                    <p>Click here to see job details:  <a href='riteway.sonderdev.com/opportunities/?proposal=".$request->proposal_id."'>riteway.sonderdev.com/opportunities/?proposal=".$request->proposal_id."</a>.</p></html>",
                'subject' => "New Job Sold: Proposal #".$request->proposal_id
            ];
            $subject = $data['subject'];
            Mail::send('warehouse-email', $data, function ($message) use ($subject) {
                $message->from('mailgun@riteway.sonderdev.com', 'Riteway');
                $message->subject($subject);
                $message->to('wjgrass@gmail.com', 'wjgrass');
                $message->cc('MAuman@ritewayac.com','Michael Auman');
                //$message->setBody($text, 'text/html'); 
            });                    
//            Mail::raw( 'text', function ($message) {
  //          });
            
		}
        $proposal->save();
		return Redirect::to( "/opportunities?proposal=".$request->proposal_id );
    }

 /**
     * Update the proposal status.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function emailEstimate(Request $request)
    {
        $estimate_id = $request->estimate_id;
        $estimate = Estimate::find($request->estimate_id);
        $proposal = Proposal::find($estimate->proposal_id);
        $customer = $proposal->customer;
        $salutation = $customer->first_name. " " .$customer->last_name;
        $data = [
                'text' => "
            <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                <html xmlns='http://www.w3.org/1999/xhtml'>
                    <p>Dear ".$salutation."</p>,
                    <p>Thank you for allowing Rite Way to consult with you on your new heating and cooling system.</p>
                    <p>To see your estimate, please click this link:   <a href='riteway.sonderdev.com/opportunities/?present=".$estimate_id."'>riteway.sonderdev.com/opportunities/?present=".$estimate_id."</a></p>
                    <p>To gain access please enter your email or your username created for you by your Rite Way project manager.</p>
                    <p>We look forward to partnering with you and are confident that later you will look back and know that working with Rite Way was the right decision. Please contact us with any questions and we'll be happy to answer them for you.</p>

                    <p>Respectfully,
                        Rite Way Heating, Cooling & Plumbing</p>"
            ];
            $subject = "Your Rite Way Proposal";
            if($customer->email){
                Mail::send('warehouse-email', $data, function ($message) use ($subject, $customer) {
                 //   $message->to($customer->email);
                    $message->from('mailgun@riteway.sonderdev.com', 'Riteway');
                    $message->subject($subject);
                    $message->to('wjgrass@gmail.com', 'wjgrass');
                    $message->cc('MAuman@ritewayac.com','Michael Auman');
                    //$message->setBody($text, 'text/html'); 
                });                    
            }
            return Redirect::to( "/opportunities/?present=".$estimate_id);
    }
    /**
     * Update the chosen estimate.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chosen_estimate(Request $request)
    {
		$proposal = Proposal::find($request->estimate_proposal_id);
		$proposal->primary_estimate_id = $request->chosen_estimate;
		$proposal->save();
//print_r($proposal);
		return Redirect::to( '/opportunities' );
    }

    /**
     * Update the chosen plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chosen_planVOID(Request $request)
    {
		$proposal = Proposal::find($request->plan_proposal_id);
		$proposal->chosen_plan = $request->chosen_plan;
		$proposal->save();
//print_r($proposal);
		return Redirect::to( '/opportunities' );
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
		$proposal = Proposal::find($request->proposal_id);
		$proposal->active = 0;
		$proposal->save();
		return Redirect::to( '/opportunities' );
    }
}
