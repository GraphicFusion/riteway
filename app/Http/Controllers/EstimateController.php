<?php

namespace App\Http\Controllers;
use App\Estimate;
use App\Proposal;
use Redirect; 

use Illuminate\Http\Request;

class EstimateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

	/**
     * Update the chosen plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chosen_plan(Request $request)
    {
		$estimate = Estimate::find($request->estimate_id);
		$estimate->chosen_plan = $request->plan_type;
		$estimate->save();
		return Redirect::to( '/opportunities?proposal='.$estimate->proposal_id.'&estimate=' . $estimate->id );
    }

	/**
     * Update if estimate is chosen.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chosen(Request $request)
    {
		$estimate = Estimate::find($request->estimate_id);
		$estimate->chosen = $request->chosen;
		$estimate->save();
//print_r($proposal);
		return Redirect::to( '/opportunities' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
		$estimate = Estimate::find($request->estimate_id);
        if( is_object($estimate)){
    		$estimate->forceDelete();
        }
		return Redirect::to( '/opportunities' );
    }
}
