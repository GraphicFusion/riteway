<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\User;
use Auth;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$email = $request->member_email;
		$name = $request->member_name;
		$data = [];
		$rules = array(
            'email' => 'required|email|max:255|unique:users',
        );
		$data['request']['user_registration']['email'] = $email;
        $validator = Validator::make( array('email'=>$email), $rules);
        // process the login
		$data['success'] = false;
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			// store
			$user = new User;
			$user->name = $name;
			$user->first_name = $request->first_name;
			$user->last_name = $request->last_name;
			$user->email = $email;
            $user->phone = $request->phone;
			$user->password = Hash::make('123456');
			$user->save();
			if( is_object( $user ) ){
				$group_id = Auth::user()->current_group_id;
				if( $request->group_id ){
					$group_id = $request->group_id;
				}
				$group = Group::find( $group_id );		
				$user->current_group_id = $group->id;
				$user->groups()->save($group, ['title' => $request->member_title ]);
				$user->save();
			}
        }
		return Redirect::to( '/team' );
}

    /**
     * Store a user as a Lead Generator.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLeadgen(Request $request)
    {
// print_r($_POST);
		$first_name = $request->first_name;
		$last_name = $request->last_name;
		$email = $request->leadgen_email;
		$id = $request->source_id;
		$data = [];
		$save = 1;
		if( !$id ){
			$save = 0;
			$rules = array(
	            'email' => 'required|email|max:255|unique:users'
	        );
	        $validator = Validator::make( array('email'=>$email), $rules);
			$data['success'] = false;
	        if ($validator->fails()) {
				$messages = $validator->messages();
				foreach ($messages->all() as $message)
				{
					$data['request']['validation_messages'][] = $message;
				}
	        }
			else {
				$save = 1;
			}
		}
		if( $save ){
			if( $id ){
				$user = User::find($id);
			}
			else{
				$user = new User;
			}
			$user->name = $email;
			$user->first_name = $first_name;
			$user->last_name = $last_name;
			$user->email = $email;
			$user->leadgen = 1;
			$user->password = Hash::make(str_random(8));
			$user->save();
			if( is_object( $user ) ){
				$group_id = Auth::user()->current_group_id;
				if( $request->group_id ){
					$group_id = $request->group_id;
				}
				$group = Group::find( $group_id );		
				$user->groups()->save($group, ['title' => 'Lead Generator' ]);
				$user->save();
			}
		}
		return Redirect::to( '/settings/lead-generators' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change current group.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeGroup(Request $request)
    {
		$id = $request->change_group;
		$user = Auth::user();
		$user->current_group_id = $id;
		$user->save();
		return Redirect::back();
    }


    public function getLead($id)
    {
        $user = User::find($id);
        return view('modals.delete-lead', compact('user'));
    }

    public function deleteLead(Request $request)
    {
        $user = User::find($request->uid);
        $groups = $user->groups()->get();
        foreach ($groups as $key => $group) {
            $user->groups()->detach($group->id);
        }
        $user->delete();
        return redirect('/settings/lead-generators')->with(['msg'=>'Lead successfully deleted.']);
    }
}
