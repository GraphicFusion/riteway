<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\System;
use Session;
use DB;
use Redirect; 

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if( !$request->tax_credit ){
        //     $request->tax_credit = (int) 0;
        // }
        // if( !$request->energy_star ){
        //     $request->energy_star = (int) 0;
        // }
        // if( !$request->utility_rebate ){
        //     $request->utility_rebate = (int) 0;
        // }

        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $imageName = "";
        if ($_FILES['image']['size']>0) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
        /*Image Upload*/ 
         $input = $request->all();
        $input['image'] = $imageName;
//dd($input);
         $systems = Systems::create($input);
         Session::flash('success', 'New systems added successfully.');
         return redirect('/unit-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
        if( !$request->tax_credit ){
            $request->tax_credit = (int) 0;
        }
        if( !$request->energy_star ){
            $request->energy_star = (int) 0;
        }
        if( !$request->utility_rebate ){
            $request->utility_rebate = (int) 0;
        }

        if ($_FILES['image']['size']>0) {
     $this->validate($request, [
            
     //  'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:250|dimensions:max_width=250,max_height=250',
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $imageNamepath = $request->image->move(public_path('images'), $imageName);
        DB::table('systems')
            ->where('id', $request->SystemID)
            ->update([ 
                'name' => $request->name,
                'location' => $request->location,
                'parts_warranty' => $request->parts_warranty,
                'labor_warranty' => $request->labor_warranty,
                'heat_exchanger_warranty' => $request->heat_exchanger_warranty,
                'compressor_warranty' => $request->compressor_warranty,
                'seer' => $request->seer,
                'eer' => $request->eer,
                'hspf' => $request->hspf,
                'afue' => $request->afue,
                'sensible_cooling' => $request->sensible_cooling,
                'stages_of_cooling' => $request->stages_of_cooling,
                'stages_of_heating' => $request->stages_of_heating,
                'tax_credit' => $request->tax_credit,
                'energy_star' => $request->energy_star,
                'utility_rebate' => $request->utility_rebate,
                'ahri' => $request->ahri,
                'rw_system_id' => $request->rw_system_id,
                'coil_id' => $request->coil_id,
                'furnace_id' => $request->furnace_id,
                'condenser_id' => $request->condenser_id,
                'image' => $imageName,
                'order_number' => $request->order_number
            ]);

        //$systems = Systems::create($request->all());
        Session::flash('success', 'System updated successfully.');
        return redirect('/unit-list?unit_type=system');
    }
    else
    {
         $this->validate($request, [
            
       
        ]);
        DB::table('systems')
            ->where('id', $request->SystemID)
            ->update([ 
                'name' => $request->name,
                'location' => $request->location,
                'parts_warranty' => $request->parts_warranty,
                'labor_warranty' => $request->labor_warranty,
                'heat_exchanger_warranty' => $request->heat_exchanger_warranty,
                'compressor_warranty' => $request->compressor_warranty,
                'seer' => $request->seer,
                'eer' => $request->eer,
                'hspf' => $request->hspf,
                'afue' => $request->afue,
                'sensible_cooling' => $request->sensible_cooling,
                'tax_credit' => $request->tax_credit,
                'energy_star' => $request->energy_star,
                'utility_rebate' => $request->utility_rebate,
                'ahri' => $request->ahri,
                'rw_system_id' => $request->rw_system_id,
                'coil_id' => $request->coil_id,
                'furnace_id' => $request->furnace_id,
                'condenser_id' => $request->condenser_id,
                'stages_of_cooling' => $request->stages_of_cooling,
                'stages_of_heating' => $request->stages_of_heating,
                'order_number' => $request->order_number                
        ]);

        //$systems = Systems::create($request->all());
        Session::flash('success', 'System updated successfully.');
        return redirect('/unit-list?unit_type=system');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function destroy(Request $request)
    {
        $obj = System::find($request->id);
        $obj->forceDelete();
        return Redirect::to( '/unit-list?unit_type=system' );
    }
}
