<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function barchart(Request $request) 
    {
    	$currentYear = $request->cy;
        $user_id = $request->uid;
		return view('charts.bar-chart')->with(['year' => $currentYear, 'userID' => $user_id]);
    }
    public function table(Request $request) 
    {
        $currentYear = $request->cy;
        $user_id = $request->uid;
        return view('charts.table')->with(['year' => $currentYear, 'userID' => $user_id]);
    }

    public function linechart(Request $request) 
    {
    	$currentYear = $request->cy;
        $user_id = $request->uid;
		return view('charts.line-chart')->with(['year' => $currentYear, 'userID' => $user_id]);
    }

//lead

    

     public function bchartlead(Request $request) 
    {
        $currentYear = $request->cy;
        $user_id = $request->uid;
        return view('charts.bar-chart-lead')->with(['year' => $currentYear, 'userID' => $user_id]);
    }

}
