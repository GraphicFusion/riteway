<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thermostat;
use Session;
use Redirect; 
use DB;

class ThermostatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !$request->wifi ){
            $request->wifi = (int) 0;
        }
        if( !$request->touchscreen ){
            $request->touchscreen = (int) 0;
        }
        if( !$request->programmable ){
            $request->programmable = (int) 0;
        }

        $this->validate($request, [
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            // 'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $imageName = "";
        if ($_FILES['image']['size']>0) {
             $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
        $input = $request->all();
        $input['image'] = $imageName;
        $coil = Thermostat::create($input);
        Session::flash('success', 'New thermostats added successfully.');
        return redirect('/unit-list?unit_type=thermostat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if( !$request->wifi ){
            $request->wifi = (int) 0;
        }
        if( !$request->touchscreen ){
            $request->touchscreen = (int) 0;
        }
        if( !$request->programmable ){
            $request->programmable = (int) 0;
        }

        if ($_FILES['image']['size']>0) {
      /*   $this->validate($request, [
            'brand' => '',
            'model_number' => '',
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
          // 'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:250|dimensions:max_width=250,max_height=250',
        ]);
        */
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $imageNamepath = $request->image->move(public_path('images'), $imageName);
        DB::table('thermostats')
            ->where('id', $request->ThermostatsID)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'wifi' => $request->wifi,
                       'touchscreen' => $request->touchscreen,
                       'programmable' => $request->programmable,
                       'markup' => $request->markup,
                        'divide' => $request->divide,
                       'image' => $imageName,
                       'order_number' => $request->order_number

            ]);
        Session::flash('success', 'Thermostats updated successfully.');
        return redirect('/unit-list?unit_type=thermostat');
    }
    else
    {
       /*  $this->validate($request, [
            'brand' => '',
            'model_number' => '',
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            
        ]);*/
        DB::table('thermostats')
            ->where('id', $request->ThermostatsID)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'wifi' => $request->wifi,
                       'touchscreen' => $request->touchscreen,
                       'programmable' => $request->programmable,
                       'markup' => $request->markup,
                       'divide' => $request->divide,    
                       'order_number' => $request->order_number        

                     ]);
        Session::flash('success', 'Thermostats updated successfully.');
        return redirect('/unit-list?unit_type=thermostat');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
    {
        $obj = Thermostat::find($request->id);
        $obj->forceDelete();
        return Redirect::to( '/unit-list?unit_type=thermostat' );
    }
}