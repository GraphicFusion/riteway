<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Furnace;
use Session;
use Redirect; 
use DB;

class FurnaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !$request->filter_base ){
            $request->filter_base = (int) 0;
        }

        $this->validate($request, [
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $imageName = "";
        if ($_FILES['image']['size']>0) {
           $imageName = time().'.'.$request->image->getClientOriginalExtension();
          $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
         $input = $request->all();
         $input['image'] = $imageName;
        $furnace = Furnace::create($input);
        Session::flash('success', 'New furnace added successfully.');
        return redirect('/unit-list?unit_type=furnace');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {       
        if( !$request->filter_base ){
            $request->filter_base = (int) 0;
        }

        if ($_FILES['image']['size']>0) {
  $this->validate($request, [
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',         
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $imageNamepath = $request->image->move(public_path('images'), $imageName);
        DB::table('furnaces')
            ->where('id', $request->FurnaceID)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'heat_exchanger_warranty' => $request->heat_exchanger_warranty,
                       'filter_base' => $request->filter_base,
                       'afue' => $request->afue,
                       'blower_type' => $request->blower_type,
                       'blower_size' => $request->blower_size,
                       'stages' => $request->stages,
                       'capacity' => $request->capacity,
                       'furnace_air_handler' => $request->furnace_air_handler,
                       'markup' => $request->markup,
                       'divide' => $request->divide,
                       'image' => $imageName,
                       'order_number' => $request->order_number


                     ]);
        Session::flash('success', 'Furnaces updated successfully.');
        return redirect('/unit-list?unit_type=furnace');
    }
    else
    {
      $this->validate($request, [
            'price' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'depth' => 'numeric',
            'parts_warranty' => 'numeric',
            'labor_warranty' => 'numeric',
        ]);
         DB::table('furnaces')
            ->where('id', $request->FurnaceID)
            ->update([ 'brand' => $request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'heat_exchanger_warranty' => $request->heat_exchanger_warranty,
                       'filter_base' => $request->filter_base,
                       'afue' => $request->afue,
                       'blower_type' => $request->blower_type,
                       'blower_size' => $request->blower_size,
                       'stages' => $request->stages,
                       'capacity' => $request->capacity,
                       'furnace_air_handler' => $request->furnace_air_handler,
                       'markup' => $request->markup,
                       'divide' => $request->divide,
                       'order_number' => $request->order_number
                     ]);
        Session::flash('success', 'Furnaces updated successfully.');
        return redirect('/unit-list?unit_type=furnace');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
    {
        $obj = Furnaces::find($request->id);
        $obj->forceDelete();
        return Redirect::to( '/unit-list?unit_type=furnace' );
    }
}
