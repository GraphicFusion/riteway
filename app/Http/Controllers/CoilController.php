<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coil;
use Session;
use Redirect; 
use DB;


class CoilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'price' => 'numeric|max:1000000',
            'width' => 'numeric|max:240',
            'height' => 'numeric|max:240',
            'depth' => 'numeric|max:240',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:250|dimensions:max_width=250,max_height=250',
        ]);
        /*Image Upload*/ 
        $imageName = "";
        if ($_FILES['image']['size']>0) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $imageNamepath = $request->image->move(public_path('images'), $imageName);
        }
                  /*Image Upload*/ 
        $input = $request->all();
        $input['image'] = $imageName;
        $coil = Coil::create($input);
        Session::flash('success', 'New coil added successfully.');
        return redirect('/unit-list?unit_type=coil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
      $Coil = Coil::find($_POST['uid']);
      return response()->json(['Coil' => $Coil], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {


            //$coil = Coil::update($request->all());
            /*Image Upload*/ 
        if ($_FILES['image']['size']>0) {
                $this->validate($request, [
                'brand' => 'required',
               // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:250|dimensions:max_width=250,max_height=250',
            ]);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $imageNamepath = $request->image->move(public_path('images'), $imageName);
        /*Image Upload*/ 
            DB::table('coils')
            ->where('id', $request->updateid)
            ->update(['brand' =>$request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'orientation' => $request->orientation,
                       'markup' => $request->markup,
                       'divide' => $request->divide,
                       'image' => $imageName,
                       'order_number' => $request->order_number
                     ]);
            Session::flash('success', 'coil updated successfully.');
            return redirect('/unit-list?unit_type=coil');
        }
        else
        {
            /*Image Upload*/ 
            $this->validate($request, [
                'brand' => 'required',
                'model_number' => 'required',
               
            ]);
            DB::table('coils')
            ->where('id', $request->updateid)
            ->update(['brand' =>$request->brand,
                       'model_number' => $request->model_number,
                       'price' => $request->price,
                       'width' => $request->width,
                       'height' => $request->height,
                       'depth' => $request->depth,
                       'parts_warranty' => $request->parts_warranty,
                       'labor_warranty' => $request->labor_warranty,
                       'orientation' => $request->orientation,
                       'markup' => $request->markup,
                        'divide' => $request->divide,
                        'order_number' => $request->order_number

                       
                     ]);
            Session::flash('success', 'coil updated successfully.');
            return redirect('/unit-list?unit_type=coil');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $coil = Coil::find($request->id);
        $coil->forceDelete();
        return Redirect::to( '/unit-list?unit_type=coil' );
    }
}
