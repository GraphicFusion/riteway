<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coil extends Model
{
    protected $table = 'coils';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'brand',
        'model_number',
        'price',
        'width',
        'height',
        'depth',
        'parts_warranty',
        'labor_warranty',
        'orientation',
        'markup',
        'divide',
        'image',
        'order_number'
    ];
}