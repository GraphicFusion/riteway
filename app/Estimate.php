<?php

namespace App;

use App\Proposal;
use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
//    protected $appends = array('number','gold','bronze','silver','platinum');
    protected $appends = array('number');

	/**
	* Get the Proposal for the Estimate.
	*/
	public function proposal()
	{
		return $this->belongsTo('App\Proposal');
	}

	/**
	* Get Number.
	*/
	 public function getNumberAttribute()
	{
		$proposal = Proposal::find($this->proposal_id);
		$i = 1;
		foreach( $proposal->estimates as $key => $estimate ){
			if( $estimate->id == $this->id ){
				return $i;
				exit;
			}
			$i++;
		}
	}
}
