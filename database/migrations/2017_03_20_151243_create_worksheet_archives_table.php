<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksheetArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worksheet_archives', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('worksheet_id');
            $table->bigInteger('version');
            $table->bigInteger('group_id')->nullable();
            $table->string('type')->nullable();
            $table->text('worksheet_object')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worksheetarchive');
    }
}
