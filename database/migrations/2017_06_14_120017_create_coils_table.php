<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coils', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand')->nullable()->default(null);
            $table->string('model_number')->nullable()->default(null);
            $table->float('price', 8, 2)->nullable()->default(null);
            $table->float('width', 8, 2)->nullable()->default(null);
            $table->float('height', 8, 2)->nullable()->default(null);
            $table->float('depth', 8, 2)->nullable()->default(null);
            $table->integer('parts_warranty')->nullable()->default(null);
            $table->integer('labor_warranty')->nullable()->default(null);
            $table->string('orientation')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coils');
    }
}
