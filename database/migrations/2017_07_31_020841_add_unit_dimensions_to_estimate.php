<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitDimensionsToEstimate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estimates', function($table) {
            $table->float('unit_available_width')->nullable()->default(NULL);
            $table->float('coil_available_width')->nullable()->default(NULL);
            $table->float('available_height')->nullable()->default(NULL);
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
