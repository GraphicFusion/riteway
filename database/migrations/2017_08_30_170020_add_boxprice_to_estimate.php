<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoxpriceToEstimate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estimates', function($table) {
            $table->float('platinum_box')->nullable()->default(NULL);
            $table->float('silver_box')->nullable()->default(NULL);
            $table->float('gold_box')->nullable()->default(NULL);
            $table->float('bronze_box')->nullable()->default(NULL);
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
