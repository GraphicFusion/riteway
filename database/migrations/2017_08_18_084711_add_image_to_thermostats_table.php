<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToThermostatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('condensers', function($table) {
            $table->string('image')->nullable();
        });
        Schema::table('thermostats', function($table) {
            $table->string('image')->nullable();
        });
        Schema::table('furnaces', function($table) {
            $table->string('image')->nullable();
        });
        Schema::table('packages', function($table) {
            $table->string('image')->nullable();
        });
        Schema::table('systems', function($table) {
            $table->string('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
