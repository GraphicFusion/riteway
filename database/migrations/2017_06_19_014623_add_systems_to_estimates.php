<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemsToEstimates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('estimates', function($table) {
	        $table->float('platinum_system_id')->nullable()->default(NULL);
	        $table->float('gold_system_id')->nullable()->default(NULL);
	        $table->float('silver_system_id')->nullable()->default(NULL);
	        $table->float('bronze_system_id')->nullable()->default(NULL);
	    });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
