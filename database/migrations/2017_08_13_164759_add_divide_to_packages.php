<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDivideToPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function($table) {
            $table->float('divide')->nullable()->default(NULL);
        });        
        Schema::table('coils', function($table) {
            $table->float('divide')->nullable()->default(NULL);
        });        
        Schema::table('condensers', function($table) {
            $table->float('divide')->nullable()->default(NULL);
        });        
        Schema::table('furnaces', function($table) {
            $table->float('divide')->nullable()->default(NULL);
        });        
        Schema::table('splits', function($table) {
            $table->float('divide')->nullable()->default(NULL);
        });        
        Schema::table('thermostats', function($table) {
            $table->float('divide')->nullable()->default(NULL);
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
