<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parts_warranty');
            $table->integer('labor_warranty');
            $table->integer('heat_exchanger_warranty');
            $table->integer('compressor_warranty');
            $table->float('seer', 8, 2);
            $table->float('eer', 8, 2);
            $table->float('hspf', 8, 2);
            $table->float('afue', 8, 2);
            $table->float('sensible_cooling', 8, 2);
            $table->boolean('tax_credit');
            $table->boolean('energy_star');
            $table->boolean('utility_rebate');
            $table->integer('ahri');
            $table->string('rw_system_id');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
