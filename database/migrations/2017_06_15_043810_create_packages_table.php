<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand')->nullable()->default(null);
            $table->string('model_number')->nullable()->default(null);
            $table->float('platinum_price', 8, 2)->nullable()->default(null);
            $table->float('silver_price', 8, 2)->nullable()->default(null);
            $table->float('gold_price', 8, 2)->nullable()->default(null);
            $table->float('bronze_price', 8, 2)->nullable()->default(null);
            $table->float('width', 8, 2)->nullable()->default(null);
            $table->float('height', 8, 2)->nullable()->default(null);
            $table->float('depth', 8, 2)->nullable()->default(null);
            $table->integer('parts_warranty')->nullable()->default(null);
            $table->integer('labor_warranty')->nullable()->default(null);
            $table->integer('heat_exchanger_warranty')->nullable()->default(null);
            $table->integer('compressor_warranty')->nullable()->default(null);
            $table->float('seer', 8, 2)->nullable()->default(null);
            $table->float('eer', 8, 2)->nullable()->default(null);
            $table->float('hspf', 8, 2)->nullable()->default(null);
            $table->float('afue', 8, 2)->nullable()->default(null);
            $table->float('weight', 8, 2)->nullable()->default(null);
            $table->string('stages_of_cooling')->nullable()->default(null);
            $table->string('stages_of_heating')->nullable()->default(null);
            $table->string('blower_type')->nullable()->default(null);
            $table->float('capacity_for_cooling', 8, 2)->nullable()->default(null);
            $table->float('capacity_for_heating', 8, 2)->nullable()->default(null);
            $table->float('sensible_cooling', 8, 2)->nullable()->default(null);
            $table->boolean('tax_credit')->nullable()->default(null);
            $table->boolean('energy_star')->nullable()->default(null);
            $table->boolean('utility_rebate')->nullable()->default(null);
            $table->integer('ahri')->nullable()->default(null);
            $table->string('hp_ac')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
