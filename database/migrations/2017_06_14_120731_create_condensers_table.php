<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondensersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condensers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand')->default(null)->nullable();
            $table->string('model_number')->default(null)->nullable();
            $table->float('price', 8, 2)->default(null)->nullable();
            $table->float('width', 8, 2)->default(null)->nullable();
            $table->float('height', 8, 2)->default(null)->nullable();
            $table->float('depth', 8, 2)->default(null)->nullable();
            $table->integer('parts_warranty')->nullable()->default(null);
            $table->integer('labor_warranty')->nullable()->default(null);
            $table->integer('compressor_warranty')->nullable()->default(null);
            $table->string('hp_ac')->default('HP');
            $table->string('stages')->default('1');
            $table->float('capacity', 8, 2)->default(null)->nullable();
            $table->string('picture')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condensers');
    }
}
