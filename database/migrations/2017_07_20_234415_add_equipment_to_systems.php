<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEquipmentToSystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('systems', function($table) {
            $table->integer('thermostat_id')->unsigned()->nullable();
            $table->integer('coil_id')->unsigned()->nullable();
            $table->integer('furnace_id')->unsigned()->nullable();
            $table->integer('condenser_id')->unsigned()->nullable();
	    });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
