<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderNumberFieldToUnitListTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coils', function($table) {
            $table->string('order_number')->nullable();
        });      
        Schema::table('condensers', function($table) {
            $table->string('order_number')->nullable();
        });      
        Schema::table('thermostats', function($table) {
            $table->string('order_number')->nullable();
        });      
        Schema::table('furnaces', function($table) {
            $table->string('order_number')->nullable();
        });      
        Schema::table('packages', function($table) {
            $table->string('order_number')->nullable();
        });      
        Schema::table('systems', function($table) {
            $table->string('order_number')->nullable();
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
