<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricesToEstimate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('estimates', function($table) {
	        $table->float('platinum')->nullable()->default(NULL);
	        $table->float('gold')->nullable()->default(NULL);
	        $table->float('silver')->nullable()->default(NULL);
	        $table->float('bronze')->nullable()->default(NULL);
	    });        
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
