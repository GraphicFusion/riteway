<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationToUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('condensers', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
        Schema::table('coils', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
        Schema::table('furnaces', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
        Schema::table('packages', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
        Schema::table('splits', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
        Schema::table('thermostats', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
        Schema::table('systems', function($table) {
            $table->text('location')->nullable()->default(NULL);
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
