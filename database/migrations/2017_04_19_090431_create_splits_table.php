<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSplitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('splits', function (Blueprint $table) {
            $table->increments('id');

            $table->string('model', 255);
            $table->string('brand', 255);
            $table->string('pics', 255);
            $table->string('condenser', 255);
            $table->string('filter_base', 255);
            $table->string('furnace', 255);
            $table->string('coil', 255);
            $table->string('cu', 255);
            $table->string('furn_ah', 255);
            $table->string('price', 255);
            $table->string('size', 255);
            $table->string('seer', 255);
            $table->string('eer', 255);
            $table->string('hspf', 255);
            $table->string('afue', 255);
            $table->string('vs', 255);
            $table->string('fw', 255);
            $table->string('fh', 255);
            $table->string('cw', 255);
            $table->string('ch', 255);
            $table->string('tot_w', 255);
            $table->string('tot_h', 255);
            $table->string('cu_w', 255);
            $table->string('cu_d', 255);
            $table->string('cu_h', 255);
            $table->string('c_stage', 255);
            $table->string('ht_stage', 255);
            $table->string('cu_amp', 255);
            $table->string('f_ah_amp', 255);
            $table->string('compressor', 255);
            $table->string('ht_exch', 255);
            $table->string('parts', 255);
            $table->string('labor', 255);
            $table->string('stat', 255);
            $table->string('capacity', 255);
            $table->string('gaurds', 255);
            $table->string('ari', 255);
            $table->string('high', 255);
            $table->string('low', 255);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('splits');
    }
}
