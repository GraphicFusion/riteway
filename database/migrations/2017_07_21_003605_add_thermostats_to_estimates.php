<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThermostatsToEstimates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('estimates', function($table) {
	        $table->integer('platinum_thermostat_id')->nullable()->default(NULL);
	        $table->integer('gold_thermostat_id')->nullable()->default(NULL);
	        $table->integer('silver_thermostat_id')->nullable()->default(NULL);
	        $table->integer('bronze_thermostat_id')->nullable()->default(NULL);
	    });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
