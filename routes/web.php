<?php
use App\Estimate;
use App\Proposal;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//use \SanitizeMiddleware;
Auth::routes();

//Route::get('/home', 'HomeController@index');
/*Route::get('/home', function () {
	if(Auth::check() && !Auth::user()->subscribed()) {
	    return view('subscribe');
	}
//	return view('dashboard');
	return view('opportunities');
});

*/
// Logged in Access
Route::group(['middleware' => ['auth']], function(){

	Route::get('/home', function () {
		if(Auth::check() && !Auth::user()->subscribed()) {
//		    return view('subscribe');
		}
//		return view('dashboard');
		return view('opportunities');
	});

	Route::get('/', function () {
		return view('opportunities');
	});


	 Route::get('/furnace', [ function() {
	 	$type = array(
	 		"id"=>"furnace", 
	 		"name"=>"Furnace"
	 	);
	 	return view('worksheets/worksheet', ["type"=>$type]);
	 }]);
	Route::get('/split', [ function() {
		$type = array(
			"id"=>"split", 
			"name"=>"Split"
		);
		return view('worksheets/worksheet', ["type"=>$type]);
	}]);
	
	Route::get('/package', [ function() {
		$type = array(
			"id"=>"package", 
			"name"=>"Package"
		);
		return view('worksheets/worksheet', ["type"=>$type]);
	}]);

	Route::get('/customers', [ function() {
	    return view('customers');
	}]);
	Route::get('/customer/{id}', [ function($id) {
	    return view('customer', ['id' => $id]);
	}]);
	Route::get('/new-customer-proposal/{type}/{id}', 'WorksheetController@newProposal');

	Route::get('/proposals', [ function() {
	    return view('opportunities');
	}]);
	Route::post('/chosen-estimate', 'ProposalController@chosen_estimate' );
	Route::post('/chosen-plan', 'EstimateController@chosen_plan' );

	Route::post('/proposal-save', 'ProposalController@saveInfo' );
	Route::post('/opportunity-delete', 'ProposalController@destroy' );
	Route::post('/estimate-delete', 'EstimateController@destroy' );

	Route::post('/customer', 'WorksheetController@storeCustomer');
	
	// Route::post('/furnace', 'FurnaceController@store');
	Route::post('/worksheet', 'WorksheetController@store');
	Route::post('/new-member', 'UserController@store');

	Route::post('/unit', 'UnitController@store');


	Route::post('/unit-list/package', 'PackageController@postPackage');
	Route::post('/unit-list/split', 'SplitController@postSplit');
	Route::get('/team/edit/{id}', 'TeamController@show');
	Route::post('/team/update', 'TeamController@update');

	Route::get('/team/delete/{id}', 'TeamController@showDel');
	Route::post('/team/delete', 'TeamController@deleteMember');

	Route::post('/estimate-email', 'ProposalController@emailEstimate' );

});

// ALL ADMIN ACCESS

Route::group(['middleware' => ['admin']], function(){
	Route::get('/dashboard', [function() {
		return view('opportunities');
	}]);
	Route::get('/team', [ function() {
	    return view('team');
	}]);
	Route::get('/performance', [ function() {
	    return view('performance');
	}]);
	Route::get('/unit-list', [ function() {
	    return view('unit-list');
	}]);
	Route::get('/worksheets', [ function() {
	    return view('worksheets');
	}]);
	 Route::get('/edit-furnace', [ function() {
	 	$type = array(
	 		"id"=>"furnace", 
	 		"name"=>"Furnace"
	 	);
	 	return view('worksheets/edit-worksheet', ["type"=>$type]);
	 }]);
	
	Route::get('/edit-split', [ function() {
		$type = array(
			"id"=>"split", 
			"name"=>"Split"
		);
		return view('worksheets/edit-worksheet', ["type"=>$type]);
	}]);
	
	Route::get('/edit-package', [ function() {
		$type = array(
			"id"=>"package", 
			"name"=>"Package"
		);
		return view('worksheets/edit-worksheet', ["type"=>$type]);
	}]);

	//SETTINGS
	Route::get('/settings', [ function() {
		return view('settings', ["navigation"=>'settings']);
	}]);
	Route::get('/settings/lead-generators', [ function() {
		return view('settings/lead-generators', ["navigation"=>'settings']);
	}]);
	Route::get('/settings/lead-sources', [ function() {
		return view('settings/lead-sources', ["navigation"=>'settings']);
	}]);
	Route::post('/source', 'SourceController@store');
	Route::post('/new-leadgen', 'UserController@storeLeadgen');

	Route::get('/settings/lead-generators/delete/{id}','UserController@getLead');
	Route::post('/settings/lead-generators/delete','UserController@deleteLead');

	//end test page
});

Route::get('/users/{id?}', ['middleware' => ['admin'], function($id) {
    return view('users', ["id"=>$id]);
}]);
Route::post('/change-group', 'UserController@changeGroup');

// Performance page chart ajax load
Route::post('/performance/table', 'AjaxController@table')->middleware('ajax');
Route::post('/performance/bchart', 'AjaxController@barchart')->middleware('ajax');
Route::post('/performance/lchart', 'AjaxController@linechart')->middleware('ajax');
Route::post('/performance/bchartlead', 'AjaxController@bchartlead')->middleware('ajax');


// Subscription
Route::get('/subscribe', 'SubscribeController@index');
Route::post('/subscribe', 'SubscribeController@create');

Route::get('/subscribe/all-plans', 'SubscribeController@getAllPlans');
Route::get('/subscribe/checkplan/{id}', 'SubscribeController@checkPlan');


//Unit list tabs
Route::post('/new-coil', 'CoilController@store')->middleware('sanitize');
Route::post('/delete-coil', 'CoilController@destroy');
Route::post('/edit-coil', 'CoilController@edit')->middleware('ajax');
Route::post('/update-coil', 'CoilController@update');

Route::post('/new-condenser', 'CondenserController@store')->middleware('sanitize');
Route::post('/delete-condenser', 'CondenserController@destroy');
Route::post('/edit-condenser', 'CondenserController@edit')->middleware('ajax');
Route::post('/update-condenser', 'CondenserController@update');

Route::post('/new-thermostat', 'ThermostatController@store')->middleware('sanitize');
Route::post('/delete-thermostat', 'ThermostatController@destroy');
Route::post('/update-thermostat', 'ThermostatController@update');

Route::post('/new-furnace', 'FurnaceController@store')->middleware('sanitize');
Route::post('/delete-furnace', 'FurnaceController@destroy');
Route::post('/update-furnace', 'FurnaceController@update');

Route::post('/new-package', 'PackageController@store')->middleware('sanitize');
Route::post('/delete-package', 'PackageController@destroy');
Route::post('/update-package', 'PackageController@update');

Route::post('/new-systems', 'SystemController@store')->middleware('sanitize');
Route::post('/delete-system', 'SystemController@destroy');
Route::post('/update-system', 'SystemController@update');
	Route::get('/customer-estimate', [ function() {
	    return view('no-access');
   }]);
	Route::get('/opportunities', [ function() {
		if(Auth::check()){
		    return view('opportunities');
		}
		else{	
			if(array_key_exists('present', $_GET)){
				$estimate_id = $_GET['present'];
				$estimate = App\Estimate::find($estimate_id);
				$proposal = App\Proposal::find($estimate->proposal_id);
				$customer = $proposal->customer;
		//	print_r($customer);
				$loc = $proposal->location;
		//		print_r($proposal);
		//		print_r($estimate);

				// check for cookie
				if(array_key_exists('riteway_estimate_id', $_COOKIE) && $_COOKIE['riteway_estimate_id'] == $estimate_id){
				    return view('opportunities');
				}
				else{
				    return view('customer-estimate');
				}
			}
			else{
				return view('no-access');
			}
		}
	}]);
	Route::get('/estimates', [ function() {
		if(Auth::check()){
		    return view('estimate');
		}
		else{	
			if(array_key_exists('present', $_GET)){
				$estimate_id = $_GET['present'];
				$estimate = App\Estimate::find($estimate_id);
				$proposal = App\Proposal::find($estimate->proposal_id);
				$customer = $proposal->customer;
				$loc = $proposal->location;
				// check for cookie
				if(array_key_exists('riteway_estimate_id', $_COOKIE) && $_COOKIE['riteway_estimate_id'] == $estimate_id){
				    return view('estimate');
				}
				else{
				    return view('customer-estimate');
				}
			}
			else{
				return view('no-access');
			}
		}
	}]);
	Route::post('/customer-presentation', [ function() {
		if(array_key_exists('estimate_id', $_POST)){
		print_r($_POST);
			$estimate_id = $_POST['estimate_id'];
			$email = $_POST['email'];
			$estimate = App\Estimate::find($estimate_id);
			$proposal = App\Proposal::find($estimate->proposal_id);
			$customer = $proposal->customer;
	//	print_r($customer);
			$loc = $proposal->location;
	//		print_r($proposal);
	//		print_r($estimate);
			if( strtolower($email) == strtolower($customer->email)  ){
				setcookie("riteway_estimate_id", $estimate_id, time()+3600);  /* expire in 1 hour */
				// return view
				$newUrl = "/opportunities/?present=".$estimate_id;
				header('Location: '.$newUrl);
			}
		}

	}]);

	Route::get('testemail', function(){
		Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
		{
			$message->to('wjgrass@gmail.com');
		});
		return 'success!!!. its working';
	});

	Route::get('testemail1', function(){
		Mail::raw('Sending test emails with Mailgun!', function($message)
		{
			$message->to('manikandan.m@pickzy.com');
		});
		return 'working';
	});



	Route::get('sendemail', function(){
	Mail::raw('Testing email with mailgun', function($message)
	{
		$message->subject('laravel mailgun setup');
		$message->from('manikandancty@gmail.com','Pickzy');
		$message->to('wjgrass@gmail.com');
	});
	return 'Its working';
	});

	Route::get('/email-estimate', function (){
	$estimate_id = 387;//$_REQUEST['estimate_id'];
	$estimate = Estimate::find($estimate_id);
   	$proposal = Proposal::find($estimate->proposal_id);
   	$customer = $proposal->customer;
   	$salutation = $customer->first_name. " " .$customer->last_name;
            $data = [
                'text' => "this is test"
            ];
            $subject = "Your Rite Way Proposal";
            Mail::send('warehouse-email', $data, function ($message) use ($subject) {
                $message->from('mailgun@riteway.sonderdev.com', 'trip');
              //$message->subject($subject);
               //$message->to($customer->email);
                $message->to('wjgrass@gmail.com', 'wjgrass');
                //$message->cc('MAuman@ritewayac.com','Michael Auman');
                //$message->bcc('MAuman@ritewayac.com','Michael Auman');
                //$message->bcc('wjgrass@gmail.com','Grass');
               // $message->setBody($text, 'text/html'); 

            });
                         

            
});

