<?php

use Illuminate\Http\Request;
use App\Estimate;
use App\Proposal;
use App\Group;
use App\User;
use App\WorksheetArchive;
use App\Mail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

use App\Worksheet;
Route::get('/worksheet', function (Request $request) {
	$worksheet = "";//$_REQUEST['worksheet'];
	$group_id = 1;
//print_r($_REQUEST);
	$type = 'furnace';
	$worksheet = Worksheet::firstOrNew( array('group_id' => $group_id, 'type' => $type));
//	$worksheet->group_id = 1;
//	$worksheet->type = 'furnace';
	$worksheet->worksheet_object = json_encode($worksheet);
	$worksheet->save();
});
Route::post('/worksheet', function (Request $request) {

	$worksheet_obj = $_REQUEST['worksheet'];
	$group_id = $_REQUEST['group_id'];
	$type = $_REQUEST['type'];
	$worksheet = Worksheet::firstOrNew( array('group_id' => $group_id, 'type' => $type));
	if( !$worksheet->version ){
		$worksheet->version = 1;
		$worksheet->save();
//echo "pppp";
	}
	// get all estimates in this group where version number is same as this; if it returns one, save version;
	$group = Group::find( $group_id );
// this needs to be a custom query for speed
	$proposals = $group->proposals->where('status','=','pending');
	$group_estimates = [];
	$archive = 0;
	foreach( $proposals as $proposal ){
		$estimates = $proposal->estimates->where('worksheet_version','=',$worksheet->version );
	//print_r($estimates);

		if( count( $estimates ) ){
			$archive = 1;
			//break;
		}

	}

	//dd(9);
	if( $archive ){
		// archive this worksheet; increment current worksheet version

		$worksheetArchive = new WorksheetArchive;

		$worksheetArchive->version = $worksheet->version;
		//$worksheetArchive->version = 1;
		$worksheetArchive->worksheet_id = $worksheet->id;
		//$worksheetArchive->worksheet_id = 2;
		$worksheetArchive->group_id = $worksheet->group_id;
		$worksheetArchive->type = $worksheet->type;
		$worksheetArchive->worksheet_object = json_encode($worksheet_obj);
		$worksheetArchive->save();

		$worksheet->version++;
		$worksheet->save();
	}

	$estimates = Estimate::where('group_id','=',$group_id)->where('worksheet_version','=',$worksheet->version); 
	$worksheet->group_id = $group_id;
	$worksheet->type = $type;

	$worksheet->worksheet_object = json_encode($worksheet_obj);
//print_r($worksheet);
	$worksheet->save();

});
Route::post('/email-estimate', function (Request $request) {
		$estimate_id = 387;//$_REQUEST['estimate_id'];
	$estimate = Estimate::find($estimate_id);
   	$proposal = Proposal::find($estimate->proposal_id);
   	$customer = $proposal->customer;
   	$salutation = $customer->first_name. " " .$customer->last_name;
            $data = [
                'text' => "this is test"
            ];
            $subject = "Your Rite Way Proposal";
            Mail::send('warehouse-email', $data, function ($message) use ($subject) {
                $message->from('mailgun@riteway.sonderdev.com', 'trip');
              //$message->subject($subject);
               //$message->to($customer->email);
                $message->to('wjgrass@gmail.com', 'wjgrass');
                //$message->cc('MAuman@ritewayac.com','Michael Auman');
                //$message->bcc('MAuman@ritewayac.com','Michael Auman');
                //$message->bcc('wjgrass@gmail.com','Grass');
               // $message->setBody($text, 'text/html'); 

            });
                         
/*	$estimate_id = $_REQUEST['estimate_id'];
	$estimate = Estimate::find($estimate_id);
   	$proposal = Proposal::find($estimate->proposal_id);
   	$customer = $proposal->customer;
   	$salutation = $customer->first_name. " " .$customer->last_name;
            $data = [
                'text' => "
            <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                <html xmlns='http://www.w3.org/1999/xhtml'>
                    <p>Dear ".$salutation."</p>,
					<p>Thank you for allowing Rite Way to consult with you on your new heating and cooling system.</p>
					<p>To see your estimate, please click this link:   <a href='riteway.sonderdev.com/opportunities/?present=".$estimate_id."'>riteway.sonderdev.com/opportunities/?present=".$estimate_id."</a></p>
					<p>To gain access please enter your email or your username created for you by your Rite Way project manager.</p>
					<p>We look forward to partnering with you and are confident that later you will look back and know that working with Rite Way was the right decision. Please contact us with any questions and we'll be happy to answer them for you.</p>

					<p>Respectfully,
						Rite Way Heating, Cooling & Plumbing</p>"
            ];
            $subject = "Your Rite Way Proposal";
           // Mail::send('warehouse-email', $data, function ($message) use ($subject) {
               // $message->from('mailgun@riteway.sonderdev.com', 'trip');
              //$message->subject($subject);
               //$message->to($customer->email);
                //$message->to('wjgrass@gmail.com', 'wjgrass');
                //$message->cc('MAuman@ritewayac.com','Michael Auman');
                //$message->bcc('MAuman@ritewayac.com','Michael Auman');
                //$message->bcc('wjgrass@gmail.com','Grass');
               // $message->setBody($text, 'text/html'); 

            //});
  */                       

            
});

Route::post('/estimate-prices', function (Request $request) {
	$estimate = Estimate::find($request->estimate_id);
	$presentation_prices = array('this'=>'that');
	$estimate->presentation_prices = json_encode($request->presentation_prices);
	$estimate->notes = json_encode($request->notes);
//	print_r('end of ajax');
//	print_r($estimate);
	$estimate->save();
});

Route::post('/estimate', function (Request $request) {
	$estimate_obj = $_REQUEST['estimate'];
//print_r($estimate_obj);
	$estimate_id = $_REQUEST['estimate_id'];
	$worksheet_id = $_REQUEST['worksheet_id'];
	$version = $_REQUEST['version'];
	if( array_key_exists('presentation', $_REQUEST ) ){
		$presentation = $_REQUEST['presentation'];
	}
	else{
		$presentation = "";
	}
	if( array_key_exists('job_sheet', $_REQUEST ) ){
		$job_sheet = $_REQUEST['job_sheet'];
	}
	else{
		$job_sheet = "";
	}
	if( array_key_exists('includes', $_REQUEST ) ){
		$includes = $_REQUEST['includes'];
	}
	else{
		$includes = "";
	}
	$estimate = Estimate::firstOrNew( array('id' => $estimate_id ));
	$estimate->worksheet_data = json_encode($estimate_obj);
	$estimate->includes = json_encode($includes);
	$prices = $estimate_obj['prices'];
	$systems = $estimate_obj['systems'];
	$thermostats = $estimate_obj['thermostats'];
	
	$estimate->presentation = json_encode($presentation);
	$estimate->job_sheet = json_encode($job_sheet);
//print_r($estimate);
	$estimate->unit_available_width = $_REQUEST['unit_available_width'];
	$estimate->coil_available_width = $_REQUEST['coil_available_width'];
	$estimate->available_height = $_REQUEST['available_height'];
	$estimate->ac_hp = $_REQUEST['ac_hp'];
	$estimate->capacity = $_REQUEST['capacity'];
	$estimate->new_replace = $_REQUEST['new_replace'];

	$estimate->job_labor_total =  $estimate_obj['job_labor_total'];


	$estimate->platinum = $prices['platinum'];
	$estimate->platinum_box = $prices['platinum_box'];
	$estimate->gold = $prices['gold'];
	$estimate->gold_box = $prices['gold_box'];
	$estimate->silver = $prices['silver'];
	$estimate->silver_box = $prices['silver_box'];
	$estimate->bronze = $prices['bronze'];
	$estimate->bronze_box = $prices['bronze_box'];
	$estimate->platinum_system_id = $systems['platinum'];
	$estimate->gold_system_id = $systems['gold'];
	$estimate->silver_system_id = $systems['silver'];
	$estimate->bronze_system_id = $systems['bronze'];
	$estimate->platinum_thermostat_id = $thermostats['platinum'];
	$estimate->gold_thermostat_id = $thermostats['gold'];
	$estimate->silver_thermostat_id = $thermostats['silver'];
	$estimate->bronze_thermostat_id = $thermostats['bronze'];
	$estimate->worksheet_id = $worksheet_id;
	$estimate->worksheet_version = $version;
	$estimate->save();
/*
	// get all estimates in this group where version number is same as this; if it returns one, save version;
	if( @$archive ){
		// archive this worksheet; increment current worksheet version
		$worksheet->version++;
		$worksheet->save();

		$worksheetArchive = new WorksheetArchive;

		//$worksheetArchive->version = $worksheet->version;
		$worksheetArchive->version = 1;
		$worksheetArchive->worksheet_id = $worksheet->id;
		$worksheetArchive->worksheet_id = 2;
		$worksheetArchive->group_id = $worksheet->group_id;
		$worksheetArchive->type = $worksheet->type;
		$worksheetArchive->worksheet_object = $worksheet->worksheet_object;
		$worksheetArchive->save();
	}

	$estimates = Estimate::where('group_id','=',$group_id)->where('worksheet_version','=',$worksheet->version); 
	$worksheet->group_id = $group_id;
	$worksheet->type = $type;

	$worksheet->worksheet_object = json_encode($worksheet_obj);
//print_r($worksheet);
	$worksheet->save();
*/
});

// Get Proposal(s)
Route::get('/GET/{group_id}/proposal/{uri_id}', [ function(Request $request, $group_id, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	$className = 'App\Proposal';
	$modelInstance = new $className();
	
	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->where('group_id','=',$group_id)->find($id);
			$ajaxModels[$obj->id] = buildAjaxProposal( $obj );
		}
		else{
			foreach( $id as $_id){
				$obj = $modelInstance->where('group_id','=',$group_id)->find($_id);
				if( is_object($obj)){
					$ajaxModels[$obj->id] = buildAjaxProposal( $obj );
				}
			}
		}
	}
	else{
		$objs = $modelInstance->all()->where('group_id','=',$group_id);
		foreach( $objs as $obj){
			$ajaxModels[$obj->id] = buildAjaxProposal( $obj );
		}
	}
	echo json_encode($ajaxModels);
}]);

// Get Customer(s)
Route::get('/GET/{group_id}/customer/{uri_id}', [ function(Request $request, $group_id, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	$className = 'App\User';
	$modelInstance = new $className();
	
	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->where('group_id','=',$group_id)->find($id);
			$ajaxModels[$obj->id] = buildAjaxProposal( $obj );
		}
		else{
			foreach( $id as $_id){
				$obj = $modelInstance->where('group_id','=',$group_id)->find($_id);
				if( is_object($obj)){
					$ajaxModels[$obj->id] = buildAjaxProposal( $obj );
				}
			}
		}
	}
	else{		
		$obj = User::whereHas('groups', function($query){
			    $query->whereGroupId(1);  
			    $query->where('role','=','customer');  
			})->get();				
		foreach( $objs as $obj){
			$ajaxModels[$obj->id] = buildAjaxProposal( $obj );
		}
	}
	echo json_encode($ajaxModels);
}]);
Route::get('/GET/{group_id}/planTier/{uri_id}', [ function(Request $request, $group_id) {
	echo json_encode(getPlanTiers());
}]);

// Get Customer(s)
Route::get('/GET/{group_id}/system/{uri_id}', [ function(Request $request, $group_id, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	$className = 'App\System';
	$modelInstance = new $className();
	
	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->find($id);
			//echo "ibject:";
			//sprint_r($obj);
			$ajaxModels[$obj->id] = buildAjaxSystem( $obj );
		}
	}
	else{		
	}
	echo json_encode($ajaxModels);
}]);
Route::get('/GET/{group_id}/package/{uri_id}', [ function(Request $request, $group_id, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	$className = 'App\Package';
	$modelInstance = new $className();
	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->find($id);
			$ajaxModels[$obj->id] = buildAjaxSystem( $obj );
		}
	}
	else{		
	}
	echo json_encode($ajaxModels);
}]);
Route::get('/GET/{group_id}/furnace/{uri_id}', [ function(Request $request, $group_id, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	$className = 'App\Furnace';
	$modelInstance = new $className();
	
	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->find($id);
			$ajaxModels[$obj->id] = buildAjaxSystem( $obj );
		}
	}
	else{		
	}
	echo json_encode($ajaxModels);
}]);
// Get Customer(s)
Route::get('/GET/{group_id}/thermostat/{uri_id}', [ function(Request $request, $group_id, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	$className = 'App\Thermostat';
	$modelInstance = new $className();
	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->find($id);
			$ajaxModels[$obj->id] = buildAjaxThermostat( $obj );
		}
	}
	else{		
	}
	echo json_encode($ajaxModels);
}]);

/* GENERIC ROUTE FOR GETTING SINGLE INSTANCE OR MULTIPLE OR ALL PROPOSALS/ESTIMATES/CUSTOMERS/EQUIPMENT
	/	if no $id, returns array of all (pagination?)
	/	if numeric $id, returns array with single
	/	if array $id, returns array with multiple

	/ buildAjaxProposal, buildAjaxEstimate, etc are defined in helpers.php and are called generically using call_user_func
*/
Route::get('/GET/{group_id}/{type}/{uri_id}', [ function(Request $request, $group_id, $type, $uri_id) {
	$id = json_decode($uri_id);
	$ajaxModels = [];
	
	$className = 'App\\'.ucfirst($type);

	$modelInstance = new $className();

	// check if id is single or array
	if( $id &&(is_numeric($id) || is_array($id))){
		if(is_numeric($id)){
			$obj = $modelInstance->find($id);
			$ajaxModels[$obj->id] = call_user_func('buildAjax'.ucfirst( $type), $obj );
		}
		else{
			foreach( $id as $_id){
				$obj = $modelInstance->find($_id);
				if( is_object($obj)){
					$ajaxModels[$obj->id] = call_user_func('buildAjax'.ucfirst( $type), $obj );
				}
			}
		}
	}
	else{
		$obj = $modelInstance->all();
		foreach( $objs as $obj){
			$ajaxModels[$obj->id] = call_user_func('buildAjax'.ucfirst( $type), $obj );
		}
	}
	echo json_encode($ajaxModels);
}]);


Route::get('/gget/{type}', [ function($type) {
	echo "PROPOPSOPOSPOS".$type;
	print_r($request);
//    return view('customer', ['id' => $id]);
}]);
