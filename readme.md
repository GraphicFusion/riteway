# Riteway built on Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Laravel Naming Standards:
Model Naming: singular UpperCamelCase (ie, User)

## Javascript Naming Standards:
## jQuery Naming Standards:

## PHP Naming Standards: 
Classes : PascalCase (ie, UserClass )
Methods : camelCase (ie, getUser() )
Properties : camelCase
Functions : camelCase
Variables : lower_case

## CSS Naming Standards:
classes: no underscores, hyphen
ids: no hyphen, underscores

## To Do
 -- api authentication for user https://gistlog.co/JacobBennett/090369fbab0b31130b51	


## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
