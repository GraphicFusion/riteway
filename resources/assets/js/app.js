
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */


/**
 * Custom script by ps
 */
window.showMessage = function showMessage( mssg ){
	$('.fixed-message').html( mssg ).show();
	setTimeout(function(){
		$('.fixed-message').addClass('fade-message');
	}, 500);
	setTimeout(function(){
		$('.fixed-message').html( mssg ).hide();
		$('.fixed-message').removeClass('fade-message');
	}, 5000);
};





$(document).ready(function() {	
	/**** OPPORTUNITIES.BLADE.PHP ****/
	 var oTable = $('#dTables').DataTable({
	  	 "drawCallback": function( settings ) {
	        bindPropRows();
	    },
        "iDisplayLength": 20,
    	"responsive": {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table table-striped table-dt'
                } )
            }
        }
    });
    $('#search_dtable').keyup(function(){
        oTable.search($(this).val()).draw();
    });
	$('#estimate').change(function(){
		setEstimateHref();
	})
	$('#chosen_estimate').change(function(){
		$('#proposal_chosen_estimate').submit();
	})
	$('#chosen_plan').change(function(){
		$('#proposal_chosen_plan').submit();
	})
    var query_proposal = getParameterByName('proposal');
    if( query_proposal){
    	openProposal(query_proposal);
    }
	function bindPropRows(){
        $('#dTables tbody tr').on( 'click', '', function (e) {
			var $target = $(e.target);
			if( !$target.is('tr') ){
				var tr = $target.closest('tr').first();
			}
			else{
				var tr = $target;
			}
			var $id = $(tr).data('id');
			openProposal($id);
        });
	}
	function openProposal($id){
		getProposal($id, function(){
console.log("proposale:"  );
console.log(window._proposals[$id]);
			var $proposal = window._proposals[$id];
    	    $('#proposalDetailModal').modal('show');
			buildProposalModal($proposal, $id); 
		});
	}

	$(".openProposalDetailModalAlt").click(function(e) {
		var $this = $(e.target);
		var curModal = "#proposalDetailModal";
	    var newModal = "#proposalDetailAltModal";
	    var showPopup2 = false;
		$(curModal).on('hidden.bs.modal', function () {
		    if (showPopup2) {
		        $(newModal).modal('show');
		        showPopup2 = false;
		    }
		});
	    $(curModal).modal('hide');
	    showPopup2 = true;
		var estimate_id = $($this).data('id');
		var pres_estimate = $estimates[estimate_id];
		buildPreso($proposal,pres_estimate);
	});

	/**** END OPPORTUNITIES.BLADE.PHP ****/
	$('.footer').css('visibility', 'visible');
	
	// set margin bottom dynamically to body as per footer height
	function dynamic_margin_body() {
		var footer_height = $(".footer").innerHeight();
		var body_margin_bottom = footer_height + 50;
		$("body").css("margin-bottom", body_margin_bottom);
	}
	dynamic_margin_body();
	$(window).resize(function(event) {
		dynamic_margin_body();
	});

	// customer address autofill
	$("#address").geocomplete().bind("geocode:result", function(event, result){
		var $r = $(result.adr_address);
		$.each( $r, function(k,v){
			if( $(v).hasClass("street-address") ){
				$("input[name='address']").val( $(v).html());
			}
			if( $(v).hasClass("locality") ){
				$("input[name='city']").val( $(v).html());
			}
			if( $(v).hasClass("region") ){
				var $text = $(v).html();
				$("input#state").val( $text );
			}
			if( $(v).hasClass("postal-code") ){
				$("input[name='zipcode']").val( $(v).html());
			}	
		});
	});

	// Back to top animate smoothly
	$("#back_to_top").on('click', function(e) {
        e.preventDefault();
        $("html,body").animate({
            scrollTop: 0
        }, 700);
	});

	// Tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// focus on input group
 	$('.input-group').on('focus', '.form-control', function () {
      $(this).closest('.input-group, .form-group').addClass('focus');
    }).on('blur', '.form-control', function () {
      $(this).closest('.input-group, .form-group').removeClass('focus');
    });

	$(".gen-modal .close").click(function(){
		$('.gen-modal').modal('toggle');
	});

    // Close current modal and open new modal
    // this will fix the scrolling error on both modal popup in small devices
    var curModal = "#proposalDetailModal";
    var newModal = "#proposalDetailAltModal";
    var showPopup2 = false;
	$(curModal).on('hidden.bs.modal', function () {
	    if (showPopup2) {
	        $(newModal).modal('show');
	        showPopup2 = false;
	    }
	});
	$(".openProposalDetailModalAlt, #openProposalDetailModalAlt").click(function() {
	    $(curModal).modal('hide');
	    showPopup2 = true;
		var $id = $(".proposal-id").data('id');
	});


	checkitem();
	
});


var checkitem = function() {
  var $this;
  $this = $("#carousel-proposal-details");
  if ($("#carousel-proposal-details .carousel-inner .item:first").hasClass("active")) {
   // $("#proposalDetailAltModal .left").hide().css('display', 'none');
    $("#proposalDetailAltModal .right").show().css('display', 'block');
  } else if ($("#carousel-proposal-details .carousel-inner .item:last").hasClass("active")) {
    //$("#proposalDetailAltModal .right").hide().css('display', 'none');
    $("#proposalDetailAltModal .left").show().css('display', 'block');
  }
};
$("#carousel-proposal-details").on("slid.bs.carousel", "", checkitem);


// Grid Configuration for Proposal Notes (views/modals/proposal-detail-alt.blade.php)
// Uses JS Grid library added in assets
// See http://js-grid.com/docs

/*$("#jsGrid").jsGrid({
    width: "90%",
    height: "auto",

    inserting: true,
    editing: true,
    sorting: false,
    paging: false,

    data: [],

    noDataContent: "Add a Note",

    fields: [
        { name: "username", title: "User Name", type: "text", width: 100, validate: "required" },
        { name: "note", title: "Note", type: "textarea", width: 'auto' },
        { name: "date_created", title: "Created", type: "text", width: 75 },
        { name: "date_updated_last", title: "Updated", type: "text", width: 75 },
        { type: "control" }
    ]
});*/
