@extends('layouts.default')

@section('content')
<div class="container">
<?php 
	$user_id = ($id ? $id : "");
	$user = getUser( $user_id );
print_r($user->roles); ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User</div>
                <div class="panel-body">
{{  Form::open(array('url'=>'users/', 'method' => 'post', 'files'=>true)) }}

	{{ Form::label('email', 'E-Mail Address') }}
	{{ Form::email('email',$user->email,$attributes = array('autocomplete'=>'off')) }}
<br>
	{{ Form::label('first_name', 'First Name') }}
	{{ Form::text('first_name') }}
<br>
	{{ Form::label('last_name', 'Last Name') }}
	{{ Form::text('last_name') }}
<?php foreach( $user->roles  as $role ) :?>
	{{ Form::label('roles[]', 'Role') }}
	{{ Form::select('size', ['superadmin'=>'SuperAdmin','admin'=>'Admin', 'estimator' => 'Estimator'], $role) }}
	
<?php endforeach; ?>


    {{ Form::submit('Submit') }}
{{ Form::close() }}            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
