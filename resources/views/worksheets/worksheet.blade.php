<?php $user = getUser(); ?>
	<?php 
		/*
			WORKSHEET BUILDER
			Worksheet is built with:
	 			- public/js/formBuilder.js
				- app/Estimator/WorksheetBuilder.php

			Worksheet is saved by ajax function:
	 			- routes/api.php Route::post('/worksheet'  // NOTE: should clean up and not save as closure...

			If this is a new proposal, user will build customer info first saved here: WorksheetController.php storeCustomer()

		
		*/
    $admin_emails = getAdminEmails();
    $core_emails = getCoreEmails();
    //$warehouse_emails = getWarehouseEmails();
    $user_type = "";
    if( in_array($user->email,$admin_emails) || in_array($user->email,$core_emails)){
        $user_type = "admin";
    } 
   // print_r($user);
   // echo $user_type;
    //dd(9);
    if( 'admin' != $user_type ||  'team' != $user_type ){
    	$newUrl = "/";
		//	header('Location: '.$newUrl);
    }
?>
	@inject('build', 'App\Estimator\WorksheetBuilder')
	@extends('layouts.default')

	@section('content')
	<?php

		$group = App\Group::find( $user->current_group_id );
		$worksheet_model = App\Worksheet::firstOrNew( array('group_id' => $group->id, 'type' => $type)); 
	//print_r($worksheet_model);
		// need to create estimate in controller before view - so cant create twice
		$new_class = "";
		if( array_key_exists('proposal_id', $_GET ) && array_key_exists('new_estimate', $_GET ) ){
			$estimate = new App\Estimate;
			$estimate->type = $type['id'];
	// worksheet version???????????
			$estimate->worksheet_version = $worksheet_model->version;
			$estimate->worksheet_id = $worksheet_model->id;
			$estimate->save();
			$proposal = App\Proposal::find($_GET['proposal_id']);
	//print_r($proposal);
			$proposal->estimates()->save($estimate);
			$proposal_id = $proposal->id;
			$new_class = "new-estimate";
		}
		else{		
			if( array_key_exists('estimate_id', $_GET) ){
				$estimate_id = $_GET['estimate_id'];
				$estimate = App\Estimate::firstOrNew( array('id' => $_GET['estimate_id'] ));
			}
			else{
				$estimate =  new App\Estimate;
				$estimate->worksheet_version = $worksheet_model->version;
				$estimate->worksheet_id = $worksheet_model->id;
				$estimate->save();

			}
			$proposal = App\Proposal::find($estimate->proposal_id);
		}
		if($user->hasRole('admin')){
			$title = 'SuperAdmin ';
			$role = 'admin';

			$hide_input_fields['user_role'] = $user_type;
			if($proposal){
				$hide_input_fields['estimate_status'] = $proposal->status;
			}
		}
		$worksheet_data = json_decode($estimate->worksheet_data);
	//echo "wsv:".$worksheet_model->version."----ev:".$estimate->version;
		// get version - if version mismatch get archived version
		$original_version = $worksheet_model->version;
		$platinum_system = 0;
		$gold_system = 0;
		$silver_system = 0;
		$bronze_system = 0;
		$platinum_thermostat = 0;
		$gold_thermostat = 0;
		$silver_thermostat = 0;
		$bronze_thermostat = 0;
		if( $worksheet_model->version != $estimate->worksheet_version && $estimate->worksheet_id ){
			$worksheet_model = App\WorksheetArchive::where('group_id', $group->id)->where('worksheet_id', $estimate->worksheet_id)->where('version', $estimate->worksheet_version )->first(); 
		}

		$worksheet_object = json_decode($worksheet_model->worksheet_object);
		$sections = [];

		if( is_object ($worksheet_object )){
			$sections = $worksheet_object->sections;
			$proposal_id = 0;
			if( array_key_exists('estimate_id', $_GET ) ){
				$estimate_id = $_GET['estimate_id'];
				$estimate = App\Estimate::find($estimate_id);
				$platinum_system = $estimate->platinum_system_id;
				$gold_system = $estimate->gold_system_id;
				$silver_system = $estimate->silver_system_id;
				$bronze_system = $estimate->bronze_system_id;
				$platinum_thermostat = $estimate->platinum_thermostat_id;
				$gold_thermostat = $estimate->gold_thermostat_id;
				$silver_thermostat = $estimate->silver_thermostat_id;
				$bronze_thermostat = $estimate->bronze_thermostat_id;
				$proposal = App\Proposal::find($estimate->proposal_id);
				$proposal_id = $proposal->id;
			}
		}
	?>
	<script>
		<?php 
			$cats = getPresentationCategories();
			$json_cats = [];
			foreach( $cats as $cat ){
				$id = $cat['id'];
				$json_cats[$id] = $cat;
			}

		?>
		window.presentation_cats = <?php echo json_encode( $json_cats ); ?>;
	</script>
	<input type="hidden" name="" id="hide_input_fields" value='<?php echo json_encode($hide_input_fields)?>'>
	<div class="worksheet <?php echo $new_class; ?>" >
		<div class="container">
			<?php $user = getUser(); ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="panel panel-default">
		                <div class="panel-heading clearfix">
							<div class="pull-left">
								<svg version="1.1" id="home-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 56.3 48" style="enable-background:new 0 0 56.3 48;" xml:space="preserve">
									<g>
										<path class="st0" d="M28.2,37.1c1.4,0,2.7-1,2.7-2.9c0-1.6-2.7-5.5-2.7-5.5s-2.7,3.8-2.7,5.5C25.5,36.1,26.7,37.1,28.2,37.1z"/>
										<path class="st0" d=" M27.9,0L0,18.7l2.4,3.6L8,18.6V48h40.5V18.7l5.5,3.6l2.4-3.6L27.9,0z M28.1,40c-4.2,0-7.4-3.3-7.4-7.8 										c0-4.6,7.4-16.2,7.4-16.2s7.4,11.6,7.4,16.2C35.5,36.7,32.3,40,28.1,40z"/>
									</g>
								</svg>
			                	<h3>
			                		<?php echo $type['name']; ?> Worksheet
			                	</h3>
							</div>						
							<div class="pull-right">

		                	    <!--<button type="submit" class="btn btn-link">
		                	    	<?php echo embed_svg('icon', 'img/save-icon.svg', '18px', '18px'); ?>
		                	    	Save
		                	    </button>-->
		                	    <!--<a class="btn btn-link" href="{{ url('/edit-'.$type['id']) }}">Edit</a>-->
			            {{  Form::open(array('class'=>'form','url'=>'/estimate-delete', 'method' => 'post', 'files'=>true, 'style'=>'display:inline-block;')) }}
							{{ Form::hidden('estimate_id',@$estimate->id, ['class'=>'form-control', 'id'=>'estimate_id']) }}
							{{ Form::hidden('worksheet_id',$worksheet_model->id, ['class'=>'form-control', 'id'=>'worksheet_id']) }}
							{{ Form::hidden('version',$worksheet_model->version, ['class'=>'form-control', 'id'=>'version']) }}
		                	    <button type="submit" class="btn btn-link">
		                	    	Delete
		                	    </button>
						{{ Form::close() }}

		                	    <a href="{{ url('/proposals') }}" class="close">&times;</a>
							</div>
		            	</div>
			                <div class="panel-body">
			                	<div class="row">
									<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
					@if ( array_key_exists('step', $_GET) && $_GET['step'] == 'customer' )
						@include('forms.customer-form')
					@else
			            {{  Form::open(array('class'=>'form','id'=>'edit_estimate_frm','url'=>'/worksheet', 'method' => 'post', 'files'=>true)) }}
										<div class="form-group ">
											<div class="proposal_id">
												<svg version="1.1" id="file-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 20" style="enable-background:new 0 0 16 20;" xml:space="preserve">
													<path d="M10,0H2C0.9,0,0,0.9,0,2l0,16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V6L10,0z M12,16H4v-2h8V16z M12,12H4v-2h8V12z M9,7V1.5L14.5,7H9z"/>
												</svg>
											{{ Form::label('unit_location', 'Proposal ID') }}
												#<?php echo str_pad( $proposal->id,6,'0', STR_PAD_LEFT); ?> 
	| 										{{ Form::label('unit_location', 'Estimate') }} #<?php echo $estimate->number; ?> | Status: <?php echo ucfirst($proposal->status); ?>
											</div>

											<?php if( $original_version != $estimate->worksheet_version ) : ?>
												<?php
													$diff = ($original_version - $estimate->worksheet_version);
													if( $diff > 1 ){
														$mssg = "This Estimate is ".$diff." versions behind the Current Worksheet.";
													}
													else{
														$mssg = "This Estimate is ".$diff." version behind the Current Worksheet.";
													}
												?>
												<div class="worksheet-message">
													<?php echo $mssg; ?>
												</div>
											<?php endif; ?>
											<span class="status success">
												<?php echo embed_svg('icon', 'img/check-icon.svg', '20px', '20px'); ?>
											</span>
										</div>
										<?php
										$presentation =  getPresentationCategories(1);
										// $presentation = ['code_compliance' => [], 'options_accessories' => [], 'system_installation' => []]; //print_r($presentation);?>
										<?php if( count($sections )<1 ) : ?>

											<p>There is no worksheet built for this yet.</p>
										<?php else : ?>	
												<div class="inputs-wrapper">
													<div class='field-wrapper'>
													<?php if( 'split' == $type['id'] || 'furnace' == $type['id'] )  :  ?>
														<div class='form-group'>
															{{ Form::label( 'unit_available_width','A/H or Furnace Available Width', array('class'=>'repeater-label')) }}
															{{Form::text('unit_available_width',$estimate->unit_available_width, ['class'=>'form-control']) }}
														</div>
														<?php if('split' == $type['id']):  ?>
															<div class='form-group'>
																{{ Form::label( 'coil_available_width','Coil Available Width', array('class'=>'repeater-label')) }}
																{{Form::text('coil_available_width',$estimate->coil_available_width, ['class'=>'form-control']) }}
															</div>
														<?php endif; ?>
														<div class='form-group'>
															{{ Form::label( 'available_height','Total Available Height', array('class'=>'repeater-label')) }}
															{{Form::text('available_height',$estimate->available_height, ['class'=>'form-control']) }}
														</div>
													<?php endif; ?>
														<?php if('split' == $type['id'] || 'package' == $type['id']):  ?>
															<div class='form-group'>
																{{ Form::select('ac_hp', ['hp'=>'Heatpump','ac'=>'Air Conditioner'],$estimate->ac_hp, ['class'=>'form-control','id'=>'ac_hp']) }}
															</div>
														<?php endif; ?>
														<div class='form-group'>
															{{ Form::select('new_replace', ['new'=>'New','replace'=>'Replace'],$estimate->new_replace, ['class'=>'form-control','id'=>'new_replace']) }}
														</div>
														<div class='form-group'>
															{{ Form::select('capacity', [
																'.75' => '3/4 Ton Capacity',
																'1' => '1 Ton Capacity',
																'1.5' => '1.5 Ton Capacity',
																'2' => '2 Ton Capacity',
																'2.5' => '2.5 Ton Capacity',
																'3' => '3 Ton Capacity',
																'3.5' => '3.5 Ton Capacity',
																'4' => '4 Ton Capacity',
																'5' => '5 Ton Capacity'
															],$estimate->capacity, ['class'=>'form-control','id'=>'capacity']) }}
														</div>
													</div>
												</div>			
											<?php foreach( $sections as $section_key => $section ) : 
												$estimate_section = new stdClass();
												if( is_object( $worksheet_data ) ){
													$estimate_section = @$worksheet_data->sections[$section_key]; 
												}
											?>

						                		<div class="form-group">
													<h2><?php echo $section->title; ?></h2>
												</div>
						                		<div class="form-group" data-attr="start">
													<?php if( property_exists( $section, 'inputs' ) && is_array($section->inputs) && count( $section->inputs ) > 0 ) : ?>
														<?php foreach( $section->inputs as $input_key => $input ) : ?>
															<?php 
	if( !property_exists( $input, 'price_category')){
		$input->price_category = 'other';
	}
	$input_price_category = $input->price_category; ?>
															<div class="inputs-wrapper">
																<?php
																	$input->name = "input_s:".$section_key."_i:".$input_key."_"; 
																	echo "<div class='field-wrapper template-wrapper' style=''>".$build->buildField($input, $errors, '', true)."</div>";
																	if( is_object($estimate_section) && property_exists( $estimate_section, 'inputs' ) &&  array_key_exists( $input_key, $estimate_section->inputs) ){
																		$estimate_input = $estimate_section->inputs[$input_key];
	//print_r($estimate_input);
	//print_r($input);
																		if( property_exists( $estimate_input, 'repeater' ) &&  is_array( $estimate_input->repeater ) ){

																			foreach( $estimate_input->repeater as $r_key => $repeater ){
	//check if an option is "selected" 
	//print_r($repeater);
																				if( 'none' != @$repeater->selected ){
																					$selected_option_key = @$repeater->selected;
																					$presentation[$input_price_category][] = ['label' => $input->title, 'value' => @$input->choices[$selected_option_key]->title];
																				}
																				else{
																					//print_r($repeater);
																				}
																				$input->name .= "_r".$r_key; 
																				echo $build->buildField($input, $errors, $repeater, '', $input_price_category);
																			}
																		}
																		if( property_exists( $estimate_input, 'blank' ) &&  is_array( $estimate_input->blank ) ){
																			foreach( $estimate_input->blank as $r_key => $blank ){
																					//check if an option is "selected" 
																				$selected_option_key = @$blank->selected;
																				$presentation[$input_price_category][] = ['label' => $input->title, 'value' => @$input->choices[$selected_option_key]->title];
																				$input->name .= "_r".$r_key; 
																				echo $build->buildField($input, $errors, $blank, '', $input_price_category);
																			}
																		}
																	}
																	else{
																		echo "<div class='field-wrapper' style=''>".$build->buildField($input, $errors, '', '', $input_price_category)."</div><!--/field-wrapper-->";
																	}
																?>
															</div>
														<?php endforeach; ?>
													<?php endif; ?>
												</div><!--/form-group-->
						                		<hr>
											<?php endforeach; ?>
										<?php endif; ?>
										<script>
											var presentation = <?php echo json_encode($presentation); ?>;
											window.presentation = presentation;
	console.log('preso:');
	console.log(presentation);
										</script>
									</div><!--/form-group-->

			                		<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
	<p>Total Prices already reflect Category Markup</p>
										<div class="form-group col-xs-3">
											<label>Platinum Total</label>
											<input class="form-control ws-total-platinum" name="ws-total-platinum" type="text" value="">
										</div>
										<div class="form-group col-xs-3">
											<label>Gold<br>Total</label>
											<input class="form-control ws-total-gold" name="ws-total-gold" type="text" value="">
										</div>
										<div class="form-group col-xs-3">
											<label>Silver<br>Total</label>
											<input class="form-control ws-total-silver" name="ws-total-silver" type="text" value="">
										</div>
										<div class="form-group col-xs-3">
											<label>Bronze<br>Total</label>

											<input class="form-control ws-total-bronze" name="ws-total-bronze" type="text" value="">
										</div>
									</div>
			                		<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
										<?php

										function getSystemDimensions( $type, $id ){

										}
										$dimensions = [];
										//$dimensions['split'][] = ['width' => 2, 'height' => 3];
										//$dimensions['split'][] = ['width' => 3, 'height' => 4];
											$use_thermostats = 0;
											$system_filters = [];
											if( "furnace" == $estimate->type ){
												$system_label = "Furnace";
												$systems = App\Furnace::all();
												$select_system = [];
												$select_system[] = "None";

												foreach($systems as $system){
													$capacity = $system->blower_size;
													$select_system[$system->id] = $system->brand.' '.$system->model_number;
													$hp_ac = "ac";
													if( 'furnace' == strtolower($system->furnace_air_handler) ){
														$hp_ac = 'hp';
													}
													$system_filters[$system->id] = [
														'capacity' => $capacity
													];
												}
												$use_thermostats = 1;
											}
											elseif( "split" == $estimate->type ){
												$system_label = "System";
												$systems = App\System::all();
												$use_thermostats = 1;
												$select_system = [];
												$select_system[] = "None";

												foreach($systems as $system){
													$condenser = App\Condenser::find($system->condenser_id);
													//print_r($condenser);
													$capacity = 0;
													$hp_ac = "ac";
													if( is_object($condenser)){
														$capacity = $condenser->capacity;
														$hp_ac = strtolower($condenser->hp_ac);
													}
													$select_system[$system->id] = $system->name;
													$system_filters[$system->id] = [
														'hp_ac' => $hp_ac,
														'capacity' => $capacity 
													];	
												}
											}
											elseif( "package" == $estimate->type){
												$systems = App\Package::all();
												$select_system = [];
												$select_system[] = "None";
												$use_thermostats = 1;
												foreach($systems as $system){
												//	print_r($system);
													$hp_ac = "ac";
													if( 'hp' == strtolower($system->hp_ac) ){
														$hp_ac = 'hp';
													}
													$select_system[$system->id] = $system->brand . " " .$system->model_number;
													$system_filters[$system->id] = [
														'hp_ac' => $hp_ac,
														'capacity' => $system->capacity_for_cooling
													];	
												}
											}
											if( $use_thermostats ){
												$thermostats_arr = App\Thermostat::all();
												$thermostats = [];
												foreach($thermostats_arr as $arr ){
													$thermostats[$arr->id] = $arr->brand." - ".$arr->model_number;
												}
											}
											if( "package" != $estimate->type){
												foreach($systems as $system){
	//												print_r($system);
													$coil_height = 0;
													$coil_width = 0;
													if( $system->coil_id ){
														$coil =  App\Coil::find($system->coil_id);
														if( is_object( $coil ) ){
															$coil_height = $coil->height;
															$coil_width = $coil->width;
														}
													}
													$furnace_height = 0;
													$furnace_width = 0;
													if( $system->furnace_id ){
														$furnace =  App\Furnace::find($system->furnace_id);
														if( is_object( $furnace ) ){
															$furnace_height = $furnace->height;
															$furnace_width = $furnace->width;
														}
													}
													$dimensions[$system->id]['furnace'] = ['height' => $furnace_height, 'width' => $furnace_width];
													$dimensions[$system->id]['coil'] = ['height'=>$coil_height,'width' => $coil_width];
													//$select_system[$system->id] = $system->name;
												}
											}
										//	print_r($select_system);

										?>

										<script>
											window.dimensions = <?php echo json_encode($dimensions); ?>;
											window.estimate_type = "<?php echo $estimate->type; ?>";
											window.system_filters = <?php echo json_encode($system_filters); ?>;
											console.log(window.system_filters);
											console.log('window.system_filters');
										</script>
										<div class="form-group col-xs-12">
											{{ Form::label('platinum_system', 'Platinum System') }}
											<div class="platinum_size_message size_message"></div>
											{{ Form::select('platinum_system', $select_system, $platinum_system, ['class'=>'form-control']) }}
										</div>
										<?php if ($use_thermostats ) : ?>
											<div class="form-group col-xs-12">
												{{ Form::label('platinum_thermostat', 'Platinum Thermostat') }}
												{{ Form::select('platinum_thermostat', $thermostats, $platinum_thermostat, ['class'=>'form-control']) }}
											</div>
										<?php endif; ?>
										<div class="form-group col-xs-12">
											{{ Form::label('platinum_boxprice', 'Platinum Box Amount') }}
											{{ Form::text('platinum_boxprice', $estimate->platinum_box, ['class'=>'form-control']) }}
										</div>
										<div class="form-group col-xs-12">
											{{ Form::label('gold_system', 'gold System') }}
											<div class="gold_size_message size_message"></div>
											{{ Form::select('gold_system', $select_system,$gold_system, ['class'=>'form-control']) }}
										</div>
										<?php if ($use_thermostats ) : ?>
											<div class="form-group col-xs-12">
												{{ Form::label('gold_thermostat', 'Gold Thermostat') }}
												{{ Form::select('gold_thermostat', $thermostats, $gold_thermostat, ['class'=>'form-control']) }}
											</div>
										<?php endif; ?>
										<div class="form-group col-xs-12">
											{{ Form::label('gold_boxprice', 'Gold Box Amount') }}
											{{ Form::text('gold_boxprice', $estimate->gold_box, ['class'=>'form-control']) }}
										</div>

										<div class="form-group col-xs-12">
											{{ Form::label('silver_system', 'silver System') }}
											<div class="silver_size_message size_message"></div>
											{{ Form::select('silver_system', $select_system, $silver_system, ['class'=>'form-control']) }}
										</div>
										<?php if ($use_thermostats ) : ?>
											<div class="form-group col-xs-12">
												{{ Form::label('silver_thermostat', 'Silver Thermostat') }}
												{{ Form::select('silver_thermostat', $thermostats, $silver_thermostat, ['class'=>'form-control']) }}
											</div>
										<?php endif; ?>
										<div class="form-group col-xs-12">
											{{ Form::label('silver_boxprice', 'Silver Box Amount') }}
											{{ Form::text('silver_boxprice', $estimate->silver_box, ['class'=>'form-control']) }}
										</div>
										<div class="form-group col-xs-12">
											{{ Form::label('bronze_system', 'bronze System') }}
											<div class="_size_message size_message"></div>
											{{ Form::select('bronze_system', $select_system, $bronze_system, ['class'=>'form-control']) }}
										</div>
										<?php if ($use_thermostats ) : ?>
											<div class="form-group col-xs-12">
												{{ Form::label('bronze_thermostat', 'Bronze Thermostat') }}
												{{ Form::select('bronze_thermostat', $thermostats, $bronze_thermostat, ['class'=>'form-control']) }}
											</div>
										<?php endif; ?>
										<div class="form-group col-xs-12">
											{{ Form::label('bronze_boxprice', 'Bronze Box Amount') }}
											{{ Form::text('bronze_boxprice', $estimate->bronze_box, ['class'=>'form-control']) }}
										</div>
										<style>
											.incl-check {
												margin-bottom:10px !important;
											}
											.incl-check input{
												top:4px;
												position:relative;
												/* Double-sized Checkboxes */
												margin-right:20px;
												margin-left:4px;
												  -ms-transform: scale(1.7); /* IE */
												  -moz-transform: scale(1.7); /* FF */
												  -webkit-transform: scale(1.7); /* Safari and Chrome */
												  -o-transform: scale(1.7); /* Opera */
												  padding: 10px;
											}
										</style>
										<?php
										$incl = json_decode($estimate->includes);
										if(!$incl){
											$incl = new stdClass();
												$incl->trane = 0;
												$incl->tep = 0;
												$incl->lennox = 0;
												$incl->costco = 0;
												$incl->exec = 0;
												$incl->citi = 0;											
										}
											$incl_trane = $incl->trane;
											$incl_tep =  $incl->tep;
											$incl_lennox = $incl->lennox;
											$incl_costco = $incl->costco;
											$incl_exec = $incl->exec;
											$incl_citi = $incl->citi;
										?>
										<div class="form-group col-xs-12 incl-check">
											{{ Form::checkbox('incl_trane','', $incl_trane) }}
											{{ Form::label('incl_trane', 'Include Trane Rebate') }}
										</div>
										<div class="form-group col-xs-12 incl-check">
											{{ Form::checkbox('incl_tep', '',$incl_tep) }}
											{{ Form::label('incl_tep', 'Include TEP Rebate') }}
										</div>
										<div class="form-group col-xs-12 incl-check">
											{{ Form::checkbox('incl_lennox', '',$incl_lennox) }}
											{{ Form::label('incl_lennox', 'Include Lennox Rebate') }}
										</div>
										<div class="form-group col-xs-12 incl-check">
											{{ Form::checkbox('incl_costco', '',$incl_costco) }}
											{{ Form::label('incl_costco', 'Include Costco Rebate') }}
										</div>
										<div class="form-group col-xs-12 incl-check">
											{{ Form::checkbox('incl_exec', '',$incl_exec) }}
											{{ Form::label('incl_exec', 'Include Exec Rebate') }}
										</div>
										<div class="form-group col-xs-12 incl-check">
											{{ Form::checkbox('incl_citi', '',$incl_citi) }}
											{{ Form::label('incl_citi', 'Include Citi Rebate') }}
										</div>
									</div>

			                		<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
	<div class="formBuilderSubmit-wrapper">
										<div class="clearfix">
											{{ Form::submit('Save', array('class' => 'btn-submit btn btn-primary btn-block', 'id'=>'save_estimate')) }}
											<a class='btn-submit btn btn-primary btn-block' id='present_estimate' data-href='/opportunities?proposal=<?php echo $proposal->id; ?>&estimate=<?php echo $estimate->id; ?>'>Present</a>
										</div>							
	</div>									
			                		</div>
						{{ Form::close() }}  
						@endif
			                	</div>
							</div><!--/panel-body-->
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	@section('customJs')
    <script type="text/javascript">
    	
	</script>
	@endsection
	@include('modals.createestimate')
	@endsection