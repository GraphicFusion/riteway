## Worksheet Objects

## Worksheet Objects

```javascript
    $worksheet = {
        sections : [
            0 : {
                title : 'Section Title',
                inputs : [
                    0 : {
                        title : 'Input Title',
                        type : 'Repeater',
                        options : {
				cost_included : 1,
				quantity : 0,
				note : 1,
				total_feet : 1,
				included_amount : 0	
                        },
                        choices : [
                        ]
                    }
                ]
            }
        ]
    }
```