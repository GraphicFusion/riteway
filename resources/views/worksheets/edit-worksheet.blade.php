<?php $user = getUser(); 
 $admin_emails = getAdminEmails();
    $core_emails = getCoreEmails();
    //$warehouse_emails = getWarehouseEmails();
    $user_type = "";
    if( in_array($user->email,$admin_emails) || in_array($user->email,$core_emails)){
        $user_type = "admin";
    } 
    if( 'admin' != $user_type ){
        $newUrl = "/";
            header('Location: '.$newUrl);
    }
?>
@inject('build', 'App\Estimator\WorksheetBuilder')
@extends('layouts.default')

@section('content')
<?php
/*
  WORKSHEET EDITOR
  Worksheet is built with:
  - public/js/formBuilder.js
  - app/Estimator/WorksheetBuilder.php

  Worksheet is saved by ajax function:
  - routes/api.php Route::post('/worksheet'  // NOTE: should clean up and not save as closure...
 */
if ($user->hasRole('admin')) {
    $title = 'SuperAdmin ';
    $role = 'admin';
}

$group = App\Group::find($user->current_group_id);
$worksheet_model = App\Worksheet::firstOrNew(array('group_id' => $group->id, 'type' => $type));
$worksheet_object = $worksheet_model->worksheet_object;
if (is_object(json_decode($worksheet_object))) :
    ?>
    <script>
        var $loaded_worksheet = <?php echo $worksheet_object; ?>;
        console.log('loaded_worksheet');
        console.log($loaded_worksheet);
    </script>
    <?php //print_r(json_decode($worksheet_object)); ?>
<?php endif; ?>
<style>
    .worksheet .worksheet-nav .nav-wrapper {
        overflow: auto;
        max-height: 450px;
        background-color: #fff;
        padding: 10px;
        border-radius: 3px;
        border: 1px solid #ddd;
    }
    .worksheet .worksheet-nav h3 {
        margin-bottom: 0;
        margin-top: 25px;
        color: #fff;
    }
    .worksheet .worksheet-nav hr {
        margin-top: 5px;
        margin-bottom: 22px;
        border: 0;
        border-top: 1px solid #eeeeee;
    }
    .worksheet .panel-heading {
        padding: 23px 0px 6px;
        border-bottom: 1px solid #E0E0E0;
    }
    .plr0 {
        padding-left: 0;
        padding-right: 0;
        background-color: #fff;
        border-radius: 3px;
        margin-top: 6px;
    }
    ul#sections > li.ui-state-default .icon-drag span {
        float: left;
        width: 6px;
        height: 6px;
        margin: 2px 2px 0 0;
        background: #333;
        border-radius: 20px;
    }
    #add_section {
        padding-left: 37px;
        color: #333;
        font-weight: 700;
        margin-top: 20px;
        border-radius: 2px !important;
    }
    .btn-link
    {
        background-color:#fff !important;
        color:#333;
        padding: 10px;
    }
</style>
<div class="fixed-message">This is a fixed message</div>
<div class="worksheet">

    <div class="container-fluid worksheet">

        <div class="row">
            <div class="col-md-12 page_content">
                <div class="col-md-3 worksheet-nav">
                    <h3>Menu</h3>
                    <hr>
                    <div class="nav-wrapper">
                    </div>
                </div>
                <div class="col-md-9 " id="worksheet-builder" >
                    <div class="panel panel-default">
                        {{  Form::open(array('class'=>'form','url'=>'/furnace', 'method' => 'post', 'files'=>true)) }}
                        {{ Form::hidden('role', $role, ['class'=>'form-control']) }}
                        {{ Form::hidden('type', $type['id'], ['class'=>'form-control']) }}
                        {{ Form::hidden('group_id', $user->current_group_id, ['class'=>'form-control']) }}
                        {{ Form::hidden('formJSON', '', ['class'=>'form-control section_title_input']) }}

                        {{ Form::close() }}
                        <div class="panel-heading clearfix">
                            <div class="pull-left">
<!--                                <svg version="1.1" id="home-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 56.3 48" style="enable-background:new 0 0 56.3 48;" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M28.2,37.1c1.4,0,2.7-1,2.7-2.9c0-1.6-2.7-5.5-2.7-5.5s-2.7,3.8-2.7,5.5C25.5,36.1,26.7,37.1,28.2,37.1z"/>
                                        <path class="st0" d=" M27.9,0L0,18.7l2.4,3.6L8,18.6V48h40.5V18.7l5.5,3.6l2.4-3.6L27.9,0z M28.1,40c-4.2,0-7.4-3.3-7.4-7.8 										c0-4.6,7.4-16.2,7.4-16.2s7.4,11.6,7.4,16.2C35.5,36.7,32.3,40,28.1,40z"/>
                                    </g>
                                </svg>-->
                                <h3>
                                    <?php echo $title; ?> <?php echo $type['name']; ?> Worksheet Builder
                                </h3>
                            </div>						
                            <div class="pull-right">
                                <!--<a href="{{ url('/proposals') }}" class="close">&times;</a>-->
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row" >

                                <div class="col-lg-12 plr0">
                                    <ul class="clearfix" id="sections"></ul>
                                </div>									
                                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                                    <div class="clearfix text-center">
                                        <a href="javascript:void(0)" class="btn btn-link" id="add_section"><span>+</span>Add New Section</a>
                                        <div class="formBuilderSubmit-wrapper">
                                            <span class="btn form-changes"></span>
                                            {{ Form::button('<span class="main">Save</span>', array('id' => 'formBuilderSubmit','class' => 'btn-submit btn btn-primary btn-block')) }}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--/ CUSTOM TEMPLATE-->
<script id="input_custom_template" type="text/template">
    <li class="custom-wrapper">
    <div class="row">
    <h5>Custom Field</h5>
    <ul class="list-inline">
    <li><a href="javascript:void(0)" class="remove_custom">Remove Custom</a></li>
    <li><a href="javascript:void(0)" class="collapse_custom">Collapse Custom</a></li>
    </ul>
    <div class="move_icon"></div>
    <div class="custom-content">
    <div class="row">
    <div class="col-lg-12">
    <div class="form-group">
    <?php
    $custom_fields = get_custom_fields();
    $custom_options = [];
    foreach ($custom_fields as $field) {
        $id = $field['id'];
        $custom_options[$id] = $field['name'];
    }
    ?>
    {{ Form::label('custom_field', ' Custom Field') }}
    {{ Form::select('custom_field', $custom_options, '0', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    </div><!--/custom-content-->
    </div>
    </li>
</script>

<!--/ DROPDOWN TEMPLATE-->
<script id="input_dropdown_template" type="text/template">
    <li class="dropdown-wrapper">
    <div class="row bdr">
    <div class="col-sm-1">
    <div class="icon-drag">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    </div>
    </div>
    <div class="col-sm-10">
    <h5>Dropdown Field
    <span class="input-hidden-title"></span>
    </h5>
    {{-- <div class="move_icon"></div> --}}
    <div class="dropdown-content">
    <div class="row">
    <div class="col-lg-12">
    <div class="form-group">
    {{ Form::label('dropdown_title', ' Field Title') }}
    {{ Form::text('dropdown_title', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>

    <div class="row">
    <div class="col-lg-12">
    <h6 class="title">Options</h6>
    <div class="options">
    <ul class="list-inline-checkbox">
    <li>
    <div class="checkbox">
    <label for="make_repeater">
    <input type="checkbox" name="make_repeater" value="" >
    Make Repeater
    </label>
    </div>
    </li>
    <li>
    <div class="checkbox">
    <label for="quantity">
    <input type="checkbox" name="quantity" value="" >
    Quantity
    </label>
    </div>
    </li>
    <li class="allow_null_wrapper">
    <div class="checkbox">
    <label for="allow_null">
    <input type="checkbox" name="allow_null" value="" >
    Allow Null
    </label>
    </div>
    </li>
    <li class="total_feet_wrapper">
    <div class="checkbox">
    <label for="total_feet">
    <input type="checkbox" name="total_feet" value="" >
    Total Feet
    </label>
    </div>
    </li>                       
    <li class="job_sheet_wrapper">
    <div class="checkbox">
    <label for="job_sheet">
    <input type="checkbox" name="job_sheet" value="" >
    Include in Job Sheet
    </label>
    </div>
    </li>                       
    <li class="included_amount_wrapper">
    <div class="checkbox">
    <label for="included_amount_check">
    <input type="checkbox" name="included_amount_check" value="" >
    Included Amount
    </label>
    </div>
    </li>
    <li class="included_amount_wrapper">
    <div class="">
    <label for="price_category">
    Price Category
    <select name="price_category" >
    <?php
    $select = "";
    $cats = getPresentationCategories();
    foreach ($cats as $cat) {
        $select .= '<option value="' . $cat['id'] . '">' . $cat['name'] . '</option>';
    }
    echo $select;
    ?>
    </select>
    </label>
    </div>
    </li>
    </ul>
    </div>
    </div>
    </div>
    <a href="javascript:void(0)" class="remove_dropdown">Remove Dropdown</a>

    <div class="row">
    <div class="col-lg-12">
    <h6 class="title">Choices</h6>
    <div class="choices"></div>
    </div>
    </div><!--/choices-->

    <div class="row">
    <div class="col-lg-12">
    <ul class="list-inline">
    <li><a href="javascript:void(0)" class="add_dropdown_choice">Add Choice</a></li>
    </ul>
    </div>
    </div>
    </div><!--/dropdown-content-->
    </div>
    <div class="col-sm-1">
    <a href="javascript:void(0)" class="collapse_dropdown"><span class="ico minus">&minus;</span></a>
    </div>
    </div>
    </li>
</script>

<!--/ BLANKFIELD TEMPLATE-->
<script id="input_blank_template" type="text/template">
    <li class="blank-wrapper">
    <div class="row bdr">
    <div class="col-sm-1">
    <div class="icon-drag">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    </div>
    </div>
    <div class="col-sm-10">
    <h5>Blank Field
    <span class="input-hidden-title"></span>
    </h5>
    {{-- <div class="move_icon"></div> --}}
    <div class="blank-content">
    <div class="row">
    <div class="col-lg-12">
    <div class="form-group">
    {{ Form::label('blank_title', ' Field Title') }}
    {{ Form::text('blank_title', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>

    <div class="row">
    <div class="col-lg-12">
    <h6 class="title">Options</h6>
    <div class="options">
    <ul class="list-inline-checkbox">
    <li>
    <div class="checkbox">
    <label for="cost_included">
    <input type="checkbox" name="cost_included" value="">
    Cost Included
    </label>
    </div>
    </li>
    <li>
    <div class="checkbox">
    <label for="quantity">
    <input type="checkbox" name="quantity" value="">
    Quantity
    </label>
    </div>
    </li>
    <li class="included_amount_wrapper">
    <div class="">
    <label for="price_category">
    Price Category
    <select name="price_category" >
    <option value="code_compliance">Code Compliance</option>
    <option value="options_accessories">Options & Accessories</option>
    <option value="system_installation">System & Installation</option>
    <option value="prices">Additional Prices</option>
    </select>
    </label>
    </div>
    </li>
    </ul>
    </div>
    </div>
    </div>
    <a href="javascript:void(0)" class="remove_blank">Remove Blank</a>


    </div><!--/dropdown-content-->
    </div>
    <div class="col-sm-1">
    <a href="javascript:void(0)" class="collapse_blank"><span class="ico minus">&minus;</span></a>
    </div>
    </div>
    </li>
</script>

<!--/ DROPDOWN CHOICES TEMPLATE-->
<script id="dropdown_choices_template" type="text/template">
    <div class="col-lg-12 choice plr0">
    <div class="row">
    <div class="col-lg-12">
    <div class="form-group">
    {{ Form::label('choice_title', 'Choice Text') }}
    {{ Form::text('choice_title', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
    <h6 class="title">Variables</h6>
    </div>
    <div class="col-lg-12">
    <div class="form-group included_amount_input_wrapper inactive-input">
    {{ Form::label('included_amount', 'Included Amount') }}
    {{ Form::text('included_amount', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
    <h6 class="title">Prices</h6>
    <div class="form-group">
    <div class="checkbox">
    <label for="tiered_pricing">
    <input type="checkbox" name="tiered_pricing" value="">
    Tiered Pricing
    </label>
    </div>
    </div>
    </div>
    <div class="col-lg-12 all-tiers-prices">
    <h6 class="title">All Tiers</h6>
    <div class="row">
    <!--					<div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('base_price', 'Base Price') }}
    {{ Form::text('base_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    -->
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('unit_price', 'Unit Price') }}
    {{ Form::text('unit_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    </div>
    <div class="col-lg-6 plat-prices tiered">
    <h6 class="title">Platinum</h6>
    <!--				<div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('platinum_base_price', 'Base Price') }}
    {{ Form::text('platinum_base_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    -->
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('platinum_unit_price', 'Unit Price') }}
    {{ Form::text('platinum_unit_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    <div class="col-lg-6 gold-prices tiered">
    <h6 class="title">Gold</h6>
    <!--
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('gold_base_price', 'Base Price') }}
    {{ Form::text('gold_base_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    -->
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('gold_unit_price', 'Unit Price') }}
    {{ Form::text('gold_unit_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    <div class="col-lg-6 silver-prices tiered">
    <h6 class="title">Silver</h6>
    <!--
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('silver_base_price', 'Base Price') }}
    {{ Form::text('silver_base_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    -->
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('silver_unit_price', 'Unit Price') }}
    {{ Form::text('silver_unit_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    <div class="col-lg-6 bronze-prices tiered">
    <h6 class="title">Bronze</h6>
    <!--
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('bronze_base_price', 'Base Price') }}
    {{ Form::text('bronze_base_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    -->
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('bronze_unit_price', 'Unit Price') }}
    {{ Form::text('bronze_unit_price', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    <div class="col-lg-12 job-units">
    <div class="row">
    <div class="col-lg-6">
    <div class="form-group">
    {{ Form::label('job_unit', 'Job Unit') }}
    {{ Form::text('job_unit', '', ['class'=>'form-control']) }}
    </div>
    </div>
    </div>
    </div>
    </div>
    <a href="javascript:void(0)" class="remove_choice">Remove Choice</a>
    </div>
</script>

<!--/ SECTION TEMPLATE-->
<script id="section_li_template" type="text/template">
    <li class="ui-state-default section clearfix" data-fc="0">
    <div class="col-lg-8 col-lg-offset-2">
    <h4 class="section_number"></h4>
    <h3 class="section_title_head" data-section-number=""></h3>
    <div class="row is-flex">
    <div class="col-sm-1">
    <div class="icon-drag">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    </div>
    </div>
    <div class="col-sm-10">
    <div class="form-group">
    {{ Form::label('section_title', 'Section Title', array('class'=>'control-label')) }}
    {{ Form::text('section_title', '', ['class'=>'form-control section_title_input']) }}
    </div>		
    </div>
    <div class="col-sm-1">
    <a href="javascript:void(0)" class="collapse_section"><span class="ico minus">&minus;</span></a>
    </div>
    </div>

    <div class="groupOptions">
    <div class="row">
    <div class="col-sm-10 col-sm-offset-1 clearfix">
    <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <ul class="list-inline text-left">
    <li><a href="javascript:void(0)" class="add_custom" >Add Custom</a></li>
    <li><a href="javascript:void(0)" class="add_dropdown"><span>+</span>Add New Dropdown Field</a></li>
    <li><a href="javascript:void(0)" class="add_blank"><span>+</span>Add New Blank Field</a></li>
    </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <ul class="list-inline text-right">
    <li><a href="javascript:void(0)" class="edit_title">Edit Title</a></li>
    <li><a href="javascript:void(0)" class="save_title">Save Title</a></li>
    <li><a href="javascript:void(0)" class="remove_section">Remove Section</a></li>
    </ul>
    </div>
    </div>						
    </div>
    </div>

    <ul class="inputs-wrapper">
    <div class="row">
    <div class="inputs col-lg-12">
    </div>
    </div>
    </ul>
    </div>

    </div>
    </li>
</script>

@endsection