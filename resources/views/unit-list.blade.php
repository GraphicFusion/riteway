@extends('layouts.default')

@section('content')
<?php
    $active_class = [
        'coil' => '',
        'furnace' => '',
        'system' => '',
        'package' => '',
        'thermostat' => '',
        'condenser' => ''
    ];
    if( array_key_exists('unit_type', $_GET)){
        $active_type = $_GET['unit_type'];
        $active_class[$active_type] = " active";
    }
    else{
        $active_class['coil'] = " active";        
    }
    $user = getUser();
    $group = App\Group::find($user->current_group_id);
   
?>
<style>
    .panel-default > .panel-heading {
    color: #fff;
    background-color: #333333 !important;
    border-color: #333333 !important;
}
.panel-default {
    border-color: transparent !important;
}
.panel {
    margin-bottom: 22px !important;
    background-color: #333333 !important;
    border: 1px solid #333333 !important;
    border-radius: 4px !important;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) !important;;
}
table.dataTable tbody tr {
    background-color: #222222 !important;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #9E9E9E !important;
    background-color: transparent !important;
    border: none !important;;
    border-bottom: 3px solid #fff !important;
    text-align: center !important;
    padding-left: 5px !important;
    padding-right: 5px !important;
}
.table > tbody > tr:nth-of-type(even) {
    background-color: transparent !important;
}
.table > tbody > tr > th, .table > tbody > tr > td {
    font-size: 14px !important;
    color: #fff !important;
    border: none !important;
}
</style>
<div class="unit-list">
    <div class="container-fluid">
        <?php $user = getUser(); ?>
        <div class="row">
            <div class="col-md-12 page_content">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <h3>
<!--                                <svg version="1.1" id="list-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.7 23.7" style="enable-background:new 0 0 23.7 23.7;" xml:space="preserve">
                                    <path class="st0" d="M11.9,11.9c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5H11.9z M11.9,11.9c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5H11.9z
                                          M11.9,11.9c-3,0-5.5-2.5-5.5-5.5s2.5-5.5,5.5-5.5V11.9z M11.9,11.9c3,0,5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5V11.9z"/>
                                </svg>-->
                                Unit List
                            </h3>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="row">
                                <div class="col-sm-8 col-xs-12">
                                    {{-- <ul class="list-inline pull-right mb0">
										<li>
											<a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary" id="create-new-unit">Add New Unit</a>
										</li>
										<li>
											<a href="javascript:void()" data-toggle="modal" data-target="#addNewPackage" class="btn btn-primary">Add New Package</a>
										</li>
										<li>
											<a href="javascript:void()" data-toggle="modal" data-target="#addNewSplit" class="btn btn-primary">Add New Split</a>	
										</li>
									</ul> --}}
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    {{-- @include('datatable-searchform') --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has($msg))
                        <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                            {{ Session::get($msg) }}
                        </div>
                        @endif
                        @endforeach

                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>

                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="<?php echo $active_class['coil']; ?>"><a href="#coils" aria-controls="coils" role="tab" data-toggle="tab">Coils</a></li>
                                    <li role="presentation" class="<?php echo $active_class['condenser']; ?>"><a href="#condensers" aria-controls="condensers" role="tab" data-toggle="tab">Condensers</a></li>
                                    <li role="presentation" class="<?php echo $active_class['thermostat']; ?>"><a href="#thermostats" aria-controls="thermostats" role="tab" data-toggle="tab">Thermostats</a></li>
                                    <li role="presentation" class="<?php echo $active_class['furnace']; ?>"><a href="#furnaces" aria-controls="furnaces" role="tab" data-toggle="tab">Furnaces</a></li>
                                    <li role="presentation" class="<?php echo $active_class['package']; ?>"><a href="#packages" aria-controls="packages" role="tab" data-toggle="tab">Packages</a></li>
                                    <li role="presentation" class="<?php echo $active_class['system']; ?>"><a href="#systems" aria-controls="systems" role="tab" data-toggle="tab">Systems</a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane <?php echo $active_class['coil']; ?>" id="coils">
                                        <div class="row mb20">
                                            <div class="col-lg-6">
                                              <h3><div class="unitlist-header pull-left">Coils</div></h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-coil">Add New Coil</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="DT_Coils" class="table table-striped dt-responsive nowrap dataTable no-footer dtr-inline" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Brand</th>
                                                            <th>Model Number</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $coils = App\Coil::get();
                                                        $coils_json = [];
                                                        foreach ($coils as $key => $coil) {
                                                            $coils_json[$coil->id] = [
                                                                'id' => $coil->id,
                                                                'brand' => $coil->brand,
                                                                'model_number' => $coil->model_number,
                                                                'price' => $coil->price,
                                                                'width' => $coil->width,
                                                                'height' => $coil->height,
                                                                'depth' => $coil->depth,
                                                                'parts_warranty' => $coil->parts_warranty,
                                                                'labor_warranty' => $coil->labor_warranty,
                                                                'orientation' => $coil->orientation,
                                                                'markup' => $coil->markup,
                                                                'divide' => $coil->divide,
                                                                'image' => asset('/images/' . $coil->image),
                                                                'order_number' => $coil->order_number
                                                            ];
                                                            echo '<tr class="tr-coil tr-click" data-id="' . $coil->id . '">
					                                        		<td>' . $coil->id . '</td>
					                                        		<td>' . $coil->brand . '</td>
					                                        		<td>' . $coil->model_number . '</td>
				                                        		</tr>';
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane <?php echo $active_class['condenser']; ?>" id="condensers">
                                        <div class="row mb20">
                                            <div class="col-lg-6">                                                
                                                 <h3><div class="unitlist-header pull-left">Condensers</div></h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-condenser">Add New Condenser</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="DT_Condencers" class="table table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Brand</th>
                                                            <th>Model Number</th>
                                                            <th>Compressor Warranty</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php
$condensers = App\Condenser::get();
$condensers_json = [];
foreach ($condensers as $key => $condenser) {
    $condensers_json[$condenser->id] = [
        'id' => $condenser->id,
        'brand' => $condenser->brand,
        'model_number' => $condenser->model_number,
        'price' => $condenser->price,
        'width' => $condenser->width,
        'height' => $condenser->height,
        'depth' => $condenser->depth,
        'parts_warranty' => $condenser->parts_warranty,
        'labor_warranty' => $condenser->labor_warranty,
        'compressor_warranty' => $condenser->compressor_warranty,
        'hp_ac' => $condenser->hp_ac,
        'amps' => $condenser->amps,
        'stages' => $condenser->stages,
        'capacity' => $condenser->capacity,
        'location' => $condenser->location,
        'markup' => $condenser->markup,
        'divide' => $condenser->divide,
        'image' => asset('/images/' . $condenser->image),
        'order_number' => $condenser->order_number
    ];
    echo '<tr class="tr-condenser tr-click" data-id="' . $condenser->id . '">

					                                        		<td>' . $condenser->id . '</td>
					                                        		<td>' . $condenser->brand . '</td>
					                                        		<td>' . $condenser->model_number . '</td>
					                                        		<td>' . $condenser->compressor_warranty . '</td>
				                                        		</tr>';
}
// print_r($condensers_json);
?>				
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane <?php echo $active_class['thermostat']; ?>" id="thermostats">
                                        <div class="row mb20">
                                            <div class="col-lg-6">                                              
                                                <h3><div class="unitlist-header pull-left">Thermostats</div></h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-thermostat">Add New Thermostat</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="DT_thermostat" class="table table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Brand</th>
                                                            <th>Model Number</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php
$thermostats = App\Thermostat::get();
$thermostats_json = [];
foreach ($thermostats as $key => $thermostat) {
    $thermostats_json[$thermostat->id] = [
        'id' => $thermostat->id,
        'brand' => $thermostat->brand,
        'model_number' => $thermostat->model_number,
        'price' => $thermostat->price,
        'width' => $thermostat->width,
        'height' => $thermostat->height,
        'depth' => $thermostat->depth,
        'parts_warranty' => $thermostat->parts_warranty,
        'labor_warranty' => $thermostat->labor_warranty,
        'wifi' => $thermostat->wifi,
        'touchscreen' => $thermostat->touchscreen,
        'programmable' => $thermostat->programmable,
        'markup' => $thermostat->markup,
        'divide' => $thermostat->divide
        ,
        'image' => asset('/images/' . $thermostat->image),
        'order_number' => $thermostat->order_number
    ];
    echo '<tr class="tr-thermostat tr-click" data-id="' . $thermostat->id . '">

					                                        		<td>' . $thermostat->id . '</td>
					                                        		<td>' . $thermostat->brand . '</td>
					                                        		<td>' . $thermostat->model_number . '</td>
					                                        	</tr>';
}
?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane <?php echo $active_class['furnace']; ?>" id="furnaces">
                                        <div class="row mb20">
                                            <div class="col-lg-6">                                              
                                                  <h3><div class="unitlist-header pull-left">Furnace</div></h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-furnace">Add New Furnace</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="DT_furnace" class="table table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Brand</th>
                                                            <th>Model Number</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php
$furnaces = App\Furnace::get();
$furnaces_json = [];
foreach ($furnaces as $key => $furnace) {
    $id = $furnace->id;
    $furnaces_json[$id] = [
        'id' => $id,
        'brand' => $furnace->brand,
        'model_number' => $furnace->model_number,
        'price' => $furnace->price,
        'width' => $furnace->width,
        'height' => $furnace->height,
        'depth' => $furnace->depth,
        'parts_warranty' => $furnace->parts_warranty,
        'labor_warranty' => $furnace->labor_warranty,
        'heat_exchanger_warranty' => $furnace->heat_exchanger_warranty,
        'filter_base' => $furnace->filter_base,
        'afue' => $furnace->afue,
        'blower_type' => $furnace->blower_type,
        'blower_size' => $furnace->blower_size,
        'stages' => $furnace->stages,
        'capacity' => $furnace->capacity,
        'furnace_air_handler' => $furnace->furnace_air_handler,
        'markup' => $furnace->markup,
        'divide' => $furnace->divide,
        'location' => $furnace->location,
        'image' => asset('/images/' . $furnace->image),
        'order_number' => $furnace->order_number
    ];
    echo '<tr class="tr-furnace tr-click" data-id="' . $furnace->id . '">
					                                        		<td>' . $furnace->id . '</td>
					                                        		<td>' . $furnace->brand . '</td>
					                                        		<td>' . $furnace->model_number . '</td>
					                                        	</tr>';
}
?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane <?php echo $active_class['package']; ?>" id="packages">
                                        <div class="row mb20">
                                            <div class="col-lg-6">                                               
                                                  <h3><div class="unitlist-header pull-left">Packages</div></h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-packages">Add New Package</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="DT_packages" class="table table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Brand</th>
                                                            <th>Model Number</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php
$packages = App\Package::get();
$packages_json = [];
foreach ($packages as $key => $package) {
    $id = $package->id;
    $packages_json[$id] = [
        'id' => $id,
        'brand' => $package->brand,
        'model_number' => $package->model_number,
        'price' => $package->price,
        'width' => $package->width,
        'height' => $package->height,
        'depth' => $package->depth,
        'parts_warranty' => $package->parts_warranty,
        'labor_warranty' => $package->labor_warranty,
        'heat_exchanger_warranty' => $package->heat_exchanger_warranty,
        'compresssor_warranty' => $package->compresssor_warranty,
        'seer' => $package->seer,
        'eer' => $package->eer,
        'hspf' => $package->hspf,
        'afue' => $package->afue,
        'weight' => $package->weight,
        'stages_of_cooling' => $package->stages_of_cooling,
        'stages_of_heating' => $package->stages_of_heating,
        'blower_type' => $package->blower_type,
        'capacity_for_cooling' => $package->capacity_for_cooling,
        'capacity_for_heating' => $package->capacity_for_heating,
        'sensible_cooling' => $package->sensible_cooling,
        'tax_credit' => $package->tax_credit,
        'energy_star' => $package->energy_star,
        'utility_rebate' => $package->utility_rebate,
        'ahri' => $package->ahri,
        'hp_ac' => $package->hp_ac,
        'amps' => $package->amps,
        'markup' => $package->markup,
        'divide' => $package->divide,
        'location' => $package->location,
        'compressor_warranty' => $package->compressor_warranty
        ,
        'image' => asset('/images/' . $package->image),
        'order_number' => $package->order_number
    ];
    echo '<tr class="tr-package tr-click" data-id="' . $package->id . '">
					                                        		<td>' . $package->id . '</td>
					                                        		<td>' . $package->brand . '</td>
					                                        		<td>' . $package->model_number . '</td>
					                                        	</tr>';
}
?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane <?php echo $active_class['system']; ?>" id="systems">
                                        <div class="row mb20">
                                            <div class="col-lg-6">                                                
                                                  <h3><div class="unitlist-header pull-left">Systems</div></h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-systems">Add New System</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="DT_systems" class="table table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Name</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php
$systems = App\System::get();
$systems_json = [];
foreach ($systems as $key => $system) {
    $id = $system->id;
    $systems_json[$id] = [
        'id' => $id,
        'brand' => $system->brand,
        'model_number' => $system->model_number,
        'price' => $system->price,
        'width' => $system->width,
        'height' => $system->height,
        'depth' => $system->depth,
        'parts_warranty' => $system->parts_warranty,
        'labor_warranty' => $system->labor_warranty,
        'heat_exchanger_warranty' => $system->heat_exchanger_warranty,
        'compresssor_warranty' => $system->compresssor_warranty,
        'seer' => $system->seer,
        'eer' => $system->eer,
        'hspf' => $system->hspf,
        'afue' => $system->afue,
        'sensible_cooling' => $system->sensible_cooling,
        'tax_credit' => $system->tax_credit,
        'energy_star' => $system->energy_star,
        'utility_rebate' => $system->utility_rebate,
        'ahri' => $system->ahri,
        'hp_ac' => $system->hp_ac,
        'rw_system_id' => $system->rw_system_id,
        'location' => $system->location,
        'name' => $system->name,
        'condenser_id' => $system->condenser_id,
        'furnace_id' => $system->furnace_id,
        'coil_id' => $system->coil_id,
        'thermostat_id' => $system->thermostat_id,
        'markup' => $system->markup,
        'divide' => $system->divide,
        'stages_of_cooling' => $system->stages_of_cooling,
        'stages_of_heating' => $system->stages_of_heating,
        'compressor_warranty' => $system->compressor_warranty,
        'image' => asset('/images/' . $system->image),
        'order_number' => $system->order_number
    ];
    echo '<tr class="tr-system tr-click" data-id="' . $system->id . '">
																	<td>' . $system->id . '</td>
					                                        		<td>' . $system->name . '</td>
					                                        	</tr>';
}
?>


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@include ('modals.add-new-coil')
@include ('modals.add-new-condenser')
@include ('modals.add-new-thermostats')
@include ('modals.add-new-furnace')
@include ('modals.add-new-packages')
@include ('modals.add-new-systems')

@endsection

@section('customCss')
@endsection

@section('customJs')
    <script src="{{ asset('/js/plugins.js') }}"></script>
    <script type="text/javascript">
$(document).ready(function () {

    var language = {
        "search": "_INPUT_",
        "searchPlaceholder": "Search records ...",
        "lengthMenu": "_MENU_",
        "oPaginate": {
            "sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
            "sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
        }
    };


    var oTableCoils = $('#DT_Coils').DataTable({
        "stateSave": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
        "language": language,
    });


    $('#create-new-coil').on('click', function () {
        $("#image").hide();
        $(".submit-coil-cls").show();
        $("#submit-coil").hide();
        $('#add-new-coil input:not([name="_token"]):not(.btn-submit):not([name="divide"])').val('');
        $('[class*="delete"] .btn-submit').hide();
        $('#add-new-coil').modal('show');
    });

    /*Update Functionality*/



    $("#submit-coil").on("click", function (e)
    {
        $('#formcoil').attr("action", "{{ url('/update-coil') }}");
        $('#formcoil').submit();

    });

    $("#updatecondenser").on('click', function (e) {
        $('#formcondenser').attr("action", "{{ url('/update-condenser') }}");
        $('#formcondenser').submit();

    });

    $("#Thermostats").on('click', function (e) {
        $('#formThermostat').attr("action", "{{ url('/update-thermostat') }}");
        $('#formThermostat').submit();

    });


    $("#Furnaceupdate").on('click', function (e) {
        $('#formfurnace').attr("action", "{{ url('/update-furnace') }}");
        $('#formfurnace').submit();

    });


    $("#PackageID").on('click', function (e) {

        $('#PackageForm').attr("action", "{{ url('/update-package') }}");
        $('#PackageForm').submit();


    });




    $("#Systemid").on('click', function (e) {
        $('#SystemForm').attr("action", "{{ url('/update-system') }}");
        $('#SystemForm').submit();


    });


    $('#DT_Coils tbody').on('click', 'tr', function () {

        $(".submit-coil-cls").hide();
        $("#submit-coil").show();
        //$("#submit-coil").attr('value', "Update");
        $('[class*="delete"] .btn-submit').show();

        //$('[class*="delete"] .btn-submit').hide();
        $("#updateid").val($(this).attr("data-id"));
        $('#update-coil').modal('show');

    });


    $("#DT_Condencers tbody").on('click', 'tr', function () {
        $(".updatecondensercls").hide();
        $("#updatecondenser").show();
        $('[class*="delete"] .btn-submit').show();

        //$('[class*="delete"] .btn-submit').hide();
        $("#updatecondenserid").val($(this).attr("data-id"));
        $('#update-condenser').modal('show');


    });

    /*Update Functionality*/

    var oTableCondencers = $('#DT_Condencers').DataTable({
        "stateSave": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
        "language": language,
    });

    $('#create-new-condenser').on('click', function () {
        $("#image").hide();
        $(".updatecondensercls").show();
        $("#updatecondenser").hide();
        $('#add-new-condenser input:not([name="_token"]):not(.btn-submit):not([name="divide"])').val('');
        $('[class*="delete"] .btn-submit').hide();
        $('#add-new-condenser').modal('show');
    });




    $("#DT_thermostat tbody").on('click', 'tr', function () {
        $(".Thermostatscls").hide();
        $("#Thermostats").show();
        $('[class*="delete"] .btn-submit').show();

        //$('#Cdelete-form').hide();
        $("#ThermostatsID").val($(this).attr("data-id"));

    });


    $("#DT_furnace tbody").on('click', 'tr', function () {
        $(".Furnacecls").hide();
        $("#Furnaceupdate").show();
        $('[class*="delete"] .btn-submit').show();

        //	$('#Cdelete-form').hide();
        $("#FurnaceID").val($(this).attr("data-id"));
    });


    $("#DT_packages tbody").on('click', 'tr', function () {
        $(".Packagecls").hide();
        $("#PackageID").show();
        $('[class*="delete"] .btn-submit').show();

        //			$('#Cdelete-form').hide();
        $("#Packageid").val($(this).attr("data-id"));
    });


    $("#DT_systems tbody").on('click', 'tr', function () {
        $(".System-cls").hide();
        $("#Systemid").show();
        $('[class*="delete"] .btn-submit').show();

        //			$('#Cdelete-form').hide();
        $("#SystemID").val($(this).attr("data-id"));
    });


    var oTableThermostat = $('#DT_thermostat').DataTable({
        "stateSave": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
        "language": language,
    });
    $('#create-new-thermostat').on('click', function () {
        $("#image").hide();
        $(".Thermostatscls").show();
        $("#Thermostats").hide();
        $('#add-new-thermostat input:not([name="_token"]):not(.btn-submit):not([name="divide"])').val('');
        $('[class*="delete"] .btn-submit').hide();
        $('#add-new-thermostat').modal('show');
    });

    var oTableFurnace = $('#DT_furnace').DataTable({
        "stateSave": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
        "language": language,
    });
    $('#create-new-furnace').on('click', function () {
        $("#image").hide();
        $(".Furnacecls").show();
        $("#Furnaceupdate").hide();
        $('#add-new-furnace input:not([name="_token"]):not(.btn-submit):not([name="divide"])').val('');
        $('[class*="delete"] .btn-submit').hide();
        $('#add-new-furnace').modal('show');
    });

    var oTablePackages = $('#DT_packages').DataTable({
        "stateSave": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
        "language": language,
    });
    $('#create-new-packages').on('click', function () {
        $("#image").hide();
        $(".Packagecls").show();
        $("#PackageID").hide();
        $('#add-new-packages input:not([name="_token"]):not(.btn-submit):not([name="divide"])').val('');
        $('[class*="delete"] .btn-submit').hide();
        $('#add-new-packages').modal('show');
    });

    var oTableSystems = $('#DT_systems').DataTable({
        "stateSave": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
        "language": language,
    });
    $('#create-new-systems').on('click', function () {
        $("#image").hide();
        $(".System-cls").show();
        $("#Systemid").hide();
        $('#add-new-systems input:not([name="_token"]):not(.btn-submit):not([name="divide"])').val('');
        $('[class*="delete"] .btn-submit').hide();
        $('#add-new-systems').modal('show');
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = String(e.target); // newly activated tab
        var relatedTarget = String(e.relatedTarget); // previous active tab
        var tabID = target.split("#");
        if (tabID[1] == 'coils') {
            $('#DT_Coils').DataTable();
        }
        if (tabID[1] == 'condensers') {
            $('#DT_Condencers').DataTable();
        }
        if (tabID[1] == 'thermostats') {
            $('#DT_thermostat').DataTable();
        }
        if (tabID[1] == 'furnaces') {
            $('#DT_furnace').DataTable();
        }
        if (tabID[1] == 'packages') {
            $('#DT_packages').DataTable();
        }
        if (tabID[1] == 'systems') {
            $('#DT_systems').DataTable();
        }
    });
});
var coils = <?php echo json_encode($coils_json); ?>;
var condensers = <?php echo json_encode($condensers_json); ?>;
var thermostats = <?php echo json_encode($thermostats_json); ?>;
var furnaces = <?php echo json_encode($furnaces_json); ?>;
var packages = <?php echo json_encode($packages_json); ?>;
var systems = <?php echo json_encode($systems_json); ?>;
//console.log(condensers);
    </script>
    @endsection