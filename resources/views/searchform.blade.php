{{  Form::open(array('class'=>'form form-search','url'=>'/team', 'method' => 'post')) }}
	<div class="form-group no-icon">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for...">
			<span class="input-group-btn">
				<button class="btn btn-default" type="button">
					{{ Html::image('img/search-icon.svg', 'Search', array('width'=>'18px')) }}
				</button>
			</span>
		</div>
	</div>
{{ Form::close() }}