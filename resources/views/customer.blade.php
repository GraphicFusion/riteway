@extends('layouts.default')

@section('content')
<?php
	$user = getUser();
?>
<div class="customer">
	<div class="container-fluid">
	    <div class="row">
			<div class="col-md-12 page_content">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
		        		<h3 class="unitlist-header">
							Customer Information
						</h3>
					</div>
	        	</div>
				
				
				<div class="team-members">
		        	<div class="row">
						@include('forms.customer-form')
		        	</div>
				</div>

			</div>
	    </div>
	</div>
</div>

@include('modals.createestimate')
@endsection

@section('customCss')
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
@endsection