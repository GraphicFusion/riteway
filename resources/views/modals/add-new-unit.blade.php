<div class="modal fade gen-modal form-modal" id="add-new-unit">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">
					<svg version="1.1" id="list-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.7 23.7" style="enable-background:new 0 0 23.7 23.7;" xml:space="preserve">
									<path class="st0" d="M11.9,11.9c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5H11.9z M11.9,11.9c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5H11.9z
										 M11.9,11.9c-3,0-5.5-2.5-5.5-5.5s2.5-5.5,5.5-5.5V11.9z M11.9,11.9c3,0,5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5V11.9z"/>
								</svg>
					New Unit
	        	</h4>
	      	</div>
	  		<div class="modal-body">
				<div class="panel-body">
			  		<div class="row">
	            		<div class="col-lg-12">
			            {{  Form::open(array('class'=>'form','url'=>'/unit', 'method' => 'post', 'files'=>true)) }}

							<div class="form-group @if ($errors->has('unit_name')) has-error @endif">
								{{ Form::label('unit_name', 'Name') }}
								{{ Form::text('unit_name', '', ['class'=>'form-control']) }}
							
							</div>
							<div class="form-group{{ $errors->has('unit_brand') ? ' has-error' : '' }}">
								<?php $brands = []; $brandArray = getBrands(); foreach( $brandArray as $brand ) : ?>
									<?php $id = $brand['id']; $brands[$id] = $brand['name']; ?>
								<?php endforeach; ?>
								{{ Form::label('unit_brand', 'Brand') }}
								{{ Form::select('unit_brand', $brands , '0', ['class'=>'form-control']) }}

                                @if ($errors->has('unit_brand'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('brand') }}</strong>
                                    </span>
                                @endif
                            </div>
							<div class="form-group @if ($errors->has('unit_type')) has-error @endif">
								{{ Form::label('unit_type', 'Type') }}
								{{ Form::select('unit_type', array('furnace'=>'Furnace','package'=>'Package Unit','split'=>'Split Unit'), '0', ['class'=>'form-control']) }}
							
							</div>
							<div class="clearfix">
								{{ Form::submit('Submit', array('class' => 'btn-submit btn btn-primary btn-block')) }}
							</div>							
					{{ Form::close() }}  

						</div>
			  		</div>
				</div>
      		</div>
		</div>
  	</div>
</div>