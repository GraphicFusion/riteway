{{-- Add Section --}}
<div class="modal fade" id="sectionModal">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
	  		<div class="modal-body">
		  		<div class="row">
		  			<div class="col-lg-12">
			            {{  Form::open(array('class'=>'form', 'method' => 'post', 'files'=>true)) }}
							<div class="form-group">
								{{ Form::label('unit_location', 'Unit Location') }}
								{{ Form::text('unit_location', '', ['class'=>'form-control']) }}
								<div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>								
							</div>
						{{ Form::close() }}  
		  			</div>
		  		</div>
      		</div>
		</div>
  	</div>
</div>