<div class="modal fade" id="addNewSplit">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">
					<svg version="1.1" id="list-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.7 23.7" style="enable-background:new 0 0 23.7 23.7;" xml:space="preserve">
						<path class="st0" d="M11.9,11.9c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5H11.9z M11.9,11.9c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5H11.9z
							 M11.9,11.9c-3,0-5.5-2.5-5.5-5.5s2.5-5.5,5.5-5.5V11.9z M11.9,11.9c3,0,5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5V11.9z"/>
					</svg>
					New Split
	        	</h4>
	      	</div>
	  		<div class="modal-body">
				<div class="panel-body">
			  		<div class="row">
	            		<div class="col-lg-12">
				            {{ Form::open(array('class'=>'form','url'=>'/unit-list/split', 'method' => 'post')) }}

								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('model', 'Model') }}
											{{ Form::text('model', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('brand', 'Brand') }}
											{{ Form::text('brand', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('pics', 'Pics') }}
											{{ Form::text('pics', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('condenser', 'Condenser') }}
											{{ Form::text('condenser', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('filter_base', 'Filter Base') }}
											{{ Form::text('filter_base', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('furnace', 'Furnace') }}
											{{ Form::text('furnace', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('coil', 'Coil') }}
											{{ Form::text('coil', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('cu', 'CU') }}
											{{ Form::text('cu', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('furn_ah', 'Furn/AH') }}
											{{ Form::text('furn_ah', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('price', 'Price') }}
											{{ Form::text('price', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('size', 'Size') }}
											{{ Form::text('size', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('seer', 'Seer') }}
											{{ Form::text('seer', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('eer', 'EER') }}
											{{ Form::text('eer', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('hspf', 'HSPF') }}
											{{ Form::text('hspf', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('afue', 'AFUE') }}
											{{ Form::text('afue', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('vs', 'VS') }}
											{{ Form::text('vs', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('fw', 'FW') }}
											{{ Form::text('fw', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('fh', 'FH') }}
											{{ Form::text('fh', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('cw', 'CW') }}
											{{ Form::text('cw', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('ch', 'CH') }}
											{{ Form::text('ch', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('tot_w', 'Tot W') }}
											{{ Form::text('tot_w', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('tot_h', 'Tot H') }}
											{{ Form::text('tot_h', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('cu_w', 'CU W') }}
											{{ Form::text('cu_w', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('cu_d', 'CU D') }}
											{{ Form::text('cu_d', '', ['class'=>'form-control']) }}
										</div>
									</div>																	
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('cu_h', 'CU H') }}
											{{ Form::text('cu_h', '', ['class'=>'form-control']) }}
										</div>
									</div>	
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('c_stage', 'C Stage') }}
											{{ Form::text('c_stage', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('ht_stage', 'HT Stage') }}
											{{ Form::text('ht_stage', '', ['class'=>'form-control']) }}
										</div>
									</div>									
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('cu_amp', 'CU Amp') }}
											{{ Form::text('cu_amp', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('f_ah_amp', 'F AH AMP') }}
											{{ Form::text('f_ah_amp', '', ['class'=>'form-control']) }}
										</div>
									</div>									
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('compressor', 'Compressor') }}
											{{ Form::text('compressor', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('ht_exch', 'HT Exch') }}
											{{ Form::text('ht_exch', '', ['class'=>'form-control']) }}
										</div>
									</div>									
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('parts', 'Parts') }}
											{{ Form::text('parts', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('labor', 'Labor') }}
											{{ Form::text('labor', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('stat', 'Stat') }}
											{{ Form::text('stat', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('capacity', 'Capacity') }}
											{{ Form::text('capacity', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('gaurds', 'Gaurds') }}
											{{ Form::text('gaurds', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('ari', 'ARI') }}
											{{ Form::text('ari', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('high', 'High') }}
											{{ Form::text('high', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											{{ Form::label('low', 'Low') }}
											{{ Form::text('low', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group @if ($errors->has('markup')) has-error @endif">
											{{ Form::label('markup', 'Markup') }}
											{{ Form::text('markup', '', ['class'=>'form-control']) }}
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group @if ($errors->has('divide')) has-error @endif">
											{{ Form::label('divide', 'Divide') }}
											{{ Form::checkbox('divide', '1', '', ['class'=>'form-control']) }}
										</div>
									</div>
								</div>

								{{ Form::submit('Submit', array('class' => 'btn btn-primary btn-block btn-lg')) }}

							{{ Form::close() }}  
						</div>
			  		</div>
				</div>
      		</div>
		</div>
  	</div>
</div>