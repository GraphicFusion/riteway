{{-- Show Proposal Details --}}
<div class="modal fade gen-modal form-modal" id="add-new-leadgen">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">
					<svg version="1.1" id="team-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.3 21.3" style="enable-background:new 0 0 21.3 21.3;" xml:space="preserve">
						<g>
							<path class="st0" d="M10.6,0.6c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S16.1,0.6,10.6,0.6z M10.6,3.6c1.7,0,3,1.3,3,3
								s-1.3,3-3,3s-3-1.3-3-3S9,3.6,10.6,3.6z M10.6,17.8c-2.5,0-4.7-1.3-6-3.2c0-2,4-3.1,6-3.1c2,0,6,1.1,6,3.1
								C15.3,16.5,13.1,17.8,10.6,17.8z"/>
						</g>
					</svg>
					New Lead Generator
	        	</h4>
	      	</div>
	  		<div class="modal-body">
				<div class="panel-body">
			  		<div class="row">
	            		<div class="col-lg-12">
			            {{  Form::open(array('class'=>'form','url'=>'/new-leadgen', 'method' => 'post', 'files'=>true)) }}
							{{ Form::hidden('source_id', '', ['class'=>'form-control']) }}							
							<div class="form-group @if ($errors->has('first_name')) has-error @endif">
								{{ Form::label('first_name', 'First Name') }}
								{{ Form::text('first_name', '', ['class'=>'form-control']) }}							
							</div>
							<div class="form-group @if ($errors->has('last_name')) has-error @endif">
								{{ Form::label('last_name', 'Last Name') }}
								{{ Form::text('last_name', '', ['class'=>'form-control']) }}							
							</div>
							<div class="form-group{{ $errors->has('leadgen_email') ? ' has-error' : '' }}">
                                <label for="leadgen_email" class="control-label">E-Mail Address</label>
                                <input id="leadgen_email" type="email" class="form-control" name="leadgen_email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
							<div class="clearfix">
								{{ Form::submit('Submit', array('class' => 'btn-submit btn btn-primary btn-block')) }}
							</div>							
					{{ Form::close() }}  

						</div>
			  		</div>
				</div>
      		</div>
		</div>
  	</div>
</div>