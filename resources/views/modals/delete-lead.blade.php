{{-- 
	Delete lead form load via jquery 
	depends on route /settings/lead-generators/delete/{id}
--}}
<div class="modal-content">
	<div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	<h4 class="modal-title">
			<svg version="1.1" id="team-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.3 21.3" style="enable-background:new 0 0 21.3 21.3;" xml:space="preserve">
				<g>
					<path class="st0" d="M10.6,0.6c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S16.1,0.6,10.6,0.6z M10.6,3.6c1.7,0,3,1.3,3,3
						s-1.3,3-3,3s-3-1.3-3-3S9,3.6,10.6,3.6z M10.6,17.8c-2.5,0-4.7-1.3-6-3.2c0-2,4-3.1,6-3.1c2,0,6,1.1,6,3.1
						C15.3,16.5,13.1,17.8,10.6,17.8z"/>
				</g>
			</svg>
			Delete Lead - {{ $user->name }}
		</h4>
  	</div>
	<div class="modal-body">
  		<div class="row">
    		<div class="col-lg-12">
	            {{  Form::open(array('class'=>'form','url'=>'/settings/lead-generators/delete/', 'method' => 'post', 'files'=>true)) }}
					{{ Form::hidden('uid', $user->id) }}
					<h3 class="text-center">Are you sure to delete this lead.</h3>
					<div class="clearfix text-center">
						{{ Form::submit('Yes, Please Delete...', array('class' => 'btn-submit btn btn-danger')) }}
						<button type="button" class="btn btn-default" data-dismiss="modal">No, Please Wait...</button>
					</div>							
				{{ Form::close() }}  
			</div>
  		</div>
	</div>
</div>
