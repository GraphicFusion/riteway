{{-- Show Proposal Details --}}
<div class="modal fade" id="add-new-condenser">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">
					<svg version="1.1" id="team-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.3 21.3" style="enable-background:new 0 0 21.3 21.3; width:18px; height:18px;" xml:space="preserve">
						<g>
							<path class="st0" d="M10.6,0.6c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S16.1,0.6,10.6,0.6z M10.6,3.6c1.7,0,3,1.3,3,3
								s-1.3,3-3,3s-3-1.3-3-3S9,3.6,10.6,3.6z M10.6,17.8c-2.5,0-4.7-1.3-6-3.2c0-2,4-3.1,6-3.1c2,0,6,1.1,6,3.1
								C15.3,16.5,13.1,17.8,10.6,17.8z"/>
						</g>
					</svg>
					Add New Condenser
	        	</h4>
	      	</div>
	  		<div class="modal-body">
				<div class="panel-body">
			  		<div class="row">
	            		<div class="col-lg-12">

			            {{  Form::open(array('class'=>'form', 'id'=>'formcondenser','url'=>'/new-condenser', 'method' => 'post', 'files'=>true)) }}

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('brand')) has-error @endif">
										{{ Form::label('brand', 'Brand') }}
										{{ Form::text('brand', '', ['class'=>'form-control']) }}			
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('model_number')) has-error @endif">
										{{ Form::label('model_number', 'Model Number') }}
										{{ Form::text('model_number', '', ['class'=>'form-control']) }}	
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('width')) has-error @endif">
										{{ Form::label('width', 'Width (in)') }}
										{{ Form::text('width', '', ['class'=>'form-control']) }}			
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('height')) has-error @endif">
										{{ Form::label('height', 'Height (in)') }}
										{{ Form::text('height', '', ['class'=>'form-control']) }}	
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('depth')) has-error @endif">
										{{ Form::label('depth', 'Depth (in)') }}
										{{ Form::text('depth', '', ['class'=>'form-control']) }}	
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('price')) has-error @endif">
										{{ Form::label('price', 'Price ($)') }}
										{{ Form::text('price', '', ['class'=>'form-control']) }}			
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('parts_warranty')) has-error @endif">
										{{ Form::label('parts_warranty', 'Parts Warranty (years)') }}
										{{ Form::text('parts_warranty', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('labor_warranty')) has-error @endif">
										{{ Form::label('labor_warranty', 'Labor Warranty (years)') }}
										{{ Form::text('labor_warranty', '', ['class'=>'form-control']) }}	
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('compressor_warranty')) has-error @endif">
										{{ Form::label('compressor_warranty', 'Compressor Warranty (years)') }}
										{{ Form::text('compressor_warranty', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('hp_ac')) has-error @endif">
										{{ Form::label('hp_ac', 'HP / AC') }}
										<select name="hp_ac" id="hp_ac" class="form-control">
											<option value="HP">HP</option>
											<option value="AC">AC</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('compressor_warranty')) has-error @endif">
										{{ Form::label('amps', 'Amps') }}
										{{ Form::text('amps', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('stages')) has-error @endif">
										{{ Form::label('stages', 'Stages') }}
										<select name="stages" id="stages" class="form-control">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="Variable">Variable</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('capacity')) has-error @endif">
										{{ Form::label('capacity', 'Capacity') }}
										{{ Form::text('capacity', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							{{-- <div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('picture')) has-error @endif">
										{{ Form::label('picture', 'Picture') }}
										{{ Form::file('picture', ['class'=>'form-control']) }}
									</div>
								</div>
							</div> --}}
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('markup')) has-error @endif">
										{{ Form::label('markup', 'Markup') }}
										{{ Form::text('markup', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('order_number')) has-error @endif">
										{{ Form::label('order_number', 'Order Number') }}
										{{ Form::text('order_number', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
				                <div class="col-md-6">
				                    <img src="" id="image" style="height:45px;display:none;margin-bottom: 15px;" disabled>
				                    {!! Form::file('image', array('class' => 'form-control')) !!}
				                </div>
			                	<div class="col-sm-6">
									<div class="form-group @if ($errors->has('divide')) has-error @endif">
										{{ Form::label('divide', 'Divide') }}
										{{ Form::checkbox('divide', '1', '', ['class'=>'form-control']) }}
									</div>
								</div>
			                </div>

							<div class="clearfix text-right">
								{{ Form::submit('Submit', array('class' => 'btn-submit btn btn-primary updatecondensercls')) }}
								{{ Form::submit('Update', array('class' => 'btn-submit btn btn-primary', 'id' => 'updatecondenser')) }}
								{{ Form::hidden('updatecondenserid', '', ['class'=>'form-control', 'id' => 'updatecondenserid']) }}
							</div>

						{{ Form::close() }}
			            {{  Form::open(array('class'=>'form condenser-delete','url'=>'/delete-condenser', 'method' => 'post' )) }}
							{{ Form::hidden('id', '', ['class'=>'form-control']) }}
							{{ Form::button('Delete', array('type'=>"submit", 'class' => 'btn-submit btn btn-danger','id'=>"delete-form")) }}
						{{ Form::close() }}


						</div>
			  		</div>
				</div>
      		</div>
		</div>
  	</div>
</div>