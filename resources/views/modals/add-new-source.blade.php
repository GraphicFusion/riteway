<div class="modal fade gen-modal form-modal" id="add-new-source">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">
					Lead Source
	        	</h4>
	      	</div>
	  		<div class="modal-body">
				<div class="panel-body">
			  		<div class="row">
	            		<div class="col-lg-12">
			            {{  Form::open(array('class'=>'form','url'=>'/source', 'method' => 'post', 'files'=>true)) }}

							<div class="form-group @if ($errors->has('source_name')) has-error @endif">
								{{ Form::label('source_name', 'Name') }}
								{{ Form::text('source_name', '', ['class'=>'form-control']) }}
								<br>
								{{ Form::select('source_store', array('lowes' => "Lowe's", 'costco' => 'Costco'), 'lowes', ['class'=>'form-control', 'id'=>'source_store']) }}
								{{ Form::hidden('source_id', '', ['class'=>'form-control']) }}
							
							</div>
							<div class="clearfix">
								{{ Form::submit('Submit', array('class' => 'btn-submit btn btn-primary btn-block')) }}
							</div>							
					{{ Form::close() }}  

						</div>
			  		</div>
				</div>
      		</div>
		</div>
  	</div>
</div>