{{-- Show Proposal Details --}}
<div class="modal fade" id="add-new-systems">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">
					<svg version="1.1" id="team-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.3 21.3" style="enable-background:new 0 0 21.3 21.3; width:18px; height:18px;" xml:space="preserve">
						<g>
							<path class="st0" d="M10.6,0.6c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S16.1,0.6,10.6,0.6z M10.6,3.6c1.7,0,3,1.3,3,3
								s-1.3,3-3,3s-3-1.3-3-3S9,3.6,10.6,3.6z M10.6,17.8c-2.5,0-4.7-1.3-6-3.2c0-2,4-3.1,6-3.1c2,0,6,1.1,6,3.1
								C15.3,16.5,13.1,17.8,10.6,17.8z"/>
						</g>
					</svg>
					Add New System
	        	</h4>
	      	</div>
	  		<div class="modal-body">
				<div class="panel-body">
			  		<div class="row">
	            		<div class="col-lg-12">

			            {{  Form::open(array('class'=>'form', 'id' =>'SystemForm','url'=>'/new-systems', 'method' => 'post', 'files'=>true)) }}

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('name')) has-error @endif">
										{{ Form::label('name', 'System Name') }}
										{{ Form::text('name', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('parts_warranty')) has-error @endif">
										{{ Form::label('parts_warranty', 'Parts Warranty (years)') }}
										{{ Form::text('parts_warranty', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('labor_warranty')) has-error @endif">
										{{ Form::label('labor_warranty', 'Labor Warranty (years)') }}
										{{ Form::text('labor_warranty', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('heat_exchanger_warranty')) has-error @endif">
										{{ Form::label('heat_exchanger_warranty', 'Heat Exchanger Warranty (years)') }}
										{{ Form::text('heat_exchanger_warranty', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('compressor_warranty')) has-error @endif">
										{{ Form::label('compressor_warranty', 'Compressor Warranty (years)') }}
										{{ Form::text('compressor_warranty', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('seer')) has-error @endif">
										{{ Form::label('seer', 'SEER') }}
										{{ Form::text('seer', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('eer')) has-error @endif">
										{{ Form::label('eer', 'EER') }}
										{{ Form::text('eer', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('hspf')) has-error @endif">
										{{ Form::label('hspf', 'HSPF') }}
										{{ Form::text('hspf', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('afue')) has-error @endif">
										{{ Form::label('afue', 'AFUE') }}
										{{ Form::text('afue', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('sensible_cooling')) has-error @endif">
										{{ Form::label('stages_of_cooling', 'Stages of Cooling') }}
										{{ Form::text('stages_of_cooling', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('sensible_cooling')) has-error @endif">
										{{ Form::label('stages_of_heating', 'Stages of Heating') }}
										{{ Form::text('stages_of_heating', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('tax_credit')) has-error @endif">
										{{ Form::label('tax_credit', 'Tax Credit') }}
										<div class="clearfix">

											<label class="radio-inline">

											  	{{ Form::radio('tax_credit', 0, false) }} No
											</label>
											<label class="radio-inline">
											  	{{ Form::radio('tax_credit', 1, true) }} Yes
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('energy_star')) has-error @endif">
										{{ Form::label('energy_star', 'Energy Star') }}
										<div class="clearfix">
											<label class="radio-inline">
											  	{{ Form::radio('energy_star', 0, false) }} No
											</label>
											<label class="radio-inline">
											  	{{ Form::radio('energy_star', 1, true) }} Yes
											</label>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('utility_rebate')) has-error @endif">
										{{ Form::label('utility_rebate', 'Utility Rebate') }}
										<div class="clearfix">
											<label class="radio-inline">
											  	{{ Form::radio('utility_rebate', 0, false) }} No
											</label>
											<label class="radio-inline">
											  	{{ Form::radio('utility_rebate', 1, true) }} Yes
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('ahri')) has-error @endif">
										{{ Form::label('ahri', 'AHRI') }}
										{{ Form::text('ahri', '', ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group @if ($errors->has('rw_system_id')) has-error @endif">
										{{ Form::label('rw_system_id', 'RW System Id') }}
										{{ Form::text('rw_system_id', '', ['class'=>'form-control']) }}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<?php
										$coils = App\Coil::get();
										$select_coils = [];
										$select_coils[0] = "N/A";
										foreach($coils as $key => $coil) {
											$select_coils[$coil->id] = $coil->brand." ".$coil->model_number;
										}
									?>
									<div class="form-group @if ($errors->has('coil_id')) has-error @endif">
										{{ Form::label('coil_id', 'Coil') }}
										{{ Form::select('coil_id', $select_coils, null, ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<?php
										$furnaces = App\Furnace::get();
										$select_furnaces = [];
										foreach($furnaces as $key => $furnace) {
											$select_furnaces[$furnace->id] = $furnace->brand." ".$furnace->model_number;
										}
									?>
									<div class="form-group @if ($errors->has('furnace_id')) has-error @endif">
										{{ Form::label('furnace_id', 'Furnace') }}
										{{ Form::select('furnace_id', $select_furnaces, null, ['class'=>'form-control']) }}
									</div>
								</div>
								<div class="col-sm-6">
									<?php
										$coils = App\Coil::get();
										$select_coils = [];
										foreach($coils as $key => $coil) {
											$select_coils[$coil->id] = $coil->brand." ".$coil->model_number;
										}
									?>
									<?php
										$condensers = App\Condenser::get();
										$select_condensers = [];
										foreach($condensers as $key => $condenser) {
											$select_condensers[$condenser->id] = $condenser->brand." ".$condenser->model_number;
										}
									?>

									<div class="form-group @if ($errors->has('condenser_id')) has-error @endif">
										{{ Form::label('condenser_id', 'Condenser') }}
										{{ Form::select('condenser_id', $select_condensers, null, ['class'=>'form-control']) }}
									</div>
								</div>
							</div>

							<div class="row">
				                <div class="col-md-6">
				                    <img src="" id="image" style="height:45px;display:none;margin-bottom: 15px;" disabled>
				                    {!! Form::file('image', array('class' => 'form-control')) !!}
				                </div>
				                <div class="col-sm-6">
									<div class="form-group @if ($errors->has('order_number')) has-error @endif">
										{{ Form::label('order_number', 'Order Number') }}
										{{ Form::text('order_number', '', ['class'=>'form-control']) }}
									</div>
								</div>
			                </div>

							<div class="clearfix text-right">
								{{ Form::submit('Submit', array('class' => 'btn-submit btn btn-primary System-cls')) }}
								{{ Form::submit('Update', array('class' => 'btn-submit btn btn-primary' , 'id' => 'Systemid')) }}
								{{ Form::hidden('SystemID', '', ['class'=>'form-control', 'id'=>'SystemID']) }}
							</div>

						{{ Form::close() }}
			            {{  Form::open(array('class'=>'form system-delete','url'=>'/delete-system', 'method' => 'post' )) }}
							{{ Form::hidden('id', '', ['class'=>'form-control']) }}
							{{ Form::button('Delete', array('type'=>"submit", 'class' => 'btn-submit btn btn-danger','id'=>"delete-form")) }}
						{{ Form::close() }}

						</div>
			  		</div>
				</div>
      		</div>
		</div>
  	</div>
</div>