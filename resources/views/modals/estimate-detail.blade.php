{{-- Show Proposal Details Complete with carousel slide details --}}
<?php 
    if(!@$user_type){
        $user_type = "";
    }
?>
<div class="modal fade <?php echo $login_class; ?>" id="proposalDetailAltModal" data-keyboard="false" data-backdrop="static">
    <style>
        @media (min-width: 992px)
        {
            .modal-lg {
                width: 1450px;
                margin: 0 auto !important;
            }
            #proposalDetailAltModal .carousel-control {
                width: 90px;
                height: 50px;
                background: transparent;
                opacity: 1;
                filter: alpha(opacity=100);
                line-height: 50px;
            }
            #proposalDetailAltModal .carousel-control svg {
                fill: #2196F3;
                width: 18px;
            }
            .close {
                float: left;
                font-size: 36px;
                font-weight: bold;
                line-height: 1;
                color: rgb(0, 0, 0);
                text-shadow: rgb(255, 255, 255) 0px 1px 0px;
                opacity: 0.2;
            }
            #proposalDetailAltModal .carousel-control.right {
                top: 57px;
                border-radius: 6px 0 0 6px;
                text-align: left;
                padding-left: 0px;
            }
            .carousel-control
            {
                left:auto;
                right:0;
            }
            #proposalDetailAltModal .carousel-control.left {
                top: 57px;
                border-radius: 0 6px 6px 0;
                text-align: left;
                padding-right: 0px;
                right: 50px;
            }
            #proposalDetailAltModal .modal-content .modal-header {
                padding: 40px 30px;
                border-bottom: none;
                background-color: #fff;
            }
            #proposalDetailAltModal #carousel-proposal-details .item .details {
                padding: 13px 0 15px;
                background: #fff;
                border: none; 
                border-radius: 4px;
                margin-top: 20px;
            }
            .divide
            {
                border-right: 2px solid #ddd;
            }
            .boxx .badge {
                width: 100%;
                height: 45px;
                padding: 0;
                background-size: 100% 100% !important;
                border-radius: 0;
                line-height: 46px;
                text-transform: uppercase;
                letter-spacing: 1px;
                font-size: 16px;
                text-align: center;
                text-indent: 10px;
                position: relative;
                margin: 12px 0 12px 0px;
            }
            #proposalDetailAltModal #carousel-proposal-details .thumbnail .caption .table > tbody > tr > td:last-child, #proposalDetailAltModal #carousel-proposal-details .thumbnail .caption .table > tfoot > tr > td:last-child {
                text-align: left;
            }
            #proposalDetailAltModal #carousel-proposal-details .thumbnail .caption .table > tbody > tr > td, #proposalDetailAltModal #carousel-proposal-details .thumbnail .caption .table > tfoot > tr > td {
                padding: 10px 0 10px;
                font-size: 14px;
                letter-spacing: 1px;
                /* font-weight: 700; */
                text-transform: uppercase;
                color: #9E9E9E;
                vertical-align: middle;
            }
            .system-price, .box-price, .total-due, .total-investment, .finance-fee, .box-store-fee, .finance-rate, .qualified-amount
            {
                color:#000 !important;
                font-size: 18px !important;
            }
            .box_border {
                border: 1px solid #E0E0E0;
                margin-right: 13px;
                width: 23%;
                background-color: #fff;
                box-shadow: 2px 3px 5px #888888;
                margin-left: 13px;
            }
            #proposalDetailAltModal #carousel-proposal-details .item .details.pd0-15-0-15 {
                padding: 0 15px 0 15px !important;
                background-color: transparent;
            }
            .platinum-save
            {
                border-radius: 0px !important;
                background: url(img/platinum.png) !important;
                background-repeat: no-repeat;
                width: 100% !important;
                background-size: cover !important;;
                color: #fff !important;
                padding: 10px;
                border: 0px;
            }
            .gold-save
            {
                border-radius: 0px !important;
                background: url(img/gold.png) !important;
                background-repeat: no-repeat;
                width: 100% !important;
                background-size: cover !important;;
                color: #fff !important;
                padding: 10px;
                border: 0px;
            }
            .silver-save
            {
                border-radius: 0px !important;
                background: url(img/silver.png) !important;
                background-repeat: no-repeat;
                width: 100% !important;
                background-size: cover !important;;
                color: #fff !important;
                padding: 10px;
                border: 0px;
            }
            .bronze-save
            {
                border-radius: 0px !important;
                background: url(img/bronze.png) !important;
                background-repeat: no-repeat;
                width: 100% !important;
                background-size: cover !important;;
                color: #fff !important;
                padding: 10px;
                border: 0px;
            }
            .payment_form
            {
                width:100%;
                background-color: #fff;
                padding: 10px;
            }
            .row.is-flex
            {
                border-bottom: 0px;
            }
            .payment_content
            {
                width:800px;
                margin: 0 auto;
            }
            .payment_content .form-control {
                height: 60px;
                line-height: 60px;
                border-radius: 6px;
                padding-top: 0;
                padding-bottom: 0;                
                box-shadow: 2px 1px 2px #888888 !important;
            }
            .input-wrapper .form-control {
                height: 65px;
                line-height: 65px;
                border-radius: 3px;
                padding-top: 0;
                padding-bottom: 0;                
                box-shadow: 2px 1px 2px #888888 !important;
            }
            #proposalDetailAltModal #carousel-proposal-details .thumbnail .caption .table > tbody > tr > td .input-wrapper::before, #proposalDetailAltModal #carousel-proposal-details .thumbnail .caption .table > tfoot > tr > td .input-wrapper::before {
                content: "(-)";
                position: absolute;
                left: 4px;
                top: 23px;
                font-size: 14px;
                z-index: 1;
            }
            .payment_content div
            {
                margin-bottom: 25px;
            }
            .table {
                border-top: transparent !important;
                border-bottom: 1px solid #ddd;
            }
            #commission_block
            {
                background-color: #333;
                color:#fff;
            }
            #commission_block table
            {
                border-top: transparent !important;
                border-bottom: transparent !important;
            }
            .commission_block_inner
            {
                width:800px;
                margin: 0px auto;
                padding: 10px;
            }
            .commission_block_inner .table > tbody > tr > th, .table > tbody > tr > td {
                font-size: 14px;
                color: #fff;
                border: none;
            }
            #admin_only> tbody > tr > th, #admin_only > tbody > tr > td {
                font-size: 14px;
                color: #333;
                border: none;
            }
            #commission_block_tbl> tbody > tr > th,  #commission_block_tbl > tbody > tr > td {
                font-size: 14px;
                color: #fff;
                border: none;
            }
            .controls-wrapper{
                display:none; 
            }
            .empty-system .thumbnail, .empty-system .financing_select, .empty-system h3{
                display:none !important;
            }
        </style>
        {{-- <div class="strip"></div> --}}
        <div class="strip"></div>
        <div class="modal-dialog modal-lg" role="document" id="print_block">
            <div class="modal-content">
                <div class="modal-header">
                    <?php if(Auth::user() ): ?>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php endif; ?>
                    <h4 class="modal-title">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 divide">
                            <img src="{{URL::to('img/logo.svg')}}" style="width:250px; margin-top: -15px; margin-left:20px;">                                 
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 divide">
                            Opportunity: <br><span class="proposal-id" style="margin-right:20px;"></span>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 divide">
                            Estimate:<br> <span id="alt-display-estimate-name"></span>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 hide-print">
                           <?php if(Auth::user() ): ?>

                                <a id="edit_href" class="btn btn-link" style="color:#2196F3;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Edit</a> <br>
                                <a id="proposal_href" class="btn btn-link" style="color:#2196F3;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Proposal</a>
                            <?php endif; ?>
                        </div>
                        <div class="show-print">
                            Date: <br><span class="alt-rep-start" style="margin-right:20px;"></span>
                        </div>
                        <div class="show-print">
                            Expiry:<br> <span class="alt-expiration"></span>
                        </div>
                        <div class="initial-here show-print">CUSTOMER INITIAL:  <span></span></div>
                    </h4>
                </div>
                <div class="modal-body">

                    {{-- Carousel --}}
                    <div id="carousel-proposal-details" class="carousel slide" data-ride="arousel" data-interval="false">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="details">
                                    <div class="row is-flex">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-print-12 bdr-right">
                                            <div class="boxx pd40">
                                                <dl class="dl-horizontal">
                                                    <dt>Name</dt>
                                                    <dd id="alt-customer"></dd>
                                                    <dt>Phone</dt>
                                                    <dd id="alt-customer-phone"></dd>
                                                    <dt>Email</dt>
                                                    <dd id="alt-customer-email"></dd>
                                                    <dt>Address</dt>
                                                    <dd id="alt-address"></dd>
                                                </dl>
                                                <div style="display:inline-block;">
                                                    {{ Form::open(array('url' => 'estimate-email')) }}
                                                        {{Form::hidden('estimate_id','', ['class'=>'estimate_id form-control']) }}
                                                        <button class="btn btn-info plan-chosen" id="email-estimate" data-estimate-id="" >Email</button> 
                                                    {{ Form::close() }}
                                                </div>
                                                 <a href="javascript:window.print();" class="btn btn-info plan-chosen" >Print</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-print-12">
                                            <div class="boxx pd40">
                                                <dl class="dl-horizontal">
                                                    <dt>Rep</dt>
                                                    <dd id="alt-rep"></dd>
                                                    <dt>Cell</dt>
                                                    <dd id="alt-rep-phone"></dd>
                                                    <dt>Email</dt>
                                                    <dd id="alt-rep-email"></dd>
                                                    <dt>Day</dt>
                                                    <dd class="alt-rep-start"></dd>
                                                    <dt>Expir.</dt>
                                                    <dd class="alt-expiration"></dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row is-flex">
                                        <div class="col-lg-12">
                                            <div class="boxx pd40">
                                                <h3 id="replace_title">Gas Furnace Replace</h3>
                                                <p id="replace_text"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $cats = getPresentationCategories();
                                    $output = "";
                                    foreach ($cats as $cat) {
                                        if ('none' != $cat['id']) {
                                            $output .= '<div class="row bdr-bottom">
                                                <div class="col-lg-12">
                                                    <div class="boxx pd40 pdb0">
                                                        <h3>' . $cat['name'] . '</h3>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="boxx pd40">
                                                        <dl id="' . $cat['id'] . '_wrap_1">
                                                        </dl>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="boxx pd40">
                                                        <dl id="' . $cat['id'] . '_wrap_2">
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>';
                                        }
                                    }
                                    echo $output;
                                    ?>
                                </div>

                            </div>
                            <?php
                            $select = "";
                            $plans = getFinancePlans();
                            foreach ($plans as $key => $plan) {
                                $select .= '<option value="' . $key . '">' . $plan['title'] . '</option>';
                            }
                            ?>
                            <script>
                                window.finance_plans = <?php echo json_encode($plans); ?>;
                            </script>
                            <div class="item">
                                <div class="details plans pd0-15-0-15">
                                    <header class="show-print">
                                        <div>
                                            RITEWAY HEATING, COOLING, AND PLUMBING                                
                                        </div>
                                        <div>
                                            Opportunity: <br><span class="proposal-id" style="margin-right:20px;"></span>
                                        </div>
                                        <div>
                                            Estimate:<br> <span id="alt-display_estimate_name"></span>
                                        </div>
                                        <div>
                                           <?php if(Auth::user() ): ?>

                                                <a id="edit_href" class="btn btn-link" style="color:#2196F3;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Edit</a> <br>
                                                <a id="proposal_href" class="btn btn-link" style="color:#2196F3;"><i class="fa fa-chevron-right" aria-hidden="true"></i> Proposal</a>
                                            <?php endif; ?>
                                        </div>
                                        <div>
                                            Date: <br><span class="alt-rep-start" style="margin-right:20px;">02/27/2017</span>
                                        </div>
                                        <div>
                                            Expiry:<br> <span class="alt-expiration">02/27/2017</span>
                                        </div>
                                    </header>
                                    <div class="row is-flex">
                                        <?php
                                            $types = ['platinum','gold','silver','bronze'];
                                            foreach($types as $plan_type): 
                                        ?>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-print-3 bdr-right box_border <?php echo $plan_type; ?>-system">
                                                <div class="boxx">
                                                    <div class="badge <?php echo $plan_type; ?>"><?php echo $plan_type; ?></div>
                                                    <select style="display:block;" class="financing_select form-control" name="<?php echo $plan_type; ?>_financing_plan" data-price-type="<?php echo $plan_type; ?>"onChange="updateFinancing(this)" >
                                                        <?php echo $select; ?>
                                                    </select>
                                                    <h3 class="h3-title" id="<?php echo $plan_type; ?>_financing_payments"></h3>
                                                    <div class="thumbnail">
                                                        <div class="thumb-wrapper">
                                                            {{ Html::image('http://placehold.it/200x150', '', ['class' => 'unit-image']) }}
                                                        </div>              
                                                        <div class="caption">
                                                            <hr>
                                                            <div class="logo">
                                                                {{ Html::image('/img/logo-carrier.png', 'Tran') }}
                                                            </div>
                                                            <div class="title">
                                                                 Lennox 4T LRP16GE48-108
                                                            </div>
                                                            <div class="highlight-box show-print">
                                                                Financing plan name:<br/>
                                                                WF Reduced Rate 5.9%
                                                                <span><strong>$000</strong> per month</span>
                                                            </div>
                                                            <ul class="list-unstyled bullets">
                                                                <li id="clone" style="display:none"><?php echo embed_svg('icon', 'img/check-icon.svg', '12px', '12px'); ?> <span>Honeywell Touchscreen</span></li>
            
                                                            </ul>
                                                            <?php if('admin' == $user_type) : ?>
                                                                <ul class="system-components list-unstyled hide-print" style="display:none;">
                                                                    <li class="thermostat"></li>
                                                                    <li class="coil"></li>
                                                                    <li class="condenser"></li>
                                                                    <li class="furnace"></li>
                                                                </ul>
                                                            <?php endif; ?>
                                                            <hr/>
                                                            <table class="table rebate-table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Package Price:</td>
                                                                        <td class="system-price" data-val="">$00.00</td>
                                                                    </tr>                                                               
                                                                    <tr class="<?php echo $plan_type; ?>-trane-row">
                                                                        <td colspan="2" class="trane">Trane Rebate</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-trane-print"></td>
                                                                    </tr>
                                                                    <tr>                                                                   
                                                                        <td colspan="2">
                                                                            <div class="input-wrapper trane">{{Form::text($plan_type.'_trane','', ['class'=>'form-control']) }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="<?php echo $plan_type; ?>-tep-row">
                                                                        <td colspan="2" class="tep">TEP Rebate</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-tep-print"></td>
                                                                    </tr>
                                                                    <tr>                                                                    
                                                                        <td colspan="2"><div class="tep input-wrapper">{{Form::text($plan_type.'_tep','', ['class'=>'form-control']) }}</div></td>
                                                                    </tr>
                                                                    <tr class="box-row" style="">
                                                                        <td>Box</td>
                                                                        <td class="box-price" data-val="">$00.00</td>
                                                                    </tr>
                                                                    <tr class="highlight-table">
                                                                        <td>Sub Total</td>
                                                                        <td class="sub-total" data-val="">$00.00</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">Miscellaneous</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-misc-print">$00.00</td>
                                                                    </tr>
                                                                    <tr>                                                                   
                                                                        <td colspan="2"><div class="input-wrapper">{{Form::text( $plan_type.'_misc','', ['class'=>'form-control']) }}</div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Due</td>
                                                                        <td class="total-due" data-val="">$00.00</td>
                                                                    </tr>
                                                                    <tr class="<?php echo $plan_type; ?>-lennox-row">
                                                                        <td colspan="2" class="lennox">Lennox Rebate</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-lennox-print"></td>
                                                                    </tr>
                                                                    <tr>                                                                   
                                                                        <td colspan="2"><div class="lennox input-wrapper">{{Form::text($plan_type.'_lennox','', ['class'=>'form-control']) }}</div></td>
                                                                    </tr>
                                                                    <tr class="<?php echo $plan_type; ?>-costco-row">
                                                                        <td colspan="2" class="costco">Costco Cash Card</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-costco-print"></td>
                                                                    </tr>
                                                                    <tr>                                                                   
                                                                        <td colspan="2"><div class="costco input-wrapper">{{Form::text($plan_type.'_costco','', ['class'=>'form-control']) }}</div></td>
                                                                    </tr>
                                                                    <tr class="<?php echo $plan_type; ?>-exec-row">
                                                                        <td colspan="2" class="exec">2% Executive</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-exec-print"></td>
                                                                    </tr>
                                                                    <tr>                                                                    
                                                                        <td colspan="2"><div class="exec input-wrapper">{{Form::text( $plan_type.'_exec','', ['class'=>'form-control']) }}</div></td>
                                                                    </tr>
                                                                    <tr class="<?php echo $plan_type; ?>-citi-row">
                                                                        <td colspan="2" class="citi">2% Citi Card</td>
                                                                        <td class="show-print input-value <?php echo $plan_type; ?>-citi-print"></td>
                                                                    </tr>
                                                                    <tr>                                                                   
                                                                        <td colspan="2"><div class="input-wrapper citi">{{Form::text($plan_type.'_citi','', ['class'=>'form-control']) }}</div></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr class="highlight-table">
                                                                        <td>Total Investment:</td>
                                                                        <td class="total-investment">$00.00</td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                            <?php if('admin' == $user_type) : ?>

                                                                <label>Qualified Amounts</label>
                                                                <table class="table hide-print">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Finance Rate:</td>
                                                                            <td class="finance-rate"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Finance Fee:</td>
                                                                            <td class="finance-fee">$00.00</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Box Store Fee</td>
                                                                            <td class="box-store-fee">$00.00</td>
                                                                        </tr>
                                                                            <tr>
                                                                                <td>Qualified Amount</td>
                                                                                <td class="qualified-amount">$00.00</td>
                                                                            </tr>
                                                                    </tbody>
                                                                </table>
                                                            <?php endif; ?>
                                                            {{  Form::open( array('class'=>'form','id'=>'estimate_chosen_plan', 'url'=>'/chosen-plan', 'method' => 'post')) }}
                                                                {{ Form::hidden('proposal_id','', ['class'=>'form-control proposal-id']) }}
                                                                {{ Form::hidden('estimate_id','', ['class'=>'form-control estimate-id']) }}
                                                                {{ Form::hidden('plan_type',$plan_type, ['class'=>'form-control']) }}
                                                                {{ Form::submit('Select ' . $plan_type ,['class'=>'btn ' . $plan_type .'-save']) }}
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                    <label id="chosen_plan"></label>
                                    <br>
                                    <div class="payment_form">  
                                        <div class="payment_content">
                                            <div >
                                                <div class="show-print" style="display: inline-block;">
                                                    <label>System Chosen</label>
                                                    <span id="chosen_system">Platinum</span>
                                                </div>
                                                <label>Payment Method</label>
                                                <select name="payment_method" class="form-control" style="width:100%;">
                                                    <?php foreach (getPaymentMethods() as $key => $payment) : ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $payment; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="show-print" style="width: 3in" id="print_payment_method">50% Down, Balance Due on Equipment Startup</span>
                                            </div>
                                            <div >
                                                <div style="width:49%; display:inline-block;">
                                                    <label>Down Payment</label>
                                                    <select name="down_payment" class="form-control">
                                                        <?php foreach (getPayments() as $key => $payment) : ?>
                                                            <option value="<?php echo $key; ?>"><?php echo $payment; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="show-print" id="print_down_payment">Credit Card</span>
                                                </div>
                                                <div style="width:49%;display:inline-block">
                                                    <label>Downpayment Amount: </label>
                                                    {{Form::text('downpayment_amount','', ['class'=>'form-control']) }}
                                                    <span class="show-print" id="print_downpayment_amount"></span>
                                                </div>
                                            </div>
                                            <div>
                                                <div style="width:49%;display:inline-block;">
                                                    <label>Balance By: </label>
                                                    <select name="balance_by" class="form-control">
                                                        <?php foreach (getPayments() as $key => $payment) : ?>
                                                            <option value="<?php echo $key; ?>"><?php echo $payment; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="show-print" id="print_balance_by"></span>
                                                </div>
                                                <div style="width:49%;display:inline-block;">
                                                    <label>Balance Due: </label>
                                                    {{Form::text('balance_due','', ['class'=>'form-control','readonly'=>'true']) }}
                                                    <span class="show-print" id="print_balance_due"></span>
                                                </div>
                                                <div class="show-print" style="width: 100%">
                                                    <label >Notes:</label>
                                                    <span style="display: inline-block; width: auto;" class="print-notes"></span>
                                                </div>
                                            </div>
                                            <div class="hide-print" style="height:30px; padding-bottom:5px; margin-top:100px;border-top: 1px solid #ddd;">
                                                <div style="width:60%;display:inline-block;padding-top: 20px;">
                                                    <label>Signature</label>                                          
                                                </div>
                                                <div style="width:30%;display:inline-block;padding-top: 20px;">
                                                    <label>Date</label>                                          
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="approval show-print">
                                        <div class="sign-line">
                                            <div>
                                                <label>Customer Approval</label>                                          
                                            </div>
                                            <div>
                                                <label>Date</label>                                          
                                            </div>
                                        </div>
                                        <div class="sign-line">
                                            <div>
                                                <label>Contractor Approval</label>                                          
                                            </div>
                                            <div>
                                                <label>Date</label>                                          
                                            </div>
                                        </div>
                                    </div><!-- /approval -->
                                    <small class="show-print disclaimer">Amounts to be paid as stated above, in the event of non-payment, all amounts shall become immediately due and payable.  Customer further agrees to pay collection costs, reasonable Attorney's fees, court costs and interest at the rate of 1.5% per month to the date of payment in full.</small>
                                    <div class="row preso-notes bdr-bottom" style="margin-bottom: 20px;">
                                        <div class="col-xs-12">
                                            <a class="btn btn-primary pull-right" href="javascript:jQuery('.notes-group').show();">Add Note</a>
                                            <label class="no-print">Notes</label>
                                        </div>
                                        <div class="notes-group" style="display: none; padding-top: 20px;">
                                            <div class="note1">
                                                <div class="col-xs-12 col-md-6">
                                                    <label>User</label>
                                                    {{Form::text('note1_user','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-6 col-md-2">
                                                    <label>Show Customer</label>
                                                    {{Form::checkbox('note1_customer_view','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Note</label>
                                                    {{Form::textarea('note1_text','', ['class'=>'form-control','size' => '30x3', 'id'=>'note1_text']) }}
                                                </div>
                                            </div>
                                            <div class="note2">
                                                <div class="col-xs-12 col-md-6">
                                                    <label>User</label>
                                                    {{Form::text('note2_user','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-6 col-md-2">
                                                    <label>Show Customer</label>
                                                    {{Form::checkbox('note2_customer_view','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Note</label>
                                                    {{Form::textarea('note2_text','', ['class'=>'form-control','size' => '30x3','id'=>'note2_text']) }}
                                                </div>
                                            </div>
                                            <div class="note3">
                                                <div class="col-xs-12 col-md-6">
                                                    <label>User</label>
                                                    {{Form::text('note3_user','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-6 col-md-2">
                                                    <label>Show Customer</label>
                                                    {{Form::checkbox('note3_customer_view','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Note</label>
                                                    {{Form::textarea('note3_text','', ['class'=>'form-control','size' => '30x3','id'=>'note3_text']) }}
                                                </div>
                                            </div>
                                            <div class="note4">
                                                <div class="col-xs-12 col-md-6">
                                                    <label>User</label>
                                                    {{Form::text('note4_user','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-6 col-md-2">
                                                    <label>Show Customer</label>
                                                    {{Form::checkbox('note4_customer_view','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Note</label>
                                                    {{Form::textarea('note4_text','', ['class'=>'form-control','size' => '30x3', 'id'=>'note4_text']) }}
                                                </div>
                                            </div>                                              
                                            <div class="note5">
                                                <div class="col-xs-12 col-md-6">
                                                    <label>User</label>
                                                    {{Form::text('note5_user','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-6 col-md-2">
                                                    <label>Show Customer</label>
                                                    {{Form::checkbox('note5_customer_view','', ['class'=>'form-control']) }}
                                                </div>
                                                <div class="col-xs-12">
                                                    <label>Note</label>
                                                    {{Form::textarea('note5_text','', ['class'=>'form-control','size' => '30x3', 'id'=>'note5_text']) }}
                                                </div>
                                            </div>
                                        </div><!-- /notes-group -->
                                    </div><!-- /preso-notes -->
                                </div><!-- one-->
                            </div><!-- /two-->
                            <?php
                             $is_admin = 0;
                            if (Auth::check()):
                               
                                $user_roles = Auth::user()->roles;
                                foreach ($user_roles as $role) {
                                    if ($role == 'admin') {
                                        $is_admin = 1;
                                    }
                                }
                            endif;
                            if ($is_admin || $user_type == "warehouse" || $user_type == "admin") :
                                ?>
                                <div class="item">
                                        <div class="row bdr-bottom">
                                            <div class="col-lg-12">
                                                <div class="boxx pd40 pdb0">
                                                    <h3>Estimate Job Sheet (Admin Only)</h3>
                                                    <style>
                                                        .job-sheet .js-label{
                                                            font-weight:bold;
                                                        }
                                                    </style>
                                                    <table class="table job-sheet" id="admin_only">
                                                        <tr>
                                                            <td class="js-label">Job Estimator: </td>
                                                            <td id="job-estimator"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Customer Name : </td>
                                                            <td id="job-customer"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Address : </td>
                                                            <td id="job-address"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Zip Code : </td>
                                                            <td id="job-zip"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Phone : </td>
                                                            <td id="job-phone"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Email : </td>
                                                            <td id="job-email"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Type of Job : </td>
                                                            <td id="job-type"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Proposal Identifier: </td>
                                                            <td id="job-proposal-identifier"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">ARI#: </td>
                                                            <td id="job-ari"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Package Brand: </td>
                                                            <td id="job-package-brand"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Package Model: </td>
                                                            <td id="job-package-model"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Condenser Model: </td>
                                                            <td id="job-condenser"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Furnace Model: </td>
                                                            <td id="job-furnace"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Furnace Dimensions: </td>
                                                            <td id="job-furnace-dimensions"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Coil Model: </td>
                                                            <td id="job-coil-model"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Coil Dimensions: </td>
                                                            <td id="job-coil-dimensions"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Total Width Furnace & Coil: </td>
                                                            <td id="job-total-width"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Total Height Furnace & Coil: </td>
                                                            <td id="job-total-height"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Condenser Amps: </td>
                                                            <td id="job-condenser-amps"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Thermostat: </td>
                                                            <td id="job-thermostat"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Control Number: </td>
                                                            <td id="job-control-number"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="js-label">Job Labor Units: </td>
                                                            <td id="job-labor-units"></td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ($is_admin || $user_type == "admin") : ?>

                                    <div class="item">
                                        <div id="commission_block">
                                        <div class="commission_block_inner">
                                        <H3>Commission</H3>
                                        <h4>Total Due Calculator
                                        <table class="table" id="">
                                            <tbody>
                                                <tr>
                                                    <td>Package Price - Box:</td>
                                                    <td class="comm-b2"></td>
                                                </tr>
                                                <tr>
                                                    <td>Trane Rebate Credit:</td>
                                                    <td class="comm-b3">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>True Miscellaneous:</td>
                                                    <td class="comm-b6">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Lennox Rebate Fee:</td>
                                                    <td class="comm-b8">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Due:</td>
                                                    <td class="comm-b10">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Trane Rebate Amount:</td>
                                                    <td class="comm-b14">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Tep Rebate:</td>
                                                    <td class="comm-b16">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Lennox Rebate Amount:</td>
                                                    <td class="comm-b18">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Trane Rebate Amount:</td>
                                                    <td class="comm-b14">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Total:</td>
                                                    <td class="comm-b20">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Miscellaneous:</td>
                                                    <td class="comm-b22">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Box Amount:</td>
                                                    <td class="comm-b25">$00.00</td>
                                                </tr>


                                            </tbody>
                                        </table>
                                        <hr>
                                        <table class="table" id="">
                                            <tbody>
                                                <tr>
                                                    <td>Financing Plan Chosen:</td>
                                                    <td class="comm-e14"></td>
                                                </tr>
                                                <tr>
                                                    <td>Financing Plan %:</td>
                                                    <td class="comm-e15"></td>
                                                </tr>
                                                <tr>
                                                    <td>Big Box Store:</td>
                                                    <td class="comm-e18"></td>
                                                </tr>
                                                <tr>
                                                    <td>Box Store %:</td>
                                                    <td class="comm-e19"></td>
                                                </tr>
                                                <tr>
                                                    <td>Blended Financing Rate:</td>
                                                    <td class="comm-e23"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        <h4>Qualified Amount Calculator</h4>
                                        <table class="table" id="">
                                            <tbody>
                                                <tr>
                                                    <td>Net Amount:</td>
                                                    <td class="comm-g2"></td>
                                                </tr>
                                                <tr>
                                                    <td>Finance Fee:</td>
                                                    <td class="comm-g3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Box Store Fee:</td>
                                                    <td class="comm-g4"></td>
                                                </tr>
                                                <tr>
                                                    <td>Qualified Amount:</td>
                                                    <td class="comm-g5"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table" id="">
                                            <tbody>
                                                <tr>
                                                    <td>Commission Rate (ii):</td>
                                                    <td class="comm-rate-ii"></td>
                                                </tr>
                                                <tr>
                                                    <td>Qualified Amount:</td>
                                                    <td class="comm-g5">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Commission Amount:</td>
                                                    <td class="comm-amount">$00.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <h4>Commission Rate</h4>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Box:</td>
                                                    <td class="comm-box"></td>
                                                </tr>
                                                <tr>
                                                    <td>Miscellaneous:</td>
                                                    <td class="comm-misc">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Finance Fees Over 8%</td>
                                                    <td class="comm-finance-fees">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>At Large Cushion</td>
                                                    <td class="at-large">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>True Discount</td>
                                                    <td class="comm-discount">$00.00</td>
                                                </tr>
                                                <tr>
                                                    <td>(Package Price - Box)</td>
                                                    <td class="comm-sub-minus-box">.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Discount %</td>
                                                    <td class="comm-discount-percent">.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Rate Reducer</td>
                                                    <td class="comm-rate-reducer">.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Commission Discount Percent</td>
                                                    <td class="comm-commission-discount-percent"></td>
                                                </tr>
                                                <tr>
                                                    <td>Starting Commission Rate</td>
                                                    <td class="comm-starting-commission-rate"></td>
                                                </tr>
                                                <tr>
                                                    <td>Commission Rate (i)</td>
                                                    <td class="comm-commission-rate-i">.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    </div>


                            </div>   
                                                            <?php endif; ?>
    
                        </div><!-- three -->
                    </div>
                </div>
            </div>
        </div>
        <div class="controls-wrapper" >
            <a class="left carousel-control" href="#carousel-proposal-details" role="button" data-slide="prev">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
                <path d="M0,9h12.2l-5.6,5.6L8,16l8-8L8,0L6.6,1.4L12.2,7H0V9z"/>
                </svg>
            </a>
            <a class="right carousel-control" href="#carousel-proposal-details" role="button" data-slide="next">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
                <path d="M0,9h12.2l-5.6,5.6L8,16l8-8L8,0L6.6,1.4L12.2,7H0V9z"/>
                </svg>
            </a>
        </div>
    </div>
     <div id="furnace_only" style="display:none;">

       All Replacement Gas Furnace installations include and assume the following: Connection to the existing ductwork, electric service, control wiring and Flue (up to 5' each) with all new materials. Connection to the existing gas piping is included (up to 5') and includes a new stainless steel flex connector, code required dirt leg and "easy operate" safety valve. If longer connections are needed & are included those are priced below. Price further includes a digital thermostat (see package details), removal of old equipment, removal of all related debris from the premises and all handling charges. Equipment Startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="new_ac_package" style="display:none;">
      All New Gas Package Unit installations include the following: Connection to an existing duct thru the roof and one new duct thru the roof to connect to the existing duct system. A new electric circuit, drain line and control wire up to 5' each. If longer connections are needed & are included, those are priced below. A new gas line from a suitable location is included (up to 5â€™).  Price further includes a digital thermostat and unit mounting base (see package details), removal of old equipment if any, removal of all other related debris from the premises and crane & handling charges. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.

    </div>
    <div id="replace_ac_package" style="display:none;">
       All Replacement Gas Package Unit installations include the following: Connection to the existing ductwork, electric circuit (including a new disconnect box), drain piping, gas piping and control wiring if within 5'. If longer connections are needed & are included, those are priced below. Price further includes a digital thermostat and Unit mounting base (see package details), all
refrigerant and material disposal fees, removal of old equipment, all related debris from the premises and crane & handling charges. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="replace_ac_split" style="display:none;">
       All Replacement Gas Heat Split System installations include and assume the following: Connection to the existing ductwork, electric circuit (install new breaker), drain piping, refrigerant piping, control wiring and Flue (up to 5' each) with all new materials. Drain line connection also includes a "Drain Alert" to shut down the system should the drain line backup. Connection to the existing gas piping is included (up to 5') and includes a new stainless steel flex connector, code required dirt leg and "easy operate" safety valve. If longer connections are needed & are included those are priced below. Price further includes a digital thermostat (see package details), all refrigerant and material disposal fees, removal of old equipment, removal of all related debris from the premises and all handling charges. Reuse of the existing equipment mounting base is included, if new is included it will be listed below. 90' Crane charges are included if necessary. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="new_ac_split" style="display:none;">
      All New Gas Heat Split system installations include and assume the following: Connection to your existing duct system & flue (up to 5'), a new electric circuit (up to 5'), a new refrigerant lineset (up to 8'), a new drain line (up to 5' with condensate pump if listed below), and control wiring (up to 5') with all new materials. Connection to the existing gas piping is included (up to 5') and includes a new stainless steel flex connector, code required dirt leg and "easy operate" safety valve. Drain line connection also includes a "Drain Alert" to shut down the system should the drain line backup. If longer connections are needed & are included those are priced below. Price further includes a digital thermostat (see package details), equipment pad or base (see packages), removal of old equipment if any, removal of all other related debris from the premises and all handling charges. 90' Crane charges are included if necessary. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="new_hp_split" style="display:none;">
       All New Split System Heat Pump installations include and assume the following: Connection to your existing duct system (up to 5'), a new electric circuit (up to 5'), a new refrigerant line set (up to 5'), a new drain line (up to 5' with condensate pump if listed below) and control
wiring (up to 5') with all new materials. Drain line connection also includes a "Drain Alert" to shut down the system should the drain line backup. If longer connections are needed & are included those are priced below. Price further includes a digital thermostat (see package details), equipment pad or base (see packages), removal of old equipment if any, removal of all other related debris from the premises and all handling charges. 90' Crane charges are included if necessary. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="replace_hp_split" style="display:none;">
       All Replacement Split System Heat Pump installations include and assume the following: Connection to existing ductwork, electric circuit (install new breaker), drain piping, refrigerant piping and control wiring (up to 5') with all new materials. Drain line connection also includes a "Drain Alert" to shut down the system should the drain line backup. If longer connections are needed & are included, those are priced below. Price further includes a digital thermostat (see package details), all refrigerant and material disposal fees, removal of old equipment, removal of all related debris from the premises and all handling charges. Reuse of the existing equipment mounting base is included, if new is included it will be listed below. 90' Crane charges are included if necessary. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="new_hp_package" style="display:none;">
        All New Package Heat Pump installations include the following: Connection to an existing duct thru the roof and one new duct thru the roof to connect to the existing duct system. A new electric circuit, drain line and control wire up to 5' each. If longer connections are needed & are included, those are priced below. Price further includes a digital thermostat and unit mounting base (see package details), removal of old equipment if any, removal of all other related debris from the premises and crane & handling charges. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>
    <div id="replace_hp_package" style="display:none;">
       All Replacement Package Heat Pump installations include the following: Connection to the existing ductwork, electric circuit (including a new disconnect box), drain piping and control wiring if within 5'. If longer connections are needed & are included, those are priced below. Price further includes a digital thermostat and unit mounting base (see package details), all refrigerant and material disposal fees, removal of old equipment, all related debris from the premises and crane & handling charges. Your equipment startup will be documented on a unique "Birth Certificate" by Factory Authorized Technicians.
    </div>