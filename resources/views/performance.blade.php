@extends('layouts.default')

@section('content')   
<div class="performance">
    <div class="container-fluid">
        <?php $user = getUser(); ?>
        <div class="row">
            <div class="col-md-12 page_content">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h3>
      <!--                          <svg version="1.1" id="file-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 20" style="enable-background:new 0 0 16 20;" xml:space="preserve">
                                    <path d="M10,0H2C0.9,0,0,0.9,0,2l0,16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V6L10,0z M12,16H4v-2h8V16z M12,12H4v-2h8V12z M9,7V1.5L14.5,7H9z"/>
                                </svg>-->
                                Performance
                            </h3>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <a href="javascript:window.print()" style="margin-left: 15px" class="btn btn-primary pull-right">Print</a>
                                    <a href="javascript:void()" data-toggle="modal" data-target="#optionModal" class="btn btn-primary pull-right">Create New Estimate</a>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    @include('searchform')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                {{-- Content --}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body pdlr0">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="team-sales">
                                        <h3 class="title">Performance</h3>

                                        <?php
                                        $user_type = "member";
                                        if (array_key_exists('user-type', $_GET) && $_GET['user-type']) {
                                            $user_type = $_GET['user-type'];
                                        }
                                        $start = date('Y-m-d', 0);
                                        if (array_key_exists('start', $_GET) && $_GET['start']) {
                                            $start = date('Y-m-d', strtotime($_GET['start']));
                                        }
                                        $end = date('Y-m-d', time());
                                        if (array_key_exists('end', $_GET) && $_GET['end']) {
                                            $end = date('Y-m-d', strtotime($_GET['end']));
                                        }

                                        $group = App\Group::find($user->current_group_id);

                                        $users = $group->users;
                                        $options = [];
                                        foreach ($users as $tuser) {
                                            // pr($tuser->pivot->role.'-'.$tuser->first_name);
                                            if ('customer' != $tuser->pivot->role && $tuser->first_name) {
                                                $options[$tuser->id] = $tuser->first_name . " " . $tuser->last_name;
                                            }
                                        }
                                        ?>
                                        <script>
                                            var _start = '<?php echo $start; ?>';
                                            var _end = '<?php echo $end; ?>';
                                        </script>
                                        <?php
                                        //echo Form::select('team-member', $options, null, ['id' => 'team-member']);
                                        ?>
                                        <div class="col-sm-8 performance_form">
                                            {{ Form::open(array('url' => '/performance','method'=>'get')) }}
                                            <div class="col-xs-12 col-lg-3">
                                                <label> Start Date: </label> 
                                                <?php echo Form::text('start', '', array('id' => 'datepicker', 'class' => 'form-control')); ?>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <label>  End Date: </label> 
                                                <?php echo Form::text('end', '', array('id' => 'datepicker2', 'class' => 'form-control')); ?>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <label>  User Type: </label> 
                                                <?php
                                                echo Form::select('user-type', ['member' => 'Team Member', 'lead_source' => 'Lead Source', 'lead_gen' => 'Lead Generator'], $user_type, ['id' => 'user_type', 'class' => 'form-control']);
                                                ?>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <?php
                                                echo Form::submit('Search', ['class' => 'btn btn-primary']);
                                                ?>
                                            </div> 
                                            {{ Form::close() }}
                                        </div>

                                        @include('charts.table')
                                        <div class="row mb30 pdlr60">
                                            <div class="col-sm-12">
                                                <div class="row mb30">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <!--                                                            <div class="col-sm-6">
                                                                                                                            <ul class="chartYearControls clearfix">
                                                                                                                                <li><button class="btn btn-default" type="button" id="prevYearBC">&#8249;</button></li>
                                                                                                                                <li><span id="yearBC"></span></li>
                                                                                                                                <li><button class="btn btn-default" type="button" id="nextYearBC">&#8250;</button></li>
                                                                                                                            </ul>                         
                                                                                                                        </div>
                                                            -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="individual-sales">2</div>
                                    <div role="tabpanel" class="tab-pane" id="lead-source">

                                        <div class="row pdlr60">
                                            <div class="col-sm-12">
                                                <div class="row mb30">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <!--                                                            <div class="col-sm-6">
                                                                                                                            <ul class="chartYearControls clearfix">
                                                                                                                                <li><button class="btn btn-default" type="button" id="prevYearBC">&#8249;</button></li>
                                                                                                                                <li><span id="yearBC"></span></li>
                                                                                                                                <li><button class="btn btn-default" type="button" id="nextYearBC">&#8250;</button></li>
                                                                                                                            </ul>                         
                                                                                                                        </div>
                                                            -->
                                                            <div class="col-sm-6">
                                                                <?php
                                                                $optionsl = [];

                                                                $sources = App\Source::get();

                                                                foreach ($sources as $key => $source) {

                                                                    $optionsl[$source->id] = $source->name;
                                                                }

                                                                echo Form::select('team-member', $optionsl, null, ['id' => 'team-memberl']);
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6" id="barChartLegends"></div>                                 
                                                </div>
                                                <div id="loadBarCharts">
                                                    <canvas id="bar-charts"></canvas>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="lead-generator">
                                        <div class="row pdlr60">
                                            <div class="col-sm-12">

                                                <div class="row mb30">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <!--                                                            <div class="col-sm-6">
                                                                                                                            <ul class="chartYearControls clearfix">
                                                                                                                                <li><button class="btn btn-default" type="button" id="prevYearBC">&#8249;</button></li>
                                                                                                                                <li><span id="yearBC"></span></li>
                                                                                                                                <li><button class="btn btn-default" type="button" id="nextYearBC">&#8250;</button></li>
                                                                                                                            </ul>                         
                                                                                                                        </div>
                                                            -->
                                                            <div class="col-sm-6">
<?php
$team = $group->getTeam('leadgen');


$optionss = [];

$sources = App\Source::get();

foreach ($team as $leadgen) {

    $optionss[$leadgen->id] = $leadgen->first_name . " " . $leadgen->last_name;
}

echo Form::select('team-member', $optionss, null, ['id' => 'team-memberr']);
?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6" id="barChartLegendd"></div>                                 
                                                </div>


                                                <div id="loadBarChartg">
                                                    <canvas id="bar-chartg"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>                        
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('customCss')
@endsection

@section('customJs')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script src="{{ asset('/js/plugins.js') }}"></script>
<script type="text/javascript">

                                            jQuery(document).ready(function($) {
                                            /*Datatable*/
                                            var language = {
                                            "search": "_INPUT_",
                                                    "searchPlaceholder": "Search records ...",
                                                    "lengthMenu": "_MENU_",
                                                    "oPaginate": {
                                                    "sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
                                                            "sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
                                                    }
                                            };
                                            $('#leadgen').DataTable({
                                            "stateSave": true,
                                                    "lengthMenu": [ 10, 25, 50, 75, 100 ],
                                                    "iDisplayLength": 10,
                                                    "aaSorting": [[0, 'asc']],
                                                    "destroy": true,
                                                    "language": language,
                                            });
                                            $("#leadsource").DataTable({
                                            "stateSave": true,
                                                    "lengthMenu": [ 10, 25, 50, 75, 100 ],
                                                    "iDisplayLength": 10,
                                                    "aaSorting": [[0, 'asc']],
                                                    "destroy": true,
                                                    "language": language,
                                            });
                                            /*Datatable*/
                                            var d = new Date();
                                            var n = d.getFullYear();
                                            $('#yearBC, #yearTRS').text(n);
                                            $('#prevYearBC').click(function(e){
                                            var current_year = parseInt($('#yearBC').text()) - 1;
                                            $('#yearBC').text(current_year);
                                            var user = $('#team-member').val();
                                            e.preventDefault();
                                            $.ajax({
                                            url: '{{ url(' / performance / bchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
                                                    // console.log(data);
                                                    $('#loadBarChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            });
                                            $('#nextYearBC').click(function(){
                                            var current_year = parseInt($('#yearBC').text()) + 1;
                                            $('#yearBC').text(current_year);
                                            var user = $('#team-member').val();
                                            $.ajax({
                                            url: '{{ url(' / performance / bchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
//console.log('afdata');
//console.log(data);
                                                    //      $('#loadBarChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            });
                                            function loadBarChart(user){
                                            var current_year = $('#yearBC').text();
                                            $.ajax({
                                            url: '{{ url(' / performance / bchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
//console.log(data);
                                                    $('#loadBarChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            }

                                            function loadTable(user){
                                            var current_year = $('#yearBC').text();
                                            $.ajax({
                                            url: '{{ url(' / performance / table') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
//console.log(data);
                                                    alert(10);
                                                    $('#loadBarChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            }


                                            $('#team-member').change(function(event) {
                                            var $this = event.target;
                                            var user = $($this).val();
                                            alert(2);
                                            loadTable(user);
//		loadBarChart(user);
                                            });
                                            $('#prevYearTRS').click(function(){
                                            var current_year = parseInt($('#yearTRS').text()) - 1;
                                            $('#yearTRS').text(current_year);
                                            var user = $('#team-member-x').val();
                                            $.ajax({
                                            url: '{{ url(' / performance / lchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
                                                    $('#loadLineChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            });
                                            $('#nextYearTRS').click(function(){
                                            var current_year = parseInt($('#yearTRS').text()) + 1;
                                            $('#yearTRS').text(current_year);
                                            var user = $('#team-member-x').val();
                                            $.ajax({
                                            url: '{{ url(' / performance / lchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
                                                    $('#loadLineChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            });
                                            $('#team-member-x').change(function(event) {
                                            var user = $(this).val();
                                            var current_year = $('#yearTRS').text();
                                            $.ajax({
                                            url: '{{ url(' / performance / lchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
                                                    $('#loadLineChart').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            });
// Return with commas in between
                                            var numberWithCommas = function(x) {
                                            y = x.toFixed(0);
                                            console.log(y);
                                            return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                            };
                                            // BAR CHART
                                            var data = {
                                            labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
                                                    datasets: [
                                                    {
                                                    label: "Pending",
                                                            backgroundColor: "#FFE083",
                                                            borderColor: "#FFE083",
                                                            borderWidth:0,
                                                            data: [50, 160, 120, 250, 210, 120, 80, 70, 60, 190, 124, 205]
                                                    },
                                                    {
                                                    label: "Sold",
                                                            backgroundColor: "#80CBC4",
                                                            borderColor: "#80CBC4",
                                                            borderWidth:0,
                                                            data: [150, 60, 220, 50, 110, 220, 180, 90, 160, 90, 224, 40]
                                                    },
                                                    {
                                                    label: "Lost",
                                                            backgroundColor: "#F48FB1",
                                                            borderColor: "#F48FB1",
                                                            borderWidth:0,
                                                            data: [250, 260, 60, 150, 90, 170, 280, 170, 160, 90, 324, 360]
                                                    }
                                                    ]
                                            };
                                            var ctx = document.getElementById("bar-chart").getContext("2d");
                                            var myBarChart = new Chart(ctx, {
                                            type: 'bar',
                                                    data: data,
                                                    options: {
                                                    animation: {
                                                    duration: 10,
                                                    },
                                                            tooltips: {
                                                            mode: 'label',
                                                                    callbacks: {
                                                                    label: function(tooltipItem, data) {
                                                                    return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
                                                                    }
                                                                    }
                                                            },
                                                            scales: {
                                                            xAxes: [{
                                                            stacked: false,
                                                                    gridLines: {
                                                                    display: false
                                                                    },
                                                            }],
                                                                    yAxes: [{
                                                                    stacked: false,
                                                                            ticks: {
                                                                            callback: function(value) {
                                                                            return numberWithCommas(value);
                                                                            },
                                                                            },
                                                                            labels: {
                                                                            template: "<%= Number(value.toFixed(2)) + ' A'%>",
                                                                            }

                                                                    }],
                                                            },
                                                            legend: {
                                                            display: false // due to style limitation, add legend info as per design using legend template
                                                            },
                                                            responsive: true,
                                                            scaleBeginAtZero: true,
                                                            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
                                                    }
                                            });
                                            $("#barChartLegend").html(myBarChart.generateLegend());
                                            loadBarChart(1);
                                            // Line Chart
                                            var dataLine = {
                                            labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
                                                    datasets: [
                                                    {
                                                    label: "Actual",
                                                            fill: false,
                                                            lineTension: 0,
                                                            backgroundColor: "#80DDE9",
                                                            borderColor: "#80DDE9",
                                                            borderCapStyle: 'butt',
                                                            borderDash: [],
                                                            borderDashOffset: 0.0,
                                                            borderJoinStyle: 'miter',
                                                            pointBorderColor: "#80DDE9",
                                                            pointBackgroundColor: "#80DDE9",
                                                            pointBorderWidth: 5,
                                                            pointHoverRadius: 3,
                                                            pointHoverBackgroundColor: "#80DDE9",
                                                            pointHoverBorderColor: "#80DDE9",
                                                            pointHoverBorderWidth: 0,
                                                            pointRadius: 2,
                                                            pointHitRadius: 1,
                                                            data: [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000],
                                                            spanGaps: false,
                                                    },
                                                    {
                                                    label: "Average",
                                                            fill: false,
                                                            lineTension: 0,
                                                            backgroundColor: "#FFAB91",
                                                            borderColor: "#FFAB91",
                                                            borderCapStyle: 'butt',
                                                            borderDash: [],
                                                            borderDashOffset: 0.0,
                                                            borderJoinStyle: 'miter',
                                                            pointBorderColor: "#FFAB91",
                                                            pointBackgroundColor: "#FFAB91",
                                                            pointBorderWidth: 5,
                                                            pointHoverRadius: 3,
                                                            pointHoverBackgroundColor: "#FFAB91",
                                                            pointHoverBorderColor: "#FFAB91",
                                                            pointHoverBorderWidth: 0,
                                                            pointRadius: 2,
                                                            pointHitRadius: 1,
                                                            data: [1200, 2200, 3200, 4400, 5500, 6600, 7700, 8800, 9900, 5000],
                                                            spanGaps: false,
                                                    },
                                                    {
                                                    label: "Budget",
                                                            fill: false,
                                                            lineTension: 0,
                                                            backgroundColor: "#E6EE9C",
                                                            borderColor: "#E6EE9C",
                                                            borderCapStyle: 'butt',
                                                            borderDash: [],
                                                            borderDashOffset: 0.0,
                                                            borderJoinStyle: 'miter',
                                                            pointBorderColor: "#E6EE9C",
                                                            pointBackgroundColor: "#E6EE9C",
                                                            pointBorderWidth: 5,
                                                            pointHoverRadius: 3,
                                                            pointHoverBackgroundColor: "#E6EE9C",
                                                            pointHoverBorderColor: "#E6EE9C",
                                                            pointHoverBorderWidth: 0,
                                                            pointRadius: 2,
                                                            pointHitRadius: 1,
                                                            data: [1600, 2600, 1800, 3200, 4800, 5200, 6800, 7200, 8800, 9750],
                                                            spanGaps: false,
                                                    }
                                                    ]
                                            };
                                            var ctxLine = document.getElementById("line-chart").getContext("2d");
                                            var myLineChart = new Chart(ctxLine, {
                                            type: 'line',
                                                    data: dataLine,
                                                    options: {
                                                    animation: {
                                                    duration: 10,
                                                    },
                                                            tooltips: {
                                                            mode: 'label',
                                                                    callbacks: {
                                                                    label: function(tooltipItem, data) {
                                                                    return "$" + numberWithCommas(tooltipItem.yLabel);
                                                                    }
                                                                    }
                                                            },
                                                            scales: {
                                                            xAxes: [{
                                                            stacked: false,
                                                                    gridLines: {
                                                                    display: false
                                                                    },
                                                            }],
                                                                    yAxes: [{
                                                                    stacked: false,
                                                                            ticks: {
                                                                            callback: function(value) {
                                                                            return numberWithCommas(value);
                                                                            },
                                                                            },
                                                                    }],
                                                            },
                                                            legend: {
                                                            display: false // due to style limitation, add legend info as per design using legend template
                                                            },
                                                            responsive: true,
                                                            scaleBeginAtZero: true,
                                                            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                                    }
                                            });
                                            $("#lineChartLegend").html(myLineChart.generateLegend());
//#lead-source
                                            $('#team-memberl').on("change", function(event) {
                                            var $this = event.target;
                                            var user = $($this).val();
                                            loadBarCharts(user);
                                            });
                                            function loadBarCharts(user){
                                            var current_year = $('#yearBC').text();
                                            $.ajax({
                                            url: '{{ url(' / performance / bchart') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
//console.log(data);
                                                    // $('#loadBarCharts').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            }

                                            $("#barChartLegends").html(myBarChart.generateLegend());
                                            $('a[href=#lead-source]').on('shown.bs.tab', function(){
                                            // BAR CHART
                                            var data = {
                                            labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
                                                    datasets: [
                                                    {
                                                    label: "Pending",
                                                            backgroundColor: "#FFE083",
                                                            borderColor: "#FFE083",
                                                            borderWidth:0,
                                                            data: [1, 10, 3, 2, 6, 7, 10, 7, 20, 1, 5, 3]
                                                    },
                                                    {
                                                    label: "Sold",
                                                            backgroundColor: "#80CBC4",
                                                            borderColor: "#80CBC4",
                                                            borderWidth:0,
                                                            data: [1, 8, 3, 2, 6, 7, 1, 7, 1, 1, 5, 3]
                                                    },
                                                    {
                                                    label: "Lost",
                                                            backgroundColor: "#F48FB1",
                                                            borderColor: "#F48FB1",
                                                            borderWidth:0,
                                                            data: [7, 1, 3, 2, 9, 7, 10, 7, 20, 1, 5, 3]
                                                    }
                                                    ]
                                            };
                                            var ctx = document.getElementById("bar-charts").getContext("2d");
                                            var myBarChart = new Chart(ctx, {
                                            type: 'bar',
                                                    data: data,
                                                    options: {
                                                    animation: {
                                                    duration: 10,
                                                    },
                                                            tooltips: {
                                                            mode: 'label',
                                                                    callbacks: {
                                                                    label: function(tooltipItem, data) {
                                                                    return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
                                                                    }
                                                                    }
                                                            },
                                                            scales: {
                                                            xAxes: [{
                                                            stacked: false,
                                                                    gridLines: {
                                                                    display: false
                                                                    },
                                                            }],
                                                                    yAxes: [{
                                                                    stacked: false,
                                                                            ticks: {
                                                                            callback: function(value) {
                                                                            return numberWithCommas(value);
                                                                            },
                                                                            },
                                                                            labels: {
                                                                            template: "<%= Number(value.toFixed(2)) + ' A'%>",
                                                                            }

                                                                    }],
                                                            },
                                                            legend: {
                                                            display: false // due to style limitation, add legend info as per design using legend template
                                                            },
                                                            responsive: true,
                                                            scaleBeginAtZero: true,
                                                            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
                                                    }
                                            });
                                            //loadBarChart(10);
                                            });
                                            //lead-generator Tab
                                            $('a[href=#lead-generator]').on('shown.bs.tab', function(){
                                            $('#team-memberr').on("change", function(event) {
                                            var $this = event.target;
                                            var user = $($this).val();
                                            loadBarChartg(user);
                                            });
                                            function loadBarChartg(user){
                                            var current_year = $('#yearBC').text();
                                            $.ajax({
                                            url: '{{ url(' / performance / bchartlead') }}',
                                                    type: "POST",
                                                    data: {uid: user, cy: current_year},
                                                    success: function(data){
//console.log(data);
                                                    $('#loadBarChartg').html(data);
                                                    },
                                                    error: function(xhr, status, error){
                                                    console.log(xhr);
                                                    console.log(status);
                                                    console.log(error);
                                                    },
                                                    beforeSend: function(xhr){
                                                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                                    }
                                            });
                                            }

                                            $("#barChartLegendd").html(myBarChart.generateLegend());
                                            loadBarChartg(10);
                                            });
                                            });</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
                                            $(function() {

                                            $("#datepicker").val(_start);
                                            $("#datepicker").datepicker();
                                            //$( "#datepicker" ).datepicker({ defaultDate: 15 } );
                                            $("#datepicker2").val(_end);
                                            $("#datepicker2").datepicker();
                                            });
</script>
@endsection