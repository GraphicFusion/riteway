
@extends('layouts.default')
@section('content')
<?php
use App\User;
						
	/* 
		OPPORTUNITIES ARE SYNONYMOUS WITH 'PROPOSALS'
		- An Opportunity could have multiple estimates
	*/
	if(Auth::user()){
		$user = getUser();
		$group = App\Group::find( $user->current_group_id );
		$proposalsArr = $group->proposals;
		$login_class = "riteway-logged";
		$admin_emails = getAdminEmails();
		$core_emails = getCoreEmails();
		$warehouse_emails = getWarehouseEmails();
		$admin_style = "display:none;";
		if( in_array($user->email,$admin_emails) || in_array($user->email,$core_emails)){
			$login_class .= " admin-logged";
			$user_type = "admin";
			$admin_style = "";
		} 
		if( in_array($user->email,$warehouse_emails)){
			$login_class .= " warehouse-logged";
			$user_type = "warehouse";
		} 
	}
	else{
		$login_class = "customer-logged";
		$user_type = "customer";
		if(array_key_exists('present', $_GET)){
			$estimate_id = $_GET['present'];
			$estimate = App\Estimate::find($estimate_id);
			$proposal = App\Proposal::find($estimate->proposal_id);
			$group = App\Group::find( $proposal->group_id );
			$proposalsArr = $group->proposals;		
		}
		else{
			setcookie("riteway_estimate_id", '');  /* expire in 1 hour */
			$newUrl = "/customer-estimate/";
			header('Location: '.$newUrl);
		}
	}

// NEED TO USE GROUP PROPOSALS?
//	$proposalsArr = $user->proposals;
	$proposals = [];
	foreach( $proposalsArr as $proposal ){
//print_r($proposal);
		if( $proposal->active ){
			$proposal->getEstimateNumbers();
			$estimates = $proposal->estimates;
			$chosen_estimate_id = $proposal->primary_estimate_id;
			$chosen_plan = $proposal->chosen_plan;
			$estimate_select = [];
			$i = 1;
			if(is_object( $estimates)){
				foreach( $estimates as $estimate ){
					$estimate_select[] = [
						'number' => $i,
						'id' => $estimate->id,
						'type' => $estimate->type,
						'chosen' => $estimate->chosen,
						'chosen_plan' => $estimate->chosen_plan,
						'job_labor_total' => $estimate->job_labor_total,
						'notes' => $estimate->notes
					];
					$i++;
				}
			}
			$rep = $proposal->rep;
//			$type = $proposal->type;
	// print_r($rep);
			$customer = $proposal->customer;
			$start = strtotime($proposal->created_at );
			$exp = $start + 30*60*60*24;
			$loc = $proposal->location;
			$address = "";
			$zip = "";
			if( is_object( $loc ) ){
				$address = $loc->address . "," . $loc->city . " " . $loc->state;
				$zip = $loc->zipcode;
			}
			$status = 'pending';
			if( $proposal->status ){
				$status = $proposal->status;
			}
			$initials = "n/a";
			$rep_first = "";
			$rep_last= "";
			$rep_email = "";
			$rep_phone = "";
			if( is_object($rep ) ){
				$initials = $rep->getInitials();
				$rep_first = $rep->first_name;
				$rep_last = $rep->last_name;
				$rep_email = $rep->email;
				$rep_phone = $rep->phone;
			}
			if( is_object( $customer ) ){
				$cus_first = $customer->first_name;
				$cus_last = $customer->last_name;
				$cus_phone = $customer->phone;
				$cus_email = $customer->email;
			}
			$leadgen = new App\User;
			$store = "";
			if( $proposal->source_id ){
				$source = App\Source::find( $proposal->source_id );
				$store = $source->store;
			}
			$proposals[] = [
	
				'id' => $proposal->id,
				'address' => $address,
				'team' => $initials,
				'start' => date('m/d/Y',$start ),
				'expiration' => date('m/d/Y',$exp),
				'status' => $status,
				'estimates' => $estimate_select,
				'chosen_estimate_id' => $chosen_estimate_id,
				'price' => $proposal->getPrice(),
				'chosen_plan' => $chosen_plan,
				'rep_name' => $rep_first. " " . $rep_last,
				'rep_phone' => $rep_phone,
				'rep_email' => $rep_email,
				'customer_name' => $cus_first. " " . $cus_last,
				'customer_phone' => $cus_phone,
				'customer_email' => $cus_email,
				'leadgen_id' => $proposal->leadgen_id,
				'source_id' => $proposal->source_id,
				'store' => $store,
				'included_estimates' => $proposal->included_estimates,
				'zipcode' => $zip
			];
		}

	} 
// print_r($proposals);
?>
<style>
    .dataTables_wrapper {
        position: relative;
        clear: both;
        zoom: 1;
        padding-left: 15px;
        padding-right: 15px;
    }
    .proposals.customer-logged{
    	display:none !important;
    }
    tr.box-row{
    	display:none;
    }
    .admin-logged tr.box-row{
    	display: table-row !important;
    }
</style>
<div class="proposals <?php echo $login_class; ?>">
	<div class="container-fluid">
	    <div class="row">
	    </div>
	</div>
</div>
<?php if(Auth::check()): ?>
@include('modals.createestimate')
<?php endif; ?>
@include('modals.proposal-detail')
@include('modals.proposal-detail-alt')
@endsection

@section('customCss')
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
<script>

 /* HELPER FUNCTIONS */
function formatPrice(str) {
	var x = str.split('.');
	var x1 = x[0];
	var x2 = x[1];

  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + '.' + x2;
}
</script>
<script type="text/javascript">
									
									<?php
										$json_system = [];
										$json_estimates = [];
                                    	$_system_json = [];
										$_furnace_json = [];
										$_package_json = [];
										$json_system = [];
										$_thermostats_json = [];

									?>
									<?php if( isset( $estimate ) ) : ?>
										<?php 
										/*
											$_furnaces = App\Furnace::all();
											$_systems = App\System::all();
											$_packages = App\Package::all();
											$_thermostats_array = App\Thermostat::all();
											foreach( $_thermostats_array as $stat){
												$_thermostats_json[$stat->id] = $stat->getAllAttributes();
											}
											// furnaces
											foreach($_furnaces as $_furnace){												
													$attr = $_furnace->getAllAttributes();
													$attr['thermostat_obj'] = App\Thermostat::find($_furnace->thermostat_id);
													$_furnace_json[$_furnace->id] = $attr;								
											}

											// split
											foreach($_systems as $_system){												
													$attr = $_system->getAllAttributes();
													$attr['coil_obj'] = App\Coil::find($_system->coil_id);
													$attr['furnace_obj'] = App\Furnace::find($_system->furnace_id);
													$attr['thermostat_obj'] = App\Thermostat::find($_system->thermostat_id);
													$attr['condenser_obj'] = App\Condenser::find($_system->condenser_id);	
													$_system_json[$_system->id] = $attr;								
											}

											// split
											foreach($_packages as $_package){												
													$attr = $_package->getAllAttributes();
													$attr['coil_obj'] = App\Coil::find($_package->coil_id);
													$attr['furnace_obj'] = App\Furnace::find($_package->furnace_id);
													$attr['thermostat_obj'] = App\Thermostat::find($_package->thermostat_id);
													$attr['condenser_obj'] = App\Condenser::find($_package->condenser_id);	
													$_package_json[$_package->id] = $attr;								
											}

											if( "furnace" == $estimate->type ){
												$systems = App\Furnace::all();
											}
											elseif( "split" == $estimate->type ){
												$systems = App\System::all();
											}
											elseif( "package" == $estimate->type){
												$systems = App\Package::all();
											}
											foreach($systems as $system){

												if( "split" == $estimate->type || "furnace" == $estimate->type ){
													$attr = $system->getAllAttributes();
													$attr['coil_obj'] = App\Coil::find($system->coil_id);
													$attr['furnace_obj'] = App\Furnace::find($system->furnace_id);
													$attr['thermostat_obj'] = App\Thermostat::find($system->thermostat_id);
													$attr['condenser_obj'] = App\Condenser::find($system->condenser_id);
													
												}
												else{
													$attr = $system->getAllAttributes();
												}
												$json_system[$system->id] = $attr;
											}
	// NEED TO FILTER BY USER GROUP
											$json_estimates = [];
											//$estimates = App\Estimate::whereIn('proposal_id', $proposal_ids )->get();
											//foreach($estimates as $estimate){
	//print_r($estimate);
											//	$json_estimates[$estimate->id] = $estimate;
											//}
	//echo "lllllllllllllll";
	//print_r($json_estimates);
	//print_r($json_system);
	*/
										?>
									<?php endif; ?>

	var $systems = <?php echo json_encode($json_system); ?>; 
	var $_systems = <?php echo json_encode($_system_json); ?>; 
	var $_furnaces = <?php echo json_encode($_furnace_json); ?>; 
	var $_packages = <?php echo json_encode($_package_json); ?>; 
	var $_thermostats = <?php echo json_encode($_thermostats_json); ?>; 
  	var $estimates = <?php echo json_encode($json_estimates); ?>;
	var estimate_id = getQueryVariable('present');
	function bulletClone(v){
		return $("#clone").clone().appendTo('.' + v + '-system .bullets').removeAttr('id').removeAttr('style').find('span');
	}
	function getQueryVariable(variable)
	{
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	}
	function buildPreso( proposal, pres_estimate ){
		if( !pres_estimate ){
			var pres_estimate = $estimates[$('#estimate').val()];
		}
		var estimate_type = pres_estimate.type;
		console.log(pres_estimate);
		console.log('pres_estimate');
		$('.estimate_id').val(pres_estimate.id);
		// hide rebate options
		var incl =  JSON.parse(pres_estimate.includes);
		if(incl && 'undefined' != typeof incl.citi){
			if( incl.citi == 1){
				$('.citi').show();
			}
			else{
				$('.citi').hide();
			}
			if( incl.tep == 1){
				$('.tep').show();
			}
			else{
				$('.tep').hide();
			}
			if( incl.costco == 1){
				$('.costco').show();
			}
			else{
				$('.costco').hide();
			}
			if( incl.lennox == 1){
				$('.lennox').show();
			}
			else{
				$('.lennox').hide();
			}
			if( incl.trane == 1){
				$('.trane').show();
			}
			else{
				$('.trane').hide();
			}

			if( incl.exec == 1){
				$('.exec').show();
			}
			else{
				$('.exec').hide();
			}
		}
				$('#job-labor-units').html( parseFloat(pres_estimate.job_labor_total).toFixed(2));
		

		window.current_estimate = pres_estimate;
		var chosen_plan = pres_estimate.chosen_plan;
		var chosen_system_id = pres_estimate[chosen_plan + '_system_id'];
		//var chosen_system = $systems[chosen_system_id];
		var chosen_thermostat_id = pres_estimate[chosen_plan + '_thermostat_id'];
		var chosen_thermostat = $_thermostats[chosen_thermostat_id];
		console.log('chosen thermo');
		console.log(chosen_thermostat);
		if( 'undefined' != typeof pres_estimate.notes ){

			var notes = JSON.parse(pres_estimate.notes);
			$.each([1,2,3,4,5], function(k,v){
				if( 'undefined' != typeof notes && notes && 'undefined' != typeof notes[v] ){
						
					$text = notes[v].text;
					$('#note'+v+'_text').val($text);
					$user = notes[v].user;
					$('input[name=note'+v+'_user]').val($user);
					if ( notes[v].view == 1 ){
						$('input[name=note'+v+'_customer_view]').prop('checked',true);
					}
					else{
						$('input[name=note'+v+'_customer_view]').prop('checked',false);
					}
				}
			})
		}
		var systems = window['_' + pres_estimate.type + 's'],
			ucType = pres_estimate.type.charAt(0).toUpperCase() + pres_estimate.type.slice(1);
		window['get' + ucType ](chosen_system_id, function(){
			var chosen_system = systems[chosen_system_id];
		console.log('chosen_system');
		console.log(chosen_system);
alert('done');
			var $amps = "";
			var $system_price = 0;
			if( 'undefined' != typeof chosen_system.price ){
				$system_price = parseFloat(chosen_system.price);
			}
			var $condenser_price = 0;
			if( 'undefined' != typeof chosen_system.condenser_obj && chosen_system.condenser_obj ){
				if( 'undefined' != typeof chosen_system.condenser_obj.brand ){
					$('#job-condenser').html(chosen_system.condenser_obj.brand + " - " + chosen_system.condenser_obj.model_number);
				}
				if( 'undefined' != typeof chosen_system.condenser_obj.amps ){
					$amps = chosen_system.condenser_obj.amps;
				}
				$condenser_price = parseFloat(chosen_system.condenser_obj.price);
			}
			if( 'undefined' != typeof chosen_system.amps ){
				$amps = chosen_system.amps;
			}
			$('#job-condenser-amps').html($amps);
			$('#job-ari').html(chosen_system.ahri);
			$('#job-thermostat').html(chosen_thermostat);

			var $coil_price = 0;
			var coil_obj_depth = 0;
			var coil_obj_height = 0;
			if( 'undefined' != typeof chosen_system.coil_obj && chosen_system.coil_obj ){
				coil_obj_depth =  chosen_system.coil_obj.depth;
				coil_obj_height =  chosen_system.coil_obj.height;
				if( 'undefined' != typeof chosen_system.coil_obj.brand && 'undefined' != typeof chosen_system.coil_obj.model_number ){
					$('#job-coil-model').html(chosen_system.coil_obj.brand + ' - ' + chosen_system.coil_obj.model_number);
				}
				if( 'undefined' != typeof chosen_system.coil_obj.height && 'undefined' != typeof chosen_system.coil_obj.depth ){
					$('#job-coil-dimensions').html('Height: ' + chosen_system.coil_obj.height + ' Depth: ' + chosen_system.coil_obj.depth);
				}
				$coil_price = parseFloat(chosen_system.coil_obj.price);
			}

			var $furnace_price = 0;
			furnace_obj_brand = "";
			furnace_obj_model = "";
			furnace_obj_height = "";
			furnace_obj_width = "";
			if( 'undefined' != typeof chosen_system.furnace_obj && chosen_system.furnace_obj){
				$furnace_price = parseFloat(chosen_system.furnace_obj.price);
				furnace_obj_brand = chosen_system.furnace_obj.brand;
				furnace_obj_model = chosen_system.furnace_obj.model_number;
				furnace_obj_height = parseFloat(chosen_system.furnace_obj.height);
				furnace_obj_width = parseFloat(chosen_system.furnace_obj.width);
			}
			$('#job-furnace').html(furnace_obj_brand + ' - ' + furnace_obj_model);
			$('#job-furnace-dimensions').html('Height: ' + furnace_obj_height + ' Width: ' + furnace_obj_width);
			var total_width = 0;
			// check if packge:
			if('package' != estimate_type){
				if(coil_obj_depth > furnace_obj_width){
					total_width = coil_obj_depth; 
				}else{
					total_width = furnace_obj_width; 				
				}
				var total_height = coil_obj_height + furnace_obj_height;
			}
			else{
				total_width = chosen_system.width;	
				var total_height = chosen_system.height;	
				$('#job-package-brand').html( chosen_system.brand);
				$('#job-package-model').html( chosen_system.model_number);
			}
			$('#job-total-width').html( total_width);
			$('#job-total-height').html( total_height );

			var $thermostat_price = 0;
			if( 'undefined' != typeof chosen_system.thermostat_obj && chosen_system.thermostat_obj ){
				$thermostat_price = parseFloat(chosen_system.thermostat_obj.price);
			}
			var $control_price = parseFloat($thermostat_price) + parseFloat($furnace_price) + parseFloat($coil_price) + parseFloat($condenser_price) + parseFloat($system_price);
			$('#job-control-number').html($control_price);
		});
		var job_sheet_items = JSON.parse(pres_estimate.job_sheet);
		if( $.isArray(job_sheet_items) ){
			$.each(job_sheet_items, function(k,v){
				if(v.value != 'Select an Option'){
					$('table.job-sheet').append( "<tr><td class='js-label'>" + v.label +  "</td><td>" + v.value + "</td></tr>" );
				}
			});
		}

		$('.' + chosen_plan + '-save').html( 'Selected' ).addClass('btn-primary').val('Selected'); 
var sections = JSON.parse(pres_estimate.worksheet_data);
var pricing = JSON.parse(pres_estimate.presentation_prices);
		d = new Date(pres_estimate.updated_at );
		exp = new Date( proposal.expiration );
		var exp_month = exp.getMonth() + 1; // date indexes jan as 0
		if( exp.getMonth() == 12){
			exp_month = 1;
		}

		$("#alt-expiration").html( exp_month + "/" + exp.getDate()    + "/" + exp.getFullYear()  );
		$("#alt-rep-start").html( (d.getMonth() + 1)  + "/" + d.getDate()    + "/" + d.getFullYear()  );

		var $title = "";
		if( pres_estimate.new_replace == 'new'){
			$title += "New ";
		}
		if( pres_estimate.new_replace == 'replace'){
			$title += "Replacement ";
		}
		if( pres_estimate.ac_hp == 'ac'){
			$title += "Air Conditioner ";
		}
		if( pres_estimate.ac_hp == 'hp'){
			$title += "Heat Pump ";
		}
		if( pres_estimate.type == 'split'){
			$title += "Split ";
		}
		if( pres_estimate.type == 'furnace'){
			$title += "Furnace ";
		}
		if( pres_estimate.type == 'package'){
			$title += "Pack ";
		}
		$('#replace_title').html($title);
		$('#job-type').html($title);

		if( pres_estimate.type == 'furnace'){
			var replace_text = $('#furnace_only').html();			
		}
		else{
			var replace_text = $('#' + pres_estimate.new_replace + "_" + pres_estimate.ac_hp + "_" + pres_estimate.type).html();			
		}
		$('#replace_text').html(replace_text);

		var tiers = ['platinum', 'gold', 'silver', 'bronze'];
		$.each(tiers, function(k,v){
			var $paren = $('.'+v+'-system');
			var box_price = pres_estimate[v + '_box'];
			$paren.find('.box-price').html('$' + box_price).data('val',box_price);
		});

		var type = pres_estimate.type.charAt(0).toUpperCase() + pres_estimate.type.slice(1);
		$("#alt-display_estimate_name").html( '#' + pres_estimate.number + ' | ' + type );
		$('.proposal-id').val( proposal.id );
		$('.estimate-id').val( pres_estimate.id );
var preso = JSON.parse(pres_estimate.presentation);
console.log('PRESO');
console.log(preso);
alert('updatePresentationPrice is bonking');
updatePresentationPrice(pres_estimate);
alert('after update');
		if(  preso){
			alert('has preso');
			$.each(preso, function(k,v){
				var $side = 1;
				$.each(v, function(n,o){
						var $paren = $('#' + k + '_wrap_' + $side );
						if( 'undefined' != typeof o.feet && o.feet > 0 ){
							$paren.append( "<dt>" + o.label + "</dt><dd style='margin-bottom:0px;'>" + o.value + "<span style='margin-left:10px; font-weight:600; font-style:italic; '>-   Run up to " + o.feet + " feet.</span></dd>" );
						}
						else{
							$paren.append( "<dt>" + o.label + "</dt><dd>" + o.value + "</dd>" );							
						}
						if( $side == 1 ){
							$side = 2;
						}
						else{
							$side = 1;
						}
				})
			})
		}
	}
$('#proposalDetailAltModal').on('show.bs.modal', function (e) {
//var $this = $(e.target);
//	fo();
});
	$('.platinum-system input, .gold-system input, .silver-system input, .bronze-system input').on('change', function(){
		alert(3);
			updatePresentationPrice();
	});
	alert(0);
	$('.notes-group input, .rebate-table input').on('change', function(){
		alert(2);
		estimate_id = window.current_estimate.id;
		updateEstimatePrices(estimate_id);
	})
	function updateEstimatePrices($estimate_id){
		alert(1);
		var $prices = {};

		var tiers = ['platinum', 'gold', 'silver', 'bronze'];
		$.each(tiers, function(k,v){
			$prices[v] = {};
			//			$prices.platinum = {};
			$prices[v].plan = $('[name="' + v + '_financing_plan"]').val();
			$prices[v].system_price = $('.' + v + '-system .system-price').data('val');
			$prices[v].trane_rebate = $('[name="' + v + '_trane"]').val();
			$prices[v].tep_rebate = $('[name="' + v + '_tep"]').val();
			$prices[v].box_price = $('.' + v + '-system .box-price').data('val');
			$prices[v].sub_total = $('.' + v + '-system .sub-total').data('val');
			$prices[v].misc = $('[name="' + v + '_misc"]').val();
			$prices[v].total_due = $('.' + v + '-system .total-due').data('val');
			$prices[v].lennox_rebate = $('[name="' + v + '_lennox"]').val();
			$prices[v].costco = $('[name="' + v + '_costco"]').val();
			$prices[v].exec = $('[name="' + v + '_exec"]').val();
			$prices[v].citi = $('[name="' + v + '_citi"]').val();
			$prices[v].total_investment = $('.' + v + '-system .total-investment').data('val');
			$prices[v].finance_rate = $('.' + v + '-system .finance-rate').data('val');
			$prices[v].finance_fee = $('.' + v + '-system .finance-fee').data('val');
			$prices[v].boxstore_fee = $('.' + v + '-system .box-store-fee').data('val');
			$prices[v].qualified_amount = $('.' + v + '-system .qualified-amount').data('val');
		});
$prices.payment_method = $('[name="payment_method"]').val();
$prices.down_payment = $('[name="down_payment"]').val();
$prices.balance_by = $('[name="balance_by"]').val();
$prices.balance_due = $('[name="balance_due"]').val();
$prices.downpayment_amount = $('[name="downpayment_amount"]').val();

		// get notes
		var $notes= {};
		$.each([1,2,3,4,5], function(k,v){
			$notes[v] = {};
			$notes[v].text = $('#note'+v+'_text').val();
			$notes[v].user = $('input[name=note'+v+'_user]').val();
			if ($('input[name=note'+v+'_customer_view]').is(':checked') ){
				$notes[v].view = 1;
			}
			else{
				$notes[v].view = 0;				
			}
		})
		$.ajax({
			url: '/api/estimate-prices',
			data:     {
				estimate_id : $estimate_id,
				presentation_prices : $prices,
				notes : $notes
			},
			error: function() {
				console.log('error');
			},
			success: function(data) {
				console.log(data);
			},
			type: 'POST'
		});

	}

	$('input[name=downpayment_amount]').on('change',function(){
		updatePresentationPrice();
	})
	$('.plans input, .plans select').on('change',function(){
		estimate_id = window.current_estimate.id;
		updateEstimatePrices(estimate_id);
	})
	$('#email-estimate').on('click', function(){
		$.ajax({
			url: '/api/email-estimate',
			data:     {
				estimate_id : pres_estimate.id
			},
			error: function() {
				console.log('error');
			},
			success: function(data) {
				console.log(data);
				alert('Email sent');
			},
			type: 'POST'
		});
	})
	$(document).ready(function() {
    });
</script>
@endsection