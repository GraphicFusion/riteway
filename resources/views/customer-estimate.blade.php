@extends('layouts.default')

@section('content')
<?php
	if(array_key_exists('present', $_GET)){
		$estimate_id = $_GET['present'];
		$estimate = App\Estimate::find($estimate_id);
		$proposal = App\Proposal::find($estimate->proposal_id);
		$customer = $proposal->customer;
//	print_r($customer);
		$loc = $proposal->location;
//		print_r($proposal);
//		print_r($estimate);
	}

?>
<div class="customer">
	<div class="container-fluid">
	    <div class="row">
			<div class="col-md-12 page_content">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
		        		<h3 class="unitlist-header">
							Estimate #<?php echo $estimate->id; ?> for <?php echo $loc->address . "," . $loc->city . " " . $loc->state; ?>
						</h3>
					</div>
	        	</div>
				
				<div class="row" style="padding-bottom:40px;">
		        		{{  Form::open(array('class'=>'form','url'=>'/customer-presentation', 'method' => 'post', 'files'=>true)) }}
		        		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 xxs-12 col-lg-offset-4">
						<h4>To view your estimate please enter your email</h4>
			                {{ Form::hidden('estimate_id', $estimate->id, ['class'=>'form-control']) }}
			                {{ Form::label('email', 'Email') }}
			                {{ Form::text('email', '', ['class'=>'form-control']) }}
			           	<br>
		       			{{ Form::submit( 'View Estimate', array('class' => 'btn-submit btn btn-primary btn-block')) }}
			           	</div>
						{{ Form::close() }}  

				</div>

			</div>
	    </div>
	</div>
</div>

@endsection

@section('customCss')
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
@endsection