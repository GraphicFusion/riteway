
@extends('layouts.default')

@section('content')
<?php
use App\User;
//$proposal = App\Proposal::find(3);
//print_r($proposal->rep);
						
	/* 
		OPPORTUNITIES ARE SYNONYMOUS WITH 'PROPOSALS'
		- An Opportunity could have multiple estimates
	*/
	if(Auth::user()){
		$user = getUser();
		$group = App\Group::find( $user->current_group_id );
		$proposalsArr = $group->proposals;
		$login_class = "riteway-logged";
		$admin_emails = getAdminEmails();
		$core_emails = getCoreEmails();
		$warehouse_emails = getWarehouseEmails();
		$admin_style = "display:none;";
		if( in_array($user->email,$admin_emails) || in_array($user->email,$core_emails)){
			$login_class .= " admin-logged";
			$user_type = "admin";
			$admin_style = "";
		} 
		if( in_array($user->email,$warehouse_emails)){
			$login_class .= " warehouse-logged";
			$user_type = "warehouse";
		} 
	}
	else{
		$login_class = "customer-logged";
		$user_type = "customer";
		if(array_key_exists('present', $_GET)){
			$estimate_id = $_GET['present'];
			$estimate = App\Estimate::find($estimate_id);
			$proposal = App\Proposal::find($estimate->proposal_id);
			$group = App\Group::find( $proposal->group_id );
			$proposalsArr = $group->proposals;		
		}
		else{
			setcookie("riteway_estimate_id", '');  /* expire in 1 hour */
			$newUrl = "/customer-estimate/";
			header('Location: '.$newUrl);
		}
	}

// NEED TO USE GROUP PROPOSALS?
//	$proposalsArr = $user->proposals;
	$proposals = [];
	foreach( $proposalsArr as $proposal ){
//print_r($proposal);
		if( $proposal->active ){
			$proposal->getEstimateNumbers();
			$estimates = $proposal->estimates;
			$chosen_estimate_id = $proposal->primary_estimate_id;
			$chosen_plan = $proposal->chosen_plan;
			$estimate_select = [];
			$i = 1;
			if(is_object( $estimates)){
				foreach( $estimates as $estimate ){
					$estimate_select[] = [
						'number' => $i,
						'id' => $estimate->id,
						'type' => $estimate->type,
						'chosen' => $estimate->chosen,
						'chosen_plan' => $estimate->chosen_plan,
						'job_labor_total' => $estimate->job_labor_total,
						'notes' => $estimate->notes
					];
					$i++;
				}
			}
			$rep = $proposal->rep;
//			$type = $proposal->type;
	// print_r($rep);
			$customer = $proposal->customer;
			$start = strtotime($proposal->created_at );
			$exp = $start + 30*60*60*24;
			$loc = $proposal->location;
			$address = "";
			$zip = "";
			if( is_object( $loc ) ){
				$address = $loc->address . "," . $loc->city . " " . $loc->state;
				$zip = $loc->zipcode;
			}
			$status = 'pending';
			if( $proposal->status ){
				$status = $proposal->status;
			}
			$initials = "n/a";
			$rep_first = "";
			$rep_last= "";
			$rep_email = "";
			$rep_phone = "";
			if( is_object($rep ) ){
				$initials = $rep->getInitials();
				$rep_first = $rep->first_name;
				$rep_last = $rep->last_name;
				$rep_email = $rep->email;
				$rep_phone = $rep->phone;
			}
			if( is_object( $customer ) ){
				$cus_first = $customer->first_name;
				$cus_last = $customer->last_name;
				$cus_phone = $customer->phone;
				$cus_email = $customer->email;
			}
			$leadgen = new App\User;
			$store = "";
			if( $proposal->source_id ){
				$source = App\Source::find( $proposal->source_id );
				$store = $source->store;
			}
			$proposals[] = [
	
				'id' => $proposal->id,
				'address' => $address,
				'team' => $initials,
				'start' => date('m/d/Y',$start ),
				'expiration' => date('m/d/Y',$exp),
				'status' => $status,
				'estimates' => $estimate_select,
				'chosen_estimate_id' => $chosen_estimate_id,
				'price' => $proposal->getPrice(),
				'chosen_plan' => $chosen_plan,
				'rep_name' => $rep_first. " " . $rep_last,
				'rep_phone' => $rep_phone,
				'rep_email' => $rep_email,
				'customer_name' => $cus_first. " " . $cus_last,
				'customer_phone' => $cus_phone,
				'customer_email' => $cus_email,
				'leadgen_id' => $proposal->leadgen_id,
				'source_id' => $proposal->source_id,
				'store' => $store,
				'included_estimates' => $proposal->included_estimates,
				'zipcode' => $zip
			];
		}

	} 
// print_r($proposals);
?>
<style>
    .dataTables_wrapper {
        position: relative;
        clear: both;
        zoom: 1;
        padding-left: 15px;
        padding-right: 15px;
    }
    .proposals.customer-logged{
    	display:none !important;
    }
    tr.box-row{
    	display:none;
    }
    .admin-logged tr.box-row{
    	display: table-row !important;
    }
</style>
<div class="proposals <?php echo $login_class; ?>">
	<div class="container-fluid">
	    <div class="row">
	        <div class="col-md-12 page_content">
	        	<?php if (Auth::user()): ?>
 	        	<div class="page-header">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h3>
<!--                            <svg version="1.1" id="file-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 20" style="enable-background:new 0 0 16 20;" xml:space="preserve">
                                    <path d="M10,0H2C0.9,0,0,0.9,0,2l0,16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V6L10,0z M12,16H4v-2h8V16z M12,12H4v-2h8V12z M9,7V1.5L14.5,7H9z"/>
                                </svg>-->
                                Opportunities
                            </h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <a href="javascript:void()" data-toggle="modal" data-target="#optionModal" class="btn btn-primary pull-right">Create New Estimate</a>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    @include('datatable-searchform')
                                </div>
                            </div>
                        </div>
                    </div>
	        	</div>
		        <?php endif; ?>
				<script>
					var proposals = new Object();
				</script>
                <table id="dTables" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Address</th>
                            <th>Team</th>
                            <th>Expiration</th>
                            <th>Status</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php if( is_object($proposalsArr)) : ?>
	                        @foreach($proposalsArr as $proposal) 
		                        <?php if( $proposal->customer ) : ?>
		                            <tr data-id='<?php if(Auth::user()){ echo $proposal->id; } ?>'>
		                                <td>
		                                    <?php echo embed_svg('icon', 'img/tbl-row-icon.svg', '16px', '16px'); ?>
		                                    {{ $proposal->customer->first_name . " " . $proposal->customer->last_name }}
		                                </td>
		                                <td>{{ $proposal->location->address . "," . $proposal->location->city . " " . $proposal->location->state }}</td>
		                                <td>
		                                    <span class="team-member">
		                                        {{ $proposal['team'] }}
		                                    </span>
		                                </td>
		                                <td>{{ $proposal['expiration'] }}</td>
		                                <td>
		                                    @if($proposal['status'] == 'draft')
		                                        <span class="status red">{{ $proposal['status'] }}</span>    
		                                    @elseif($proposal['status'] == 'pending')
		                                        <span class="status yellow">{{ $proposal['status'] }}</span>    
		                                    @elseif($proposal['status'] == 'lost')
		                                        <span class="status default">{{ $proposal['status'] }}</span>    
		                                    @elseif($proposal['status'] == 'sold')
		                                        <span class="status green">{{ $proposal['status'] }}</span>    
		                                    @else
		                                        <span class="status default">No Status</span>    
		                                    @endif
		                                </td>
										<td>
											<?php echo "$".$proposal->price; ?>
										</td>
		                            </tr>
		                        <?php endif; ?>


		                    @endforeach
	                    <?php endif; ?>
                    </tbody>
                </table>
	        </div>
	    </div>
	</div>
</div>
<?php if(Auth::check()): ?>
@include('modals.createestimate')
<?php endif; ?>
@include('modals.proposal-detail')
@include('modals.estimate-detail')
@endsection

@section('customCss')
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
<script>

 /* HELPER FUNCTIONS */
function formatPrice(str) {
	var x = str.split('.');
	var x1 = x[0];
	var x2 = x[1];

  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + '.' + x2;
}
</script>
<script type="text/javascript">

	var estimate_id = getQueryVariable('present');
	function bulletClone(v){
		return $("#clone").clone().appendTo('.' + v + '-system .bullets').removeAttr('id').removeAttr('style').find('span');
	}
	function getQueryVariable(variable)
	{
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	}
	$('.platinum-system input, .gold-system input, .silver-system input, .bronze-system input').on('change', function(){
			updatePresentationPrice();
	});
	$('.notes-group input').on('change', function(){
		estimate_id = window.current_estimate.id;
		updateEstimatePrices(estimate_id);
	})

	
	$('input[name=downpayment_amount]').on('change',function(){
		estimate_id = window.current_estimate.id;
		getEstimate(estimate_id, function(){
			var estimate = window._estimates[id];
			updatePresentationPrice(estimate);
		});	
	})
	
	$('#email-estimate').on('click', function(){
		$.ajax({
			url: '/api/email-estimate',
			data:     {
				estimate_id : pres_estimate.id
			},
			error: function() {
				console.log('error');
			},
			success: function(data) {
				console.log(data);
				alert('Email sent');
			},
			type: 'POST'
		});
	})
</script>
@endsection