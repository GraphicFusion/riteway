<?php
$is_existing = 0;
if (isset($id) && 'none' != $id) {
    $member = App\User::find($id);
    $memberLoc = $member->locations()->first();
    $is_existing = 1;
} else {
    $member = new App\User;
    $memberLoc = new stdClass();
    $memberLoc->address = "";
    $memberLoc->city = "";
    $memberLoc->state = "";
    $memberLoc->zipcode = "";
}
?>

{{  Form::open(array('class'=>'form','url'=>'/customer', 'method' => 'post', 'files'=>true)) }}
@if( isset($type) )
{{ Form::hidden('worksheet_type', $type['id']) }}
@endif
<?php if ($is_existing) : ?>
    <div class="customer-form">
        <div class="row customer-form_btn">
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 xxs-12">
                <a class="btn-submit btn btn-primary btn-block" href="/new-customer-proposal/package/<?php echo $id; ?>">Create a New Package</a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 xxs-12">
                <a class="btn-submit btn btn-primary btn-block" href="/new-customer-proposal/split/<?php echo $id; ?>">Create a New Split</a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 xxs-12">
                <a class="btn-submit btn btn-primary btn-block" href="/new-customer-proposal/furnace/<?php echo $id; ?>">Create a New Furnace</a>
            </div>
        </div>
<?php endif; ?>
    <div class="error">
    <?php
    if (@$_GET['message'] == 'email') {
        echo "<p>That email has already been used.</p>";
    }
    ?>
    </div>
    <div class="form-group @if (($errors->has('first_name') || ($errors->has('space_h')))) has-error @endif">
        <div class="row">
            @if( isset($type) )
            <h2>
                Customer Information
            </h2>
            @endif
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 xxs-12">
                {{ Form::label('first_name', 'First Name') }}
                {{ Form::text('first_name', $member->first_name, ['class'=>'form-control']) }}
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 xxs-12">
                {{ Form::label('last_name', 'Last Name') }}
                {{ Form::text('last_name', $member->last_name, ['class'=>'form-control']) }}
            </div>
            <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

            @if ($errors->has('space_h') || $errors->has('space_w'))
            <span class="status error">
<?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
            </span>
            @else
            @endif

        </div>
    </div>
    <div class="form-group @if ($errors->has('email')) has-error @endif">
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', $member->email, ['class'=>'form-control']) }}
        <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

        @if ($errors->has('email'))
        <span class="status error">
<?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
        </span>
        @else
        <span class="status success">
<?php echo embed_svg('icon', 'img/check-icon.svg', '20px', '20px'); ?>
        </span>
        @endif
    </div>
    <div class="form-group @if ($errors->has('role')) has-error @endif">
        {{ Form::label('role', 'Property Role') }}
        {{ Form::select('role', array('owner'=>'Owner','manager'=>'Property Manager','renter'=>'Renter','agent'=>'Agent'),'', ['class'=>'form-control']) }}
        <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

        @if ($errors->has('role'))
        <span class="status error">
<?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
        </span>
        @else
        <span class="status success">
<?php echo embed_svg('icon', 'img/check-icon.svg', '20px', '20px'); ?>
        </span>
        @endif
    </div>
    <div class="form-group @if ($errors->has('phone')) has-error @endif">
        {{ Form::label('phone', 'Phone Number') }}
        {{ Form::text('phone', $member->phone, ['class'=>'form-control']) }}
        <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

        @if ($errors->has('phone'))
        <span class="status error">
<?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
        </span>
        @else
        <span class="status success">
<?php echo embed_svg('icon', 'img/check-icon.svg', '20px', '20px'); ?>
        </span>
        @endif
    </div>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2UmPhISedfL4Vz9Jn7W3V3TxU_TjQkHM&libraries=places"></script>
    <div class="form-group @if ($errors->has('address')) has-error @endif">
        {{ Form::label('address', 'Street Address') }}
        {{ Form::text('address', $memberLoc->address, ['class'=>'form-control']) }}
        <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

        @if ($errors->has('unit_location'))
        <span class="status error">
<?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
        </span>
        @else
        <span class="status success">
<?php echo embed_svg('icon', 'img/check-icon.svg', '20px', '20px'); ?>
        </span>
        @endif
    </div>
    <div class="form-group @if (($errors->has('city') || ($errors->has('state')))) has-error @endif">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 xxs-12">
                {{ Form::label('city', 'City') }}
                {{ Form::text('city', $memberLoc->city, ['class'=>'form-control']) }}
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 xxs-12">
                {{ Form::label('state', 'State') }}
                {{ Form::text('state', $memberLoc->state, ['class'=>'form-control']) }}
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 xxs-12">
                {{ Form::label('zipcode', 'Zip Code') }}
                {{ Form::text('zipcode', $memberLoc->zipcode, ['class'=>'form-control']) }}
            </div>
            <div class="ttip" data-toggle="tooltip" data-placement="top" title="Help Top"><?php echo embed_svg('icon', 'img/help-icon.svg', '20px', '20px'); ?></div>

            @if ($errors->has('space_h') || $errors->has('space_w'))
            <span class="status error">
<?php echo embed_svg('icon', 'img/close-icon.svg', '20px', '20px'); ?>
            </span>
            @else
            @endif

        </div>
    </div>
<?php if (isset($type)) {
    $text = "Next";
} else {
    $text = "Save";
} ?>
{{ Form::submit( $text, array('class' => 'btn-submit btn btn-primary btn-block')) }}
{{ Form::close() }}  
</div>
