
@extends('layouts.default')

@section('content')
<?php
$user = getUser();
$group = App\Group::find($user->current_group_id);
$customers = $group->getCustomers();
?>

<div class="customers ">
    <div class="container-fluid">
        <?php $user = getUser(); ?>
        <div class="row">
            <div class="col-md-12 page_content">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h3>
<!--                                <svg version="1.1" id="user-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.6 20.6" style="enable-background:new 0 0 20.6 20.6;" xml:space="preserve">
                                    <path class="st0" d="M0.3,2.5v15.6c0,1.2,1,2.2,2.2,2.2h15.6c1.2,0,2.2-1,2.2-2.2V2.5c0-1.2-1-2.2-2.2-2.2H2.5
                                          C1.3,0.3,0.3,1.3,0.3,2.5z M13.6,6.9c0,1.8-1.5,3.3-3.3,3.3S6.9,8.8,6.9,6.9s1.5-3.3,3.3-3.3S13.6,5.1,13.6,6.9z M3.6,15.8
                                          c0-2.2,4.4-3.4,6.7-3.4s6.7,1.2,6.7,3.4v1.1H3.6V15.8z"/>
                                </svg>-->
                                Customers
                            </h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6 col-xs-12">
                                    @include('datatable-searchform')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <table id="customerTables" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Team</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customers as $customer) 
                        <tr data-customer-id="{{ $customer['id'] }}">
                            <td>
                                <?php echo embed_svg('icon', 'img/user-icon.svg', '16px', '16px'); ?>
                                {{ $customer['name'] }}
                            </td>
                            <td>{{ $customer['address'] }}</td>
                            <td>
                                <span class="team-member">
                                    {{ $customer['team'] }}
                                </span>

                            </td>
                            <td>
                                @if($customer['status'] == 'draft')
                                <span class="status red">{{ $customer['status'] }}</span>    
                                @elseif($customer['status'] == 'pending')
                                <span class="status yellow">{{ $customer['status'] }}</span>    
                                @elseif($customer['status'] == 'lost')
                                <span class="status default">{{ $customer['status'] }}</span>    
                                @elseif($customer['status'] == 'sold')
                                <span class="status green">{{ $customer['status'] }}</span>    
                                @else
                                <span class="status default">No Status</span>    
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('modals.createestimate')
@endsection

@section('customCss')
@endsection

@section('customJs')
    <script src="{{ asset('/js/plugins.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var oTable = $('#customerTables').DataTable({
                "iDisplayLength": 20,
                "aaSorting": [],
                "drawCallback": function (settings) {
                    bindCustomerRows();
                },
                "responsive": {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function (row) {
                                var data = row.data();
                                return 'Details for ' + data[0];
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                            tableClass: 'table table-striped table-dt'
                        })
                    }
                }
            });
            $('#search_dtable').keyup(function () {
                oTable.search($(this).val()).draw();
            });
            bindCustomerRows();
            function bindCustomerRows() {
                $('[data-customer-id]').on('click', function (e) {
                    var $e = $(e.target);
                    if (!$e.data('customer-id')) {
                        var parent = $e.closest($('[data-customer-id]'));
                        var $customer_id = $(parent).data('customer-id');

                    } else {
                        var $customer_id = $e.data('customer-id');
                    }
                    window.location = '{{ url('/customer/') }}' + '/' + $customer_id;
                })
            }
            ;
        });
    </script>
@endsection