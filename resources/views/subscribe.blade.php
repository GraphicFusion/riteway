@extends('layouts.default')

@section('content')
<div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h3>
                                <svg version="1.1" id="user-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.6 20.6" style="enable-background:new 0 0 20.6 20.6;" xml:space="preserve">
                                    <path class="st0" d="M0.3,2.5v15.6c0,1.2,1,2.2,2.2,2.2h15.6c1.2,0,2.2-1,2.2-2.2V2.5c0-1.2-1-2.2-2.2-2.2H2.5 C1.3,0.3,0.3,1.3,0.3,2.5z M13.6,6.9c0,1.8-1.5,3.3-3.3,3.3S6.9,8.8,6.9,6.9s1.5-3.3,3.3-3.3S13.6,5.1,13.6,6.9z M3.6,15.8
                                    c0-2.2,4.4-3.4,6.7-3.4s6.7,1.2,6.7,3.4v1.1H3.6V15.8z"/>
                                </svg>
                                Subscribe
                            </h3>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
								@if (Auth::check())
								    @if (!Auth::user()->subscribed())
								        {{-- <p>You are logged in but not subscribed. <a href="">Join Now</a></p> --}}

								    
								        <form action="{{ url('/subscribe') }}" method="post" id="payment-form">
								        {{ csrf_field() }}
								              <div class="form-row">
								                <label for="card-element">
								                  Credit or debit card
								                </label>
								                <div id="card-element">
								                  <!-- a Stripe Element will be inserted here. -->
								                </div>

								                <!-- Used to display form errors -->
								                <div id="card-errors"></div>
								              </div>

								              <button>Submit Payment</button>
								        </form>

								    @else
								        <p>You are logged in & subscribed, Thanks!!!</p>
								    @endif
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('customJs')
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
// Create a Stripe client
var stripe = Stripe('{!! env('STRIPE_SECRET') !!}');
// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '24px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server

      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
@endsection
