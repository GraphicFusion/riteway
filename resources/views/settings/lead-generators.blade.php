@extends('layouts.default')

@section('content')
<?php
	$user = getUser();
	$group = App\Group::find($user->current_group_id);
	$team = $group->getTeam('leadgen');
?>
<div class="">
	<div class="container-fluid">
	    <div class="row">
			<div class="col-md-12 page_content">
                @include('lead-generators-table')
			</div>
	    </div>
	</div>
</div>

@endsection

