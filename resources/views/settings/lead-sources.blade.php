@extends('layouts.default')

@section('content')
<?php
	$user = getUser();
	$group = App\Group::find($user->current_group_id);
	$team = $group->getTeam('leadgen');
?>
<div class="">
	<div class="container-fluid">
	    <div class="row">
			<div class="col-md-12 page_content">
				<div class="page-header">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
			        		<h3>
								{{-- <svg version="1.1" id="team-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.3 21.3" style="enable-background:new 0 0 21.3 21.3;" xml:space="preserve">
									<g>
										<path class="st0" d="M10.6,0.6c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S16.1,0.6,10.6,0.6z M10.6,3.6c1.7,0,3,1.3,3,3
											s-1.3,3-3,3s-3-1.3-3-3S9,3.6,10.6,3.6z M10.6,17.8c-2.5,0-4.7-1.3-6-3.2c0-2,4-3.1,6-3.1c2,0,6,1.1,6,3.1
											C15.3,16.5,13.1,17.8,10.6,17.8z"/>
									</g>
								</svg> --}}
			        			Lead Sources
		        			</h3>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-source">Create New Lead Source</a>
								</div>
								<div class="col-sm-6 col-xs-12">
									@include('searchform')
								</div>
							</div>
						</div>
					</div>
	        	</div>
				
				<hr/>
				
				<div class="row">
					<div class="col-lg-12">
                        <table id="DT_Coils" class="table table-striped dt-responsive nowrap" width="100%">
                        	<thead>
                        		<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Store Type</th>
								</tr>
                        	</thead>
                        	<tbody>
                        		<?php
                        			$sources = App\Source::get();
                        			foreach($sources as $key => $source) {
                        				echo '<tr class="lead-source" data-id="'.$source->id.'" data-name="'.$source->name.'" data-store="'.$source->store.'" >
                                    		<td>'.$source->id.'</td>
                                    		<td>'.$source->name.'</td>
                                    		<td>'.ucfirst($source->store).'</td>
                                		</tr>';
                        			}
                        		?>
                        	</tbody>
                        </table>
					</div>
				</div>

			</div>
	    </div>
	</div>
</div>

@include('modals.createestimate')
@include('modals.add-new-source')
@endsection

@section('customCss')
    <link href="{{ asset('/css/plugins.css') }}" rel="stylesheet">
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#create-new-source').on( 'click', function () {
            $('#add-new-source').modal('show');
        });
        $('.lead-source td').click(function(e){
			var $this = $(e.target).closest('tr'),
				name = $this.data('name'),
				store = $this.data('store'),
				id = $this.data('id');

console.log($this);
console.log(		    $('input[name="source_name"]'));
		    $('input[name="source_name"]').val(name);
		    $('input[name="source_id"]').val(id);
		    $('#source_store').val(store);
		    $('#add-new-source').modal('show');
		    console.log($('input[name="source_store"]'));
		});
    });
</script>
@endsection