				<div class="page-header">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
			        		<h3>
			        			Lead Generators
		        			</h3>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-leadgen">Create New Lead Generator</a>
								</div>
								<div class="col-sm-6 col-xs-12">
								</div>
							</div>
						</div>
					</div>
	        	</div>
				
				<hr/>
				<?php
					if(Session::get('msg')) {
						echo '<div class="alert alert-success" role="alert">'.Session::get('msg').'</div>';
					}
				?>
				<div class="row">
					<div class="col-lg-12">
                        <table id="" class="table table-striped dt-responsive nowrap" width="100%">
                        	<thead>
                        		<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
                        	</thead>
                        	<tbody>
                        		<?php
                        			$sources = App\Source::get();
									foreach( $team as $leadgen ) {
                        				echo '<tr class="leadgen" data-id="'.$leadgen->id.'" data-first_name="'.$leadgen->first_name.'" data-last_name="'.$leadgen->last_name.'" data-email="'.$leadgen->email.'">
                                    		<td>'.$leadgen->id.'</td>
                                    		<td>'.$leadgen->first_name.' '.$leadgen->last_name.'</td>
                                    		<td>'.$leadgen->email.'</td>
                                    		<td>
												<a href="javascript:void(0)" data-href="'.url('/settings/lead-generators/delete/'.$leadgen->id).'" class="btn btn-sm btn-danger deleteLead">Delete</a>
                                    		</td>
                                		</tr>';
                        			}
                        		?>
                        	</tbody>
                        </table>
					</div>
				</div>

@include('modals.createestimate')
@include('modals.add-new-leadgen')

<div class="modal fade gen-modal form-modal" id="delete-lead">
	<div class="modal-dialog modal-lg" role="document" id="loadForm"></div>
</div>

@section('customCss')
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#create-new-leadgen').on( 'click', function () {
        	$('input').val();
            $('#add-new-leadgen').modal('show');
        });
        $('.leadgen td:not(:last-child)').click(function(e){
			var $this = $(e.target).closest('tr'),
				first_name = $this.data('first_name'),
				last_name = $this.data('last_name'),
				email = $this.data('email'),
				id = $this.data('id');

// console.log(last_name);
// console.log($('input[name="first_name"]'));

		    $('input[name="first_name"]').val(first_name);
		    $('input[name="last_name"]').val(last_name);
		    $('input[name="leadgen_email"]').val(email);
		    $('input[name="source_id"]').val(id);
		    $('#add-new-leadgen').modal('show');
		});

		$('.deleteLead').click(function(event) {
			$('#delete-lead #loadForm').load($(this).data('href'));
		    $('#delete-lead').modal('show');
		});

    });

    $(document).ready(function() {
        $('#create-new-member').on( 'click', function () {
            $('#add-new-member').modal('show');
        });

        $('.teamMember').click(function(){
		    $('#edit-member #loadForm').load($(this).data('href'));
		    $('#edit-member').modal('show');
		});

		$('.teamMemberDelete').click(function(event) {
			$('#delete-member #loadForm').load($(this).data('href'));
		    $('#delete-member').modal('show');
		});
    });
</script>

@endsection