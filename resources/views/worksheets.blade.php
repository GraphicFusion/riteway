
@extends('layouts.default')

@section('content')
<?php
$user = Auth::user();
$group = App\Group::find($user->current_group_id);
?>
<style>
    .worksheets .thumbnail .img-wrap {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    display: block;
    margin: 0 auto 21px;
    line-height: 88px;
    text-align: center;
    padding-top: 3px !important;
}
</style>
<div class="worksheets">
    <div class="container-fluid">
<?php $user = getUser(); ?>
        <div class="row">
            <div class="col-md-12 page_content">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h3>
<!--                                <svg version="1.1" id="file-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 20" style="enable-background:new 0 0 16 20;" xml:space="preserve">
                                    <path d="M10,0H2C0.9,0,0,0.9,0,2l0,16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V6L10,0z M12,16H4v-2h8V16z M12,12H4v-2h8V12z M9,7V1.5L14.5,7H9z"/>
                                </svg>-->
                                Worksheets
                            </h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <a href="javascript:void()" data-toggle="modal" data-target="#optionModal" class="btn btn-primary pull-right">Create New Estimate</a>
                        </div>
                    </div>
                </div>

                <div class="row worksheet_content display-flex">
                    <div class="col-sm-4 col-xs-12">
                        <div class="thumbnail style-box blue">
                            <div class="img-wrap">
                           <img src="{{URL::to('img/home-icon-1.png')}}" style="width:56px;height: 56px;"> 
                            </div>
                            <div class="caption">
                                <h3>Package Unit</h3>
                                <hr/>
                                <ul class="list-unstyled list-inline">
                                    <?php
                                    $worksheet_model = App\Worksheet::firstOrNew(array('group_id' => $group->id, 'type' => 'package'));
                                    $worksheet_object = json_decode($worksheet_model->worksheet_object);
                                    ?>

<?php if (is_object($worksheet_object)) : ?>
                                        <li><a href="{{ url('/package?step=customer') }}" class="btn btn-primary">Run</a></li>
                                        <li><a href="{{ url('/edit-package') }}" class="btn btn-primary">Edit</a></li>
<?php else : ?>
                                        <li>
                                            <a class="btn btn-primary" href="{{ url('/edit-package') }}">
                                                Build a Package Worksheet
                                            </a>
                                        </li>
<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="thumbnail style-box purple">
                            <div class="img-wrap">
  <img src="{{URL::to('img/home-icon-2.png')}}" style="width:56px;height: 56px;"> 
                            </div>
                            <div class="caption">
                                <h3>Split System</h3>
                                <hr/>
                                <ul class="list-unstyled list-inline">
                                    <?php
                                    $worksheet_model = App\Worksheet::firstOrNew(array('group_id' => $group->id, 'type' => 'split'));
                                    $worksheet_object = json_decode($worksheet_model->worksheet_object);
                                    ?>

<?php if (is_object($worksheet_object)) : ?>
                                        <li><a href="{{ url('/split?step=customer') }}" class="btn btn-primary">Run</a></li>
                                        <li><a href="{{ url('/edit-split') }}" class="btn btn-primary">Edit</a></li>
<?php else : ?>
                                        <li>
                                            <a class="btn btn-primary" href="{{ url('/edit-split') }}">
                                                Build a Split Worksheet
                                            </a>
                                        </li>
<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="thumbnail style-box orange">
                            <div class="img-wrap">
  <img src="{{URL::to('img/home-icon-3.png')}}" style="width:56px;height: 56px;"> 
                            </div>
                            <div class="caption">
                                <h3>Furnace</h3>
                                <hr/>
                                <ul class="list-unstyled list-inline">
                                    <?php
                                    $worksheet_model = App\Worksheet::firstOrNew(array('group_id' => $group->id, 'type' => 'furnace'));
                                    $worksheet_object = json_decode($worksheet_model->worksheet_object);
                                    ?>

<?php if (is_object($worksheet_object)) : ?>
                                        <li><a href="{{ url('/furnace?step=customer') }}" class="btn btn-primary">Run</a></li>
                                        <li><a href="{{ url('/edit-furnace') }}" class="btn btn-primary">Edit</a></li>
<?php else : ?>
                                        <li>
                                            <a class="btn btn-primary" href="{{ url('/edit-furnace') }}">
                                                Build a Furnace Worksheet
                                            </a>
                                        </li>
<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.createestimate')
@endsection
