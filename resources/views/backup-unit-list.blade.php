
@extends('layouts.default')

@section('content')
<?php
	$user = getUser();
	$group = App\Group::find($user->current_group_id);
	$unitsArr = App\Unit::all();
	$units = [];
	foreach( $unitsArr as $unit ){
		$units[] = [
			'name' => $unit->name,
			'brand' => 'logo-'.$unit->brand.'.png',
			'type' => $unit->type
		];
	}
?>
<div class="unit-list">
	<div class="container">
	<?php $user = getUser(); ?>
	    <div class="row">
	        <div class="col-md-12">
	        	<div class="page-header">
	                <div class="row">
	                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
	                        <h3>
	                        	<svg version="1.1" id="list-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.7 23.7" style="enable-background:new 0 0 23.7 23.7;" xml:space="preserve">
									<path class="st0" d="M11.9,11.9c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5H11.9z M11.9,11.9c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5H11.9z
										 M11.9,11.9c-3,0-5.5-2.5-5.5-5.5s2.5-5.5,5.5-5.5V11.9z M11.9,11.9c3,0,5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5V11.9z"/>
								</svg>
	                            Unit List
	                        </h3>
	                    </div>
	                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="row">
								<div class="col-sm-8 col-xs-12">
									<ul class="list-inline pull-right mb0">
										<li>

											<a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary" id="create-new-unit">Add New Unit</a>
										</li>
										<li>
											<a href="javascript:void()" data-toggle="modal" data-target="#addNewPackage" class="btn btn-primary">Add New Package</a>		
										</li>
										<li>
											<a href="javascript:void()" data-toggle="modal" data-target="#addNewSplit" class="btn btn-primary">Add New Split</a>	
										</li>
									</ul>
								</div>
								<div class="col-sm-4 col-xs-12">
									@include('datatable-searchform')
								</div>
							</div>
	                    </div>
	                </div>
	        	</div>
				
				{{-- Shown alert when new pachage id received. --}}
				@if(isset($package_id) && !empty($package_id))
					<div class="alert alert-success">
						<p>New package record created successfully. Package id: {{ $package_id }}</p>
					</div>
				@endif

				{{-- Shown alert when new split id received. --}}
				@if(isset($split_id) && !empty($split_id))
					<div class="alert alert-success">
						<p>New split record created successfully. Split id: {{ $split_id }}</p>
					</div>
				@endif

	        	<?php
	                $unitsvoid = [
	                    ['name'=>'Xv90 Two Stage Variable Speed Gas', 'brand'=>'logo-tran.png', 'type'=>'Furnance'],
	                    ['name'=>'Ducane (by Lennox) R410 Central A/C Air Conditioner 18 SEER', 'brand'=>'logo-lennox.png', 'type'=>'Split System'],
	                    ['name'=>'Rheem 2.5 Ton 14 SEER Horizontal', 'brand'=>'logo-ruud.png', 'type'=>'Package Unit'],
	                    ['name'=>'3.5 Ton 16 SEER Air Conditioner R410A Refrigerant', 'brand'=>'logo-carrier.png', 'type'=>'Split System'],
	                    ['name'=>'Xv90 Two Stage Variable Speed Gas', 'brand'=>'logo-tran.png', 'type'=>'Furnance'],
	                    ['name'=>'Ducane (by Lennox) R410 Central A/C Air Conditioner 18 SEER', 'brand'=>'logo-lennox.png', 'type'=>'Split System'],
	                    ['name'=>'Rheem 2.5 Ton 14 SEER Horizontal', 'brand'=>'logo-ruud.png', 'type'=>'Package Unit'],
	                    ['name'=>'3.5 Ton 16 SEER Air Conditioner R410A Refrigerant', 'brand'=>'logo-carrier.png', 'type'=>'Split System'],
	                    ['name'=>'Xv90 Two Stage Variable Speed Gas', 'brand'=>'logo-tran.png', 'type'=>'Furnance'],
	                    ['name'=>'Ducane (by Lennox) R410 Central A/C Air Conditioner 18 SEER', 'brand'=>'logo-lennox.png', 'type'=>'Split System'],
	                    ['name'=>'Rheem 2.5 Ton 14 SEER Horizontal', 'brand'=>'logo-ruud.png', 'type'=>'Package Unit'],
	                    ['name'=>'3.5 Ton 16 SEER Air Conditioner R410A Refrigerant', 'brand'=>'logo-carrier.png', 'type'=>'Split System']
                    ];
                ?>
	        	<div class="row">
	        		<div class="col-lg-12">
						<table id="dTable" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <th>Name</th>
					                <th>Brand</th>
					                <th>Type</th>
					            </tr>
					        </thead>
					        <tbody>
					        	@foreach($units as $unit)
					        		<tr>
					        			<td>
					        				<?php echo embed_svg('icon', 'img/list-icon.svg', '16px', '16px'); ?>
					        				{{ $unit['name'] }}
				        				</td>
					        			<td>{{ Html::image('img/'.$unit['brand'], 'Brand Logo') }}</td>
					        			<td>{{ $unit['type'] }}</td>
					        		</tr>
					        	@endforeach
					        </tbody>
					    </table>
	        		</div>
	        	</div>
	    	</div>
	    </div>
	</div>
</div>

@include('modals.createestimate')
@include('modals.add-new-unit')
@include('modals.add-new-package')
@include('modals.add-new-split')
@endsection

@section('customCss')
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var oTable = $('#dTable').DataTable({
        	"iDisplayLength": 20,
        	"aaSorting": [],
		  	"responsive": {
	            details: {
	                display: $.fn.dataTable.Responsive.display.modal( {
	                    header: function ( row ) {
	                        var data = row.data();
	                        return 'Details for '+data[0];
	                    }
	                } ),
	                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
	                    tableClass: 'table table-striped table-dt'
	                } )
	            }
	        }
        });
        $('#search_dtable').keyup(function(){
            oTable.search($(this).val()).draw();
        });
		$('#create-new-unit').on( 'click', function () {
            $('#add-new-unit').modal('show');
        });
    });
</script>
@endsection