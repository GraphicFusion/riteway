<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- DNS Prefetch -->
    <link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Riteway') }}</title>

    @yield('customCss')
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet"> 
    <link href="{{ asset('/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <!--[if lt IE 9]>
        <script src="{{ asset('/js/html5-3.6-respond-1.4.2.min.js') }}"></script>
    <![endif]-->
</head>
<?php 
    $class = "";
    if(Auth::check()):
        $user_roles = Auth::user()->roles;
        foreach( $user_roles as $role){
            if( $role == 'admin'){
                $class = "admin-unlock";
            }
        }
    endif;
?>
<body class="<?php echo $class; ?>">
	<?php 
		$settings_nav = 0;
		if( isset($navigation) && $navigation == 'settings' ){
			$settings_nav = 1;
		}
		$selects = "";
		if (!Auth::guest()){
			$groups = Auth::user()->groups;
			$selects = "";
			if( count( $groups ) > 1 ){ 
				$selects = "<span>Change Company to:</span>";
				foreach($groups as $g ){
					$group = $g->getAttributes();
					if( $g->id == Auth::user()->current_group_id ){
						$curr = $group;
					}
					else{
						$selects .= "<a data-group='".$group['id']."'>".$group['name']."</a>";
					}
				}
			} 
		}
	?>
    <div id="app">
        <div class="strip"></div>
        {{-- <div class="strip"></div> --}}
        <header class="header">
                <div class="container-fluid">
                    <div class="row">                    
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center head_logo">
                            <div class="col-lg-4 col-md-5 col-sm-5 col-xs-5"> 
                                 <a class="main-logo" href="{{ url('/') }}"> 
                                 <img src="{{URL::to('img/logo.svg')}}" style="width:300px; margin-left:20px;"> 
                                </a>
                            </div>
                           <!-- <div class="col-lg-8 col-md-7 col-sm-7 col-xs-7"> 
                                 <h4 class="text-left logo_text"> HEATING, COOLING <br>
                                & PLUMBING </h4>
                            </div>-->
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 text-right">
                        
                            @if( count($selects) > 1 || 1 == 1)
                            @if(Auth::check())
                            <a href="#" class="side-logo dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
<?php echo embed_svg('logo', 'img/corner-logo.svg', '20px', '20px'); ?>
                                Riteway 
                                <span class="caret"></span>

                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="selects">
<?php echo $selects; ?>
                                </li>
                            </ul>
                            @endif
                            @else
                            <div class="side-logo" href="javascript:void(0)">
<?php echo embed_svg('logo', 'img/corner-logo.svg', '20px', '20px'); ?>
                                Riteway 
                            </div>

                            @endif

                            @if (Auth::guest())
                            <ul class="list-inline top_nav pull-right">
                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Register</a></li>
                            </ul>
                            @else
                            <?php
                            $groups = Auth::user()->groups;
                            $selects = "";
                            if (count($groups) > 1) {
                                $selects = "<span>Change Company to:</span>";
                                foreach ($groups as $g) {
                                    $group = $g->getAttributes();
                                    if ($g->id == Auth::user()->current_group_id) {
                                        $curr = $group;
                                    } else {
                                        $selects .= "<a data-group='" . $group['id'] . "'>" . $group['name'] . "</a>";
                                    }
                                }
                            }
                            ?>

                            <div class="dropdown pull-right top_nav_2">
                                <a id="dLabel" data-target="#" href="#x" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }} 
                                    @if ( !empty($curr) )
                                    logged in for <?php print_r($curr); /* echo $curr['name']; */ ?>
                                    @endif
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li>
                                        @if( $settings_nav )
                                        <a href="{{ url('/dashboard') }}">Dashboard</a>
                                        @else
                                        <a href="{{ url('/settings/lead-generators') }}">Settings</a>
                                        @endif
                                    </li>
                                    <?php if ($selects) : ?>
                                        <li class="selects">
                                            <?php echo $selects; ?>
                                        </li>
                                        <form id='group-form' action='{{ url('/change-group') }}' method='POST' style='display: none;'>
                                            {{ csrf_field() }}
                                            <input type='hidden' id='change_group' name='change_group'>
                                        </form>
                                    <?php endif; ?>    
                                </ul>
                            </div>

                            @endif
                        </div>
                    </div>
                </div>
            </header>        
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    {{-- <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Riteway') }}
                    </a> --}}
                    <span class="navbar-brand visible-xs">Menu</span>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
					<?php $route = Route::getCurrentRoute()->getPath();?>
					@if( $settings_nav )
	                    <ul class="nav navbar-nav">
						    <li class="<?php if( 'settings/lead-generators' == $route){ echo "active"; }?>"><a href="{{ url('/settings/lead-generators') }}">Lead Generators</a></li>
						    <li class="<?php if( 'settings/lead-sources' == $route){ echo "active"; }?>"><a href="{{ url('/settings/lead-sources') }}">Lead Sources</a></li>
						    <li class="<?php if( 'performance' == $route){ echo "active"; }?>"><a href="{{ url('/performance') }}">Performance</a></li>
						</ul>
					@else
	                    <ul class="nav navbar-nav">
<!--						    <li class="<?php if( '/' == $route){ echo "active"; }?>"><a href="{{ url('/dashboard') }}">Dashboard</a></li>-->
						    <li class="<?php if( 'performance' == $route){ echo "active"; }?>"><a href="{{ url('/performance') }}">Performance</a></li>
						    <li class="<?php if( 'team' == $route){ echo "active"; }?>"><a href="{{ url('/team') }}">Team</a></li>
						    <li class="<?php if( 'unit-list' == $route){ echo "active"; }?>"><a href="{{ url('/unit-list') }}">Unit List</a></li>
						    <li class="<?php if( 'customers' == $route){ echo "active"; }?>"><a href="{{ url('/customers') }}">Customers</a></li>
						    <li class="<?php if( 'opportunities' == $route){ echo "active"; }?>"><a href="{{ url('/opportunities') }}">Opportunities</a></li>
						    <li class="<?php if( 'worksheets' == $route){ echo "active"; }?>"><a href="{{ url('/worksheets') }}">Worksheets</a></li>
	                    </ul>
					@endif
                </div>
            </div>
        </nav>
        
        <div class="content">
            @yield('content')
        </div>
    </div>
    <footer class="footer">
        <div class="bg-primary text-center">
            <a href="javascript:void()" data-toggle="modal" data-target="#optionModal">
                <?php echo embed_svg('icon-estimate', 'img/create-estimate-icon.svg', '16px', '16px'); ?>
                Create New Estimate
            </a>
        </div>
        <div class="credits">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <p>&copy; 2017 Riteway Heating & Cooling. All Rights Reserved.</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <a id="back_to_top" href="#x">
                            <?php echo embed_svg('icon-back-to-top', 'img/arrow-up.svg', '16px', '16px'); ?>
                            Back To Top
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="strip"></div>
    </footer>

    <!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{ asset('/js/plugins.js') }}"></script>
<script src="{{ asset('/js/helpers.js') }}"></script>
<script src="{{ asset('/js/presentationBuilder.js') }}"></script>
<script src="{{ asset('/js/ajax.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> --}}
<script src="{{ asset('/js/formBuilder.js') }}"></script>
<script src="{{ asset('/js/worksheetBuilder.js') }}"></script>

<script src="{{ asset('/js/jquery.geocomplete.js') }}"></script>

@yield('customJs')
</body>
</html>
