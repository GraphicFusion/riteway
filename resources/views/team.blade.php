@extends('layouts.default')

@section('content')
<?php
$user = getUser();
$group = App\Group::find($user->current_group_id);
$team = $group->getTeam();
?>
<style>
    .row.display-flex {
        display: flex;
        flex-wrap: wrap;
        padding-left: 17px;
        padding-right: 17px;
    }
</style>
<div class="team">
	<div class="container">
	    <div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
			        		<h3>
								<svg version="1.1" id="team-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.3 21.3" style="enable-background:new 0 0 21.3 21.3;" xml:space="preserve">
									<g>
										<path class="st0" d="M10.6,0.6c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S16.1,0.6,10.6,0.6z M10.6,3.6c1.7,0,3,1.3,3,3
											s-1.3,3-3,3s-3-1.3-3-3S9,3.6,10.6,3.6z M10.6,17.8c-2.5,0-4.7-1.3-6-3.2c0-2,4-3.1,6-3.1c2,0,6,1.1,6,3.1
											C15.3,16.5,13.1,17.8,10.6,17.8z"/>
									</g>
								</svg>
			        			Team
		        			</h3>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<a href="javascript:void()" data-toggle="modal" data-target="#x" class="btn btn-primary pull-right" id="create-new-member">Create New Member</a>
								</div>
								<div class="col-sm-6 col-xs-12">
								</div>
							</div>
						</div>
					</div>
	        	</div>
				
				<hr/>

				<?php
					if(Session::get('msg')) {
						echo '<div class="alert alert-success" role="alert">'.Session::get('msg').'</div>';
					}
				?>
				
				<div class="team-members">
		        	<div class="row">
						<?php foreach( $team as $member ) :
						 ?>
			        		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 teamM">
			        			<a href="javascript:void(0)" data-href="{{ URL::to('/team/edit/'.$member->id) }}" class="thumbnail teamMember">
			        				<div class="img-wrapper">
			        					<span><?php echo $member->getInitials(); ?></span>
			        				</div>
			        				<div class="caption">
			        					<h3><?php echo $member->name; ?></h3>
			        					<p class="help-block"><?php if( $member->leadgen){ echo "Lead Generator"; }else{ echo $member->title; } ?></p>
			        				</div>
				        		</a>
				        		<span href="javascript:void(0)" class="teamMemberDelete" data-href="{{ URL::to('/team/delete/'.$member->id) }}">&times;</span>
			        		</div>
						<?php endforeach; ?>
		        	</div>	    <div class="row">


		        		
	    </div>

                </div>

            </div>
        </div>
    </div>
</div>

@include('modals.createestimate')
@include('modals.add-new-member')

<div class="modal fade gen-modal form-modal" id="edit-member">
    <div class="modal-dialog modal-lg" role="document" id="loadForm"></div>
</div>

<div class="modal fade gen-modal form-modal" id="delete-member">
    <div class="modal-dialog modal-lg" role="document" id="loadForm"></div>
</div>
@endsection

@section('customCss')
    <style>
        .teamMember { position: relative; z-index: 1020;}
        .teamM:hover .teamMember { background: #fff !important; }
        .teamMemberDelete { display: none; position: absolute; top: 0; right: 15px; background: red; width: 30px; height: 30px; line-height: 1.2; text-align: center; font-size: 22px; color:#fff; z-index: 1030; cursor: pointer;}
        .teamM:hover .teamMemberDelete { display: block }
    </style>
@endsection

@section('customJs')
<script src="{{ asset('/js/plugins.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#create-new-member').on( 'click', function () {
            $('#add-new-member').modal('show');
        });

        $('.teamMember').click(function(){
		    $('#edit-member #loadForm').load($(this).data('href'));
		    $('#edit-member').modal('show');
		});

		$('.teamMemberDelete').click(function(event) {
			$('#delete-member #loadForm').load($(this).data('href'));
		    $('#delete-member').modal('show');
		});
    });
</script>
@endsection