<canvas class="<?php echo $year.'-'.$userID; ?>" id="line-chart-x"></canvas>

<div class="row mtb30">
    <div class="col-sm-6">
        <div class="stats">
            <h3>$100</h3>
            Estimated Earned Spiffs
        </div>
    </div>
    <div class="col-sm-6">
        <div class="stats">
            <h3>$8000</h3>
            Estimated Earned Commission
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {  
    // Return with commas in between
    var numberWithCommas = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    // Line Chart
    var dataLine = {
        labels: ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"],
        datasets: [
            {
                label: "Average",
                fill: false,
                lineTension: 0,
                backgroundColor: "#80DDE9",
                borderColor: "#80DDE9",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#80DDE9",
                pointBackgroundColor: "#80DDE9",
                pointBorderWidth: 5,
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "#80DDE9",
                pointHoverBorderColor: "#80DDE9",
                pointHoverBorderWidth: 0,
                pointRadius: 2,
                pointHitRadius: 1,
                data: [1500,1800,2500,7000,4000,6000,3000,1000,9000,10000],
                spanGaps: false,
            },
            {
                label: "Budget to Variable",
                fill: false,
                lineTension: 0,
                backgroundColor: "#FFAB91",
                borderColor: "#FFAB91",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#FFAB91",
                pointBackgroundColor: "#FFAB91",
                pointBorderWidth: 5,
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "#FFAB91",
                pointHoverBorderColor: "#FFAB91",
                pointHoverBorderWidth: 0,
                pointRadius: 2,
                pointHitRadius: 1,
                data: [1200,2200,3200,4400,5500,6600,7700,8800,9900,5000],
                spanGaps: false,
            },
            {
                label: "Actual Total",
                fill: false,
                lineTension: 0,
                backgroundColor: "#E6EE9C",
                borderColor: "#E6EE9C",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#E6EE9C",
                pointBackgroundColor: "#E6EE9C",
                pointBorderWidth: 5,
                pointHoverRadius: 3,    
                pointHoverBackgroundColor: "#E6EE9C",
                pointHoverBorderColor: "#E6EE9C",
                pointHoverBorderWidth: 0,
                pointRadius: 2,
                pointHitRadius: 1,
                data: [2600,1600,3800,6000,4000,2200,7800,4200,3200,9750],
                spanGaps: false,
            }
        ]
    };
    var ctxLine = document.getElementById("line-chart-x").getContext("2d");
    var myLineChart = new Chart(ctxLine, {
        type: 'line',
        data: dataLine,
        options: {
            animation: {
                duration: 10,
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(tooltipItem, data) { 
                        return "$" + numberWithCommas(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                xAxes: [{ 
                    stacked: false, 
                    gridLines: { 
                        display: false 
                    },
                }],
                yAxes: [{ 
                    stacked: false, 
                    ticks: {
                        callback: function(value) { 
                            return numberWithCommas(value); 
                        },
                    }, 
                }],
            },
            legend: {
                display: false // due to style limitation, add legend info as per design using legend template
            },
            responsive: true,
            scaleBeginAtZero: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        }
    });
    $("#lineChartLegend").html(myLineChart.generateLegend());
});
</script>