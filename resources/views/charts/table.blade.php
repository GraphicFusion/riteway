  
  <?php
   $user_proposals = [];
    if( 'member' == $user_type){
        $users = App\User::whereHas('Groups', function($query){
            $query->where('role','=','admin')
                ->orWhere('role','=','team');            ;
             //->whererole('team');  
        })->whereNull('leadgen')->get(); 
    }
    if( 'lead_source' == $user_type){
        $users = App\Source::get(); 
    }

    if( 'lead_gen' == $user_type){
        $users = App\User::where('leadgen','=',1)->get(); 
    }
    $proposals = ['pending'=>array_fill(0,12,0),'sold'=>array_fill(0,12,0),'lost'=>array_fill(0,12,0)];
?>                                              <div class="col-sm-12">
                                                    <table class="table table-striped dt-responsive nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th># Leads</th>
                                                                <th># Sales</th>
                                                                <th>Total Revenue</th>
                                                                <th>Avg Ticket</th>
                                                                <th>Closing %</th>
                                                            </tr>
                                                        </thead>
                                                        <?php
                                                            foreach( $users as $user) :?>

                                                            <?php
        $user_solds = 0;
    if( 'member' == $user_type){
         $user_proposals = App\Proposal::whereHas('users', function($query)  use ($user){
            $query->where('users.id','=', $user->id);  
        })->whereDate('created_at', '>', $start)->whereDate('created_at', '<=', $end)->get();  
    }
    if( 'lead_source' == $user_type){
        $user_proposals = App\Proposal::where('source_id', $user->id )->whereDate('created_at', '>', $start)->whereDate('created_at', '<=', $end)->get();       
    }
    if( 'lead_gen' == $user_type){
        $user_proposals = App\Proposal::where('leadgen_id', $user->id )->whereDate('created_at', '>', $start)->whereDate('created_at', '<=', $end)->get();
    }
    $user_leads = count($user_proposals);
    $chosen_price = 0;
        foreach( $user_proposals as $prop){
            $total_sales = 0;
            $status = $prop->status;
            // check if sold date occured before last date
            //echo "Sold Date:".$prop->sold_date." ---- End: ".$end."<br>";
            if( $prop->status == 'sold' && strtotime($prop->sold_date) < strtotime($end)){
                $month = date('n', strtotime( $prop->sold_date ));
                $user_solds++;
                $inc_estimates = json_decode($prop->included_estimates);
                foreach( $prop->estimates as $est){
                    if( is_array($inc_estimates) && in_array($est->id, $inc_estimates)){
//                    print_r($est);
                        $chosen_plan = $est->chosen_plan;
                    $decode = json_decode($est['presentation_prices']);
                    $chosen = $decode->$chosen_plan;
  //                  print_r($chosen);
//echo $chosen_plan."----";
                        $chosen_price += $chosen->system_price;
                    }

                }
            }
            else{
                $month = date('n', strtotime( $prop->created_at ));
            }
            $month = $month -1;
           // if( !array_key_exists($month, $proposals[$status]) ){
             //   $proposals[$status][$month] = 0;
          // }
//            $proposals[$status][$month]++;
        }
        $avg_ticket = 0;
        if( $user_solds ){
            $avg_ticket = $chosen_price/$user_solds;
        }
        $closing_percent = 0;
        if( $user_leads ){
            $closing = $user_solds/$user_leads;
            if( $closing ){
                $closing_percent = round($closing,2) * 100 ."%"; 
            }
        }
?>
                                                        <tr>
                                                            <td><?php echo $user->name; ?></td>
                                                            <td><?php echo $user_leads; ?></td>
                                                            <td><?php echo $user_solds; ?></td>
                                                            <td><?php echo $chosen_price; ?></td>
                                                            <td><?php echo round($avg_ticket,2); ?></td>
                                                            <td><?php echo $closing_percent; ?></td>
                                                        </tr>
<?php endforeach; ?>
                                                    </table>
                                                </div>
