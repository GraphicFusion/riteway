<canvas class="<?php echo $year.'-'.$userID; ?>" id="bar-chart-x"></canvas>
<div class="row mtb30 pdlr60">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="row">
            <div class="col-sm-8">
                <canvas id="pie-chart"></canvas>
            </div>
            <div class="col-sm-4" id="pieChartLegend"></div>
        </div>
    </div>
</div>

<div class="row mb30 pdlr60">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
        <div class="stats">
            <h3>21.2%</h3>
            Closing Ratio
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
        <div class="stats">
            <h3>1,234</h3>
            Actual Leads
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
        <div class="stats">
            <h3>00.70</h3>
            Average Days to Close
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
        <div class="stats">
            <h3>43</h3>
            Carry Over
        </div>
    </div>
</div>


<script type="text/javascript">
jQuery(document).ready(function($) {  
<?php
	$user = App\User::find($userID);

	$proposals = ['pending'=>array_fill(0,12,0),'sold'=>array_fill(0,12,0),'lost'=>array_fill(0,12,0)];

	$user_proposals = 
		App\Proposal::whereHas('users', function($query)  use ($user){
	    	$query->where('users.id','=', $user->id);  
		})->whereDate('created_at', '<', date('Y-m-d'))->get();  
	foreach( $user_proposals as $prop){
		$status = $prop->status;
		if( $prop->status == 'sold' ){
			$month = date('n', strtotime( $prop->sold_date ));
		}
		else{
			$month = date('n', strtotime( $prop->created_at ));
		}
		$month = $month -1;
		if( !array_key_exists($month, $proposals[$status]) ){
			$proposals[$status][$month] = 0;
		}
		$proposals[$status][$month]++;
	}
//print_r($proposals);
	

?>
    // BAR CHART
    var data = {
        labels: ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"],
        datasets: [
            {
                label: "Pending",
                backgroundColor: "#FFE083",
                borderColor: "#FFE083",
                borderWidth:0,

                data: <?php echo json_encode( $proposals['pending'] ); ?>
           },
            {
                label: "Sold",
                backgroundColor: "#80CBC4",
                borderColor: "#80CBC4",
                borderWidth:0,
               data: <?php echo json_encode( $proposals['sold'] ); ?>
            },
            {
                label: "Lost",
                backgroundColor: "#F48FB1",
                borderColor: "#F48FB1",
                borderWidth:0,
                data: <?php echo json_encode( $proposals['lost'] ); ?>
            }
        ]
    };

    // Return with commas in between
    var numberWithCommas = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var ctx = document.getElementById("bar-chart-x").getContext("2d");
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            animation: {
                duration: 10,
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(tooltipItem, data) { 
                        return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                xAxes: [{ 
                    stacked: false, 
                    gridLines: { 
                        display: false 
                    },
                }],
                yAxes: [{ 
                    stacked: false, 
                    ticks: {
                        callback: function(value) { 
							return value.toFixed(1);
//                            return numberWithCommas(value); 
                        },
                    },
					labels: {
    					template: "<%= Number(value.toFixed(2)) + ' A'%>", 
					}
                }],
            },
            legend: {
                display: false // due to style limitation, add legend info as per design using legend template
            },
            responsive: true,
            scaleBeginAtZero: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        }
    });
    $("#barChartLegend").html(myBarChart.generateLegend());
   

    // PIE 
    var dataPie = {
        labels: ["Pending","Sold","Lost"],
        datasets: [
            {
<?php if( $userID == 2 ) : ?>
                data: [250, 150, 110],
<?php else: ?>
                data: [10, 450, 80],
<?php endif; ?>
                backgroundColor: [
                    "#FFE083",
                    "#80CBC4",
                    "#F48FB1"
                ]
            }]
    };
    var ctxPie = document.getElementById("pie-chart").getContext("2d");
    var myPieChart = new Chart(ctxPie,{
        type: 'pie',
        data: dataPie,
        options: {
            legend: {
                display: false // due to style limitation, add legend info as per design using legend template
            },
            responsive: true,
            scaleBeginAtZero: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        }
    });
    $("#pieChartLegend").html(myPieChart.generateLegend());

});
</script>